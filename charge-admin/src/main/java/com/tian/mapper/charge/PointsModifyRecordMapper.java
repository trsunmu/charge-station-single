package com.tian.mapper.charge;

import com.tian.entity.PointsModifyRecord;
import com.tian.entity.PointsModifyRecordExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 用户积分变更 PointsModifyRecordMapper
 * @author fuce_自动生成
 * @email ${email}
 * @date 2023-11-14 10:08:21
 */
public interface PointsModifyRecordMapper {
      	   	      	      	      	      	      
    long countByExample(PointsModifyRecordExample example);

    int deleteByExample(PointsModifyRecordExample example);
		
    int deleteByPrimaryKey(Integer id);
		
    int insert(PointsModifyRecord record);

    int insertSelective(PointsModifyRecord record);

    List<PointsModifyRecord> selectByExample(PointsModifyRecordExample example);
		
    PointsModifyRecord selectByPrimaryKey(Integer id);
		
    int updateByExampleSelective(@Param("record") PointsModifyRecord record, @Param("example") PointsModifyRecordExample example);

    int updateByExample(@Param("record") PointsModifyRecord record, @Param("example") PointsModifyRecordExample example); 
		
    int updateByPrimaryKeySelective(PointsModifyRecord record);

    int updateByPrimaryKey(PointsModifyRecord record);
  	  	
}