package com.tian.mapper.charge;

import com.tian.entity.InvitedRegisterRecord;
import com.tian.entity.InvitedRegisterRecordExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 邀请注册 InvitedRegisterRecordMapper
 * @author tianwc_自动生成
 * @email ${email}
 * @date 2023-11-14 10:26:03
 */
public interface InvitedRegisterRecordMapper {
      	   	      	      	      	      	      
    long countByExample(InvitedRegisterRecordExample example);

    int deleteByExample(InvitedRegisterRecordExample example);
		
    int deleteByPrimaryKey(Integer id);
		
    int insert(InvitedRegisterRecord record);

    int insertSelective(InvitedRegisterRecord record);

    List<InvitedRegisterRecord> selectByExample(InvitedRegisterRecordExample example);
		
    InvitedRegisterRecord selectByPrimaryKey(Integer id);
		
    int updateByExampleSelective(@Param("record") InvitedRegisterRecord record, @Param("example") InvitedRegisterRecordExample example);

    int updateByExample(@Param("record") InvitedRegisterRecord record, @Param("example") InvitedRegisterRecordExample example); 
		
    int updateByPrimaryKeySelective(InvitedRegisterRecord record);

    int updateByPrimaryKey(InvitedRegisterRecord record);
  	  	
}