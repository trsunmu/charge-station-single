package com.tian.mapper.charge;

import com.tian.entity.ExceptionalRetroaction;
import com.tian.entity.ExceptionalRetroactionExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 异常反馈 ExceptionalRetroactionMapper
 * @author fuce_自动生成
 * @email ${email}
 * @date 2023-11-13 20:35:29
 */
public interface ExceptionalRetroactionMapper {
      	   	      	      	      	      	      	      	      	      	      	      	      	      
    long countByExample(ExceptionalRetroactionExample example);

    int deleteByExample(ExceptionalRetroactionExample example);
		
    int deleteByPrimaryKey(Long id);
		
    int insert(ExceptionalRetroaction record);

    int insertSelective(ExceptionalRetroaction record);

    List<ExceptionalRetroaction> selectByExample(ExceptionalRetroactionExample example);
		
    ExceptionalRetroaction selectByPrimaryKey(Long id);
		
    int updateByExampleSelective(@Param("record") ExceptionalRetroaction record, @Param("example") ExceptionalRetroactionExample example);

    int updateByExample(@Param("record") ExceptionalRetroaction record, @Param("example") ExceptionalRetroactionExample example); 
		
    int updateByPrimaryKeySelective(ExceptionalRetroaction record);

    int updateByPrimaryKey(ExceptionalRetroaction record);
  	  	
}