package com.tian.mapper.charge;

import com.tian.entity.GlobalProperty;
import com.tian.entity.GlobalPropertyExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 全局配置 GlobalPropertyMapper
 * @author tianwc_自动生成
 * @email ${email}
 * @date 2023-11-14 10:29:12
 */
public interface GlobalPropertyMapper {
      	   	      	      	      	      	      	      
    long countByExample(GlobalPropertyExample example);

    int deleteByExample(GlobalPropertyExample example);
		
    int deleteByPrimaryKey(Integer id);
		
    int insert(GlobalProperty record);

    int insertSelective(GlobalProperty record);

    List<GlobalProperty> selectByExample(GlobalPropertyExample example);
		
    GlobalProperty selectByPrimaryKey(Integer id);
		
    int updateByExampleSelective(@Param("record") GlobalProperty record, @Param("example") GlobalPropertyExample example);

    int updateByExample(@Param("record") GlobalProperty record, @Param("example") GlobalPropertyExample example); 
		
    int updateByPrimaryKeySelective(GlobalProperty record);

    int updateByPrimaryKey(GlobalProperty record);
  	  	
}