package com.tian.mapper.charge;

import com.tian.entity.InvestorDetail;
import com.tian.entity.InvestorDetailExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 投资人详情 InvestorDetailMapper
 * @author tianwc_自动生成
 * @email ${email}
 * @date 2023-11-14 10:27:39
 */
public interface InvestorDetailMapper {
      	   	      	      	      	      	      	      	      	      	      	      
    long countByExample(InvestorDetailExample example);

    int deleteByExample(InvestorDetailExample example);
		
    int deleteByPrimaryKey(Integer id);
		
    int insert(InvestorDetail record);

    int insertSelective(InvestorDetail record);

    List<InvestorDetail> selectByExample(InvestorDetailExample example);
		
    InvestorDetail selectByPrimaryKey(Integer id);
		
    int updateByExampleSelective(@Param("record") InvestorDetail record, @Param("example") InvestorDetailExample example);

    int updateByExample(@Param("record") InvestorDetail record, @Param("example") InvestorDetailExample example); 
		
    int updateByPrimaryKeySelective(InvestorDetail record);

    int updateByPrimaryKey(InvestorDetail record);
  	  	
}