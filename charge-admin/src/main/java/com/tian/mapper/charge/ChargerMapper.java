package com.tian.mapper.charge;

import com.tian.entity.Charger;
import com.tian.entity.ChargerExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * @author tianwc 公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年11月10日 19:27
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 */
public interface ChargerMapper {

    long countByExample(ChargerExample example);

    int deleteByExample(ChargerExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Charger record);

    int insertSelective(Charger record);

    List<Charger> selectByExample(ChargerExample example);

    Charger selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Charger record, @Param("example") ChargerExample example);

    int updateByExample(@Param("record") Charger record, @Param("example") ChargerExample example);

    int updateByPrimaryKeySelective(Charger record);

    int updateByPrimaryKey(Charger record);

}