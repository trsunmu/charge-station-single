package com.tian.mapper.charge;

import com.tian.entity.PayOrder;
import com.tian.entity.PayOrderExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 支付单 PayOrderMapper
 * @author fuce_自动生成
 * @email ${email}
 * @date 2023-11-13 20:02:55
 */
public interface PayOrderMapper {
      	   	      	      	      	      	      	      	      	      
    long countByExample(PayOrderExample example);
		
    int insert(PayOrder record);

    int insertSelective(PayOrder record);

    List<PayOrder> selectByExample(PayOrderExample example);
		
    PayOrder selectByPrimaryKey(Integer id);
		
    int updateByExampleSelective(@Param("record") PayOrder record, @Param("example") PayOrderExample example);

    int updateByExample(@Param("record") PayOrder record, @Param("example") PayOrderExample example); 
		
    int updateByPrimaryKeySelective(PayOrder record);

    int updateByPrimaryKey(PayOrder record);
  	  	
}