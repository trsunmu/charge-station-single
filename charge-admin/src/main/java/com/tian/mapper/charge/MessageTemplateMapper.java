package com.tian.mapper.charge;

import com.tian.entity.MessageTemplate;
import com.tian.entity.MessageTemplateExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 短信模板管理 MessageTemplateMapper
 * @author fuce_自动生成
 * @email ${email}
 * @date 2023-11-14 09:42:16
 */
public interface MessageTemplateMapper {
      	   	      	      	      	      	      	      	      
    long countByExample(MessageTemplateExample example);

    int deleteByExample(MessageTemplateExample example);
		
    int deleteByPrimaryKey(Integer id);
		
    int insert(MessageTemplate record);

    int insertSelective(MessageTemplate record);

    List<MessageTemplate> selectByExample(MessageTemplateExample example);
		
    MessageTemplate selectByPrimaryKey(Integer id);
		
    int updateByExampleSelective(@Param("record") MessageTemplate record, @Param("example") MessageTemplateExample example);

    int updateByExample(@Param("record") MessageTemplate record, @Param("example") MessageTemplateExample example); 
		
    int updateByPrimaryKeySelective(MessageTemplate record);

    int updateByPrimaryKey(MessageTemplate record);
  	  	
}