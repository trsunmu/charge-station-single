package com.tian.mapper.charge;

import java.util.List;

import com.tian.entity.GoodsInfo;
import com.tian.model.custom.GoodsInfoExample;
import org.apache.ibatis.annotations.Param;

/**
 * 商品信息表 GoodsInfoMapper
 */
public interface GoodsInfoMapper {

    long countByExample(GoodsInfoExample example);

    int deleteByExample(GoodsInfoExample example);
		
    int deleteByPrimaryKey(Integer id);
		
    int insert(GoodsInfo record);

    int insertSelective(GoodsInfo record);

    List<GoodsInfo> selectByExample(GoodsInfoExample example);
		
    GoodsInfo selectByPrimaryKey(Integer id);
		
    int updateByExampleSelective(@Param("record") GoodsInfo record, @Param("example") GoodsInfoExample example);

    int updateByExample(@Param("record") GoodsInfo record, @Param("example") GoodsInfoExample example); 
		
    int updateByPrimaryKeySelective(GoodsInfo record);

    int updateByPrimaryKey(GoodsInfo record);
  	  	
}