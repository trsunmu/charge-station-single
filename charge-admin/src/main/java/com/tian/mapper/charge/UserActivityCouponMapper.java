package com.tian.mapper.charge;

import com.tian.entity.UserActivityCoupon;
import com.tian.entity.UserActivityCouponExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 用户优惠券 UserActivityCouponMapper
 * @author tianwc_自动生成
 * @email ${email}
 * @date 2023-11-14 10:23:45
 */
public interface UserActivityCouponMapper {
      	   	      	      	      	      	      
    long countByExample(UserActivityCouponExample example);

    int deleteByExample(UserActivityCouponExample example);
		
    int deleteByPrimaryKey(Integer id);
		
    int insert(UserActivityCoupon record);

    int insertSelective(UserActivityCoupon record);

    List<UserActivityCoupon> selectByExample(UserActivityCouponExample example);
		
    UserActivityCoupon selectByPrimaryKey(Integer id);
		
    int updateByExampleSelective(@Param("record") UserActivityCoupon record, @Param("example") UserActivityCouponExample example);

    int updateByExample(@Param("record") UserActivityCoupon record, @Param("example") UserActivityCouponExample example); 
		
    int updateByPrimaryKeySelective(UserActivityCoupon record);

    int updateByPrimaryKey(UserActivityCoupon record);
  	  	
}