package com.tian.mapper.charge;

import com.tian.entity.CarInfo;
import com.tian.entity.CarInfoExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 汽车信息 CarInfoMapper
 * @author fuce_自动生成
 * @email ${email}
 * @date 2023-11-14 09:50:18
 */
public interface CarInfoMapper {
      	   	      	      	      	      	      	      	      	      	      	      
    long countByExample(CarInfoExample example);

    int deleteByExample(CarInfoExample example);
		
    int deleteByPrimaryKey(Integer id);
		
    int insert(CarInfo record);

    int insertSelective(CarInfo record);

    List<CarInfo> selectByExample(CarInfoExample example);
		
    CarInfo selectByPrimaryKey(Integer id);
		
    int updateByExampleSelective(@Param("record") CarInfo record, @Param("example") CarInfoExample example);

    int updateByExample(@Param("record") CarInfo record, @Param("example") CarInfoExample example); 
		
    int updateByPrimaryKeySelective(CarInfo record);

    int updateByPrimaryKey(CarInfo record);
  	  	
}