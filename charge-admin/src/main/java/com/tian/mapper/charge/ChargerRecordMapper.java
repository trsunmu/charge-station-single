package com.tian.mapper.charge;

import com.tian.entity.ChargerRecord;
import com.tian.entity.ChargerRecordExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年11月10日 10:28
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 */
public interface ChargerRecordMapper {

    long countByExample(ChargerRecordExample example);

    int deleteByExample(ChargerRecordExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(ChargerRecord record);

    int insertSelective(ChargerRecord record);

    List<ChargerRecord> selectByExample(ChargerRecordExample example);

    ChargerRecord selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") ChargerRecord record, @Param("example") ChargerRecordExample example);

    int updateByExample(@Param("record") ChargerRecord record, @Param("example") ChargerRecordExample example);

    int updateByPrimaryKeySelective(ChargerRecord record);

    int updateByPrimaryKey(ChargerRecord record);

}
