package com.tian.mapper.charge;

import com.tian.entity.Activity;
import com.tian.entity.ActivityExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 活动表 ActivityMapper
 * @author fuce_自动生成
 * @email ${email}
 * @date 2023-11-14 09:56:24
 */
public interface ActivityMapper {
      	   	      	      	      	      	      	      	      	      	      
    long countByExample(ActivityExample example);

    int deleteByExample(ActivityExample example);
		
    int deleteByPrimaryKey(Integer id);
		
    int insert(Activity record);

    int insertSelective(Activity record);

    List<Activity> selectByExample(ActivityExample example);
		
    Activity selectByPrimaryKey(Integer id);
		
    int updateByExampleSelective(@Param("record") Activity record, @Param("example") ActivityExample example);

    int updateByExample(@Param("record") Activity record, @Param("example") ActivityExample example); 
		
    int updateByPrimaryKeySelective(Activity record);

    int updateByPrimaryKey(Activity record);
  	  	
}