package com.tian.mapper.charge;

import com.tian.entity.ActivityGoods;
import com.tian.entity.ActivityGoodsExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 活动商品 ActivityGoodsMapper
 * @author tianwc_自动生成
 * @email ${email}
 * @date 2023-11-14 10:19:38
 */
public interface ActivityGoodsMapper {
      	   	      	      	      	      	      	      	      
    long countByExample(ActivityGoodsExample example);

    int deleteByExample(ActivityGoodsExample example);
		
    int deleteByPrimaryKey(Integer id);
		
    int insert(ActivityGoods record);

    int insertSelective(ActivityGoods record);

    List<ActivityGoods> selectByExample(ActivityGoodsExample example);
		
    ActivityGoods selectByPrimaryKey(Integer id);
		
    int updateByExampleSelective(@Param("record") ActivityGoods record, @Param("example") ActivityGoodsExample example);

    int updateByExample(@Param("record") ActivityGoods record, @Param("example") ActivityGoodsExample example); 
		
    int updateByPrimaryKeySelective(ActivityGoods record);

    int updateByPrimaryKey(ActivityGoods record);
  	  	
}