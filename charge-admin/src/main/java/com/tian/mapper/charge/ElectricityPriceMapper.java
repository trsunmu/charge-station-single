package com.tian.mapper.charge;

import com.tian.entity.ElectricityPrice;
import com.tian.entity.ElectricityPriceExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 电价 ElectricityPriceMapper
 * @author tianwc_自动生成
 * @email ${email}
 * @date 2023-11-14 10:26:58
 */
public interface ElectricityPriceMapper {
      	   	      	      	      	      	      	      
    long countByExample(ElectricityPriceExample example);

    int deleteByExample(ElectricityPriceExample example);
		
    int deleteByPrimaryKey(Integer id);
		
    int insert(ElectricityPrice record);

    int insertSelective(ElectricityPrice record);

    List<ElectricityPrice> selectByExample(ElectricityPriceExample example);
		
    ElectricityPrice selectByPrimaryKey(Integer id);
		
    int updateByExampleSelective(@Param("record") ElectricityPrice record, @Param("example") ElectricityPriceExample example);

    int updateByExample(@Param("record") ElectricityPrice record, @Param("example") ElectricityPriceExample example); 
		
    int updateByPrimaryKeySelective(ElectricityPrice record);

    int updateByPrimaryKey(ElectricityPrice record);
  	  	
}