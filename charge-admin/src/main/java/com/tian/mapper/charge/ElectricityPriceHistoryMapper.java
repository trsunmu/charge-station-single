package com.tian.mapper.charge;

import com.tian.entity.ElectricityPriceHistory;
import com.tian.entity.ElectricityPriceHistoryExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 电价变更 ElectricityPriceHistoryMapper
 * @author tianwc_自动生成
 * @email ${email}
 * @date 2023-11-14 10:28:12
 */
public interface ElectricityPriceHistoryMapper {
      	   	      	      	      	      	      	      
    long countByExample(ElectricityPriceHistoryExample example);

    int deleteByExample(ElectricityPriceHistoryExample example);
		
    int deleteByPrimaryKey(Integer id);
		
    int insert(ElectricityPriceHistory record);

    int insertSelective(ElectricityPriceHistory record);

    List<ElectricityPriceHistory> selectByExample(ElectricityPriceHistoryExample example);
		
    ElectricityPriceHistory selectByPrimaryKey(Integer id);
		
    int updateByExampleSelective(@Param("record") ElectricityPriceHistory record, @Param("example") ElectricityPriceHistoryExample example);

    int updateByExample(@Param("record") ElectricityPriceHistory record, @Param("example") ElectricityPriceHistoryExample example); 
		
    int updateByPrimaryKeySelective(ElectricityPriceHistory record);

    int updateByPrimaryKey(ElectricityPriceHistory record);
  	  	
}