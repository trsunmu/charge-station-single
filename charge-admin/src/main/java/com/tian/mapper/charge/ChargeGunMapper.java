package com.tian.mapper.charge;
import com.tian.entity.ChargeGun;
import com.tian.entity.ChargeGunExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * @author tianwc 公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年11月10日 17:17
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 */
public interface ChargeGunMapper {

    long countByExample(ChargeGunExample example);

    int deleteByExample(ChargeGunExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(ChargeGun record);

    int insertSelective(ChargeGun record);

    List<ChargeGun> selectByExample(ChargeGunExample example);

    ChargeGun selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") ChargeGun record, @Param("example") ChargeGunExample example);

    int updateByExample(@Param("record") ChargeGun record, @Param("example") ChargeGunExample example);

    int updateByPrimaryKeySelective(ChargeGun record);

    int updateByPrimaryKey(ChargeGun record);

}
