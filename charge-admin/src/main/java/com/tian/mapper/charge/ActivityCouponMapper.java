package com.tian.mapper.charge;
import com.tian.entity.ActivityCoupon;
import com.tian.entity.ActivityCouponExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;
/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年11月11日 15:51
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 */
public interface ActivityCouponMapper {

    long countByExample(ActivityCouponExample example);

    int deleteByExample(ActivityCouponExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(ActivityCoupon record);

    int insertSelective(ActivityCoupon record);

    List<ActivityCoupon> selectByExample(ActivityCouponExample example);

    ActivityCoupon selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") ActivityCoupon record, @Param("example") ActivityCouponExample example);

    int updateByExample(@Param("record") ActivityCoupon record, @Param("example") ActivityCouponExample example);

    int updateByPrimaryKeySelective(ActivityCoupon record);

    int updateByPrimaryKey(ActivityCoupon record);

}
