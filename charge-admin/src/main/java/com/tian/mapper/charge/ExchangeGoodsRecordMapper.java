package com.tian.mapper.charge;

import com.tian.entity.ExchangeGoodsRecord;
import com.tian.entity.ExchangeGoodsRecordExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 商品兑换 ExchangeGoodsRecordMapper
 * @author fuce_自动生成
 * @email ${email}
 * @date 2023-11-14 10:14:10
 */
public interface ExchangeGoodsRecordMapper {
      	   	      	      	      	      
    long countByExample(ExchangeGoodsRecordExample example);

    int deleteByExample(ExchangeGoodsRecordExample example);
		
    int deleteByPrimaryKey(Integer id);
		
    int insert(ExchangeGoodsRecord record);

    int insertSelective(ExchangeGoodsRecord record);

    List<ExchangeGoodsRecord> selectByExample(ExchangeGoodsRecordExample example);
		
    ExchangeGoodsRecord selectByPrimaryKey(Integer id);
		
    int updateByExampleSelective(@Param("record") ExchangeGoodsRecord record, @Param("example") ExchangeGoodsRecordExample example);

    int updateByExample(@Param("record") ExchangeGoodsRecord record, @Param("example") ExchangeGoodsRecordExample example); 
		
    int updateByPrimaryKeySelective(ExchangeGoodsRecord record);

    int updateByPrimaryKey(ExchangeGoodsRecord record);
  	  	
}