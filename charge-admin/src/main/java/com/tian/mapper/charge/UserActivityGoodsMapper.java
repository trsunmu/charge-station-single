package com.tian.mapper.charge;

import com.tian.entity.UserActivityGoods;
import com.tian.entity.UserActivityGoodsExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 用户商品 UserActivityGoodsMapper
 * @author tianwc_自动生成
 * @email ${email}
 * @date 2023-11-14 10:25:07
 */
public interface UserActivityGoodsMapper {
      	   	      	      	      	      	      
    long countByExample(UserActivityGoodsExample example);

    int deleteByExample(UserActivityGoodsExample example);
		
    int deleteByPrimaryKey(Integer id);
		
    int insert(UserActivityGoods record);

    int insertSelective(UserActivityGoods record);

    List<UserActivityGoods> selectByExample(UserActivityGoodsExample example);
		
    UserActivityGoods selectByPrimaryKey(Integer id);
		
    int updateByExampleSelective(@Param("record") UserActivityGoods record, @Param("example") UserActivityGoodsExample example);

    int updateByExample(@Param("record") UserActivityGoods record, @Param("example") UserActivityGoodsExample example); 
		
    int updateByPrimaryKeySelective(UserActivityGoods record);

    int updateByPrimaryKey(UserActivityGoods record);
  	  	
}