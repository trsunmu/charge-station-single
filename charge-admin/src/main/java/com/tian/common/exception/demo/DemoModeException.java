package com.tian.common.exception.demo;

/**
 * 演示模式异常
* @ClassName: DemoModeException
* @author tianwc
* @date 2019-11-08 15:45
*
 */
public class DemoModeException extends RuntimeException
{
    private static final long serialVersionUID = 1L;

    public DemoModeException()
    {
    }
}
