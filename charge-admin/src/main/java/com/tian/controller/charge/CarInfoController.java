package com.tian.controller.charge;

import com.tian.common.base.BaseController;
import com.tian.common.domain.AjaxResult;
import com.tian.common.domain.ResultTable;
import com.tian.common.log.Log;
import com.tian.model.custom.Tablepar;
import com.tian.entity.CarInfo;
import com.tian.service.charge.CarInfoService;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import cn.dev33.satoken.annotation.SaCheckPermission;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @author tianwc 公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年11月17日 16:51
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 *
 * 汽车认证状态
 */
@Api(value = "汽车信息")
@Controller
@RequestMapping("/CarInfoController")
public class CarInfoController extends BaseController {

    private String prefix = "charge/carInfo";

    @Resource
    private CarInfoService carInfoService;


    /**
     * 汽车信息页面展示
     *
     * @param model
     * @return String
     * @author tianwc
     */
    @ApiOperation(value = "分页跳转", notes = "分页跳转")
    @GetMapping("/view")
    @SaCheckPermission("gen:carInfo:view")
    public String view(ModelMap model) {
        return prefix + "/list";
    }

    /**
     * list集合
     *
     * @param tablepar
     * @param searchText
     * @return
     */
    //@Log(title = "汽车信息", action = "111")
    @ApiOperation(value = "分页跳转", notes = "分页跳转")
    @GetMapping("/list")
    @SaCheckPermission("gen:carInfo:list")
    @ResponseBody
    public ResultTable list(Tablepar tablepar, CarInfo carInfo) {
        PageInfo<CarInfo> page = carInfoService.list(tablepar, carInfo);
        return pageTable(page.getList(), page.getTotal());
    }

    /**
     * 新增跳转
     */
    @ApiOperation(value = "新增跳转", notes = "新增跳转")
    @GetMapping("/add")
    public String add(ModelMap modelMap) {
        return prefix + "/add";
    }

    /**
     * 汽车信息删除
     *
     * @param ids
     * @return
     */
    //@Log(title = "汽车信息删除", action = "111")
    @ApiOperation(value = "认证通过", notes = "删除")
    @DeleteMapping("/remove")
    @SaCheckPermission("gen:carInfo:remove")
    @ResponseBody
    public AjaxResult remove(String ids) {
        int b = carInfoService.deleteByPrimaryKey(ids);
        if (b > 0) {
            return success();
        } else {
            return error();
        }
    }


    /**
     * 修改跳转
     */
    @ApiOperation(value = "修改跳转", notes = "修改跳转")
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap map) {
        map.put("CarInfo", carInfoService.selectByPrimaryKey(id));

        return prefix + "/edit";
    }

    /**
     * 修改保存
     */
    @Log(title = "汽车信息认证审核", action = "汽车信息认证审核")
    @ApiOperation(value = "汽车信息认证审核", notes = "汽车信息认证审核")
    @SaCheckPermission("gen:carInfo:edit")
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(CarInfo carInfo) {
        return toAjax(carInfoService.updateByPrimaryKeySelective(carInfo));
    }
}
