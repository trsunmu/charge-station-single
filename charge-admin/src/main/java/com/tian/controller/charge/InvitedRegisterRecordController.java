package com.tian.controller.charge;

import com.tian.common.base.BaseController;
import com.tian.common.domain.AjaxResult;
import com.tian.common.domain.ResultTable;
import com.tian.model.custom.Tablepar;
import com.tian.entity.InvitedRegisterRecord;
import com.tian.service.charge.InvitedRegisterRecordService;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import cn.dev33.satoken.annotation.SaCheckPermission;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

/**
 * 邀请注册Controller
 * @ClassName: InvitedRegisterRecordController
 * @author tianwc
 * @date 2023-11-14 10:26:03
 */
@Api(value = "邀请注册")
@Controller
@RequestMapping("/InvitedRegisterRecordController")
public class InvitedRegisterRecordController extends BaseController{
	
	private String prefix = "charge/invitedRegisterRecord";
	
	@Autowired
	private InvitedRegisterRecordService invitedRegisterRecordService;
	
	
	/**
	 * 邀请注册页面展示
	 * @param model
	 * @return String
	 * @author tianwc
	 */
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/view")
	@SaCheckPermission("gen:invitedRegisterRecord:view")
    public String view(ModelMap model)
    {
        return prefix + "/list";
    }
	
	/**
	 * list集合
	 * @param tablepar
	 * @param searchText
	 * @return
	 */
	//@Log(title = "邀请注册", action = "111")
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/list")
	@SaCheckPermission("gen:invitedRegisterRecord:list")
	@ResponseBody
	public ResultTable list(Tablepar tablepar,InvitedRegisterRecord invitedRegisterRecord){
		PageInfo<InvitedRegisterRecord> page=invitedRegisterRecordService.list(tablepar,invitedRegisterRecord) ; 
		return pageTable(page.getList(),page.getTotal());
	}
	
	/**
     * 新增跳转
     */
	@ApiOperation(value = "新增跳转", notes = "新增跳转")
    @GetMapping("/add")
    public String add(ModelMap modelMap)
    {
        return prefix + "/add";
    }
	
    /**
     * 新增保存
     * @param 
     * @return
     */
	//@Log(title = "邀请注册新增", action = "111")
	@ApiOperation(value = "新增", notes = "新增")
	@PostMapping("/add")
	@SaCheckPermission("gen:invitedRegisterRecord:add")
	@ResponseBody
	public AjaxResult add(InvitedRegisterRecord invitedRegisterRecord){
		int b=invitedRegisterRecordService.insertSelective(invitedRegisterRecord);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	/**
	 * 邀请注册删除
	 * @param ids
	 * @return
	 */
	//@Log(title = "邀请注册删除", action = "111")
	@ApiOperation(value = "删除", notes = "删除")
	@DeleteMapping("/remove")
	@SaCheckPermission("gen:invitedRegisterRecord:remove")
	@ResponseBody
	public AjaxResult remove(String ids){
		int b=invitedRegisterRecordService.deleteByPrimaryKey(ids);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	
	/**
	 * 修改跳转
	 * @param id
	 * @param mmap
	 * @return
	 */
	@ApiOperation(value = "修改跳转", notes = "修改跳转")
	@GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap map)
    {
        map.put("InvitedRegisterRecord", invitedRegisterRecordService.selectByPrimaryKey(id));

        return prefix + "/edit";
    }
	
	/**
     * 修改保存
     */
    //@Log(title = "邀请注册修改", action = "111")
	@ApiOperation(value = "修改保存", notes = "修改保存")
    @SaCheckPermission("gen:invitedRegisterRecord:edit")
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(InvitedRegisterRecord invitedRegisterRecord)
    {
        return toAjax(invitedRegisterRecordService.updateByPrimaryKeySelective(invitedRegisterRecord));
    }
    
    
    /**
	 * 修改状态
	 * @param record
	 * @return
	 */
    @PutMapping("/updateVisible")
	@ResponseBody
    public AjaxResult updateVisible(@RequestBody InvitedRegisterRecord invitedRegisterRecord){
		int i=invitedRegisterRecordService.updateVisible(invitedRegisterRecord);
		return toAjax(i);
	}

    
    

	
}
