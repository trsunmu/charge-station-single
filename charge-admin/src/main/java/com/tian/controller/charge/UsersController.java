package com.tian.controller.charge;

import com.tian.common.base.BaseController;
import com.tian.common.domain.AjaxResult;
import com.tian.common.domain.ResultTable;
import com.tian.model.custom.Tablepar;
import com.tian.entity.Users;
import com.tian.service.charge.UsersService;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import cn.dev33.satoken.annotation.SaCheckPermission;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年11月09日 22:41
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 */
@Api(value = "用户表")
@Controller
@RequestMapping("/UsersController")
public class UsersController extends BaseController {

    private String prefix = "charge/users";

    @Autowired
    private UsersService usersService;


    /**
     * 用户表页面展示
     *
     * @param model
     * @return String
     * @author tianwc
     */
    @ApiOperation(value = "分页跳转", notes = "分页跳转")
    @GetMapping("/view")
    @SaCheckPermission("gen:users:view")
    public String view(ModelMap model) {
        return prefix + "/list";
    }

    /**
     * list集合
     *
     */
    //@Log(title = "用户表", action = "111")
    @ApiOperation(value = "分页跳转", notes = "分页跳转")
    @GetMapping("/list")
    @SaCheckPermission("gen:users:list")
    @ResponseBody
    public ResultTable list(Tablepar tablepar, Users users) {
        PageInfo<Users> page = usersService.list(tablepar, users);
        return pageTable(page.getList(), page.getTotal());
    }

    /**
     * 新增跳转
     */
    @ApiOperation(value = "新增跳转", notes = "新增跳转")
    @GetMapping("/add")
    public String add(ModelMap modelMap) {
        return prefix + "/add";
    }

    /**
     * 新增保存
     *
     * @param
     * @return
     */
    //@Log(title = "用户表新增", action = "111")
    @ApiOperation(value = "新增", notes = "新增")
    @PostMapping("/add")
    @SaCheckPermission("gen:users:add")
    @ResponseBody
    public AjaxResult add(Users users) {
        int b = usersService.insertSelective(users);
        if (b > 0) {
            return success();
        } else {
            return error();
        }
    }

    /**
     * 用户表删除
     *
     * @param ids
     * @return
     */
    //@Log(title = "用户表删除", action = "111")
    @ApiOperation(value = "删除", notes = "删除")
    @DeleteMapping("/remove")
    @SaCheckPermission("gen:users:remove")
    @ResponseBody
    public AjaxResult remove(String ids) {
        int b = usersService.deleteByPrimaryKey(ids);
        if (b > 0) {
            return success();
        } else {
            return error();
        }
    }


    /**
     * 修改跳转
     *
     */
    @ApiOperation(value = "修改跳转", notes = "修改跳转")
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap map) {
        map.put("Users", usersService.selectByPrimaryKey(id));

        return prefix + "/edit";
    }

    /**
     * 修改保存
     */
    //@Log(title = "用户表修改", action = "111")
    @ApiOperation(value = "修改保存", notes = "修改保存")
    @SaCheckPermission("gen:users:edit")
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(Users users) {
        return toAjax(usersService.updateByPrimaryKeySelective(users));
    }


    /**
     * 修改状态
     *
     */
    @PutMapping("/updateVisible")
    @ResponseBody
    public AjaxResult updateVisible(@RequestBody Users users) {
        int i = usersService.updateVisible(users);
        return toAjax(i);
    }
}