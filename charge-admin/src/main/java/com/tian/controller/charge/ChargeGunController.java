package com.tian.controller.charge;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.github.pagehelper.PageInfo;
import com.tian.common.base.BaseController;
import com.tian.common.domain.AjaxResult;
import com.tian.common.domain.ResultTable;
import com.tian.entity.ChargeGun;
import com.tian.model.custom.Tablepar;
import com.tian.service.charge.ChargeGunService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年11月10日 18:52
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 */
@Api(value = "充电枪管理")
@Controller
@RequestMapping("/ChargeGunController")
public class ChargeGunController extends BaseController {

    private String prefix = "charge/gun";

    @Resource
    private ChargeGunService chargeGunService;


    /**
     * 充电枪页面展示
     */
    @ApiOperation(value = "分页跳转", notes = "分页跳转")
    @GetMapping("/view")
    @SaCheckPermission("gen:chargeGun:view")
    public String view(ModelMap model) {
        return prefix + "/list";
    }

    /**
     * list集合
     */
    //@Log(title = "充电枪", action = "111")
    @ApiOperation(value = "分页跳转", notes = "分页跳转")
    @GetMapping("/list")
    @SaCheckPermission("gen:chargeGun:list")
    @ResponseBody
    public ResultTable list(Tablepar tablepar, ChargeGun chargeGun) {
        PageInfo<ChargeGun> page = chargeGunService.list(tablepar, chargeGun);
        return pageTable(page.getList(), page.getTotal());
    }

    /**
     * 新增跳转
     */
    @ApiOperation(value = "新增跳转", notes = "新增跳转")
    @GetMapping("/add")
    public String add(ModelMap modelMap) {
        return prefix + "/add";
    }

    /**
     * 新增保存
     */
    //@Log(title = "充电枪新增", action = "111")
    @ApiOperation(value = "新增", notes = "新增")
    @PostMapping("/add")
    @SaCheckPermission("gen:chargeGun:add")
    @ResponseBody
    public AjaxResult add(ChargeGun chargeGun) {
        int b = chargeGunService.insertSelective(chargeGun);
        if (b > 0) {
            return success();
        } else {
            return error();
        }
    }

    /**
     * 充电枪删除
     */
    //@Log(title = "充电枪删除", action = "111")
    @ApiOperation(value = "删除", notes = "删除")
    @DeleteMapping("/remove")
    @SaCheckPermission("gen:chargeGun:remove")
    @ResponseBody
    public AjaxResult remove(String ids) {
        int b = chargeGunService.deleteByPrimaryKey(ids);
        if (b > 0) {
            return success();
        } else {
            return error();
        }
    }


    /**
     * 修改跳转
     */
    @ApiOperation(value = "修改跳转", notes = "修改跳转")
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap map) {
        map.put("ChargeGun", chargeGunService.selectByPrimaryKey(id));

        return prefix + "/edit";
    }

    /**
     * 修改保存
     */
    //@Log(title = "充电枪修改", action = "111")
    @ApiOperation(value = "修改保存", notes = "修改保存")
    @SaCheckPermission("gen:chargeGun:edit")
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(ChargeGun chargeGun) {
        return toAjax(chargeGunService.updateByPrimaryKeySelective(chargeGun));
    }


    /**
     * 修改状态
     */
    @PutMapping("/updateVisible")
    @ResponseBody
    public AjaxResult updateVisible(@RequestBody ChargeGun chargeGun) {
        int i = chargeGunService.updateVisible(chargeGun);
        return toAjax(i);
    }


}
