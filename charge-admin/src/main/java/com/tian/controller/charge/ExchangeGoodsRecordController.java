package com.tian.controller.charge;

import com.tian.common.base.BaseController;
import com.tian.common.domain.AjaxResult;
import com.tian.common.domain.ResultTable;
import com.tian.model.custom.Tablepar;
import com.tian.entity.ExchangeGoodsRecord;
import com.tian.service.charge.ExchangeGoodsRecordService;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import cn.dev33.satoken.annotation.SaCheckPermission;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

/**
 * 商品兑换Controller
 * @ClassName: ExchangeGoodsRecordController
 * @author tianwc
 * @date 2023-11-14 10:14:10
 */
@Api(value = "商品兑换")
@Controller
@RequestMapping("/ExchangeGoodsRecordController")
public class ExchangeGoodsRecordController extends BaseController{
	
	private String prefix = "charge/exchangeGoodsRecord";
	
	@Autowired
	private ExchangeGoodsRecordService exchangeGoodsRecordService;
	
	
	/**
	 * 商品兑换页面展示
	 * @param model
	 * @return String
	 * @author tianwc
	 */
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/view")
	@SaCheckPermission("gen:exchangeGoodsRecord:view")
    public String view(ModelMap model)
    {
        return prefix + "/list";
    }
	
	/**
	 * list集合
	 * @param tablepar
	 * @param searchText
	 * @return
	 */
	//@Log(title = "商品兑换", action = "111")
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/list")
	@SaCheckPermission("gen:exchangeGoodsRecord:list")
	@ResponseBody
	public ResultTable list(Tablepar tablepar,ExchangeGoodsRecord exchangeGoodsRecord){
		PageInfo<ExchangeGoodsRecord> page=exchangeGoodsRecordService.list(tablepar,exchangeGoodsRecord) ; 
		return pageTable(page.getList(),page.getTotal());
	}
	
	/**
     * 新增跳转
     */
	@ApiOperation(value = "新增跳转", notes = "新增跳转")
    @GetMapping("/add")
    public String add(ModelMap modelMap)
    {
        return prefix + "/add";
    }
	
    /**
     * 新增保存
     * @param 
     * @return
     */
	//@Log(title = "商品兑换新增", action = "111")
	@ApiOperation(value = "新增", notes = "新增")
	@PostMapping("/add")
	@SaCheckPermission("gen:exchangeGoodsRecord:add")
	@ResponseBody
	public AjaxResult add(ExchangeGoodsRecord exchangeGoodsRecord){
		int b=exchangeGoodsRecordService.insertSelective(exchangeGoodsRecord);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	/**
	 * 商品兑换删除
	 * @param ids
	 * @return
	 */
	//@Log(title = "商品兑换删除", action = "111")
	@ApiOperation(value = "删除", notes = "删除")
	@DeleteMapping("/remove")
	@SaCheckPermission("gen:exchangeGoodsRecord:remove")
	@ResponseBody
	public AjaxResult remove(String ids){
		int b=exchangeGoodsRecordService.deleteByPrimaryKey(ids);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	
	/**
	 * 修改跳转
	 * @param id
	 * @param mmap
	 * @return
	 */
	@ApiOperation(value = "修改跳转", notes = "修改跳转")
	@GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap map)
    {
        map.put("ExchangeGoodsRecord", exchangeGoodsRecordService.selectByPrimaryKey(id));

        return prefix + "/edit";
    }
	
	/**
     * 修改保存
     */
    //@Log(title = "商品兑换修改", action = "111")
	@ApiOperation(value = "修改保存", notes = "修改保存")
    @SaCheckPermission("gen:exchangeGoodsRecord:edit")
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(ExchangeGoodsRecord exchangeGoodsRecord)
    {
        return toAjax(exchangeGoodsRecordService.updateByPrimaryKeySelective(exchangeGoodsRecord));
    }
    
    
    /**
	 * 修改状态
	 * @param record
	 * @return
	 */
    @PutMapping("/updateVisible")
	@ResponseBody
    public AjaxResult updateVisible(@RequestBody ExchangeGoodsRecord exchangeGoodsRecord){
		int i=exchangeGoodsRecordService.updateVisible(exchangeGoodsRecord);
		return toAjax(i);
	}

    
    

	
}
