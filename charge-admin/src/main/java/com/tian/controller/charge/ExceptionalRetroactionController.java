package com.tian.controller.charge;

import com.tian.common.base.BaseController;
import com.tian.common.domain.AjaxResult;
import com.tian.common.domain.ResultTable;
import com.tian.model.custom.Tablepar;
import com.tian.entity.ExceptionalRetroaction;
import com.tian.service.charge.ExceptionalRetroactionService;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import cn.dev33.satoken.annotation.SaCheckPermission;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

/**
 * 异常反馈Controller
 * @ClassName: ExceptionalRetroactionController
 * @author tianwc
 * @date 2023-11-13 20:35:29
 */
@Api(value = "异常反馈")
@Controller
@RequestMapping("/ExceptionalRetroactionController")
public class ExceptionalRetroactionController extends BaseController{
	
	private String prefix = "charge/exceptionalRetroaction";
	
	@Autowired
	private ExceptionalRetroactionService exceptionalRetroactionService;
	
	
	/**
	 * 异常反馈页面展示
	 * @param model
	 * @return String
	 * @author tianwc
	 */
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/view")
	@SaCheckPermission("gen:exceptionalRetroaction:view")
    public String view(ModelMap model)
    {
        return prefix + "/list";
    }
	
	/**
	 * list集合
	 */
	//@Log(title = "异常反馈", action = "111")
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/list")
	@SaCheckPermission("gen:exceptionalRetroaction:list")
	@ResponseBody
	public ResultTable list(Tablepar tablepar,ExceptionalRetroaction exceptionalRetroaction){
		PageInfo<ExceptionalRetroaction> page=exceptionalRetroactionService.list(tablepar,exceptionalRetroaction) ; 
		return pageTable(page.getList(),page.getTotal());
	}
	
	/**
     * 新增跳转
     */
	@ApiOperation(value = "新增跳转", notes = "新增跳转")
    @GetMapping("/add")
    public String add(ModelMap modelMap)
    {
        return prefix + "/add";
    }
	
    /**
     * 新增保存
     * @param 
     * @return
     */
	//@Log(title = "异常反馈新增", action = "111")
	@ApiOperation(value = "新增", notes = "新增")
	@PostMapping("/add")
	@SaCheckPermission("gen:exceptionalRetroaction:add")
	@ResponseBody
	public AjaxResult add(ExceptionalRetroaction exceptionalRetroaction){
		int b=exceptionalRetroactionService.insertSelective(exceptionalRetroaction);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	/**
	 * 异常反馈删除
	 * @param ids
	 * @return
	 */
	//@Log(title = "异常反馈删除", action = "111")
	@ApiOperation(value = "删除", notes = "删除")
	@DeleteMapping("/remove")
	@SaCheckPermission("gen:exceptionalRetroaction:remove")
	@ResponseBody
	public AjaxResult remove(String ids){
		int b=exceptionalRetroactionService.deleteByPrimaryKey(ids);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	
	/**
	 * 修改跳转
	 * @param id
	 * @param mmap
	 * @return
	 */
	@ApiOperation(value = "修改跳转", notes = "修改跳转")
	@GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap map)
    {
        map.put("ExceptionalRetroaction", exceptionalRetroactionService.selectByPrimaryKey(id));

        return prefix + "/edit";
    }
	
	/**
     * 修改保存
     */
    //@Log(title = "异常反馈修改", action = "111")
	@ApiOperation(value = "修改保存", notes = "修改保存")
    @SaCheckPermission("gen:exceptionalRetroaction:edit")
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(ExceptionalRetroaction exceptionalRetroaction)
    {
        return toAjax(exceptionalRetroactionService.updateByPrimaryKeySelective(exceptionalRetroaction));
    }
    
    
    /**
	 * 修改状态
	 * @param record
	 * @return
	 */
    @PutMapping("/updateVisible")
	@ResponseBody
    public AjaxResult updateVisible(@RequestBody ExceptionalRetroaction exceptionalRetroaction){
		int i=exceptionalRetroactionService.updateVisible(exceptionalRetroaction);
		return toAjax(i);
	}

    
    

	
}
