package com.tian.controller.charge;

import com.tian.common.base.BaseController;
import com.tian.common.domain.AjaxResult;
import com.tian.common.domain.ResultTable;
import com.tian.model.custom.Tablepar;
import com.tian.entity.GlobalProperty;
import com.tian.service.charge.GlobalPropertyService;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import cn.dev33.satoken.annotation.SaCheckPermission;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

/**
 * 全局配置Controller
 * @ClassName: GlobalPropertyController
 * @author tianwc
 * @date 2023-11-14 10:29:12
 */
@Api(value = "全局配置")
@Controller
@RequestMapping("/GlobalPropertyController")
public class GlobalPropertyController extends BaseController{
	
	private String prefix = "charge/globalProperty";
	
	@Autowired
	private GlobalPropertyService globalPropertyService;
	
	
	/**
	 * 全局配置页面展示
	 * @param model
	 * @return String
	 * @author tianwc
	 */
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/view")
	@SaCheckPermission("gen:globalProperty:view")
    public String view(ModelMap model)
    {
        return prefix + "/list";
    }
	
	/**
	 * list集合
	 * @param tablepar
	 * @param searchText
	 * @return
	 */
	//@Log(title = "全局配置", action = "111")
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/list")
	@SaCheckPermission("gen:globalProperty:list")
	@ResponseBody
	public ResultTable list(Tablepar tablepar,GlobalProperty globalProperty){
		PageInfo<GlobalProperty> page=globalPropertyService.list(tablepar,globalProperty) ; 
		return pageTable(page.getList(),page.getTotal());
	}
	
	/**
     * 新增跳转
     */
	@ApiOperation(value = "新增跳转", notes = "新增跳转")
    @GetMapping("/add")
    public String add(ModelMap modelMap)
    {
        return prefix + "/add";
    }
	
    /**
     * 新增保存
     * @param 
     * @return
     */
	//@Log(title = "全局配置新增", action = "111")
	@ApiOperation(value = "新增", notes = "新增")
	@PostMapping("/add")
	@SaCheckPermission("gen:globalProperty:add")
	@ResponseBody
	public AjaxResult add(GlobalProperty globalProperty){
		int b=globalPropertyService.insertSelective(globalProperty);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	/**
	 * 全局配置删除
	 * @param ids
	 * @return
	 */
	//@Log(title = "全局配置删除", action = "111")
	@ApiOperation(value = "删除", notes = "删除")
	@DeleteMapping("/remove")
	@SaCheckPermission("gen:globalProperty:remove")
	@ResponseBody
	public AjaxResult remove(String ids){
		int b=globalPropertyService.deleteByPrimaryKey(ids);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	
	/**
	 * 修改跳转
	 * @param id
	 * @param mmap
	 * @return
	 */
	@ApiOperation(value = "修改跳转", notes = "修改跳转")
	@GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap map)
    {
        map.put("GlobalProperty", globalPropertyService.selectByPrimaryKey(id));

        return prefix + "/edit";
    }
	
	/**
     * 修改保存
     */
    //@Log(title = "全局配置修改", action = "111")
	@ApiOperation(value = "修改保存", notes = "修改保存")
    @SaCheckPermission("gen:globalProperty:edit")
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(GlobalProperty globalProperty)
    {
        return toAjax(globalPropertyService.updateByPrimaryKeySelective(globalProperty));
    }
    
    
    /**
	 * 修改状态
	 * @param record
	 * @return
	 */
    @PutMapping("/updateVisible")
	@ResponseBody
    public AjaxResult updateVisible(@RequestBody GlobalProperty globalProperty){
		int i=globalPropertyService.updateVisible(globalProperty);
		return toAjax(i);
	}

    
    

	
}
