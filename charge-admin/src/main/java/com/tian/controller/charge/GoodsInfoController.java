package com.tian.controller.charge;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.github.pagehelper.PageInfo;
import com.tian.common.base.BaseController;
import com.tian.common.domain.AjaxResult;
import com.tian.common.domain.ResultTable;
import com.tian.entity.GoodsInfo;
import com.tian.model.custom.Tablepar;
import com.tian.service.charge.GoodsInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

/**
 * 商品信息表Controller
 *
 * @author tianwc
 * @ClassName: GoodsInfoController
 * @date 2023-11-09 15:28:47
 */
@Api(value = "商品信息表")
@Controller
@RequestMapping("/GoodsInfoController")
public class GoodsInfoController extends BaseController {

    private String prefix = "charge/goodsInfo";

    @Autowired
    private GoodsInfoService goodsInfoService;


    /**
     * 商品信息表页面展示
     *
     * @param model
     * @return String
     * @author tianwc
     */
    @ApiOperation(value = "分页跳转", notes = "分页跳转")
    @GetMapping("/view")
    @SaCheckPermission("gen:goodsInfo:view")
    public String view(ModelMap model) {
        return prefix + "/list";
    }

    /**
     * list集合
     */
    //@Log(title = "商品信息表", action = "111")
    @ApiOperation(value = "分页跳转", notes = "分页跳转")
    @GetMapping("/list")
    @SaCheckPermission("gen:goodsInfo:list")
    @ResponseBody
    public ResultTable list(Tablepar tablepar, GoodsInfo goodsInfo) {
        PageInfo<GoodsInfo> page = goodsInfoService.list(tablepar, goodsInfo);
        return pageTable(page.getList(), page.getTotal());
    }

    /**
     * 新增跳转
     */
    @ApiOperation(value = "新增跳转", notes = "新增跳转")
    @GetMapping("/add")
    public String add(ModelMap modelMap) {
        return prefix + "/add";
    }

    /**
     * 新增保存
     *
     * @param
     * @return
     */
    //@Log(title = "商品信息表新增", action = "111")
    @ApiOperation(value = "新增", notes = "新增")
    @PostMapping("/add")
    @SaCheckPermission("gen:goodsInfo:add")
    @ResponseBody
    public AjaxResult add(GoodsInfo goodsInfo) {
        int b = goodsInfoService.insertSelective(goodsInfo);
        if (b > 0) {
            return success();
        } else {
            return error();
        }
    }

    /**
     * 商品信息表删除
     *
     * @param ids
     * @return
     */
    //@Log(title = "商品信息表删除", action = "111")
    @ApiOperation(value = "删除", notes = "删除")
    @DeleteMapping("/remove")
    @SaCheckPermission("gen:goodsInfo:remove")
    @ResponseBody
    public AjaxResult remove(String ids) {
        int b = goodsInfoService.deleteByPrimaryKey(ids);
        if (b > 0) {
            return success();
        } else {
            return error();
        }
    }


    /**
     * 修改跳转
     */
    @ApiOperation(value = "修改跳转", notes = "修改跳转")
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap map) {
        map.put("GoodsInfo", goodsInfoService.selectByPrimaryKey(id));
        return prefix + "/edit";
    }

    /**
     * 修改保存
     */
    //@Log(title = "商品信息表修改", action = "111")
    @ApiOperation(value = "修改保存", notes = "修改保存")
    @SaCheckPermission("gen:goodsInfo:edit")
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(GoodsInfo goodsInfo) {
        return toAjax(goodsInfoService.updateByPrimaryKeySelective(goodsInfo));
    }


    /**
     * 修改状态
     *
     * @param record
     * @return
     */
    @PutMapping("/updateVisible")
    @ResponseBody
    public AjaxResult updateVisible(@RequestBody GoodsInfo goodsInfo) {
        int i = goodsInfoService.updateVisible(goodsInfo);
        return toAjax(i);
    }


}
