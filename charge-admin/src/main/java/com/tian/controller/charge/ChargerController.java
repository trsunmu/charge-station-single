package com.tian.controller.charge;

import com.tian.common.base.BaseController;
import com.tian.common.domain.AjaxResult;
import com.tian.common.domain.ResultTable;
import com.tian.model.custom.Tablepar;
import com.tian.entity.Charger;
import com.tian.service.charge.ChargerService;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import cn.dev33.satoken.annotation.SaCheckPermission;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年11月10日 19:31
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 */
@Api(value = "充电桩")
@Controller
@RequestMapping("/ChargerController")
public class ChargerController extends BaseController {

    private String prefix = "charge/charger";

    @Resource
    private ChargerService chargerService;


    /**
     * 充电桩页面展示
     */
    @ApiOperation(value = "分页跳转", notes = "分页跳转")
    @GetMapping("/view")
    @SaCheckPermission("gen:charger:view")
    public String view(ModelMap model) {
        return prefix + "/list";
    }

    /**
     * list集合
     */
    //@Log(title = "充电桩", action = "111")
    @ApiOperation(value = "分页跳转", notes = "分页跳转")
    @GetMapping("/list")
    @SaCheckPermission("gen:charger:list")
    @ResponseBody
    public ResultTable list(Tablepar tablepar, Charger charger) {
        PageInfo<Charger> page = chargerService.list(tablepar, charger);
        return pageTable(page.getList(), page.getTotal());
    }

    /**
     * 新增跳转
     */
    @ApiOperation(value = "新增跳转", notes = "新增跳转")
    @GetMapping("/add")
    public String add(ModelMap modelMap) {
        return prefix + "/add";
    }

    /**
     * 新增保存
     */
    //@Log(title = "充电桩新增", action = "111")
    @ApiOperation(value = "新增", notes = "新增")
    @PostMapping("/add")
    @SaCheckPermission("gen:charger:add")
    @ResponseBody
    public AjaxResult add(Charger charger) {
        int b = chargerService.insertSelective(charger);
        if (b > 0) {
            return success();
        } else {
            return error();
        }
    }

    /**
     * 充电桩删除
     */
    //@Log(title = "充电桩删除", action = "111")
    @ApiOperation(value = "删除", notes = "删除")
    @DeleteMapping("/remove")
    @SaCheckPermission("gen:charger:remove")
    @ResponseBody
    public AjaxResult remove(String ids) {
        int b = chargerService.deleteByPrimaryKey(ids);
        if (b > 0) {
            return success();
        } else {
            return error();
        }
    }


    /**
     * 修改跳转
     */
    @ApiOperation(value = "修改跳转", notes = "修改跳转")
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap map) {
        map.put("Charger", chargerService.selectByPrimaryKey(id));

        return prefix + "/edit";
    }

    /**
     * 修改保存
     */
    //@Log(title = "充电桩修改", action = "111")
    @ApiOperation(value = "修改保存", notes = "修改保存")
    @SaCheckPermission("gen:charger:edit")
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(Charger charger) {
        return toAjax(chargerService.updateByPrimaryKeySelective(charger));
    }


    /**
     * 修改状态
     */
    @PutMapping("/updateVisible")
    @ResponseBody
    public AjaxResult updateVisible(@RequestBody Charger charger) {
        int i = chargerService.updateVisible(charger);
        return toAjax(i);
    }

}
