package com.tian.controller.charge;

import com.tian.common.base.BaseController;
import com.tian.common.domain.AjaxResult;
import com.tian.common.domain.ResultTable;
import com.tian.model.custom.Tablepar;
import com.tian.entity.Coupon;
import com.tian.service.charge.CouponService;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import cn.dev33.satoken.annotation.SaCheckPermission;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年11月10日 17:00
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 */
@Api(value = "优惠券")
@Controller
@RequestMapping("/CouponController")
public class CouponController extends BaseController{

    private String prefix = "charge/coupon";

    @Autowired
    private CouponService couponService;


    /**
     * 优惠券页面展示
     * @param model
     * @return String
     * @author tianwc
     */
    @ApiOperation(value = "分页跳转", notes = "分页跳转")
    @GetMapping("/view")
    @SaCheckPermission("gen:coupon:view")
    public String view(ModelMap model)
    {
        return prefix + "/list";
    }

    /**
     * list集合
     */
    //@Log(title = "优惠券", action = "111")
    @ApiOperation(value = "分页跳转", notes = "分页跳转")
    @GetMapping("/list")
    @SaCheckPermission("gen:coupon:list")
    @ResponseBody
    public ResultTable list(Tablepar tablepar,Coupon coupon){
        PageInfo<Coupon> page=couponService.list(tablepar,coupon) ;
        return pageTable(page.getList(),page.getTotal());
    }

    /**
     * 新增跳转
     */
    @ApiOperation(value = "新增跳转", notes = "新增跳转")
    @GetMapping("/add")
    public String add(ModelMap modelMap)
    {
        return prefix + "/add";
    }

    /**
     * 新增保存
     */
    //@Log(title = "优惠券新增", action = "111")
    @ApiOperation(value = "新增", notes = "新增")
    @PostMapping("/add")
    @SaCheckPermission("gen:coupon:add")
    @ResponseBody
    public AjaxResult add(Coupon coupon){
        int b=couponService.insertSelective(coupon);
        if(b>0){
            return success();
        }else{
            return error();
        }
    }

    /**
     * 优惠券删除
     */
    //@Log(title = "优惠券删除", action = "111")
    @ApiOperation(value = "删除", notes = "删除")
    @DeleteMapping("/remove")
    @SaCheckPermission("gen:coupon:remove")
    @ResponseBody
    public AjaxResult remove(String ids){
        int b=couponService.deleteByPrimaryKey(ids);
        if(b>0){
            return success();
        }else{
            return error();
        }
    }


    /**
     * 修改跳转
     */
    @ApiOperation(value = "修改跳转", notes = "修改跳转")
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap map)
    {
        map.put("Coupon", couponService.selectByPrimaryKey(id));

        return prefix + "/edit";
    }

    /**
     * 修改保存
     */
    //@Log(title = "优惠券修改", action = "111")
    @ApiOperation(value = "修改保存", notes = "修改保存")
    @SaCheckPermission("gen:coupon:edit")
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(Coupon coupon)
    {
        return toAjax(couponService.updateByPrimaryKeySelective(coupon));
    }


    /**
     * 修改状态
     */
    @PutMapping("/updateVisible")
    @ResponseBody
    public AjaxResult updateVisible(@RequestBody Coupon coupon){
        int i=couponService.updateVisible(coupon);
        return toAjax(i);
    }





}
