package com.tian.controller.charge;

import com.tian.common.base.BaseController;
import com.tian.common.domain.AjaxResult;
import com.tian.common.domain.ResultTable;
import com.tian.model.custom.Tablepar;
import com.tian.entity.PointsModifyRecord;
import com.tian.service.charge.PointsModifyRecordService;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import cn.dev33.satoken.annotation.SaCheckPermission;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

/**
 * 用户积分变更Controller
 * @ClassName: PointsModifyRecordController
 * @author tianwc
 * @date 2023-11-14 10:08:21
 */
@Api(value = "用户积分变更")
@Controller
@RequestMapping("/PointsModifyRecordController")
public class PointsModifyRecordController extends BaseController{
	
	private String prefix = "charge/pointsModifyRecord";
	
	@Autowired
	private PointsModifyRecordService pointsModifyRecordService;
	
	
	/**
	 * 用户积分变更页面展示
	 * @param model
	 * @return String
	 * @author tianwc
	 */
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/view")
	@SaCheckPermission("gen:pointsModifyRecord:view")
    public String view(ModelMap model)
    {
        return prefix + "/list";
    }
	
	/**
	 * list集合
	 * @param tablepar
	 * @param searchText
	 * @return
	 */
	//@Log(title = "用户积分变更", action = "111")
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/list")
	@SaCheckPermission("gen:pointsModifyRecord:list")
	@ResponseBody
	public ResultTable list(Tablepar tablepar,PointsModifyRecord pointsModifyRecord){
		PageInfo<PointsModifyRecord> page=pointsModifyRecordService.list(tablepar,pointsModifyRecord) ; 
		return pageTable(page.getList(),page.getTotal());
	}
	
	/**
     * 新增跳转
     */
	@ApiOperation(value = "新增跳转", notes = "新增跳转")
    @GetMapping("/add")
    public String add(ModelMap modelMap)
    {
        return prefix + "/add";
    }
	
    /**
     * 新增保存
     * @param 
     * @return
     */
	//@Log(title = "用户积分变更新增", action = "111")
	@ApiOperation(value = "新增", notes = "新增")
	@PostMapping("/add")
	@SaCheckPermission("gen:pointsModifyRecord:add")
	@ResponseBody
	public AjaxResult add(PointsModifyRecord pointsModifyRecord){
		int b=pointsModifyRecordService.insertSelective(pointsModifyRecord);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	/**
	 * 用户积分变更删除
	 * @param ids
	 * @return
	 */
	//@Log(title = "用户积分变更删除", action = "111")
	@ApiOperation(value = "删除", notes = "删除")
	@DeleteMapping("/remove")
	@SaCheckPermission("gen:pointsModifyRecord:remove")
	@ResponseBody
	public AjaxResult remove(String ids){
		int b=pointsModifyRecordService.deleteByPrimaryKey(ids);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	
	/**
	 * 修改跳转
	 * @param id
	 * @param mmap
	 * @return
	 */
	@ApiOperation(value = "修改跳转", notes = "修改跳转")
	@GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap map)
    {
        map.put("PointsModifyRecord", pointsModifyRecordService.selectByPrimaryKey(id));

        return prefix + "/edit";
    }
	
	/**
     * 修改保存
     */
    //@Log(title = "用户积分变更修改", action = "111")
	@ApiOperation(value = "修改保存", notes = "修改保存")
    @SaCheckPermission("gen:pointsModifyRecord:edit")
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(PointsModifyRecord pointsModifyRecord)
    {
        return toAjax(pointsModifyRecordService.updateByPrimaryKeySelective(pointsModifyRecord));
    }
    
    
    /**
	 * 修改状态
	 * @param record
	 * @return
	 */
    @PutMapping("/updateVisible")
	@ResponseBody
    public AjaxResult updateVisible(@RequestBody PointsModifyRecord pointsModifyRecord){
		int i=pointsModifyRecordService.updateVisible(pointsModifyRecord);
		return toAjax(i);
	}

    
    

	
}
