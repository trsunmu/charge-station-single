package com.tian.controller.charge;

import com.github.pagehelper.PageInfo;
import com.tian.common.base.BaseController;
import com.tian.common.domain.AjaxResult;
import com.tian.common.domain.ResultTable;
import com.tian.entity.ChargerRecord;
import com.tian.model.custom.Tablepar;
import com.tian.service.charge.ChargerRecordService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import cn.dev33.satoken.annotation.SaCheckPermission;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年11月10日 10:32
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 */
@Api(value = "")
@Controller
@RequestMapping("/ChargerRecordController")
public class ChargerRecordController extends BaseController {

    private String prefix = "charge/chargerecord";

    @Autowired
    private ChargerRecordService chargerRecordService;


    /**
     * 页面展示
     */
    @ApiOperation(value = "分页跳转", notes = "分页跳转")
    @GetMapping("/view")
    @SaCheckPermission("gen:chargerRecord:view")
    public String view(ModelMap model)
    {
        return prefix + "/list";
    }

    /**
     * list集合
     */
    //@Log(title = "", action = "111")
    @ApiOperation(value = "分页跳转", notes = "分页跳转")
    @GetMapping("/list")
    @SaCheckPermission("gen:chargerRecord:list")
    @ResponseBody
    public ResultTable list(Tablepar tablepar, ChargerRecord chargerRecord){
        PageInfo<ChargerRecord> page=chargerRecordService.list(tablepar,chargerRecord) ;
        return pageTable(page.getList(),page.getTotal());
    }

    /**
     * 新增跳转
     */
    @ApiOperation(value = "新增跳转", notes = "新增跳转")
    @GetMapping("/add")
    public String add(ModelMap modelMap)
    {
        return prefix + "/add";
    }

    /**
     * 新增保存
     */
    //@Log(title = "新增", action = "111")
    @ApiOperation(value = "新增", notes = "新增")
    @PostMapping("/add")
    @SaCheckPermission("gen:chargerRecord:add")
    @ResponseBody
    public AjaxResult add(ChargerRecord chargerRecord){
        int b=chargerRecordService.insertSelective(chargerRecord);
        if(b>0){
            return success();
        }else{
            return error();
        }
    }

    /**
     * 删除
     */
    //@Log(title = "删除", action = "111")
    @ApiOperation(value = "删除", notes = "删除")
    @DeleteMapping("/remove")
    @SaCheckPermission("gen:chargerRecord:remove")
    @ResponseBody
    public AjaxResult remove(String ids){
        int b=chargerRecordService.deleteByPrimaryKey(ids);
        if(b>0){
            return success();
        }else{
            return error();
        }
    }


    /**
     * 修改跳转
     */
    @ApiOperation(value = "修改跳转", notes = "修改跳转")
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap map)
    {
        map.put("ChargerRecord", chargerRecordService.selectByPrimaryKey(id));

        return prefix + "/edit";
    }

    /**
     * 修改保存
     */
    //@Log(title = "修改", action = "111")
    @ApiOperation(value = "修改保存", notes = "修改保存")
    @SaCheckPermission("gen:chargerRecord:edit")
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(ChargerRecord chargerRecord)
    {
        return toAjax(chargerRecordService.updateByPrimaryKeySelective(chargerRecord));
    }


    /**
     * 修改状态
     */
    @PutMapping("/updateVisible")
    @ResponseBody
    public AjaxResult updateVisible(@RequestBody ChargerRecord chargerRecord){
        int i=chargerRecordService.updateVisible(chargerRecord);
        return toAjax(i);
    }





}