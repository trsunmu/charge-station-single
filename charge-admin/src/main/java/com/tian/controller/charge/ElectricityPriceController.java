package com.tian.controller.charge;

import com.tian.common.base.BaseController;
import com.tian.common.domain.AjaxResult;
import com.tian.common.domain.ResultTable;
import com.tian.model.custom.Tablepar;
import com.tian.entity.ElectricityPrice;
import com.tian.service.charge.ElectricityPriceService;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import cn.dev33.satoken.annotation.SaCheckPermission;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

/**
 * 电价Controller
 * @ClassName: ElectricityPriceController
 * @author tianwc
 * @date 2023-11-14 10:26:58
 */
@Api(value = "电价")
@Controller
@RequestMapping("/ElectricityPriceController")
public class ElectricityPriceController extends BaseController{
	
	private String prefix = "charge/electricityPrice";
	
	@Autowired
	private ElectricityPriceService electricityPriceService;
	
	
	/**
	 * 电价页面展示
	 * @param model
	 * @return String
	 * @author tianwc
	 */
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/view")
	@SaCheckPermission("gen:electricityPrice:view")
    public String view(ModelMap model)
    {
        return prefix + "/list";
    }
	
	/**
	 * list集合
	 * @param tablepar
	 * @param searchText
	 * @return
	 */
	//@Log(title = "电价", action = "111")
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/list")
	@SaCheckPermission("gen:electricityPrice:list")
	@ResponseBody
	public ResultTable list(Tablepar tablepar,ElectricityPrice electricityPrice){
		PageInfo<ElectricityPrice> page=electricityPriceService.list(tablepar,electricityPrice) ; 
		return pageTable(page.getList(),page.getTotal());
	}
	
	/**
     * 新增跳转
     */
	@ApiOperation(value = "新增跳转", notes = "新增跳转")
    @GetMapping("/add")
    public String add(ModelMap modelMap)
    {
        return prefix + "/add";
    }
	
    /**
     * 新增保存
     * @param 
     * @return
     */
	//@Log(title = "电价新增", action = "111")
	@ApiOperation(value = "新增", notes = "新增")
	@PostMapping("/add")
	@SaCheckPermission("gen:electricityPrice:add")
	@ResponseBody
	public AjaxResult add(ElectricityPrice electricityPrice){
		int b=electricityPriceService.insertSelective(electricityPrice);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	/**
	 * 电价删除
	 * @param ids
	 * @return
	 */
	//@Log(title = "电价删除", action = "111")
	@ApiOperation(value = "删除", notes = "删除")
	@DeleteMapping("/remove")
	@SaCheckPermission("gen:electricityPrice:remove")
	@ResponseBody
	public AjaxResult remove(String ids){
		int b=electricityPriceService.deleteByPrimaryKey(ids);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	
	/**
	 * 修改跳转
	 * @param id
	 * @param mmap
	 * @return
	 */
	@ApiOperation(value = "修改跳转", notes = "修改跳转")
	@GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap map)
    {
        map.put("ElectricityPrice", electricityPriceService.selectByPrimaryKey(id));

        return prefix + "/edit";
    }
	
	/**
     * 修改保存
     */
    //@Log(title = "电价修改", action = "111")
	@ApiOperation(value = "修改保存", notes = "修改保存")
    @SaCheckPermission("gen:electricityPrice:edit")
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(ElectricityPrice electricityPrice)
    {
        return toAjax(electricityPriceService.updateByPrimaryKeySelective(electricityPrice));
    }
    
    
    /**
	 * 修改状态
	 * @param record
	 * @return
	 */
    @PutMapping("/updateVisible")
	@ResponseBody
    public AjaxResult updateVisible(@RequestBody ElectricityPrice electricityPrice){
		int i=electricityPriceService.updateVisible(electricityPrice);
		return toAjax(i);
	}

    
    

	
}
