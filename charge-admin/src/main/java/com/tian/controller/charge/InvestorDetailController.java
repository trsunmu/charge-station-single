package com.tian.controller.charge;

import com.tian.common.base.BaseController;
import com.tian.common.domain.AjaxResult;
import com.tian.common.domain.ResultTable;
import com.tian.model.custom.Tablepar;
import com.tian.entity.InvestorDetail;
import com.tian.service.charge.InvestorDetailService;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import cn.dev33.satoken.annotation.SaCheckPermission;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

/**
 * 投资人详情Controller
 * @ClassName: InvestorDetailController
 * @author tianwc
 * @date 2023-11-14 10:27:39
 */
@Api(value = "投资人详情")
@Controller
@RequestMapping("/InvestorDetailController")
public class InvestorDetailController extends BaseController{
	
	private String prefix = "charge/investorDetail";
	
	@Autowired
	private InvestorDetailService investorDetailService;
	
	
	/**
	 * 投资人详情页面展示
	 * @param model
	 * @return String
	 * @author tianwc
	 */
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/view")
	@SaCheckPermission("gen:investorDetail:view")
    public String view(ModelMap model)
    {
        return prefix + "/list";
    }
	
	/**
	 * list集合
	 * @param tablepar
	 * @param searchText
	 * @return
	 */
	//@Log(title = "投资人详情", action = "111")
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/list")
	@SaCheckPermission("gen:investorDetail:list")
	@ResponseBody
	public ResultTable list(Tablepar tablepar,InvestorDetail investorDetail){
		PageInfo<InvestorDetail> page=investorDetailService.list(tablepar,investorDetail) ; 
		return pageTable(page.getList(),page.getTotal());
	}
	
	/**
     * 新增跳转
     */
	@ApiOperation(value = "新增跳转", notes = "新增跳转")
    @GetMapping("/add")
    public String add(ModelMap modelMap)
    {
        return prefix + "/add";
    }
	
    /**
     * 新增保存
     * @param 
     * @return
     */
	//@Log(title = "投资人详情新增", action = "111")
	@ApiOperation(value = "新增", notes = "新增")
	@PostMapping("/add")
	@SaCheckPermission("gen:investorDetail:add")
	@ResponseBody
	public AjaxResult add(InvestorDetail investorDetail){
		int b=investorDetailService.insertSelective(investorDetail);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	/**
	 * 投资人详情删除
	 * @param ids
	 * @return
	 */
	//@Log(title = "投资人详情删除", action = "111")
	@ApiOperation(value = "删除", notes = "删除")
	@DeleteMapping("/remove")
	@SaCheckPermission("gen:investorDetail:remove")
	@ResponseBody
	public AjaxResult remove(String ids){
		int b=investorDetailService.deleteByPrimaryKey(ids);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	
	/**
	 * 修改跳转
	 * @param id
	 * @param mmap
	 * @return
	 */
	@ApiOperation(value = "修改跳转", notes = "修改跳转")
	@GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap map)
    {
        map.put("InvestorDetail", investorDetailService.selectByPrimaryKey(id));

        return prefix + "/edit";
    }
	
	/**
     * 修改保存
     */
    //@Log(title = "投资人详情修改", action = "111")
	@ApiOperation(value = "修改保存", notes = "修改保存")
    @SaCheckPermission("gen:investorDetail:edit")
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(InvestorDetail investorDetail)
    {
        return toAjax(investorDetailService.updateByPrimaryKeySelective(investorDetail));
    }
    
    
    /**
	 * 修改状态
	 * @param record
	 * @return
	 */
    @PutMapping("/updateVisible")
	@ResponseBody
    public AjaxResult updateVisible(@RequestBody InvestorDetail investorDetail){
		int i=investorDetailService.updateVisible(investorDetail);
		return toAjax(i);
	}

    
    

	
}
