package com.tian.controller.charge;

import com.tian.common.base.BaseController;
import com.tian.common.domain.AjaxResult;
import com.tian.common.domain.ResultTable;
import com.tian.model.custom.Tablepar;
import com.tian.entity.MessageTemplate;
import com.tian.service.charge.MessageTemplateService;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import cn.dev33.satoken.annotation.SaCheckPermission;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

/**
 * 短信模板管理Controller
 * @ClassName: MessageTemplateController
 * @author tianwc
 * @date 2023-11-14 09:42:16
 */
@Api(value = "短信模板管理")
@Controller
@RequestMapping("/MessageTemplateController")
public class MessageTemplateController extends BaseController{
	
	private String prefix = "charge/messageTemplate";
	
	@Autowired
	private MessageTemplateService messageTemplateService;
	
	
	/**
	 * 短信模板管理页面展示
	 * @param model
	 * @return String
	 * @author tianwc
	 */
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/view")
	@SaCheckPermission("gen:messageTemplate:view")
    public String view(ModelMap model)
    {
        return prefix + "/list";
    }
	
	/**
	 * list集合
	 * @param tablepar
	 * @param searchText
	 * @return
	 */
	//@Log(title = "短信模板管理", action = "111")
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/list")
	@SaCheckPermission("gen:messageTemplate:list")
	@ResponseBody
	public ResultTable list(Tablepar tablepar,MessageTemplate messageTemplate){
		PageInfo<MessageTemplate> page=messageTemplateService.list(tablepar,messageTemplate) ; 
		return pageTable(page.getList(),page.getTotal());
	}
	
	/**
     * 新增跳转
     */
	@ApiOperation(value = "新增跳转", notes = "新增跳转")
    @GetMapping("/add")
    public String add(ModelMap modelMap)
    {
        return prefix + "/add";
    }
	
    /**
     * 新增保存
     * @param 
     * @return
     */
	//@Log(title = "短信模板管理新增", action = "111")
	@ApiOperation(value = "新增", notes = "新增")
	@PostMapping("/add")
	@SaCheckPermission("gen:messageTemplate:add")
	@ResponseBody
	public AjaxResult add(MessageTemplate messageTemplate){
		int b=messageTemplateService.insertSelective(messageTemplate);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	/**
	 * 短信模板管理删除
	 * @param ids
	 * @return
	 */
	//@Log(title = "短信模板管理删除", action = "111")
	@ApiOperation(value = "删除", notes = "删除")
	@DeleteMapping("/remove")
	@SaCheckPermission("gen:messageTemplate:remove")
	@ResponseBody
	public AjaxResult remove(String ids){
		int b=messageTemplateService.deleteByPrimaryKey(ids);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	
	/**
	 * 修改跳转
	 * @param id
	 * @param mmap
	 * @return
	 */
	@ApiOperation(value = "修改跳转", notes = "修改跳转")
	@GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap map)
    {
        map.put("MessageTemplate", messageTemplateService.selectByPrimaryKey(id));

        return prefix + "/edit";
    }
	
	/**
     * 修改保存
     */
    //@Log(title = "短信模板管理修改", action = "111")
	@ApiOperation(value = "修改保存", notes = "修改保存")
    @SaCheckPermission("gen:messageTemplate:edit")
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(MessageTemplate messageTemplate)
    {
        return toAjax(messageTemplateService.updateByPrimaryKeySelective(messageTemplate));
    }
    
    
    /**
	 * 修改状态
	 * @param record
	 * @return
	 */
    @PutMapping("/updateVisible")
	@ResponseBody
    public AjaxResult updateVisible(@RequestBody MessageTemplate messageTemplate){
		int i=messageTemplateService.updateVisible(messageTemplate);
		return toAjax(i);
	}

    
    

	
}
