package com.tian.controller.charge;

import com.tian.common.base.BaseController;
import com.tian.common.domain.AjaxResult;
import com.tian.common.domain.ResultTable;
import com.tian.model.custom.Tablepar;
import com.tian.entity.UserActivityGoods;
import com.tian.service.charge.UserActivityGoodsService;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import cn.dev33.satoken.annotation.SaCheckPermission;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

/**
 * 用户商品Controller
 * @ClassName: UserActivityGoodsController
 * @author tianwc
 * @date 2023-11-14 10:25:07
 */
@Api(value = "用户商品")
@Controller
@RequestMapping("/UserActivityGoodsController")
public class UserActivityGoodsController extends BaseController{
	
	private String prefix = "charge/userActivityGoods";
	
	@Autowired
	private UserActivityGoodsService userActivityGoodsService;
	
	
	/**
	 * 用户商品页面展示
	 * @param model
	 * @return String
	 * @author tianwc
	 */
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/view")
	@SaCheckPermission("gen:userActivityGoods:view")
    public String view(ModelMap model)
    {
        return prefix + "/list";
    }
	
	/**
	 * list集合
	 * @param tablepar
	 * @param searchText
	 * @return
	 */
	//@Log(title = "用户商品", action = "111")
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/list")
	@SaCheckPermission("gen:userActivityGoods:list")
	@ResponseBody
	public ResultTable list(Tablepar tablepar,UserActivityGoods userActivityGoods){
		PageInfo<UserActivityGoods> page=userActivityGoodsService.list(tablepar,userActivityGoods) ; 
		return pageTable(page.getList(),page.getTotal());
	}
	
	/**
     * 新增跳转
     */
	@ApiOperation(value = "新增跳转", notes = "新增跳转")
    @GetMapping("/add")
    public String add(ModelMap modelMap)
    {
        return prefix + "/add";
    }
	
    /**
     * 新增保存
     * @param 
     * @return
     */
	//@Log(title = "用户商品新增", action = "111")
	@ApiOperation(value = "新增", notes = "新增")
	@PostMapping("/add")
	@SaCheckPermission("gen:userActivityGoods:add")
	@ResponseBody
	public AjaxResult add(UserActivityGoods userActivityGoods){
		int b=userActivityGoodsService.insertSelective(userActivityGoods);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	/**
	 * 用户商品删除
	 * @param ids
	 * @return
	 */
	//@Log(title = "用户商品删除", action = "111")
	@ApiOperation(value = "删除", notes = "删除")
	@DeleteMapping("/remove")
	@SaCheckPermission("gen:userActivityGoods:remove")
	@ResponseBody
	public AjaxResult remove(String ids){
		int b=userActivityGoodsService.deleteByPrimaryKey(ids);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	
	/**
	 * 修改跳转
	 * @param id
	 * @param mmap
	 * @return
	 */
	@ApiOperation(value = "修改跳转", notes = "修改跳转")
	@GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap map)
    {
        map.put("UserActivityGoods", userActivityGoodsService.selectByPrimaryKey(id));

        return prefix + "/edit";
    }
	
	/**
     * 修改保存
     */
    //@Log(title = "用户商品修改", action = "111")
	@ApiOperation(value = "修改保存", notes = "修改保存")
    @SaCheckPermission("gen:userActivityGoods:edit")
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(UserActivityGoods userActivityGoods)
    {
        return toAjax(userActivityGoodsService.updateByPrimaryKeySelective(userActivityGoods));
    }
    
    
    /**
	 * 修改状态
	 * @param record
	 * @return
	 */
    @PutMapping("/updateVisible")
	@ResponseBody
    public AjaxResult updateVisible(@RequestBody UserActivityGoods userActivityGoods){
		int i=userActivityGoodsService.updateVisible(userActivityGoods);
		return toAjax(i);
	}

    
    

	
}
