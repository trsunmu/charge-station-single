package com.tian.controller.charge;

import com.tian.common.base.BaseController;
import com.tian.common.domain.AjaxResult;
import com.tian.common.domain.ResultTable;
import com.tian.model.custom.Tablepar;
import com.tian.entity.PayOrder;
import com.tian.service.charge.PayOrderService;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import cn.dev33.satoken.annotation.SaCheckPermission;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

/**
 * 支付单Controller
 * @ClassName: PayOrderController
 * @author tianwc
 * @date 2023-11-13 20:02:55
 */
@Api(value = "支付单")
@Controller
@RequestMapping("/PayOrderController")
public class PayOrderController extends BaseController{
	
	private String prefix = "charge/payorder";
	
	@Autowired
	private PayOrderService payOrderService;
	
	
	/**
	 * 支付单页面展示
	 */
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/view")
	@SaCheckPermission("gen:payOrder:view")
    public String view(ModelMap model)
    {
        return prefix + "/list";
    }
	
	/**
	 * list集合
	 */
	//@Log(title = "支付单", action = "111")
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/list")
	@SaCheckPermission("gen:payOrder:list")
	@ResponseBody
	public ResultTable list(Tablepar tablepar,PayOrder payOrder){
		PageInfo<PayOrder> page=payOrderService.list(tablepar,payOrder) ; 
		return pageTable(page.getList(),page.getTotal());
	}
	
	/**
     * 新增跳转
     */
	@ApiOperation(value = "新增跳转", notes = "新增跳转")
    @GetMapping("/add")
    public String add(ModelMap modelMap)
    {
        return prefix + "/add";
    }
	
    /**
     * 新增保存
     */
	//@Log(title = "支付单新增", action = "111")
	@ApiOperation(value = "新增", notes = "新增")
	@PostMapping("/add")
	@SaCheckPermission("gen:payOrder:add")
	@ResponseBody
	public AjaxResult add(PayOrder payOrder){
		int b=payOrderService.insertSelective(payOrder);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	

	
	
	/**
	 * 修改跳转
	 */
	@ApiOperation(value = "修改跳转", notes = "修改跳转")
	@GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap map)
    {
        map.put("PayOrder", payOrderService.selectByPrimaryKey(id));

        return prefix + "/edit";
    }
	
	/**
     * 修改保存
     */
    //@Log(title = "支付单修改", action = "111")
	@ApiOperation(value = "修改保存", notes = "修改保存")
    @SaCheckPermission("gen:payOrder:edit")
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(PayOrder payOrder)
    {
        return toAjax(payOrderService.updateByPrimaryKeySelective(payOrder));
    }
    
    
    /**
	 * 修改状态
	 */
    @PutMapping("/updateVisible")
	@ResponseBody
    public AjaxResult updateVisible(@RequestBody PayOrder payOrder){
		int i=payOrderService.updateVisible(payOrder);
		return toAjax(i);
	}
}
