package com.tian.controller.charge;

import com.tian.common.base.BaseController;
import com.tian.common.domain.AjaxResult;
import com.tian.common.domain.ResultTable;
import com.tian.model.custom.Tablepar;
import com.tian.entity.UserActivityCoupon;
import com.tian.service.charge.UserActivityCouponService;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import cn.dev33.satoken.annotation.SaCheckPermission;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

/**
 * 用户优惠券Controller
 * @ClassName: UserActivityCouponController
 * @author tianwc
 * @date 2023-11-14 10:23:45
 */
@Api(value = "用户优惠券")
@Controller
@RequestMapping("/UserActivityCouponController")
public class UserActivityCouponController extends BaseController{
	
	private String prefix = "charge/userActivityCoupon";
	
	@Autowired
	private UserActivityCouponService userActivityCouponService;
	
	
	/**
	 * 用户优惠券页面展示
	 * @param model
	 * @return String
	 * @author tianwc
	 */
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/view")
	@SaCheckPermission("gen:userActivityCoupon:view")
    public String view(ModelMap model)
    {
        return prefix + "/list";
    }
	
	/**
	 * list集合
	 * @param tablepar
	 * @param searchText
	 * @return
	 */
	//@Log(title = "用户优惠券", action = "111")
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/list")
	@SaCheckPermission("gen:userActivityCoupon:list")
	@ResponseBody
	public ResultTable list(Tablepar tablepar,UserActivityCoupon userActivityCoupon){
		PageInfo<UserActivityCoupon> page=userActivityCouponService.list(tablepar,userActivityCoupon) ; 
		return pageTable(page.getList(),page.getTotal());
	}
	
	/**
     * 新增跳转
     */
	@ApiOperation(value = "新增跳转", notes = "新增跳转")
    @GetMapping("/add")
    public String add(ModelMap modelMap)
    {
        return prefix + "/add";
    }
	
    /**
     * 新增保存
     * @param 
     * @return
     */
	//@Log(title = "用户优惠券新增", action = "111")
	@ApiOperation(value = "新增", notes = "新增")
	@PostMapping("/add")
	@SaCheckPermission("gen:userActivityCoupon:add")
	@ResponseBody
	public AjaxResult add(UserActivityCoupon userActivityCoupon){
		int b=userActivityCouponService.insertSelective(userActivityCoupon);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	/**
	 * 用户优惠券删除
	 * @param ids
	 * @return
	 */
	//@Log(title = "用户优惠券删除", action = "111")
	@ApiOperation(value = "删除", notes = "删除")
	@DeleteMapping("/remove")
	@SaCheckPermission("gen:userActivityCoupon:remove")
	@ResponseBody
	public AjaxResult remove(String ids){
		int b=userActivityCouponService.deleteByPrimaryKey(ids);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	
	/**
	 * 修改跳转
	 * @param id
	 * @param mmap
	 * @return
	 */
	@ApiOperation(value = "修改跳转", notes = "修改跳转")
	@GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap map)
    {
        map.put("UserActivityCoupon", userActivityCouponService.selectByPrimaryKey(id));

        return prefix + "/edit";
    }
	
	/**
     * 修改保存
     */
    //@Log(title = "用户优惠券修改", action = "111")
	@ApiOperation(value = "修改保存", notes = "修改保存")
    @SaCheckPermission("gen:userActivityCoupon:edit")
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(UserActivityCoupon userActivityCoupon)
    {
        return toAjax(userActivityCouponService.updateByPrimaryKeySelective(userActivityCoupon));
    }
    
    
    /**
	 * 修改状态
	 * @param record
	 * @return
	 */
    @PutMapping("/updateVisible")
	@ResponseBody
    public AjaxResult updateVisible(@RequestBody UserActivityCoupon userActivityCoupon){
		int i=userActivityCouponService.updateVisible(userActivityCoupon);
		return toAjax(i);
	}

    
    

	
}
