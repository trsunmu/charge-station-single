package com.tian.service.charge;

import java.util.List;
import java.util.Arrays;

import org.springframework.stereotype.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import cn.hutool.core.util.StrUtil;
import com.tian.common.base.BaseService;
import com.tian.common.support.ConvertUtil;
import com.tian.mapper.charge.ChargeGunMapper;
import com.tian.entity.ChargeGun;
import com.tian.entity.ChargeGunExample;
import com.tian.model.custom.Tablepar;
import com.tian.util.SnowflakeIdWorker;
import com.tian.util.StringUtils;

import javax.annotation.Resource;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年11月10日 17:18
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 */
@Service
public class ChargeGunService implements BaseService<ChargeGun, ChargeGunExample> {
    @Resource
    private ChargeGunMapper chargeGunMapper;


    /**
     * 分页查询
     */
    public PageInfo<ChargeGun> list(Tablepar tablepar, ChargeGun chargeGun) {
        ChargeGunExample testExample = new ChargeGunExample();
        //搜索
        if (StrUtil.isNotEmpty(tablepar.getSearchText())) {//小窗体
            testExample.createCriteria().andLikeQuery2(tablepar.getSearchText());
        } else {//大搜索
            testExample.createCriteria().andLikeQuery(chargeGun);
        }
        //表格排序
        //if(StrUtil.isNotEmpty(tablepar.getOrderByColumn())) {
        //	testExample.setOrderByClause(StringUtils.toUnderScoreCase(tablepar.getOrderByColumn()) +" "+tablepar.getIsAsc());
        //}else{
        //	testExample.setOrderByClause("id ASC");
        //}
        PageHelper.startPage(tablepar.getPage(), tablepar.getLimit());
        List<ChargeGun> list = chargeGunMapper.selectByExample(testExample);
        PageInfo<ChargeGun> pageInfo = new PageInfo<ChargeGun>(list);
        return pageInfo;
    }

    @Override
    public int deleteByPrimaryKey(String ids) {

        Integer[] integers = ConvertUtil.toIntArray(",", ids);
        List<Integer> stringB = Arrays.asList(integers);
        ChargeGunExample example = new ChargeGunExample();
        example.createCriteria().andIdIn(stringB);
        return chargeGunMapper.deleteByExample(example);


    }


    @Override
    public ChargeGun selectByPrimaryKey(String id) {

        Integer id1 = Integer.valueOf(id);
        return chargeGunMapper.selectByPrimaryKey(id1);

    }


    @Override
    public int updateByPrimaryKeySelective(ChargeGun record) {
        return chargeGunMapper.updateByPrimaryKeySelective(record);
    }


    /**
     * 添加
     */
    @Override
    public int insertSelective(ChargeGun record) {

        record.setId(null);


        return chargeGunMapper.insertSelective(record);
    }


    @Override
    public int updateByExampleSelective(ChargeGun record, ChargeGunExample example) {

        return chargeGunMapper.updateByExampleSelective(record, example);
    }


    @Override
    public int updateByExample(ChargeGun record, ChargeGunExample example) {

        return chargeGunMapper.updateByExample(record, example);
    }

    @Override
    public List<ChargeGun> selectByExample(ChargeGunExample example) {

        return chargeGunMapper.selectByExample(example);
    }


    @Override
    public long countByExample(ChargeGunExample example) {

        return chargeGunMapper.countByExample(example);
    }


    @Override
    public int deleteByExample(ChargeGunExample example) {

        return chargeGunMapper.deleteByExample(example);
    }

    /**
     * 修改权限状态展示或者不展示
     *
     * @param chargeGun
     * @return
     */
    public int updateVisible(ChargeGun chargeGun) {
        return chargeGunMapper.updateByPrimaryKeySelective(chargeGun);
    }


}
