package com.tian.service.charge;

import cn.hutool.core.util.StrUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.tian.common.base.BaseService;
import com.tian.common.support.ConvertUtil;
import com.tian.mapper.charge.ChargerRecordMapper;
import com.tian.entity.ChargerRecord;
import com.tian.entity.ChargerRecordExample;
import com.tian.model.custom.Tablepar;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年11月10日 10:31
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 */
@Service
public class ChargerRecordService implements BaseService<ChargerRecord, ChargerRecordExample> {
    @Resource
    private ChargerRecordMapper chargerRecordMapper;


    /**
     * 分页查询
     */
    public PageInfo<ChargerRecord> list(Tablepar tablepar, ChargerRecord chargerRecord) {
        ChargerRecordExample testExample = new ChargerRecordExample();
        //搜索
        if (StrUtil.isNotEmpty(tablepar.getSearchText())) {//小窗体
            testExample.createCriteria().andLikeQuery2(tablepar.getSearchText());
        } else {//大搜索
            testExample.createCriteria().andLikeQuery(chargerRecord);
        }
        //表格排序
        //if(StrUtil.isNotEmpty(tablepar.getOrderByColumn())) {
        //	testExample.setOrderByClause(StringUtils.toUnderScoreCase(tablepar.getOrderByColumn()) +" "+tablepar.getIsAsc());
        //}else{
        //	testExample.setOrderByClause("id ASC");
        //}
        PageHelper.startPage(tablepar.getPage(), tablepar.getLimit());
        List<ChargerRecord> list = chargerRecordMapper.selectByExample(testExample);
        PageInfo<ChargerRecord> pageInfo = new PageInfo<ChargerRecord>(list);
        return pageInfo;
    }

    @Override
    public int deleteByPrimaryKey(String ids) {

        Integer[] integers = ConvertUtil.toIntArray(",", ids);
        List<Integer> stringB = Arrays.asList(integers);
        ChargerRecordExample example = new ChargerRecordExample();
        example.createCriteria().andIdIn(stringB);
        return chargerRecordMapper.deleteByExample(example);


    }


    @Override
    public ChargerRecord selectByPrimaryKey(String id) {

        Integer id1 = Integer.valueOf(id);
        return chargerRecordMapper.selectByPrimaryKey(id1);

    }


    @Override
    public int updateByPrimaryKeySelective(ChargerRecord record) {
        return chargerRecordMapper.updateByPrimaryKeySelective(record);
    }


    /**
     * 添加
     */
    @Override
    public int insertSelective(ChargerRecord record) {

        record.setId(null);


        return chargerRecordMapper.insertSelective(record);
    }


    @Override
    public int updateByExampleSelective(ChargerRecord record, ChargerRecordExample example) {

        return chargerRecordMapper.updateByExampleSelective(record, example);
    }


    @Override
    public int updateByExample(ChargerRecord record, ChargerRecordExample example) {

        return chargerRecordMapper.updateByExample(record, example);
    }

    @Override
    public List<ChargerRecord> selectByExample(ChargerRecordExample example) {

        return chargerRecordMapper.selectByExample(example);
    }


    @Override
    public long countByExample(ChargerRecordExample example) {

        return chargerRecordMapper.countByExample(example);
    }


    @Override
    public int deleteByExample(ChargerRecordExample example) {

        return chargerRecordMapper.deleteByExample(example);
    }

    /**
     * 修改权限状态展示或者不展示
     *
     * @param chargerRecord
     * @return
     */
    public int updateVisible(ChargerRecord chargerRecord) {
        return chargerRecordMapper.updateByPrimaryKeySelective(chargerRecord);
    }


}
