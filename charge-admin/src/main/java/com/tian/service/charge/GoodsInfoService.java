package com.tian.service.charge;

import java.util.Date;
import java.util.List;
import java.util.Arrays;

import org.springframework.stereotype.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import cn.hutool.core.util.StrUtil;
import com.tian.common.base.BaseService;
import com.tian.common.support.ConvertUtil;
import com.tian.mapper.charge.GoodsInfoMapper;
import com.tian.entity.GoodsInfo;
import com.tian.model.custom.GoodsInfoExample;
import com.tian.model.custom.Tablepar;

import javax.annotation.Resource;

/**
 * 商品信息表 GoodsInfoService
 *
 * @author fuce_自动生成
 * @Title: GoodsInfoService.java 
 * @Package com.fc.v2.service 
 * @email ${email}
 * @date 2023-11-09 15:28:47  
 **/
@Service
public class GoodsInfoService implements BaseService<GoodsInfo, GoodsInfoExample> {
    @Resource
    private GoodsInfoMapper goodsInfoMapper;


    /**
     * 分页查询
     */
    public PageInfo<GoodsInfo> list(Tablepar tablepar, GoodsInfo goodsInfo) {
        GoodsInfoExample testExample = new GoodsInfoExample();
        //搜索
        if (StrUtil.isNotEmpty(tablepar.getSearchText())) {//小窗体
            testExample.createCriteria().andLikeQuery2(tablepar.getSearchText());
        } else {//大搜索
            testExample.createCriteria().andLikeQuery(goodsInfo);
        }
        //表格排序
        //if(StrUtil.isNotEmpty(tablepar.getOrderByColumn())) {
        //	testExample.setOrderByClause(StringUtils.toUnderScoreCase(tablepar.getOrderByColumn()) +" "+tablepar.getIsAsc());
        //}else{
        //	testExample.setOrderByClause("id ASC");
        //}
        PageHelper.startPage(tablepar.getPage(), tablepar.getLimit());
        List<GoodsInfo> list = goodsInfoMapper.selectByExample(testExample);
        PageInfo<GoodsInfo> pageInfo = new PageInfo<GoodsInfo>(list);
        return pageInfo;
    }

    @Override
    public int deleteByPrimaryKey(String ids) {

        Integer[] integers = ConvertUtil.toIntArray(",", ids);
        List<Integer> stringB = Arrays.asList(integers);
        GoodsInfoExample example = new GoodsInfoExample();
        example.createCriteria().andIdIn(stringB);
        return goodsInfoMapper.deleteByExample(example);


    }


    @Override
    public GoodsInfo selectByPrimaryKey(String id) {

        Integer id1 = Integer.valueOf(id);
        return goodsInfoMapper.selectByPrimaryKey(id1);

    }


    @Override
    public int updateByPrimaryKeySelective(GoodsInfo record) {
        return goodsInfoMapper.updateByPrimaryKeySelective(record);
    }


    /**
     * 添加
     */
    @Override
    public int insertSelective(GoodsInfo record) {

        record.setId(null);
        record.setStatus(0);
        record.setCreateTime(new Date());

        return goodsInfoMapper.insertSelective(record);
    }


    @Override
    public int updateByExampleSelective(GoodsInfo record, GoodsInfoExample example) {

        return goodsInfoMapper.updateByExampleSelective(record, example);
    }


    @Override
    public int updateByExample(GoodsInfo record, GoodsInfoExample example) {

        return goodsInfoMapper.updateByExample(record, example);
    }

    @Override
    public List<GoodsInfo> selectByExample(GoodsInfoExample example) {

        return goodsInfoMapper.selectByExample(example);
    }


    @Override
    public long countByExample(GoodsInfoExample example) {

        return goodsInfoMapper.countByExample(example);
    }


    @Override
    public int deleteByExample(GoodsInfoExample example) {

        return goodsInfoMapper.deleteByExample(example);
    }

    /**
     * 修改权限状态展示或者不展示
     *
     * @param goodsInfo
     * @return
     */
    public int updateVisible(GoodsInfo goodsInfo) {
        return goodsInfoMapper.updateByPrimaryKeySelective(goodsInfo);
    }


}
