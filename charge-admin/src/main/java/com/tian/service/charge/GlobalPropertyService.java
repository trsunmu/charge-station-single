package com.tian.service.charge;

import java.util.List;
import java.util.Arrays;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import cn.hutool.core.util.StrUtil;
import com.tian.common.base.BaseService;
import com.tian.common.support.ConvertUtil;
import com.tian.mapper.charge.GlobalPropertyMapper;
import com.tian.entity.GlobalProperty;
import com.tian.entity.GlobalPropertyExample;
import com.tian.model.custom.Tablepar;

/**
 * 全局配置 GlobalPropertyService
 * @Title: GlobalPropertyService.java 
 * @Package com.tian.service 
 * @author tianwc_自动生成
 * @email ${email}
 * @date 2023-11-14 10:29:12  
 **/
@Service
public class GlobalPropertyService implements BaseService<GlobalProperty, GlobalPropertyExample>{
	@Autowired
	private GlobalPropertyMapper globalPropertyMapper;
	
      	   	      	      	      	      	      	      	
	/**
	 * 分页查询
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	 public PageInfo<GlobalProperty> list(Tablepar tablepar,GlobalProperty globalProperty){
	        GlobalPropertyExample testExample=new GlobalPropertyExample();
			//搜索
			if(StrUtil.isNotEmpty(tablepar.getSearchText())) {//小窗体
	        	testExample.createCriteria().andLikeQuery2(tablepar.getSearchText());
	        }else {//大搜索
	        	testExample.createCriteria().andLikeQuery(globalProperty);
	        }
			//表格排序
			//if(StrUtil.isNotEmpty(tablepar.getOrderByColumn())) {
	        //	testExample.setOrderByClause(StringUtils.toUnderScoreCase(tablepar.getOrderByColumn()) +" "+tablepar.getIsAsc());
	        //}else{
	        //	testExample.setOrderByClause("id ASC");
	        //}
	        PageHelper.startPage(tablepar.getPage(), tablepar.getLimit());
	        List<GlobalProperty> list= globalPropertyMapper.selectByExample(testExample);
	        PageInfo<GlobalProperty> pageInfo = new PageInfo<GlobalProperty>(list);
	        return  pageInfo;
	 }

	@Override
	public int deleteByPrimaryKey(String ids) {
					
			Integer[] integers = ConvertUtil.toIntArray(",", ids);
			List<Integer> stringB = Arrays.asList(integers);
			GlobalPropertyExample example=new GlobalPropertyExample();
			example.createCriteria().andIdIn(stringB);
			return globalPropertyMapper.deleteByExample(example);
			
				
	}
	
	
	@Override
	public GlobalProperty selectByPrimaryKey(String id) {
				
			Integer id1 = Integer.valueOf(id);
			return globalPropertyMapper.selectByPrimaryKey(id1);
				
	}

	
	@Override
	public int updateByPrimaryKeySelective(GlobalProperty record) {
		return globalPropertyMapper.updateByPrimaryKeySelective(record);
	}
	
	
	/**
	 * 添加
	 */
	@Override
	public int insertSelective(GlobalProperty record) {
				
		record.setId(null);
		
				
		return globalPropertyMapper.insertSelective(record);
	}
	
	
	@Override
	public int updateByExampleSelective(GlobalProperty record, GlobalPropertyExample example) {
		
		return globalPropertyMapper.updateByExampleSelective(record, example);
	}

	
	@Override
	public int updateByExample(GlobalProperty record, GlobalPropertyExample example) {
		
		return globalPropertyMapper.updateByExample(record, example);
	}

	@Override
	public List<GlobalProperty> selectByExample(GlobalPropertyExample example) {
		
		return globalPropertyMapper.selectByExample(example);
	}

	
	@Override
	public long countByExample(GlobalPropertyExample example) {
		
		return globalPropertyMapper.countByExample(example);
	}

	
	@Override
	public int deleteByExample(GlobalPropertyExample example) {
		
		return globalPropertyMapper.deleteByExample(example);
	}
	
	/**
	 * 修改权限状态展示或者不展示
	 * @param globalProperty
	 * @return
	 */
	public int updateVisible(GlobalProperty globalProperty) {
		return globalPropertyMapper.updateByPrimaryKeySelective(globalProperty);
	}


}
