package com.tian.service.charge;

import java.util.List;
import java.util.Arrays;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import cn.hutool.core.util.StrUtil;
import com.tian.common.base.BaseService;
import com.tian.common.support.ConvertUtil;
import com.tian.mapper.charge.ExceptionalRetroactionMapper;
import com.tian.entity.ExceptionalRetroaction;
import com.tian.entity.ExceptionalRetroactionExample;
import com.tian.model.custom.Tablepar;

/**
 * 异常反馈 ExceptionalRetroactionService
 * @Title: ExceptionalRetroactionService.java 
 * @Package com.tian.service 
 * @author fuce_自动生成
 * @email ${email}
 * @date 2023-11-13 20:35:29  
 **/
@Service
public class ExceptionalRetroactionService implements BaseService<ExceptionalRetroaction, ExceptionalRetroactionExample>{
	@Autowired
	private ExceptionalRetroactionMapper exceptionalRetroactionMapper;
	
      	   	      	      	      	      	      	      	      	      	      	      	      	      	
	/**
	 * 分页查询
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	 public PageInfo<ExceptionalRetroaction> list(Tablepar tablepar,ExceptionalRetroaction exceptionalRetroaction){
	        ExceptionalRetroactionExample testExample=new ExceptionalRetroactionExample();
			//搜索
			if(StrUtil.isNotEmpty(tablepar.getSearchText())) {//小窗体
	        	testExample.createCriteria().andLikeQuery2(tablepar.getSearchText());
	        }else {//大搜索
	        	testExample.createCriteria().andLikeQuery(exceptionalRetroaction);
	        }
			//表格排序
			//if(StrUtil.isNotEmpty(tablepar.getOrderByColumn())) {
	        //	testExample.setOrderByClause(StringUtils.toUnderScoreCase(tablepar.getOrderByColumn()) +" "+tablepar.getIsAsc());
	        //}else{
	        //	testExample.setOrderByClause("id ASC");
	        //}
	        PageHelper.startPage(tablepar.getPage(), tablepar.getLimit());
	        List<ExceptionalRetroaction> list= exceptionalRetroactionMapper.selectByExample(testExample);
	        PageInfo<ExceptionalRetroaction> pageInfo = new PageInfo<ExceptionalRetroaction>(list);
	        return  pageInfo;
	 }

	@Override
	public int deleteByPrimaryKey(String ids) {
				
			Long[] integers = ConvertUtil.toLongArray(",", ids);
			List<Long> stringB = Arrays.asList(integers);
			ExceptionalRetroactionExample example=new ExceptionalRetroactionExample();
			example.createCriteria().andIdIn(stringB);
			return exceptionalRetroactionMapper.deleteByExample(example);
		
				
	}
	
	
	@Override
	public ExceptionalRetroaction selectByPrimaryKey(String id) {
				
			Long id1 = Long.valueOf(id);
			return exceptionalRetroactionMapper.selectByPrimaryKey(id1);
			
				
	}

	
	@Override
	public int updateByPrimaryKeySelective(ExceptionalRetroaction record) {
		return exceptionalRetroactionMapper.updateByPrimaryKeySelective(record);
	}
	
	
	/**
	 * 添加
	 */
	@Override
	public int insertSelective(ExceptionalRetroaction record) {
				
		record.setId(null);
		
				
		return exceptionalRetroactionMapper.insertSelective(record);
	}
	
	
	@Override
	public int updateByExampleSelective(ExceptionalRetroaction record, ExceptionalRetroactionExample example) {
		
		return exceptionalRetroactionMapper.updateByExampleSelective(record, example);
	}

	
	@Override
	public int updateByExample(ExceptionalRetroaction record, ExceptionalRetroactionExample example) {
		
		return exceptionalRetroactionMapper.updateByExample(record, example);
	}

	@Override
	public List<ExceptionalRetroaction> selectByExample(ExceptionalRetroactionExample example) {
		
		return exceptionalRetroactionMapper.selectByExample(example);
	}

	
	@Override
	public long countByExample(ExceptionalRetroactionExample example) {
		
		return exceptionalRetroactionMapper.countByExample(example);
	}

	
	@Override
	public int deleteByExample(ExceptionalRetroactionExample example) {
		
		return exceptionalRetroactionMapper.deleteByExample(example);
	}
	
	/**
	 * 修改权限状态展示或者不展示
	 * @param exceptionalRetroaction
	 * @return
	 */
	public int updateVisible(ExceptionalRetroaction exceptionalRetroaction) {
		return exceptionalRetroactionMapper.updateByPrimaryKeySelective(exceptionalRetroaction);
	}


}
