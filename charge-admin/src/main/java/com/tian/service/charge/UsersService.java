package com.tian.service.charge;

import cn.hutool.core.util.StrUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.tian.common.base.BaseService;
import com.tian.common.support.ConvertUtil;
import com.tian.mapper.charge.UsersMapper;
import com.tian.entity.Users;
import com.tian.entity.UsersExample;
import com.tian.model.custom.Tablepar;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年11月09日 22:34
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 */


@Service
public class UsersService implements BaseService<Users, UsersExample> {
    @Resource
    private UsersMapper usersMapper;


    /**
     * 分页查询
     */
    public PageInfo<Users> list(Tablepar tablepar, Users users) {
        UsersExample testExample = new UsersExample();
        //搜索
        if (StrUtil.isNotEmpty(tablepar.getSearchText())) {//小窗体
            testExample.createCriteria().andLikeQuery2(tablepar.getSearchText());
        } else {//大搜索
            testExample.createCriteria().andLikeQuery(users);
        }
        //表格排序
        //if(StrUtil.isNotEmpty(tablepar.getOrderByColumn())) {
        //	testExample.setOrderByClause(StringUtils.toUnderScoreCase(tablepar.getOrderByColumn()) +" "+tablepar.getIsAsc());
        //}else{
        //	testExample.setOrderByClause("id ASC");
        //}
        PageHelper.startPage(tablepar.getPage(), tablepar.getLimit());
        List<Users> list = usersMapper.selectByExample(testExample);
        PageInfo<Users> pageInfo = new PageInfo<Users>(list);
        return pageInfo;
    }

    @Override
    public int deleteByPrimaryKey(String ids) {

        Integer[] integers = ConvertUtil.toIntArray(",", ids);
        List<Integer> stringB = Arrays.asList(integers);
        UsersExample example = new UsersExample();
        example.createCriteria().andIdIn(stringB);
        return usersMapper.deleteByExample(example);


    }


    @Override
    public Users selectByPrimaryKey(String id) {

        Integer id1 = Integer.valueOf(id);
        return usersMapper.selectByPrimaryKey(id1);

    }


    @Override
    public int updateByPrimaryKeySelective(Users record) {
        return usersMapper.updateByPrimaryKeySelective(record);
    }


    /**
     * 添加
     */
    @Override
    public int insertSelective(Users record) {

        record.setId(null);


        return usersMapper.insertSelective(record);
    }


    @Override
    public int updateByExampleSelective(Users record, UsersExample example) {

        return usersMapper.updateByExampleSelective(record, example);
    }


    @Override
    public int updateByExample(Users record, UsersExample example) {

        return usersMapper.updateByExample(record, example);
    }

    @Override
    public List<Users> selectByExample(UsersExample example) {

        return usersMapper.selectByExample(example);
    }


    @Override
    public long countByExample(UsersExample example) {

        return usersMapper.countByExample(example);
    }


    @Override
    public int deleteByExample(UsersExample example) {

        return usersMapper.deleteByExample(example);
    }

    /**
     * 修改权限状态展示或者不展示
     *
     * @param users
     * @return
     */
    public int updateVisible(Users users) {
        return usersMapper.updateByPrimaryKeySelective(users);
    }


}