package com.tian.service.charge;

import java.util.List;
import java.util.Arrays;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import cn.hutool.core.util.StrUtil;
import com.tian.common.base.BaseService;
import com.tian.common.support.ConvertUtil;
import com.tian.mapper.charge.ElectricityPriceMapper;
import com.tian.entity.ElectricityPrice;
import com.tian.entity.ElectricityPriceExample;
import com.tian.model.custom.Tablepar;

/**
 * 电价 ElectricityPriceService
 * @Title: ElectricityPriceService.java 
 * @Package com.tian.service 
 * @author tianwc_自动生成
 * @email ${email}
 * @date 2023-11-14 10:26:58  
 **/
@Service
public class ElectricityPriceService implements BaseService<ElectricityPrice, ElectricityPriceExample>{
	@Autowired
	private ElectricityPriceMapper electricityPriceMapper;
	
      	   	      	      	      	      	      	      	
	/**
	 * 分页查询
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	 public PageInfo<ElectricityPrice> list(Tablepar tablepar,ElectricityPrice electricityPrice){
	        ElectricityPriceExample testExample=new ElectricityPriceExample();
			//搜索
			if(StrUtil.isNotEmpty(tablepar.getSearchText())) {//小窗体
	        	testExample.createCriteria().andLikeQuery2(tablepar.getSearchText());
	        }else {//大搜索
	        	testExample.createCriteria().andLikeQuery(electricityPrice);
	        }
			//表格排序
			//if(StrUtil.isNotEmpty(tablepar.getOrderByColumn())) {
	        //	testExample.setOrderByClause(StringUtils.toUnderScoreCase(tablepar.getOrderByColumn()) +" "+tablepar.getIsAsc());
	        //}else{
	        //	testExample.setOrderByClause("id ASC");
	        //}
	        PageHelper.startPage(tablepar.getPage(), tablepar.getLimit());
	        List<ElectricityPrice> list= electricityPriceMapper.selectByExample(testExample);
	        PageInfo<ElectricityPrice> pageInfo = new PageInfo<ElectricityPrice>(list);
	        return  pageInfo;
	 }

	@Override
	public int deleteByPrimaryKey(String ids) {
					
			Integer[] integers = ConvertUtil.toIntArray(",", ids);
			List<Integer> stringB = Arrays.asList(integers);
			ElectricityPriceExample example=new ElectricityPriceExample();
			example.createCriteria().andIdIn(stringB);
			return electricityPriceMapper.deleteByExample(example);
			
				
	}
	
	
	@Override
	public ElectricityPrice selectByPrimaryKey(String id) {
				
			Integer id1 = Integer.valueOf(id);
			return electricityPriceMapper.selectByPrimaryKey(id1);
				
	}

	
	@Override
	public int updateByPrimaryKeySelective(ElectricityPrice record) {
		return electricityPriceMapper.updateByPrimaryKeySelective(record);
	}
	
	
	/**
	 * 添加
	 */
	@Override
	public int insertSelective(ElectricityPrice record) {
				
		record.setId(null);
		
				
		return electricityPriceMapper.insertSelective(record);
	}
	
	
	@Override
	public int updateByExampleSelective(ElectricityPrice record, ElectricityPriceExample example) {
		
		return electricityPriceMapper.updateByExampleSelective(record, example);
	}

	
	@Override
	public int updateByExample(ElectricityPrice record, ElectricityPriceExample example) {
		
		return electricityPriceMapper.updateByExample(record, example);
	}

	@Override
	public List<ElectricityPrice> selectByExample(ElectricityPriceExample example) {
		
		return electricityPriceMapper.selectByExample(example);
	}

	
	@Override
	public long countByExample(ElectricityPriceExample example) {
		
		return electricityPriceMapper.countByExample(example);
	}

	
	@Override
	public int deleteByExample(ElectricityPriceExample example) {
		
		return electricityPriceMapper.deleteByExample(example);
	}
	
	/**
	 * 修改权限状态展示或者不展示
	 * @param electricityPrice
	 * @return
	 */
	public int updateVisible(ElectricityPrice electricityPrice) {
		return electricityPriceMapper.updateByPrimaryKeySelective(electricityPrice);
	}


}
