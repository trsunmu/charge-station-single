package com.tian.service.charge;

import java.util.List;
import java.util.Arrays;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import cn.hutool.core.util.StrUtil;
import com.tian.common.base.BaseService;
import com.tian.common.support.ConvertUtil;
import com.tian.mapper.charge.UserActivityCouponMapper;
import com.tian.entity.UserActivityCoupon;
import com.tian.entity.UserActivityCouponExample;
import com.tian.model.custom.Tablepar;

/**
 * 用户优惠券 UserActivityCouponService
 * @Title: UserActivityCouponService.java 
 * @Package com.tian.service 
 * @author tianwc_自动生成
 * @email ${email}
 * @date 2023-11-14 10:23:45  
 **/
@Service
public class UserActivityCouponService implements BaseService<UserActivityCoupon, UserActivityCouponExample>{
	@Autowired
	private UserActivityCouponMapper userActivityCouponMapper;
	
      	   	      	      	      	      	      	
	/**
	 * 分页查询
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	 public PageInfo<UserActivityCoupon> list(Tablepar tablepar,UserActivityCoupon userActivityCoupon){
	        UserActivityCouponExample testExample=new UserActivityCouponExample();
			//搜索
			if(StrUtil.isNotEmpty(tablepar.getSearchText())) {//小窗体
	        	testExample.createCriteria().andLikeQuery2(tablepar.getSearchText());
	        }else {//大搜索
	        	testExample.createCriteria().andLikeQuery(userActivityCoupon);
	        }
			//表格排序
			//if(StrUtil.isNotEmpty(tablepar.getOrderByColumn())) {
	        //	testExample.setOrderByClause(StringUtils.toUnderScoreCase(tablepar.getOrderByColumn()) +" "+tablepar.getIsAsc());
	        //}else{
	        //	testExample.setOrderByClause("id ASC");
	        //}
	        PageHelper.startPage(tablepar.getPage(), tablepar.getLimit());
	        List<UserActivityCoupon> list= userActivityCouponMapper.selectByExample(testExample);
	        PageInfo<UserActivityCoupon> pageInfo = new PageInfo<UserActivityCoupon>(list);
	        return  pageInfo;
	 }

	@Override
	public int deleteByPrimaryKey(String ids) {
					
			Integer[] integers = ConvertUtil.toIntArray(",", ids);
			List<Integer> stringB = Arrays.asList(integers);
			UserActivityCouponExample example=new UserActivityCouponExample();
			example.createCriteria().andIdIn(stringB);
			return userActivityCouponMapper.deleteByExample(example);
			
				
	}
	
	
	@Override
	public UserActivityCoupon selectByPrimaryKey(String id) {
				
			Integer id1 = Integer.valueOf(id);
			return userActivityCouponMapper.selectByPrimaryKey(id1);
				
	}

	
	@Override
	public int updateByPrimaryKeySelective(UserActivityCoupon record) {
		return userActivityCouponMapper.updateByPrimaryKeySelective(record);
	}
	
	
	/**
	 * 添加
	 */
	@Override
	public int insertSelective(UserActivityCoupon record) {
				
		record.setId(null);
		
				
		return userActivityCouponMapper.insertSelective(record);
	}
	
	
	@Override
	public int updateByExampleSelective(UserActivityCoupon record, UserActivityCouponExample example) {
		
		return userActivityCouponMapper.updateByExampleSelective(record, example);
	}

	
	@Override
	public int updateByExample(UserActivityCoupon record, UserActivityCouponExample example) {
		
		return userActivityCouponMapper.updateByExample(record, example);
	}

	@Override
	public List<UserActivityCoupon> selectByExample(UserActivityCouponExample example) {
		
		return userActivityCouponMapper.selectByExample(example);
	}

	
	@Override
	public long countByExample(UserActivityCouponExample example) {
		
		return userActivityCouponMapper.countByExample(example);
	}

	
	@Override
	public int deleteByExample(UserActivityCouponExample example) {
		
		return userActivityCouponMapper.deleteByExample(example);
	}
	
	/**
	 * 修改权限状态展示或者不展示
	 * @param userActivityCoupon
	 * @return
	 */
	public int updateVisible(UserActivityCoupon userActivityCoupon) {
		return userActivityCouponMapper.updateByPrimaryKeySelective(userActivityCoupon);
	}


}
