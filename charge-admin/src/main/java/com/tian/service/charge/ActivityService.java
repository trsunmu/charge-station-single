package com.tian.service.charge;

import java.util.List;
import java.util.Arrays;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import cn.hutool.core.util.StrUtil;
import com.tian.common.base.BaseService;
import com.tian.common.support.ConvertUtil;
import com.tian.mapper.charge.ActivityMapper;
import com.tian.entity.Activity;
import com.tian.entity.ActivityExample;
import com.tian.model.custom.Tablepar;

/**
 * 活动表 ActivityService
 * @Title: ActivityService.java 
 * @Package com.tian.service 
 * @author fuce_自动生成
 * @email ${email}
 * @date 2023-11-14 09:56:24  
 **/
@Service
public class ActivityService implements BaseService<Activity, ActivityExample>{
	@Autowired
	private ActivityMapper activityMapper;
	
      	   	      	      	      	      	      	      	      	      	      	
	/**
	 * 分页查询
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	 public PageInfo<Activity> list(Tablepar tablepar,Activity activity){
	        ActivityExample testExample=new ActivityExample();
			//搜索
			if(StrUtil.isNotEmpty(tablepar.getSearchText())) {//小窗体
	        	testExample.createCriteria().andLikeQuery2(tablepar.getSearchText());
	        }else {//大搜索
	        	testExample.createCriteria().andLikeQuery(activity);
	        }
			//表格排序
			//if(StrUtil.isNotEmpty(tablepar.getOrderByColumn())) {
	        //	testExample.setOrderByClause(StringUtils.toUnderScoreCase(tablepar.getOrderByColumn()) +" "+tablepar.getIsAsc());
	        //}else{
	        //	testExample.setOrderByClause("id ASC");
	        //}
	        PageHelper.startPage(tablepar.getPage(), tablepar.getLimit());
	        List<Activity> list= activityMapper.selectByExample(testExample);
	        PageInfo<Activity> pageInfo = new PageInfo<Activity>(list);
	        return  pageInfo;
	 }

	@Override
	public int deleteByPrimaryKey(String ids) {
					
			Integer[] integers = ConvertUtil.toIntArray(",", ids);
			List<Integer> stringB = Arrays.asList(integers);
			ActivityExample example=new ActivityExample();
			example.createCriteria().andIdIn(stringB);
			return activityMapper.deleteByExample(example);
			
				
	}
	
	
	@Override
	public Activity selectByPrimaryKey(String id) {
				
			Integer id1 = Integer.valueOf(id);
			return activityMapper.selectByPrimaryKey(id1);
				
	}

	
	@Override
	public int updateByPrimaryKeySelective(Activity record) {
		return activityMapper.updateByPrimaryKeySelective(record);
	}
	
	
	/**
	 * 添加
	 */
	@Override
	public int insertSelective(Activity record) {
				
		record.setId(null);
		
				
		return activityMapper.insertSelective(record);
	}
	
	
	@Override
	public int updateByExampleSelective(Activity record, ActivityExample example) {
		
		return activityMapper.updateByExampleSelective(record, example);
	}

	
	@Override
	public int updateByExample(Activity record, ActivityExample example) {
		
		return activityMapper.updateByExample(record, example);
	}

	@Override
	public List<Activity> selectByExample(ActivityExample example) {
		
		return activityMapper.selectByExample(example);
	}

	
	@Override
	public long countByExample(ActivityExample example) {
		
		return activityMapper.countByExample(example);
	}

	
	@Override
	public int deleteByExample(ActivityExample example) {
		
		return activityMapper.deleteByExample(example);
	}
	
	/**
	 * 修改权限状态展示或者不展示
	 * @param activity
	 * @return
	 */
	public int updateVisible(Activity activity) {
		return activityMapper.updateByPrimaryKeySelective(activity);
	}


}
