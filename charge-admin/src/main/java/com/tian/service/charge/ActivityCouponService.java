package com.tian.service.charge;
import java.util.List;
import java.util.Arrays;
import org.springframework.stereotype.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import cn.hutool.core.util.StrUtil;
import com.tian.common.base.BaseService;
import com.tian.common.support.ConvertUtil;
import com.tian.mapper.charge.ActivityCouponMapper;
import com.tian.entity.ActivityCoupon;
import com.tian.entity.ActivityCouponExample;
import com.tian.model.custom.Tablepar;
import com.tian.util.SnowflakeIdWorker;
import com.tian.util.StringUtils;

import javax.annotation.Resource;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年11月11日 15:53
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 */
@Service
public class ActivityCouponService implements BaseService<ActivityCoupon, ActivityCouponExample>{
    @Resource
    private ActivityCouponMapper activityCouponMapper;


    /**
     * 分页查询
     */
    public PageInfo<ActivityCoupon> list(Tablepar tablepar,ActivityCoupon activityCoupon){
        ActivityCouponExample testExample=new ActivityCouponExample();
        //搜索
        if(StrUtil.isNotEmpty(tablepar.getSearchText())) {//小窗体
            testExample.createCriteria().andLikeQuery2(tablepar.getSearchText());
        }else {//大搜索
            testExample.createCriteria().andLikeQuery(activityCoupon);
        }
        //表格排序
        //if(StrUtil.isNotEmpty(tablepar.getOrderByColumn())) {
        //	testExample.setOrderByClause(StringUtils.toUnderScoreCase(tablepar.getOrderByColumn()) +" "+tablepar.getIsAsc());
        //}else{
        //	testExample.setOrderByClause("id ASC");
        //}
        PageHelper.startPage(tablepar.getPage(), tablepar.getLimit());
        List<ActivityCoupon> list= activityCouponMapper.selectByExample(testExample);
        PageInfo<ActivityCoupon> pageInfo = new PageInfo<ActivityCoupon>(list);
        return  pageInfo;
    }

    @Override
    public int deleteByPrimaryKey(String ids) {

        Integer[] integers = ConvertUtil.toIntArray(",", ids);
        List<Integer> stringB = Arrays.asList(integers);
        ActivityCouponExample example=new ActivityCouponExample();
        example.createCriteria().andIdIn(stringB);
        return activityCouponMapper.deleteByExample(example);


    }


    @Override
    public ActivityCoupon selectByPrimaryKey(String id) {

        Integer id1 = Integer.valueOf(id);
        return activityCouponMapper.selectByPrimaryKey(id1);

    }


    @Override
    public int updateByPrimaryKeySelective(ActivityCoupon record) {
        return activityCouponMapper.updateByPrimaryKeySelective(record);
    }


    /**
     * 添加
     */
    @Override
    public int insertSelective(ActivityCoupon record) {

        record.setId(null);


        return activityCouponMapper.insertSelective(record);
    }


    @Override
    public int updateByExampleSelective(ActivityCoupon record, ActivityCouponExample example) {

        return activityCouponMapper.updateByExampleSelective(record, example);
    }


    @Override
    public int updateByExample(ActivityCoupon record, ActivityCouponExample example) {

        return activityCouponMapper.updateByExample(record, example);
    }

    @Override
    public List<ActivityCoupon> selectByExample(ActivityCouponExample example) {

        return activityCouponMapper.selectByExample(example);
    }


    @Override
    public long countByExample(ActivityCouponExample example) {

        return activityCouponMapper.countByExample(example);
    }


    @Override
    public int deleteByExample(ActivityCouponExample example) {

        return activityCouponMapper.deleteByExample(example);
    }

    /**
     * 修改权限状态展示或者不展示
     * @param activityCoupon
     * @return
     */
    public int updateVisible(ActivityCoupon activityCoupon) {
        return activityCouponMapper.updateByPrimaryKeySelective(activityCoupon);
    }


}

