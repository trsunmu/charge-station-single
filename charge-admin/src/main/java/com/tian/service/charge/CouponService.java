package com.tian.service.charge;

import java.util.Date;
import java.util.List;
import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import cn.hutool.core.util.StrUtil;
import com.tian.common.base.BaseService;
import com.tian.common.support.ConvertUtil;
import com.tian.mapper.charge.CouponMapper;
import com.tian.entity.Coupon;
import com.tian.entity.CouponExample;
import com.tian.model.custom.Tablepar;
import com.tian.util.SnowflakeIdWorker;
import com.tian.util.StringUtils;

import javax.annotation.Resource;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年11月10日 16:59
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 */
@Service
public class CouponService implements BaseService<Coupon, CouponExample> {
    @Resource
    private CouponMapper couponMapper;


    /**
     * 分页查询
     */
    public PageInfo<Coupon> list(Tablepar tablepar, Coupon coupon) {
        CouponExample testExample = new CouponExample();
        //搜索
        if (StrUtil.isNotEmpty(tablepar.getSearchText())) {//小窗体
            testExample.createCriteria().andLikeQuery2(tablepar.getSearchText());
        } else {//大搜索
            testExample.createCriteria().andLikeQuery(coupon);
        }
        //表格排序
        //if(StrUtil.isNotEmpty(tablepar.getOrderByColumn())) {
        //	testExample.setOrderByClause(StringUtils.toUnderScoreCase(tablepar.getOrderByColumn()) +" "+tablepar.getIsAsc());
        //}else{
        //	testExample.setOrderByClause("id ASC");
        //}
        PageHelper.startPage(tablepar.getPage(), tablepar.getLimit());
        List<Coupon> list = couponMapper.selectByExample(testExample);
        PageInfo<Coupon> pageInfo = new PageInfo<Coupon>(list);
        return pageInfo;
    }

    @Override
    public int deleteByPrimaryKey(String ids) {

        Integer[] integers = ConvertUtil.toIntArray(",", ids);
        List<Integer> stringB = Arrays.asList(integers);
        CouponExample example = new CouponExample();
        example.createCriteria().andIdIn(stringB);
        return couponMapper.deleteByExample(example);


    }


    @Override
    public Coupon selectByPrimaryKey(String id) {

        Integer id1 = Integer.valueOf(id);
        return couponMapper.selectByPrimaryKey(id1);

    }


    @Override
    public int updateByPrimaryKeySelective(Coupon record) {
        return couponMapper.updateByPrimaryKeySelective(record);
    }


    /**
     * 添加
     */
    @Override
    public int insertSelective(Coupon record) {
        Date date = new Date();
        record.setCreateTime(date);
        record.setStatus(0);
        record.setUpdateTime(date);

        return couponMapper.insertSelective(record);
    }


    @Override
    public int updateByExampleSelective(Coupon record, CouponExample example) {

        return couponMapper.updateByExampleSelective(record, example);
    }


    @Override
    public int updateByExample(Coupon record, CouponExample example) {

        return couponMapper.updateByExample(record, example);
    }

    @Override
    public List<Coupon> selectByExample(CouponExample example) {

        return couponMapper.selectByExample(example);
    }


    @Override
    public long countByExample(CouponExample example) {

        return couponMapper.countByExample(example);
    }


    @Override
    public int deleteByExample(CouponExample example) {

        return couponMapper.deleteByExample(example);
    }

    /**
     * 修改权限状态展示或者不展示
     *
     * @param coupon
     * @return
     */
    public int updateVisible(Coupon coupon) {
        return couponMapper.updateByPrimaryKeySelective(coupon);
    }


}
