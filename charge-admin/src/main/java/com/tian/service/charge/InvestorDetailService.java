package com.tian.service.charge;

import java.util.List;
import java.util.Arrays;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import cn.hutool.core.util.StrUtil;
import com.tian.common.base.BaseService;
import com.tian.common.support.ConvertUtil;
import com.tian.mapper.charge.InvestorDetailMapper;
import com.tian.entity.InvestorDetail;
import com.tian.entity.InvestorDetailExample;
import com.tian.model.custom.Tablepar;

/**
 * 投资人详情 InvestorDetailService
 * @Title: InvestorDetailService.java 
 * @Package com.tian.service 
 * @author tianwc_自动生成
 * @email ${email}
 * @date 2023-11-14 10:27:39  
 **/
@Service
public class InvestorDetailService implements BaseService<InvestorDetail, InvestorDetailExample>{
	@Autowired
	private InvestorDetailMapper investorDetailMapper;
	
      	   	      	      	      	      	      	      	      	      	      	      	
	/**
	 * 分页查询
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	 public PageInfo<InvestorDetail> list(Tablepar tablepar,InvestorDetail investorDetail){
	        InvestorDetailExample testExample=new InvestorDetailExample();
			//搜索
			if(StrUtil.isNotEmpty(tablepar.getSearchText())) {//小窗体
	        	testExample.createCriteria().andLikeQuery2(tablepar.getSearchText());
	        }else {//大搜索
	        	testExample.createCriteria().andLikeQuery(investorDetail);
	        }
			//表格排序
			//if(StrUtil.isNotEmpty(tablepar.getOrderByColumn())) {
	        //	testExample.setOrderByClause(StringUtils.toUnderScoreCase(tablepar.getOrderByColumn()) +" "+tablepar.getIsAsc());
	        //}else{
	        //	testExample.setOrderByClause("id ASC");
	        //}
	        PageHelper.startPage(tablepar.getPage(), tablepar.getLimit());
	        List<InvestorDetail> list= investorDetailMapper.selectByExample(testExample);
	        PageInfo<InvestorDetail> pageInfo = new PageInfo<InvestorDetail>(list);
	        return  pageInfo;
	 }

	@Override
	public int deleteByPrimaryKey(String ids) {
					
			Integer[] integers = ConvertUtil.toIntArray(",", ids);
			List<Integer> stringB = Arrays.asList(integers);
			InvestorDetailExample example=new InvestorDetailExample();
			example.createCriteria().andIdIn(stringB);
			return investorDetailMapper.deleteByExample(example);
			
				
	}
	
	
	@Override
	public InvestorDetail selectByPrimaryKey(String id) {
				
			Integer id1 = Integer.valueOf(id);
			return investorDetailMapper.selectByPrimaryKey(id1);
				
	}

	
	@Override
	public int updateByPrimaryKeySelective(InvestorDetail record) {
		return investorDetailMapper.updateByPrimaryKeySelective(record);
	}
	
	
	/**
	 * 添加
	 */
	@Override
	public int insertSelective(InvestorDetail record) {
				
		record.setId(null);
		
				
		return investorDetailMapper.insertSelective(record);
	}
	
	
	@Override
	public int updateByExampleSelective(InvestorDetail record, InvestorDetailExample example) {
		
		return investorDetailMapper.updateByExampleSelective(record, example);
	}

	
	@Override
	public int updateByExample(InvestorDetail record, InvestorDetailExample example) {
		
		return investorDetailMapper.updateByExample(record, example);
	}

	@Override
	public List<InvestorDetail> selectByExample(InvestorDetailExample example) {
		
		return investorDetailMapper.selectByExample(example);
	}

	
	@Override
	public long countByExample(InvestorDetailExample example) {
		
		return investorDetailMapper.countByExample(example);
	}

	
	@Override
	public int deleteByExample(InvestorDetailExample example) {
		
		return investorDetailMapper.deleteByExample(example);
	}
	
	/**
	 * 修改权限状态展示或者不展示
	 * @param investorDetail
	 * @return
	 */
	public int updateVisible(InvestorDetail investorDetail) {
		return investorDetailMapper.updateByPrimaryKeySelective(investorDetail);
	}


}
