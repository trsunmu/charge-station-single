package com.tian.service.charge;

import java.util.List;
import java.util.Arrays;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import cn.hutool.core.util.StrUtil;
import com.tian.common.base.BaseService;
import com.tian.common.support.ConvertUtil;
import com.tian.mapper.charge.ExchangeGoodsRecordMapper;
import com.tian.entity.ExchangeGoodsRecord;
import com.tian.entity.ExchangeGoodsRecordExample;
import com.tian.model.custom.Tablepar;

/**
 * 商品兑换 ExchangeGoodsRecordService
 * @Title: ExchangeGoodsRecordService.java 
 * @Package com.tian.service 
 * @author fuce_自动生成
 * @email ${email}
 * @date 2023-11-14 10:14:10  
 **/
@Service
public class ExchangeGoodsRecordService implements BaseService<ExchangeGoodsRecord, ExchangeGoodsRecordExample>{
	@Autowired
	private ExchangeGoodsRecordMapper exchangeGoodsRecordMapper;
	
      	   	      	      	      	      	
	/**
	 * 分页查询
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	 public PageInfo<ExchangeGoodsRecord> list(Tablepar tablepar,ExchangeGoodsRecord exchangeGoodsRecord){
	        ExchangeGoodsRecordExample testExample=new ExchangeGoodsRecordExample();
			//搜索
			if(StrUtil.isNotEmpty(tablepar.getSearchText())) {//小窗体
	        	testExample.createCriteria().andLikeQuery2(tablepar.getSearchText());
	        }else {//大搜索
	        	testExample.createCriteria().andLikeQuery(exchangeGoodsRecord);
	        }
			//表格排序
			//if(StrUtil.isNotEmpty(tablepar.getOrderByColumn())) {
	        //	testExample.setOrderByClause(StringUtils.toUnderScoreCase(tablepar.getOrderByColumn()) +" "+tablepar.getIsAsc());
	        //}else{
	        //	testExample.setOrderByClause("id ASC");
	        //}
	        PageHelper.startPage(tablepar.getPage(), tablepar.getLimit());
	        List<ExchangeGoodsRecord> list= exchangeGoodsRecordMapper.selectByExample(testExample);
	        PageInfo<ExchangeGoodsRecord> pageInfo = new PageInfo<ExchangeGoodsRecord>(list);
	        return  pageInfo;
	 }

	@Override
	public int deleteByPrimaryKey(String ids) {
					
			Integer[] integers = ConvertUtil.toIntArray(",", ids);
			List<Integer> stringB = Arrays.asList(integers);
			ExchangeGoodsRecordExample example=new ExchangeGoodsRecordExample();
			example.createCriteria().andIdIn(stringB);
			return exchangeGoodsRecordMapper.deleteByExample(example);
			
				
	}
	
	
	@Override
	public ExchangeGoodsRecord selectByPrimaryKey(String id) {
				
			Integer id1 = Integer.valueOf(id);
			return exchangeGoodsRecordMapper.selectByPrimaryKey(id1);
				
	}

	
	@Override
	public int updateByPrimaryKeySelective(ExchangeGoodsRecord record) {
		return exchangeGoodsRecordMapper.updateByPrimaryKeySelective(record);
	}
	
	
	/**
	 * 添加
	 */
	@Override
	public int insertSelective(ExchangeGoodsRecord record) {
				
		record.setId(null);
		
				
		return exchangeGoodsRecordMapper.insertSelective(record);
	}
	
	
	@Override
	public int updateByExampleSelective(ExchangeGoodsRecord record, ExchangeGoodsRecordExample example) {
		
		return exchangeGoodsRecordMapper.updateByExampleSelective(record, example);
	}

	
	@Override
	public int updateByExample(ExchangeGoodsRecord record, ExchangeGoodsRecordExample example) {
		
		return exchangeGoodsRecordMapper.updateByExample(record, example);
	}

	@Override
	public List<ExchangeGoodsRecord> selectByExample(ExchangeGoodsRecordExample example) {
		
		return exchangeGoodsRecordMapper.selectByExample(example);
	}

	
	@Override
	public long countByExample(ExchangeGoodsRecordExample example) {
		
		return exchangeGoodsRecordMapper.countByExample(example);
	}

	
	@Override
	public int deleteByExample(ExchangeGoodsRecordExample example) {
		
		return exchangeGoodsRecordMapper.deleteByExample(example);
	}
	
	/**
	 * 修改权限状态展示或者不展示
	 * @param exchangeGoodsRecord
	 * @return
	 */
	public int updateVisible(ExchangeGoodsRecord exchangeGoodsRecord) {
		return exchangeGoodsRecordMapper.updateByPrimaryKeySelective(exchangeGoodsRecord);
	}


}
