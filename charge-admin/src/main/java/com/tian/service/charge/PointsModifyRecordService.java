package com.tian.service.charge;

import java.util.List;
import java.util.Arrays;

import org.springframework.stereotype.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import cn.hutool.core.util.StrUtil;
import com.tian.common.base.BaseService;
import com.tian.common.support.ConvertUtil;
import com.tian.mapper.charge.PointsModifyRecordMapper;
import com.tian.entity.PointsModifyRecord;
import com.tian.entity.PointsModifyRecordExample;
import com.tian.model.custom.Tablepar;

import javax.annotation.Resource;

/**
 * 用户积分变更 PointsModifyRecordService
 *
 * @author fuce_自动生成
 * @Title: PointsModifyRecordService.java 
 * @Package com.tian.service 
 * @email ${email}
 * @date 2023-11-14 10:08:21  
 **/
@Service
public class PointsModifyRecordService implements BaseService<PointsModifyRecord, PointsModifyRecordExample> {

    @Resource
    private PointsModifyRecordMapper pointsModifyRecordMapper;


    /**
     * 分页查询
     *
     */
    public PageInfo<PointsModifyRecord> list(Tablepar tablepar, PointsModifyRecord pointsModifyRecord) {
        PointsModifyRecordExample testExample = new PointsModifyRecordExample();
        //搜索
        if (StrUtil.isNotEmpty(tablepar.getSearchText())) {//小窗体
            testExample.createCriteria().andLikeQuery2(tablepar.getSearchText());
        } else {//大搜索
            testExample.createCriteria().andLikeQuery(pointsModifyRecord);
        }
        //表格排序
        //if(StrUtil.isNotEmpty(tablepar.getOrderByColumn())) {
        //	testExample.setOrderByClause(StringUtils.toUnderScoreCase(tablepar.getOrderByColumn()) +" "+tablepar.getIsAsc());
        //}else{
        //	testExample.setOrderByClause("id ASC");
        //}
        PageHelper.startPage(tablepar.getPage(), tablepar.getLimit());
        List<PointsModifyRecord> list = pointsModifyRecordMapper.selectByExample(testExample);
        PageInfo<PointsModifyRecord> pageInfo = new PageInfo<PointsModifyRecord>(list);
        return pageInfo;
    }

    @Override
    public int deleteByPrimaryKey(String ids) {

        Integer[] integers = ConvertUtil.toIntArray(",", ids);
        List<Integer> stringB = Arrays.asList(integers);
        PointsModifyRecordExample example = new PointsModifyRecordExample();
        example.createCriteria().andIdIn(stringB);
        return pointsModifyRecordMapper.deleteByExample(example);


    }


    @Override
    public PointsModifyRecord selectByPrimaryKey(String id) {

        Integer id1 = Integer.valueOf(id);
        return pointsModifyRecordMapper.selectByPrimaryKey(id1);

    }


    @Override
    public int updateByPrimaryKeySelective(PointsModifyRecord record) {
        return pointsModifyRecordMapper.updateByPrimaryKeySelective(record);
    }


    /**
     * 添加
     */
    @Override
    public int insertSelective(PointsModifyRecord record) {

        record.setId(null);


        return pointsModifyRecordMapper.insertSelective(record);
    }


    @Override
    public int updateByExampleSelective(PointsModifyRecord record, PointsModifyRecordExample example) {

        return pointsModifyRecordMapper.updateByExampleSelective(record, example);
    }


    @Override
    public int updateByExample(PointsModifyRecord record, PointsModifyRecordExample example) {

        return pointsModifyRecordMapper.updateByExample(record, example);
    }

    @Override
    public List<PointsModifyRecord> selectByExample(PointsModifyRecordExample example) {

        return pointsModifyRecordMapper.selectByExample(example);
    }


    @Override
    public long countByExample(PointsModifyRecordExample example) {

        return pointsModifyRecordMapper.countByExample(example);
    }


    @Override
    public int deleteByExample(PointsModifyRecordExample example) {

        return pointsModifyRecordMapper.deleteByExample(example);
    }

    /**
     * 修改权限状态展示或者不展示
     *
     * @param pointsModifyRecord
     * @return
     */
    public int updateVisible(PointsModifyRecord pointsModifyRecord) {
        return pointsModifyRecordMapper.updateByPrimaryKeySelective(pointsModifyRecord);
    }


}
