package com.tian.service.charge;

import java.util.List;
import java.util.Arrays;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import cn.hutool.core.util.StrUtil;
import com.tian.common.base.BaseService;
import com.tian.common.support.ConvertUtil;
import com.tian.mapper.charge.UserActivityGoodsMapper;
import com.tian.entity.UserActivityGoods;
import com.tian.entity.UserActivityGoodsExample;
import com.tian.model.custom.Tablepar;

/**
 * 用户商品 UserActivityGoodsService
 * @Title: UserActivityGoodsService.java 
 * @Package com.tian.service 
 * @author tianwc_自动生成
 * @email ${email}
 * @date 2023-11-14 10:25:07  
 **/
@Service
public class UserActivityGoodsService implements BaseService<UserActivityGoods, UserActivityGoodsExample>{
	@Autowired
	private UserActivityGoodsMapper userActivityGoodsMapper;
	
      	   	      	      	      	      	      	
	/**
	 * 分页查询
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	 public PageInfo<UserActivityGoods> list(Tablepar tablepar,UserActivityGoods userActivityGoods){
	        UserActivityGoodsExample testExample=new UserActivityGoodsExample();
			//搜索
			if(StrUtil.isNotEmpty(tablepar.getSearchText())) {//小窗体
	        	testExample.createCriteria().andLikeQuery2(tablepar.getSearchText());
	        }else {//大搜索
	        	testExample.createCriteria().andLikeQuery(userActivityGoods);
	        }
			//表格排序
			//if(StrUtil.isNotEmpty(tablepar.getOrderByColumn())) {
	        //	testExample.setOrderByClause(StringUtils.toUnderScoreCase(tablepar.getOrderByColumn()) +" "+tablepar.getIsAsc());
	        //}else{
	        //	testExample.setOrderByClause("id ASC");
	        //}
	        PageHelper.startPage(tablepar.getPage(), tablepar.getLimit());
	        List<UserActivityGoods> list= userActivityGoodsMapper.selectByExample(testExample);
	        PageInfo<UserActivityGoods> pageInfo = new PageInfo<UserActivityGoods>(list);
	        return  pageInfo;
	 }

	@Override
	public int deleteByPrimaryKey(String ids) {
					
			Integer[] integers = ConvertUtil.toIntArray(",", ids);
			List<Integer> stringB = Arrays.asList(integers);
			UserActivityGoodsExample example=new UserActivityGoodsExample();
			example.createCriteria().andIdIn(stringB);
			return userActivityGoodsMapper.deleteByExample(example);
			
				
	}
	
	
	@Override
	public UserActivityGoods selectByPrimaryKey(String id) {
				
			Integer id1 = Integer.valueOf(id);
			return userActivityGoodsMapper.selectByPrimaryKey(id1);
				
	}

	
	@Override
	public int updateByPrimaryKeySelective(UserActivityGoods record) {
		return userActivityGoodsMapper.updateByPrimaryKeySelective(record);
	}
	
	
	/**
	 * 添加
	 */
	@Override
	public int insertSelective(UserActivityGoods record) {
				
		record.setId(null);
		
				
		return userActivityGoodsMapper.insertSelective(record);
	}
	
	
	@Override
	public int updateByExampleSelective(UserActivityGoods record, UserActivityGoodsExample example) {
		
		return userActivityGoodsMapper.updateByExampleSelective(record, example);
	}

	
	@Override
	public int updateByExample(UserActivityGoods record, UserActivityGoodsExample example) {
		
		return userActivityGoodsMapper.updateByExample(record, example);
	}

	@Override
	public List<UserActivityGoods> selectByExample(UserActivityGoodsExample example) {
		
		return userActivityGoodsMapper.selectByExample(example);
	}

	
	@Override
	public long countByExample(UserActivityGoodsExample example) {
		
		return userActivityGoodsMapper.countByExample(example);
	}

	
	@Override
	public int deleteByExample(UserActivityGoodsExample example) {
		
		return userActivityGoodsMapper.deleteByExample(example);
	}
	
	/**
	 * 修改权限状态展示或者不展示
	 * @param userActivityGoods
	 * @return
	 */
	public int updateVisible(UserActivityGoods userActivityGoods) {
		return userActivityGoodsMapper.updateByPrimaryKeySelective(userActivityGoods);
	}


}
