package com.tian.service.charge;

import java.util.List;
import java.util.Arrays;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import cn.hutool.core.util.StrUtil;
import com.tian.common.base.BaseService;
import com.tian.common.support.ConvertUtil;
import com.tian.mapper.charge.InvitedRegisterRecordMapper;
import com.tian.entity.InvitedRegisterRecord;
import com.tian.entity.InvitedRegisterRecordExample;
import com.tian.model.custom.Tablepar;

/**
 * 邀请注册 InvitedRegisterRecordService
 * @Title: InvitedRegisterRecordService.java 
 * @Package com.tian.service 
 * @author tianwc_自动生成
 * @email ${email}
 * @date 2023-11-14 10:26:03  
 **/
@Service
public class InvitedRegisterRecordService implements BaseService<InvitedRegisterRecord, InvitedRegisterRecordExample>{
	@Autowired
	private InvitedRegisterRecordMapper invitedRegisterRecordMapper;
	
      	   	      	      	      	      	      	
	/**
	 * 分页查询
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	 public PageInfo<InvitedRegisterRecord> list(Tablepar tablepar,InvitedRegisterRecord invitedRegisterRecord){
	        InvitedRegisterRecordExample testExample=new InvitedRegisterRecordExample();
			//搜索
			if(StrUtil.isNotEmpty(tablepar.getSearchText())) {//小窗体
	        	testExample.createCriteria().andLikeQuery2(tablepar.getSearchText());
	        }else {//大搜索
	        	testExample.createCriteria().andLikeQuery(invitedRegisterRecord);
	        }
			//表格排序
			//if(StrUtil.isNotEmpty(tablepar.getOrderByColumn())) {
	        //	testExample.setOrderByClause(StringUtils.toUnderScoreCase(tablepar.getOrderByColumn()) +" "+tablepar.getIsAsc());
	        //}else{
	        //	testExample.setOrderByClause("id ASC");
	        //}
	        PageHelper.startPage(tablepar.getPage(), tablepar.getLimit());
	        List<InvitedRegisterRecord> list= invitedRegisterRecordMapper.selectByExample(testExample);
	        PageInfo<InvitedRegisterRecord> pageInfo = new PageInfo<InvitedRegisterRecord>(list);
	        return  pageInfo;
	 }

	@Override
	public int deleteByPrimaryKey(String ids) {
					
			Integer[] integers = ConvertUtil.toIntArray(",", ids);
			List<Integer> stringB = Arrays.asList(integers);
			InvitedRegisterRecordExample example=new InvitedRegisterRecordExample();
			example.createCriteria().andIdIn(stringB);
			return invitedRegisterRecordMapper.deleteByExample(example);
			
				
	}
	
	
	@Override
	public InvitedRegisterRecord selectByPrimaryKey(String id) {
				
			Integer id1 = Integer.valueOf(id);
			return invitedRegisterRecordMapper.selectByPrimaryKey(id1);
				
	}

	
	@Override
	public int updateByPrimaryKeySelective(InvitedRegisterRecord record) {
		return invitedRegisterRecordMapper.updateByPrimaryKeySelective(record);
	}
	
	
	/**
	 * 添加
	 */
	@Override
	public int insertSelective(InvitedRegisterRecord record) {
				
		record.setId(null);
		
				
		return invitedRegisterRecordMapper.insertSelective(record);
	}
	
	
	@Override
	public int updateByExampleSelective(InvitedRegisterRecord record, InvitedRegisterRecordExample example) {
		
		return invitedRegisterRecordMapper.updateByExampleSelective(record, example);
	}

	
	@Override
	public int updateByExample(InvitedRegisterRecord record, InvitedRegisterRecordExample example) {
		
		return invitedRegisterRecordMapper.updateByExample(record, example);
	}

	@Override
	public List<InvitedRegisterRecord> selectByExample(InvitedRegisterRecordExample example) {
		
		return invitedRegisterRecordMapper.selectByExample(example);
	}

	
	@Override
	public long countByExample(InvitedRegisterRecordExample example) {
		
		return invitedRegisterRecordMapper.countByExample(example);
	}

	
	@Override
	public int deleteByExample(InvitedRegisterRecordExample example) {
		
		return invitedRegisterRecordMapper.deleteByExample(example);
	}
	
	/**
	 * 修改权限状态展示或者不展示
	 * @param invitedRegisterRecord
	 * @return
	 */
	public int updateVisible(InvitedRegisterRecord invitedRegisterRecord) {
		return invitedRegisterRecordMapper.updateByPrimaryKeySelective(invitedRegisterRecord);
	}


}
