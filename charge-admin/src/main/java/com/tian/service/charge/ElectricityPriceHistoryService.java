package com.tian.service.charge;

import java.util.List;
import java.util.Arrays;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import cn.hutool.core.util.StrUtil;
import com.tian.common.base.BaseService;
import com.tian.common.support.ConvertUtil;
import com.tian.mapper.charge.ElectricityPriceHistoryMapper;
import com.tian.entity.ElectricityPriceHistory;
import com.tian.entity.ElectricityPriceHistoryExample;
import com.tian.model.custom.Tablepar;

/**
 * 电价变更 ElectricityPriceHistoryService
 * @Title: ElectricityPriceHistoryService.java 
 * @Package com.tian.service 
 * @author tianwc_自动生成
 * @email ${email}
 * @date 2023-11-14 10:28:12  
 **/
@Service
public class ElectricityPriceHistoryService implements BaseService<ElectricityPriceHistory, ElectricityPriceHistoryExample>{
	@Autowired
	private ElectricityPriceHistoryMapper electricityPriceHistoryMapper;
	
      	   	      	      	      	      	      	      	
	/**
	 * 分页查询
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	 public PageInfo<ElectricityPriceHistory> list(Tablepar tablepar,ElectricityPriceHistory electricityPriceHistory){
	        ElectricityPriceHistoryExample testExample=new ElectricityPriceHistoryExample();
			//搜索
			if(StrUtil.isNotEmpty(tablepar.getSearchText())) {//小窗体
	        	testExample.createCriteria().andLikeQuery2(tablepar.getSearchText());
	        }else {//大搜索
	        	testExample.createCriteria().andLikeQuery(electricityPriceHistory);
	        }
			//表格排序
			//if(StrUtil.isNotEmpty(tablepar.getOrderByColumn())) {
	        //	testExample.setOrderByClause(StringUtils.toUnderScoreCase(tablepar.getOrderByColumn()) +" "+tablepar.getIsAsc());
	        //}else{
	        //	testExample.setOrderByClause("id ASC");
	        //}
	        PageHelper.startPage(tablepar.getPage(), tablepar.getLimit());
	        List<ElectricityPriceHistory> list= electricityPriceHistoryMapper.selectByExample(testExample);
	        PageInfo<ElectricityPriceHistory> pageInfo = new PageInfo<ElectricityPriceHistory>(list);
	        return  pageInfo;
	 }

	@Override
	public int deleteByPrimaryKey(String ids) {
					
			Integer[] integers = ConvertUtil.toIntArray(",", ids);
			List<Integer> stringB = Arrays.asList(integers);
			ElectricityPriceHistoryExample example=new ElectricityPriceHistoryExample();
			example.createCriteria().andIdIn(stringB);
			return electricityPriceHistoryMapper.deleteByExample(example);
			
				
	}
	
	
	@Override
	public ElectricityPriceHistory selectByPrimaryKey(String id) {
				
			Integer id1 = Integer.valueOf(id);
			return electricityPriceHistoryMapper.selectByPrimaryKey(id1);
				
	}

	
	@Override
	public int updateByPrimaryKeySelective(ElectricityPriceHistory record) {
		return electricityPriceHistoryMapper.updateByPrimaryKeySelective(record);
	}
	
	
	/**
	 * 添加
	 */
	@Override
	public int insertSelective(ElectricityPriceHistory record) {
				
		record.setId(null);
		
				
		return electricityPriceHistoryMapper.insertSelective(record);
	}
	
	
	@Override
	public int updateByExampleSelective(ElectricityPriceHistory record, ElectricityPriceHistoryExample example) {
		
		return electricityPriceHistoryMapper.updateByExampleSelective(record, example);
	}

	
	@Override
	public int updateByExample(ElectricityPriceHistory record, ElectricityPriceHistoryExample example) {
		
		return electricityPriceHistoryMapper.updateByExample(record, example);
	}

	@Override
	public List<ElectricityPriceHistory> selectByExample(ElectricityPriceHistoryExample example) {
		
		return electricityPriceHistoryMapper.selectByExample(example);
	}

	
	@Override
	public long countByExample(ElectricityPriceHistoryExample example) {
		
		return electricityPriceHistoryMapper.countByExample(example);
	}

	
	@Override
	public int deleteByExample(ElectricityPriceHistoryExample example) {
		
		return electricityPriceHistoryMapper.deleteByExample(example);
	}
	
	/**
	 * 修改权限状态展示或者不展示
	 * @param electricityPriceHistory
	 * @return
	 */
	public int updateVisible(ElectricityPriceHistory electricityPriceHistory) {
		return electricityPriceHistoryMapper.updateByPrimaryKeySelective(electricityPriceHistory);
	}


}
