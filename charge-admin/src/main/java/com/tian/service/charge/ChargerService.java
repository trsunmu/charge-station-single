package com.tian.service.charge;

import cn.hutool.core.util.StrUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.tian.common.base.BaseService;
import com.tian.common.support.ConvertUtil;
import com.tian.mapper.charge.ChargerMapper;
import com.tian.entity.Charger;
import com.tian.entity.ChargerExample;
import com.tian.model.custom.Tablepar;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年11月10日 19:30
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 */
@Service
public class ChargerService implements BaseService<Charger, ChargerExample> {
    @Resource
    private ChargerMapper chargerMapper;


    /**
     * 分页查询
     */
    public PageInfo<Charger> list(Tablepar tablepar, Charger charger) {
        ChargerExample testExample = new ChargerExample();
        //搜索
        if (StrUtil.isNotEmpty(tablepar.getSearchText())) {//小窗体
            testExample.createCriteria().andLikeQuery2(tablepar.getSearchText());
        } else {//大搜索
            testExample.createCriteria().andLikeQuery(charger);
        }
        //表格排序
        //if(StrUtil.isNotEmpty(tablepar.getOrderByColumn())) {
        //	testExample.setOrderByClause(StringUtils.toUnderScoreCase(tablepar.getOrderByColumn()) +" "+tablepar.getIsAsc());
        //}else{
        //	testExample.setOrderByClause("id ASC");
        //}
        PageHelper.startPage(tablepar.getPage(), tablepar.getLimit());
        List<Charger> list = chargerMapper.selectByExample(testExample);
        PageInfo<Charger> pageInfo = new PageInfo<Charger>(list);
        return pageInfo;
    }

    @Override
    public int deleteByPrimaryKey(String ids) {

        Integer[] integers = ConvertUtil.toIntArray(",", ids);
        List<Integer> stringB = Arrays.asList(integers);
        ChargerExample example = new ChargerExample();
        example.createCriteria().andIdIn(stringB);
        return chargerMapper.deleteByExample(example);


    }


    @Override
    public Charger selectByPrimaryKey(String id) {

        Integer id1 = Integer.valueOf(id);
        return chargerMapper.selectByPrimaryKey(id1);

    }


    @Override
    public int updateByPrimaryKeySelective(Charger record) {
        return chargerMapper.updateByPrimaryKeySelective(record);
    }


    /**
     * 添加
     */
    @Override
    public int insertSelective(Charger record) {

        record.setId(null);


        return chargerMapper.insertSelective(record);
    }


    @Override
    public int updateByExampleSelective(Charger record, ChargerExample example) {

        return chargerMapper.updateByExampleSelective(record, example);
    }


    @Override
    public int updateByExample(Charger record, ChargerExample example) {

        return chargerMapper.updateByExample(record, example);
    }

    @Override
    public List<Charger> selectByExample(ChargerExample example) {

        return chargerMapper.selectByExample(example);
    }


    @Override
    public long countByExample(ChargerExample example) {

        return chargerMapper.countByExample(example);
    }


    @Override
    public int deleteByExample(ChargerExample example) {

        return chargerMapper.deleteByExample(example);
    }

    /**
     * 修改权限状态展示或者不展示
     *
     * @param charger
     * @return
     */
    public int updateVisible(Charger charger) {
        return chargerMapper.updateByPrimaryKeySelective(charger);
    }


}
