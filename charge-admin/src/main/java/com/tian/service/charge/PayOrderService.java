package com.tian.service.charge;

import java.util.List;
import java.util.Arrays;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import cn.hutool.core.util.StrUtil;
import com.tian.common.base.BaseService;
import com.tian.common.support.ConvertUtil;
import com.tian.mapper.charge.PayOrderMapper;
import com.tian.entity.PayOrder;
import com.tian.entity.PayOrderExample;
import com.tian.model.custom.Tablepar;

import javax.annotation.Resource;

/**
 * 支付单 PayOrderService
 * @Title: PayOrderService.java 
 * @Package com.tian.service 
 * @author fuce_自动生成
 * @email ${email}
 * @date 2023-11-13 20:02:55  
 **/
@Service
public class PayOrderService implements BaseService<PayOrder, PayOrderExample>{
	@Resource
	private PayOrderMapper payOrderMapper;

	/**
	 * 分页查询
	 */
	 public PageInfo<PayOrder> list(Tablepar tablepar,PayOrder payOrder){
	        PayOrderExample testExample=new PayOrderExample();
			//搜索
			if(StrUtil.isNotEmpty(tablepar.getSearchText())) {//小窗体
	        	testExample.createCriteria().andLikeQuery2(tablepar.getSearchText());
	        }else {//大搜索
	        	testExample.createCriteria().andLikeQuery(payOrder);
	        }
			//表格排序
			//if(StrUtil.isNotEmpty(tablepar.getOrderByColumn())) {
	        //	testExample.setOrderByClause(StringUtils.toUnderScoreCase(tablepar.getOrderByColumn()) +" "+tablepar.getIsAsc());
	        //}else{
	        //	testExample.setOrderByClause("id ASC");
	        //}
	        PageHelper.startPage(tablepar.getPage(), tablepar.getLimit());
	        List<PayOrder> list= payOrderMapper.selectByExample(testExample);
	        PageInfo<PayOrder> pageInfo = new PageInfo<PayOrder>(list);
	        return  pageInfo;
	 }

	@Override
	public PayOrder selectByPrimaryKey(String id) {
				
			Integer id1 = Integer.valueOf(id);
			return payOrderMapper.selectByPrimaryKey(id1);
				
	}

	
	@Override
	public int updateByPrimaryKeySelective(PayOrder record) {
		return payOrderMapper.updateByPrimaryKeySelective(record);
	}


	@Override
	public int deleteByPrimaryKey(String id) {
		return 0;
	}

	/**
	 * 添加
	 */
	@Override
	public int insertSelective(PayOrder record) {
				
		record.setId(null);
		
				
		return payOrderMapper.insertSelective(record);
	}
	
	
	@Override
	public int updateByExampleSelective(PayOrder record, PayOrderExample example) {
		
		return payOrderMapper.updateByExampleSelective(record, example);
	}

	
	@Override
	public int updateByExample(PayOrder record, PayOrderExample example) {
		
		return payOrderMapper.updateByExample(record, example);
	}

	@Override
	public List<PayOrder> selectByExample(PayOrderExample example) {
		
		return payOrderMapper.selectByExample(example);
	}

	
	@Override
	public long countByExample(PayOrderExample example) {
		
		return payOrderMapper.countByExample(example);
	}

	@Override
	public int deleteByExample(PayOrderExample example) {
		return 0;
	}


	/**
	 * 修改权限状态展示或者不展示
	 * @param payOrder
	 * @return
	 */
	public int updateVisible(PayOrder payOrder) {
		return payOrderMapper.updateByPrimaryKeySelective(payOrder);
	}


}
