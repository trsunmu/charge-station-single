package com.tian.service.charge;

import java.util.List;
import java.util.Arrays;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import cn.hutool.core.util.StrUtil;
import com.tian.common.base.BaseService;
import com.tian.common.support.ConvertUtil;
import com.tian.mapper.charge.MessageTemplateMapper;
import com.tian.entity.MessageTemplate;
import com.tian.entity.MessageTemplateExample;
import com.tian.model.custom.Tablepar;

/**
 * 短信模板管理 MessageTemplateService
 * @Title: MessageTemplateService.java 
 * @Package com.tian.service 
 * @author fuce_自动生成
 * @email ${email}
 * @date 2023-11-14 09:42:16  
 **/
@Service
public class MessageTemplateService implements BaseService<MessageTemplate, MessageTemplateExample>{
	@Autowired
	private MessageTemplateMapper messageTemplateMapper;
	
      	   	      	      	      	      	      	      	      	
	/**
	 * 分页查询
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	 public PageInfo<MessageTemplate> list(Tablepar tablepar,MessageTemplate messageTemplate){
	        MessageTemplateExample testExample=new MessageTemplateExample();
			//搜索
			if(StrUtil.isNotEmpty(tablepar.getSearchText())) {//小窗体
	        	testExample.createCriteria().andLikeQuery2(tablepar.getSearchText());
	        }else {//大搜索
	        	testExample.createCriteria().andLikeQuery(messageTemplate);
	        }
			//表格排序
			//if(StrUtil.isNotEmpty(tablepar.getOrderByColumn())) {
	        //	testExample.setOrderByClause(StringUtils.toUnderScoreCase(tablepar.getOrderByColumn()) +" "+tablepar.getIsAsc());
	        //}else{
	        //	testExample.setOrderByClause("id ASC");
	        //}
	        PageHelper.startPage(tablepar.getPage(), tablepar.getLimit());
	        List<MessageTemplate> list= messageTemplateMapper.selectByExample(testExample);
	        PageInfo<MessageTemplate> pageInfo = new PageInfo<MessageTemplate>(list);
	        return  pageInfo;
	 }

	@Override
	public int deleteByPrimaryKey(String ids) {
					
			Integer[] integers = ConvertUtil.toIntArray(",", ids);
			List<Integer> stringB = Arrays.asList(integers);
			MessageTemplateExample example=new MessageTemplateExample();
			example.createCriteria().andIdIn(stringB);
			return messageTemplateMapper.deleteByExample(example);
			
				
	}
	
	
	@Override
	public MessageTemplate selectByPrimaryKey(String id) {
				
			Integer id1 = Integer.valueOf(id);
			return messageTemplateMapper.selectByPrimaryKey(id1);
				
	}

	
	@Override
	public int updateByPrimaryKeySelective(MessageTemplate record) {
		return messageTemplateMapper.updateByPrimaryKeySelective(record);
	}
	
	
	/**
	 * 添加
	 */
	@Override
	public int insertSelective(MessageTemplate record) {
				
		record.setId(null);
		
				
		return messageTemplateMapper.insertSelective(record);
	}
	
	
	@Override
	public int updateByExampleSelective(MessageTemplate record, MessageTemplateExample example) {
		
		return messageTemplateMapper.updateByExampleSelective(record, example);
	}

	
	@Override
	public int updateByExample(MessageTemplate record, MessageTemplateExample example) {
		
		return messageTemplateMapper.updateByExample(record, example);
	}

	@Override
	public List<MessageTemplate> selectByExample(MessageTemplateExample example) {
		
		return messageTemplateMapper.selectByExample(example);
	}

	
	@Override
	public long countByExample(MessageTemplateExample example) {
		
		return messageTemplateMapper.countByExample(example);
	}

	
	@Override
	public int deleteByExample(MessageTemplateExample example) {
		
		return messageTemplateMapper.deleteByExample(example);
	}
	
	/**
	 * 修改权限状态展示或者不展示
	 * @param messageTemplate
	 * @return
	 */
	public int updateVisible(MessageTemplate messageTemplate) {
		return messageTemplateMapper.updateByPrimaryKeySelective(messageTemplate);
	}


}
