package com.tian.service.charge;

import java.util.List;
import java.util.Arrays;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import cn.hutool.core.util.StrUtil;
import com.tian.common.base.BaseService;
import com.tian.common.support.ConvertUtil;
import com.tian.mapper.charge.ActivityGoodsMapper;
import com.tian.entity.ActivityGoods;
import com.tian.entity.ActivityGoodsExample;
import com.tian.model.custom.Tablepar;

/**
 * 活动商品 ActivityGoodsService
 * @Title: ActivityGoodsService.java 
 * @Package com.tian.service 
 * @author tianwc_自动生成
 * @email ${email}
 * @date 2023-11-14 10:19:38  
 **/
@Service
public class ActivityGoodsService implements BaseService<ActivityGoods, ActivityGoodsExample>{
	@Autowired
	private ActivityGoodsMapper activityGoodsMapper;
	
      	   	      	      	      	      	      	      	      	
	/**
	 * 分页查询
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	 public PageInfo<ActivityGoods> list(Tablepar tablepar,ActivityGoods activityGoods){
	        ActivityGoodsExample testExample=new ActivityGoodsExample();
			//搜索
			if(StrUtil.isNotEmpty(tablepar.getSearchText())) {//小窗体
	        	testExample.createCriteria().andLikeQuery2(tablepar.getSearchText());
	        }else {//大搜索
	        	testExample.createCriteria().andLikeQuery(activityGoods);
	        }
			//表格排序
			//if(StrUtil.isNotEmpty(tablepar.getOrderByColumn())) {
	        //	testExample.setOrderByClause(StringUtils.toUnderScoreCase(tablepar.getOrderByColumn()) +" "+tablepar.getIsAsc());
	        //}else{
	        //	testExample.setOrderByClause("id ASC");
	        //}
	        PageHelper.startPage(tablepar.getPage(), tablepar.getLimit());
	        List<ActivityGoods> list= activityGoodsMapper.selectByExample(testExample);
	        PageInfo<ActivityGoods> pageInfo = new PageInfo<ActivityGoods>(list);
	        return  pageInfo;
	 }

	@Override
	public int deleteByPrimaryKey(String ids) {
					
			Integer[] integers = ConvertUtil.toIntArray(",", ids);
			List<Integer> stringB = Arrays.asList(integers);
			ActivityGoodsExample example=new ActivityGoodsExample();
			example.createCriteria().andIdIn(stringB);
			return activityGoodsMapper.deleteByExample(example);
			
				
	}
	
	
	@Override
	public ActivityGoods selectByPrimaryKey(String id) {
				
			Integer id1 = Integer.valueOf(id);
			return activityGoodsMapper.selectByPrimaryKey(id1);
				
	}

	
	@Override
	public int updateByPrimaryKeySelective(ActivityGoods record) {
		return activityGoodsMapper.updateByPrimaryKeySelective(record);
	}
	
	
	/**
	 * 添加
	 */
	@Override
	public int insertSelective(ActivityGoods record) {
				
		record.setId(null);
		
				
		return activityGoodsMapper.insertSelective(record);
	}
	
	
	@Override
	public int updateByExampleSelective(ActivityGoods record, ActivityGoodsExample example) {
		
		return activityGoodsMapper.updateByExampleSelective(record, example);
	}

	
	@Override
	public int updateByExample(ActivityGoods record, ActivityGoodsExample example) {
		
		return activityGoodsMapper.updateByExample(record, example);
	}

	@Override
	public List<ActivityGoods> selectByExample(ActivityGoodsExample example) {
		
		return activityGoodsMapper.selectByExample(example);
	}

	
	@Override
	public long countByExample(ActivityGoodsExample example) {
		
		return activityGoodsMapper.countByExample(example);
	}

	
	@Override
	public int deleteByExample(ActivityGoodsExample example) {
		
		return activityGoodsMapper.deleteByExample(example);
	}
	
	/**
	 * 修改权限状态展示或者不展示
	 * @param activityGoods
	 * @return
	 */
	public int updateVisible(ActivityGoods activityGoods) {
		return activityGoodsMapper.updateByPrimaryKeySelective(activityGoods);
	}


}
