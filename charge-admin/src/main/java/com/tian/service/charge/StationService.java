package com.tian.service.charge;

import cn.hutool.core.util.StrUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.tian.common.base.BaseService;
import com.tian.common.support.ConvertUtil;
import com.tian.mapper.charge.StationMapper;
import com.tian.mapper.charge.UsersMapper;
import com.tian.entity.Station;
import com.tian.entity.StationExample;
import com.tian.entity.Users;
import com.tian.entity.UsersExample;
import com.tian.model.custom.Tablepar;
import com.tian.util.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年11月10日 20:10
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 */
@Service
public class StationService implements BaseService<Station, StationExample> {
    @Resource
    private StationMapper stationMapper;
    @Resource
    private UsersMapper usersMapper;

    /**
     * 分页查询
     */
    public PageInfo<Station> list(Tablepar tablepar, Station station) {
        StationExample testExample = new StationExample();
        //搜索
        if (StrUtil.isNotEmpty(tablepar.getSearchText())) {//小窗体
            testExample.createCriteria().andLikeQuery2(tablepar.getSearchText());
        } else {//大搜索
            testExample.createCriteria().andLikeQuery(station);
        }
        //表格排序
        if (StrUtil.isNotEmpty(tablepar.getOrderByColumn())) {
            testExample.setOrderByClause(StringUtils.toUnderScoreCase(tablepar.getOrderByColumn()) + " " + tablepar.getIsAsc());
        } else {
            testExample.setOrderByClause("id ASC");
        }

        PageHelper.startPage(tablepar.getPage(), tablepar.getLimit());

        List<Station> list = stationMapper.selectByExample(testExample);
        //投资人名称
        if (!CollectionUtils.isEmpty(list)) {
            List<Integer> invertorIdList = new ArrayList<>();
            for (Station s : list) {
                invertorIdList.add(s.getInvestorId());
            }
            UsersExample example = new UsersExample();
            example.createCriteria().andIdIn(invertorIdList);
            List<Users> users = usersMapper.selectByExample(example);
            //List转map
            Map<Integer, Users> usersMap = users.stream().collect(Collectors.toMap(Users::getId, Function.identity()));
            for (Station s : list) {
                s.setInvestorName(usersMap.get(s.getInvestorId()).getRealName());
            }
        }
        return new PageInfo<>(list);
    }

    @Override
    public int deleteByPrimaryKey(String ids) {

        Integer[] integers = ConvertUtil.toIntArray(",", ids);
        List<Integer> stringB = Arrays.asList(integers);
        StationExample example = new StationExample();
        example.createCriteria().andIdIn(stringB);
        return stationMapper.deleteByExample(example);


    }


    @Override
    public Station selectByPrimaryKey(String id) {

        Integer id1 = Integer.valueOf(id);
        return stationMapper.selectByPrimaryKey(id1);

    }


    @Override
    public int updateByPrimaryKeySelective(Station record) {
        return stationMapper.updateByPrimaryKeySelective(record);
    }


    /**
     * 添加
     */
    @Override
    public int insertSelective(Station record) {

        record.setId(null);


        return stationMapper.insertSelective(record);
    }


    @Override
    public int updateByExampleSelective(Station record, StationExample example) {

        return stationMapper.updateByExampleSelective(record, example);
    }


    @Override
    public int updateByExample(Station record, StationExample example) {

        return stationMapper.updateByExample(record, example);
    }

    @Override
    public List<Station> selectByExample(StationExample example) {

        return stationMapper.selectByExample(example);
    }


    @Override
    public long countByExample(StationExample example) {

        return stationMapper.countByExample(example);
    }


    @Override
    public int deleteByExample(StationExample example) {

        return stationMapper.deleteByExample(example);
    }

    /**
     * 修改权限状态展示或者不展示
     *
     * @param station
     * @return
     */
    public int updateVisible(Station station) {
        return stationMapper.updateByPrimaryKeySelective(station);
    }


}