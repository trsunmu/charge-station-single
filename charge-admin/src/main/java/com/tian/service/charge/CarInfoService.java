package com.tian.service.charge;

import java.util.Date;
import java.util.List;
import java.util.Arrays;

import com.tian.model.auto.TsysUser;
import com.tian.satoken.SaTokenUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import cn.hutool.core.util.StrUtil;
import com.tian.common.base.BaseService;
import com.tian.common.support.ConvertUtil;
import com.tian.mapper.charge.CarInfoMapper;
import com.tian.entity.CarInfo;
import com.tian.entity.CarInfoExample;
import com.tian.model.custom.Tablepar;

/**
 * 汽车信息 CarInfoService
 * @Title: CarInfoService.java 
 * @Package com.tian.service 
 * @author fuce_自动生成
 * @email ${email}
 * @date 2023-11-14 09:50:18  
 **/
@Service
public class CarInfoService implements BaseService<CarInfo, CarInfoExample>{
	@Autowired
	private CarInfoMapper carInfoMapper;


	/**
	 * 分页查询
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	public PageInfo<CarInfo> list(Tablepar tablepar,CarInfo carInfo){
		CarInfoExample testExample=new CarInfoExample();
		//搜索
		if(StrUtil.isNotEmpty(tablepar.getSearchText())) {//小窗体
			testExample.createCriteria().andLikeQuery2(tablepar.getSearchText());
		}else {//大搜索
			testExample.createCriteria().andLikeQuery(carInfo);
		}
		//表格排序
		//if(StrUtil.isNotEmpty(tablepar.getOrderByColumn())) {
		//	testExample.setOrderByClause(StringUtils.toUnderScoreCase(tablepar.getOrderByColumn()) +" "+tablepar.getIsAsc());
		//}else{
		//	testExample.setOrderByClause("id ASC");
		//}
		PageHelper.startPage(tablepar.getPage(), tablepar.getLimit());
		List<CarInfo> list= carInfoMapper.selectByExample(testExample);
		PageInfo<CarInfo> pageInfo = new PageInfo<CarInfo>(list);
		return  pageInfo;
	}

	@Override
	public int deleteByPrimaryKey(String ids) {

		Integer[] integers = ConvertUtil.toIntArray(",", ids);
		List<Integer> stringB = Arrays.asList(integers);
		CarInfoExample example=new CarInfoExample();
		example.createCriteria().andIdIn(stringB);
		return carInfoMapper.deleteByExample(example);


	}


	@Override
	public CarInfo selectByPrimaryKey(String id) {

		Integer id1 = Integer.valueOf(id);
		return carInfoMapper.selectByPrimaryKey(id1);

	}


	@Override
	public int updateByPrimaryKeySelective(CarInfo record) {
		CarInfo carInfo=carInfoMapper.selectByPrimaryKey(record.getId());
		carInfo.setCarIdentifyStatus(record.getCarIdentifyStatus());
		return carInfoMapper.updateByPrimaryKeySelective(carInfo);
	}


	/**
	 * 添加
	 */
	@Override
	public int insertSelective(CarInfo record) {
		return carInfoMapper.insertSelective(record);
	}


	@Override
	public int updateByExampleSelective(CarInfo record, CarInfoExample example) {

		return carInfoMapper.updateByExampleSelective(record, example);
	}


	@Override
	public int updateByExample(CarInfo record, CarInfoExample example) {

		return carInfoMapper.updateByExample(record, example);
	}

	@Override
	public List<CarInfo> selectByExample(CarInfoExample example) {

		return carInfoMapper.selectByExample(example);
	}


	@Override
	public long countByExample(CarInfoExample example) {

		return carInfoMapper.countByExample(example);
	}


	@Override
	public int deleteByExample(CarInfoExample example) {

		return carInfoMapper.deleteByExample(example);
	}

	/**
	 * 修改权限状态展示或者不展示
	 * @param carInfo
	 * @return
	 */
	public int updateVisible(CarInfo carInfo) {
		return carInfoMapper.updateByPrimaryKeySelective(carInfo);
	}


}
