package com.tian.model.custom.autocode;

import com.tian.model.auto.TSysDictData;
import com.tian.model.auto.TSysDictType;

import java.util.List;

public class AutoDictType {
	//字典表
	private TSysDictType dictType;
	//字典表里面的数据
	private List<TSysDictData> dictDatas;

	public TSysDictType getDictType() {
		return dictType;
	}

	public void setDictType(TSysDictType dictType) {
		this.dictType = dictType;
	}

	public List<TSysDictData> getDictDatas() {
		return dictDatas;
	}

	public void setDictDatas(List<TSysDictData> dictDatas) {
		this.dictDatas = dictDatas;
	}

	public AutoDictType(TSysDictType dictType, List<TSysDictData> dictDatas) {
		super();
		this.dictType = dictType;
		this.dictDatas = dictDatas;
	}

	public AutoDictType() {
		super();
	}
	
	
	
	
	
}
