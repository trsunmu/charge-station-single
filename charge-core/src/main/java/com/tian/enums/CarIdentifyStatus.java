package com.tian.enums;

/**
 * @author tianwc 公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年11月17日 16:51
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 *
 * 汽车认证状态
 */
public enum CarIdentifyStatus {
    INIT(0,"资料待提交"),
    CHECKING(1,"资料已提交"),
    PASSED(2,"认证通过"),
    FAILED(3,"认证失败");;
    private final int status;
    private final String desc;

    CarIdentifyStatus(int status, String desc) {
        this.status = status;
        this.desc = desc;
    }

    public int getStatus() {
        return status;
    }

    public String getDesc() {
        return desc;
    }

}
