package com.tian.enums;

/**
 * {@code @description:} 站内信状态枚举
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024/1/12 9:37
 * {@code @version:} 1.0
 */
public enum SystemUserMessageStatusEnum {
    UN_READ(0, "未阅读"),
    READ(1, "已阅读"),
    DELETE(2, "已删除");
    private final int status;
    private final String desc;

    SystemUserMessageStatusEnum(int status, String desc) {
        this.status = status;
        this.desc = desc;
    }

    public int getStatus() {
        return status;
    }

    public String getDesc() {
        return desc;
    }

    public static SystemUserMessageStatusEnum getSystemUserMessageStatusEnumByStatus(int status) {
        for (SystemUserMessageStatusEnum systemUserMessageStatusEnum : SystemUserMessageStatusEnum.values()) {
            if (systemUserMessageStatusEnum.status == status) {
                return systemUserMessageStatusEnum;
            }
        }
        return UN_READ;
    }
}
