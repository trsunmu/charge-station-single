package com.tian.enums;

/**
 * @author tianwc 公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年11月21日 10:31
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 * <p>
 * 优惠券类型
 */
public enum CouponTypeEnum {
    /**
     * 如果是金额类型则直接抵扣金额
     */
    AMOUNT(0, "金额类型优惠券"),
    /**
     * 如果是折扣类型，那就需要进行这算，再抵扣金额
     * 比如：8折优惠券，存1-0.8=0.2
     * 支付100，优惠金额是20，实际支付金额是80
     */
    DISCOUNT(1, "折扣类型优惠券");
    private final int type;
    private final String des;

    CouponTypeEnum(int type, String des) {
        this.type = type;
        this.des = des;
    }

    public int getType() {
        return type;
    }

    public String getDes() {
        return des;
    }
}
