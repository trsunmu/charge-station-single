package com.tian.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年11月10日 19:23
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 */
public class Charger implements Serializable {
    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "")
    private Integer id;

    @ApiModelProperty(value = "")
    private Integer stationId;

    @ApiModelProperty(value = "")
    private String chargerCode;

    @ApiModelProperty(value = "")
    private String name;

    @ApiModelProperty(value = "")
    private Integer chargerType;

    @ApiModelProperty(value = "")
    private Integer status;

    @ApiModelProperty(value = "")
    private String parkNo;

    @ApiModelProperty(value = "")
    private String manufacturer;

    @ApiModelProperty(value = "")
    private String voltage;

    @ApiModelProperty(value = "")
    private String chargerModel;

    @ApiModelProperty(value = "")
    private Integer electricityPriceId;

    @ApiModelProperty(value = "")
    private String power;

    @ApiModelProperty(value = "")
    private String supportCars;

    @ApiModelProperty(value = "")
    private BigDecimal price;

    @ApiModelProperty(value = "")
    private String description;

    @ApiModelProperty(value = "")
    private String qrcode;

    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id =  id;
    }
    @JsonProperty("stationId")
    public Integer getStationId() {
        return stationId;
    }

    public void setStationId(Integer stationId) {
        this.stationId =  stationId;
    }
    @JsonProperty("chargerCode")
    public String getChargerCode() {
        return chargerCode;
    }

    public void setChargerCode(String chargerCode) {
        this.chargerCode =  chargerCode;
    }
    @JsonProperty("name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name =  name;
    }
    @JsonProperty("chargerType")
    public Integer getChargerType() {
        return chargerType;
    }

    public void setChargerType(Integer chargerType) {
        this.chargerType =  chargerType;
    }
    @JsonProperty("status")
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status =  status;
    }
    @JsonProperty("parkNo")
    public String getParkNo() {
        return parkNo;
    }

    public void setParkNo(String parkNo) {
        this.parkNo =  parkNo;
    }
    @JsonProperty("manufacturer")
    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer =  manufacturer;
    }
    @JsonProperty("voltage")
    public String getVoltage() {
        return voltage;
    }

    public void setVoltage(String voltage) {
        this.voltage =  voltage;
    }
    @JsonProperty("chargerModel")
    public String getChargerModel() {
        return chargerModel;
    }

    public void setChargerModel(String chargerModel) {
        this.chargerModel =  chargerModel;
    }
    @JsonProperty("electricityPriceId")
    public Integer getElectricityPriceId() {
        return electricityPriceId;
    }

    public void setElectricityPriceId(Integer electricityPriceId) {
        this.electricityPriceId =  electricityPriceId;
    }
    @JsonProperty("power")
    public String getPower() {
        return power;
    }

    public void setPower(String power) {
        this.power =  power;
    }
    @JsonProperty("supportCars")
    public String getSupportCars() {
        return supportCars;
    }

    public void setSupportCars(String supportCars) {
        this.supportCars =  supportCars;
    }
    @JsonProperty("price")
    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price =  price;
    }
    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description =  description;
    }
    @JsonProperty("qrcode")
    public String getQrcode() {
        return qrcode;
    }

    public void setQrcode(String qrcode) {
        this.qrcode =  qrcode;
    }


    public Charger(Integer id,Integer stationId,String chargerCode,String name,Integer chargerType,Integer status,String parkNo,String manufacturer,String voltage,String chargerModel,Integer electricityPriceId,String power,String supportCars,BigDecimal price,String description,String qrcode) {

        this.id = id;

        this.stationId = stationId;

        this.chargerCode = chargerCode;

        this.name = name;

        this.chargerType = chargerType;

        this.status = status;

        this.parkNo = parkNo;

        this.manufacturer = manufacturer;

        this.voltage = voltage;

        this.chargerModel = chargerModel;

        this.electricityPriceId = electricityPriceId;

        this.power = power;

        this.supportCars = supportCars;

        this.price = price;

        this.description = description;

        this.qrcode = qrcode;

    }

    public Charger() {
        super();
    }
}