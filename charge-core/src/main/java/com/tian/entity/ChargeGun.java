package com.tian.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年11月10日 17:16
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 */
public class ChargeGun implements Serializable {
    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "")
    private Integer id;

    @ApiModelProperty(value = "")
    private Integer chargerId;

    @ApiModelProperty(value = "")
    private String name;

    @ApiModelProperty(value = "")
    private String gunNo;

    @ApiModelProperty(value = "")
    private String voltage;

    @ApiModelProperty(value = "")
    private String electricity;

    @ApiModelProperty(value = "")
    private String power;

    @ApiModelProperty(value = "")
    private Integer status;

    @ApiModelProperty(value = "")
    private String description;

    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id =  id;
    }
    @JsonProperty("chargerId")
    public Integer getChargerId() {
        return chargerId;
    }

    public void setChargerId(Integer chargerId) {
        this.chargerId =  chargerId;
    }
    @JsonProperty("name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name =  name;
    }
    @JsonProperty("gunNo")
    public String getGunNo() {
        return gunNo;
    }

    public void setGunNo(String gunNo) {
        this.gunNo =  gunNo;
    }
    @JsonProperty("voltage")
    public String getVoltage() {
        return voltage;
    }

    public void setVoltage(String voltage) {
        this.voltage =  voltage;
    }
    @JsonProperty("electricity")
    public String getElectricity() {
        return electricity;
    }

    public void setElectricity(String electricity) {
        this.electricity =  electricity;
    }
    @JsonProperty("power")
    public String getPower() {
        return power;
    }

    public void setPower(String power) {
        this.power =  power;
    }
    @JsonProperty("status")
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status =  status;
    }
    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description =  description;
    }


    public ChargeGun(Integer id,Integer chargerId,String name,String gunNo,String voltage,String electricity,String power,Integer status,String description) {

        this.id = id;

        this.chargerId = chargerId;

        this.name = name;

        this.gunNo = gunNo;

        this.voltage = voltage;

        this.electricity = electricity;

        this.power = power;

        this.status = status;

        this.description = description;

    }

    public ChargeGun() {
        super();
    }



}