package com.tian.entity;

import cn.hutool.core.date.DateUtil;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.Date;

public class GlobalProperty implements Serializable {
    private static final long serialVersionUID = 1L;

	
	@ApiModelProperty(value = "")
	private Integer id;
	
	@ApiModelProperty(value = "业务类型")
	private Integer busiType;
	
	@ApiModelProperty(value = "")
	private Integer busiDefault;
	
	@ApiModelProperty(value = "")
	private String busiDesc;
	
	@ApiModelProperty(value = "")
	private Integer status;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	@ApiModelProperty(value = "")
	private Date createTime;
	
	@JsonProperty("id")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id =  id;
	}
	@JsonProperty("busiType")
	public Integer getBusiType() {
		return busiType;
	}

	public void setBusiType(Integer busiType) {
		this.busiType =  busiType;
	}
	@JsonProperty("busiDefault")
	public Integer getBusiDefault() {
		return busiDefault;
	}

	public void setBusiDefault(Integer busiDefault) {
		this.busiDefault =  busiDefault;
	}
	@JsonProperty("busiDesc")
	public String getBusiDesc() {
		return busiDesc;
	}

	public void setBusiDesc(String busiDesc) {
		this.busiDesc =  busiDesc;
	}
	@JsonProperty("status")
	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status =  status;
	}
	@JsonProperty("createTime")
	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime =  createTime;
	}

												
	public GlobalProperty(Integer id,Integer busiType,Integer busiDefault,String busiDesc,Integer status,Date createTime) {
				
		this.id = id;
				
		this.busiType = busiType;
				
		this.busiDefault = busiDefault;
				
		this.busiDesc = busiDesc;
				
		this.status = status;
				
		this.createTime = createTime;
				
	}

	public GlobalProperty() {
	    super();
	}

	public String dateToStringConvert(Date date) {
		if(date!=null) {
			return DateUtil.format(date, "yyyy-MM-dd HH:mm:ss");
		}
		return "";
	}
	

}