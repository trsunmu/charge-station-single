package com.tian.entity;

import cn.hutool.core.date.DateUtil;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class ActivityGoods implements Serializable {
    private static final long serialVersionUID = 1L;

	
	@ApiModelProperty(value = "")
	private Integer id;
	
	@ApiModelProperty(value = "")
	private Integer activityId;
	
	@ApiModelProperty(value = "")
	private Integer goodsId;
	
	@ApiModelProperty(value = "")
	private Integer points;
	
	@ApiModelProperty(value = "")
	private BigDecimal money;
	
	@ApiModelProperty(value = "")
	private Integer type;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	@ApiModelProperty(value = "")
	private Date createTime;
	
	@JsonProperty("id")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id =  id;
	}
	@JsonProperty("activityId")
	public Integer getActivityId() {
		return activityId;
	}

	public void setActivityId(Integer activityId) {
		this.activityId =  activityId;
	}
	@JsonProperty("goodsId")
	public Integer getGoodsId() {
		return goodsId;
	}

	public void setGoodsId(Integer goodsId) {
		this.goodsId =  goodsId;
	}
	@JsonProperty("points")
	public Integer getPoints() {
		return points;
	}

	public void setPoints(Integer points) {
		this.points =  points;
	}
	@JsonProperty("money")
	public BigDecimal getMoney() {
		return money;
	}

	public void setMoney(BigDecimal money) {
		this.money =  money;
	}
	@JsonProperty("type")
	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type =  type;
	}
	@JsonProperty("createTime")
	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime =  createTime;
	}

														
	public ActivityGoods(Integer id,Integer activityId,Integer goodsId,Integer points,BigDecimal money,Integer type,Date createTime) {
				
		this.id = id;
				
		this.activityId = activityId;
				
		this.goodsId = goodsId;
				
		this.points = points;
				
		this.money = money;
				
		this.type = type;
				
		this.createTime = createTime;
				
	}

	public ActivityGoods() {
	    super();
	}

	public String dateToStringConvert(Date date) {
		if(date!=null) {
			return DateUtil.format(date, "yyyy-MM-dd HH:mm:ss");
		}
		return "";
	}
	

}