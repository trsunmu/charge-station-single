package com.tian.entity;

import cn.hutool.core.date.DateUtil;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年11月09日 22:27
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 * <p>
 * 用户信息
 */

public class Users implements Serializable {
    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "")
    private Integer id;

    @ApiModelProperty(value = "")
    private String nickName;

    @ApiModelProperty(value = "")
    private String realName;

    @ApiModelProperty(value = "")
    private String userPassword;

    @ApiModelProperty(value = "")
    private Integer userType;

    @ApiModelProperty(value = "")
    private Integer gender;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @ApiModelProperty(value = "")
    private Date birthday;

    @ApiModelProperty(value = "")
    private String avatarUrl;

    @ApiModelProperty(value = "")
    private String phone;

    @ApiModelProperty(value = "")
    private String idNo;

    @ApiModelProperty(value = "")
    private String province;

    @ApiModelProperty(value = "")
    private String city;

    @ApiModelProperty(value = "")
    private String area;

    @ApiModelProperty(value = "")
    private String detailAddress;

    @ApiModelProperty(value = "")
    private Integer status;

    @ApiModelProperty(value = "")
    private String email;

    @ApiModelProperty(value = "")
    private BigDecimal money;

    @ApiModelProperty(value = "")
    private Integer points;

    @ApiModelProperty(value = "")
    private String invitationCode;

    @ApiModelProperty(value = "")
    private String invitedCode;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @ApiModelProperty(value = "")
    private Date createTime;

    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @JsonProperty("nickName")
    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    @JsonProperty("realName")
    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    @JsonProperty("userPassword")
    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    @JsonProperty("userType")
    public Integer getUserType() {
        return userType;
    }

    public void setUserType(Integer userType) {
        this.userType = userType;
    }

    @JsonProperty("gender")
    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    @JsonProperty("birthday")
    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    @JsonProperty("avatarUrl")
    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    @JsonProperty("phone")
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @JsonProperty("idNo")
    public String getIdNo() {
        return idNo;
    }

    public void setIdNo(String idNo) {
        this.idNo = idNo;
    }

    @JsonProperty("province")
    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    @JsonProperty("city")
    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @JsonProperty("area")
    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    @JsonProperty("detailAddress")
    public String getDetailAddress() {
        return detailAddress;
    }

    public void setDetailAddress(String detailAddress) {
        this.detailAddress = detailAddress;
    }

    @JsonProperty("status")
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @JsonProperty("email")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @JsonProperty("money")
    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    @JsonProperty("points")
    public Integer getPoints() {
        return points;
    }

    public void setPoints(Integer points) {
        this.points = points;
    }

    @JsonProperty("invitationCode")
    public String getInvitationCode() {
        return invitationCode;
    }

    public void setInvitationCode(String invitationCode) {
        this.invitationCode = invitationCode;
    }

    @JsonProperty("invitedCode")
    public String getInvitedCode() {
        return invitedCode;
    }

    public void setInvitedCode(String invitedCode) {
        this.invitedCode = invitedCode;
    }

    @JsonProperty("createTime")
    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }


    public Users(Integer id, String nickName, String realName, String userPassword, Integer userType, Integer gender, Date birthday, String avatarUrl, String phone, String idNo, String province, String city, String area, String detailAddress, Integer status, String email, BigDecimal money, Integer points, String invitationCode, String invitedCode, Date createTime) {

        this.id = id;

        this.nickName = nickName;

        this.realName = realName;

        this.userPassword = userPassword;

        this.userType = userType;

        this.gender = gender;

        this.birthday = birthday;

        this.avatarUrl = avatarUrl;

        this.phone = phone;

        this.idNo = idNo;

        this.province = province;

        this.city = city;

        this.area = area;

        this.detailAddress = detailAddress;

        this.status = status;

        this.email = email;

        this.money = money;

        this.points = points;

        this.invitationCode = invitationCode;

        this.invitedCode = invitedCode;

        this.createTime = createTime;

    }

    public Users() {
        super();
    }

    public String dateToStringConvert(Date date) {
        if (date != null) {
            return DateUtil.format(date, "yyyy-MM-dd HH:mm:ss");
        }
        return "";
    }


}
