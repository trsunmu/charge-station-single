package com.tian.entity;

import cn.hutool.core.date.DateUtil;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.Date;

public class UserActivityGoods implements Serializable {
    private static final long serialVersionUID = 1L;

	
	@ApiModelProperty(value = "")
	private Integer id;
	
	@ApiModelProperty(value = "")
	private Integer userId;
	
	@ApiModelProperty(value = "")
	private Integer goodsInfoId;
	
	@ApiModelProperty(value = "")
	private Integer status;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	@ApiModelProperty(value = "")
	private Date usedTime;
	
	@JsonProperty("id")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id =  id;
	}
	@JsonProperty("userId")
	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId =  userId;
	}
	@JsonProperty("goodsInfoId")
	public Integer getGoodsInfoId() {
		return goodsInfoId;
	}

	public void setGoodsInfoId(Integer goodsInfoId) {
		this.goodsInfoId =  goodsInfoId;
	}
	@JsonProperty("status")
	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status =  status;
	}
	@JsonProperty("usedTime")
	public Date getUsedTime() {
		return usedTime;
	}

	public void setUsedTime(Date usedTime) {
		this.usedTime =  usedTime;
	}

										
	public UserActivityGoods(Integer id,Integer userId,Integer goodsInfoId,Integer status,Date usedTime) {
				
		this.id = id;
				
		this.userId = userId;
				
		this.goodsInfoId = goodsInfoId;
				
		this.status = status;
				
		this.usedTime = usedTime;
				
	}

	public UserActivityGoods() {
	    super();
	}

	public String dateToStringConvert(Date date) {
		if(date!=null) {
			return DateUtil.format(date, "yyyy-MM-dd HH:mm:ss");
		}
		return "";
	}
	

}