package com.tian.entity;

import cn.hutool.core.date.DateUtil;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class PayOrder implements Serializable {
    private static final long serialVersionUID = 1L;

	
	@ApiModelProperty(value = "")
	private Integer id;

	@ApiModelProperty(value = "")
	private Integer userCouponId;
	
	@ApiModelProperty(value = "")
	private String orderNo;
	
	@ApiModelProperty(value = "")
	private Integer businessType;
	
	@ApiModelProperty(value = "")
	private Integer chargerRecordId;
	
	@ApiModelProperty(value = "")
	private String payOrder;
	
	@ApiModelProperty(value = "")
	private BigDecimal money;
	
	@ApiModelProperty(value = "")
	private Integer status;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	@ApiModelProperty(value = "")
	private Date createTime;
	
	@JsonProperty("id")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id =  id;
	}
	@JsonProperty("orderNo")
	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo =  orderNo;
	}
	@JsonProperty("businessType")
	public Integer getBusinessType() {
		return businessType;
	}

	public void setBusinessType(Integer businessType) {
		this.businessType =  businessType;
	}
	@JsonProperty("chargerRecordId")
	public Integer getChargerRecordId() {
		return chargerRecordId;
	}

	public void setChargerRecordId(Integer chargerRecordId) {
		this.chargerRecordId =  chargerRecordId;
	}
	@JsonProperty("payOrder")
	public String getPayOrder() {
		return payOrder;
	}

	public void setPayOrder(String payOrder) {
		this.payOrder =  payOrder;
	}
	@JsonProperty("money")
	public BigDecimal getMoney() {
		return money;
	}

	public void setMoney(BigDecimal money) {
		this.money =  money;
	}
	@JsonProperty("status")
	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status =  status;
	}
	@JsonProperty("createTime")
	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime =  createTime;
	}

	public Integer getUserCouponId() {
		return userCouponId;
	}

	public void setUserCouponId(Integer userCouponId) {
		this.userCouponId = userCouponId;
	}

	public PayOrder() {
	    super();
	}

	public PayOrder(Integer id, Integer userCouponId, String orderNo, Integer businessType, Integer chargerRecordId, String payOrder, BigDecimal money, Integer status, Date createTime) {
		this.id = id;
		this.userCouponId = userCouponId;
		this.orderNo = orderNo;
		this.businessType = businessType;
		this.chargerRecordId = chargerRecordId;
		this.payOrder = payOrder;
		this.money = money;
		this.status = status;
		this.createTime = createTime;
	}

	public String dateToStringConvert(Date date) {
		if(date!=null) {
			return DateUtil.format(date, "yyyy-MM-dd HH:mm:ss");
		}
		return "";
	}
	

}