package com.tian.entity;

import cn.hutool.core.date.DateUtil;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年11月10日 16:56
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 */
public class Coupon implements Serializable {
    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "")
    private Integer id;

    @ApiModelProperty(value = "")
    private Integer couponType;

    @ApiModelProperty(value = "")
    private String couponCode;

    @ApiModelProperty(value = "")
    private String name;

    @ApiModelProperty(value = "")
    private BigDecimal couponValue;

    @ApiModelProperty(value = "")
    private Integer points;

    @ApiModelProperty(value = "")
    private Integer status;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    @ApiModelProperty(value = "")
    private Date startDate;

    private String startDateStr;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    @ApiModelProperty(value = "")
    private Date endDate;

    private String endDateStr;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    @ApiModelProperty(value = "")
    private Date createTime;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    @ApiModelProperty(value = "")
    private Date updateTime;

    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id =  id;
    }
    @JsonProperty("couponType")
    public Integer getCouponType() {
        return couponType;
    }

    public void setCouponType(Integer couponType) {
        this.couponType =  couponType;
    }
    @JsonProperty("couponCode")
    public String getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(String couponCode) {
        this.couponCode =  couponCode;
    }
    @JsonProperty("couponValue")
    public BigDecimal getCouponValue() {
        return couponValue;
    }

    public void setCouponValue(BigDecimal couponValue) {
        this.couponValue =  couponValue;
    }
    @JsonProperty("points")
    public Integer getPoints() {
        return points;
    }

    public void setPoints(Integer points) {
        this.points =  points;
    }
    @JsonProperty("status")
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status =  status;
    }
    @JsonProperty("startDate")
    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate =  startDate;
    }
    @JsonProperty("endDate")
    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate =  endDate;
    }
    @JsonProperty("createTime")
    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime =  createTime;
    }
    @JsonProperty("updateTime")
    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime =  updateTime;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStartDateStr() {
        return startDateStr;
    }

    public void setStartDateStr(String startDateStr) {
        this.startDateStr = startDateStr;
    }

    public String getEndDateStr() {
        return endDateStr;
    }

    public void setEndDateStr(String endDateStr) {
        this.endDateStr = endDateStr;
    }

    public Coupon() {
        super();
    }

    public Coupon(Integer id, Integer couponType, String couponCode, String name, BigDecimal couponValue, Integer points, Integer status, Date startDate, String startDateStr, Date endDate, String endDateStr, Date createTime, Date updateTime) {
        this.id = id;
        this.couponType = couponType;
        this.couponCode = couponCode;
        this.name = name;
        this.couponValue = couponValue;
        this.points = points;
        this.status = status;
        this.startDate = startDate;
        this.startDateStr = startDateStr;
        this.endDate = endDate;
        this.endDateStr = endDateStr;
        this.createTime = createTime;
        this.updateTime = updateTime;
    }

    public String dateToStringConvert(Date date) {
        if(date!=null) {
            return DateUtil.format(date, "yyyy-MM-dd HH:mm:ss");
        }
        return "";
    }


}
