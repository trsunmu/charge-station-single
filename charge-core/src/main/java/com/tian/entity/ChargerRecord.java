package com.tian.entity;

import cn.hutool.core.date.DateUtil;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年11月10日 10:23
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 */
public class ChargerRecord implements Serializable {
    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "")
    private Integer id;

    @ApiModelProperty(value = "")
    private Integer chargerGunId;

    @ApiModelProperty(value = "")
    private Integer userId;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    @ApiModelProperty(value = "")
    private Date startTime;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    @ApiModelProperty(value = "")
    private Date endTime;

    @ApiModelProperty(value = "")
    private BigDecimal quantity;

    @ApiModelProperty(value = "")
    private BigDecimal price;

    @ApiModelProperty(value = "")
    private BigDecimal money;

    @ApiModelProperty(value = "")
    private Integer payStatus;

    @ApiModelProperty(value = "")
    private Integer chargeStatus;

    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id =  id;
    }
    @JsonProperty("chargerGunId")
    public Integer getChargerGunId() {
        return chargerGunId;
    }

    public void setChargerGunId(Integer chargerGunId) {
        this.chargerGunId =  chargerGunId;
    }
    @JsonProperty("userId")
    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId =  userId;
    }
    @JsonProperty("startTime")
    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime =  startTime;
    }
    @JsonProperty("endTime")
    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime =  endTime;
    }
    @JsonProperty("quantity")
    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity =  quantity;
    }
    @JsonProperty("price")
    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price =  price;
    }
    @JsonProperty("money")
    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money =  money;
    }
    @JsonProperty("payStatus")
    public Integer getPayStatus() {
        return payStatus;
    }

    public void setPayStatus(Integer payStatus) {
        this.payStatus =  payStatus;
    }
    @JsonProperty("chargeStatus")
    public Integer getChargeStatus() {
        return chargeStatus;
    }

    public void setChargeStatus(Integer chargeStatus) {
        this.chargeStatus =  chargeStatus;
    }


    public ChargerRecord(Integer id,Integer chargerGunId,Integer userId,Date startTime,Date endTime,BigDecimal quantity,BigDecimal price,BigDecimal money,Integer payStatus,Integer chargeStatus) {

        this.id = id;

        this.chargerGunId = chargerGunId;

        this.userId = userId;

        this.startTime = startTime;

        this.endTime = endTime;

        this.quantity = quantity;

        this.price = price;

        this.money = money;

        this.payStatus = payStatus;

        this.chargeStatus = chargeStatus;

    }

    public ChargerRecord() {
        super();
    }

    public String dateToStringConvert(Date date) {
        if(date!=null) {
            return DateUtil.format(date, "yyyy-MM-dd HH:mm:ss");
        }
        return "";
    }


}