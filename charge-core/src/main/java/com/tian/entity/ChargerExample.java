package com.tian.entity;

import cn.hutool.core.util.StrUtil;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年11月10日 19:25
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 */
public class ChargerExample {

    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public ChargerExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }


        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLike(Integer value) {
            addCriterion("id like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotLike(Integer value) {
            addCriterion("id not like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }


        public Criteria andStationIdIsNull() {
            addCriterion("station_id is null");
            return (Criteria) this;
        }

        public Criteria andStationIdIsNotNull() {
            addCriterion("station_id is not null");
            return (Criteria) this;
        }

        public Criteria andStationIdEqualTo(Integer value) {
            addCriterion("station_id =", value, "stationId");
            return (Criteria) this;
        }

        public Criteria andStationIdNotEqualTo(Integer value) {
            addCriterion("station_id <>", value, "stationId");
            return (Criteria) this;
        }

        public Criteria andStationIdGreaterThan(Integer value) {
            addCriterion("station_id >", value, "stationId");
            return (Criteria) this;
        }

        public Criteria andStationIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("station_id >=", value, "stationId");
            return (Criteria) this;
        }

        public Criteria andStationIdLessThan(Integer value) {
            addCriterion("station_id <", value, "stationId");
            return (Criteria) this;
        }

        public Criteria andStationIdLessThanOrEqualTo(Integer value) {
            addCriterion("station_id <=", value, "stationId");
            return (Criteria) this;
        }

        public Criteria andStationIdLike(Integer value) {
            addCriterion("station_id like", value, "stationId");
            return (Criteria) this;
        }

        public Criteria andStationIdNotLike(Integer value) {
            addCriterion("station_id not like", value, "stationId");
            return (Criteria) this;
        }

        public Criteria andStationIdIn(List<Integer> values) {
            addCriterion("station_id in", values, "stationId");
            return (Criteria) this;
        }

        public Criteria andStationIdNotIn(List<Integer> values) {
            addCriterion("station_id not in", values, "stationId");
            return (Criteria) this;
        }

        public Criteria andStationIdBetween(Integer value1, Integer value2) {
            addCriterion("station_id between", value1, value2, "stationId");
            return (Criteria) this;
        }

        public Criteria andStationIdNotBetween(Integer value1, Integer value2) {
            addCriterion("station_id not between", value1, value2, "stationId");
            return (Criteria) this;
        }


        public Criteria andChargerCodeIsNull() {
            addCriterion("charger_code is null");
            return (Criteria) this;
        }

        public Criteria andChargerCodeIsNotNull() {
            addCriterion("charger_code is not null");
            return (Criteria) this;
        }

        public Criteria andChargerCodeEqualTo(String value) {
            addCriterion("charger_code =", value, "chargerCode");
            return (Criteria) this;
        }

        public Criteria andChargerCodeNotEqualTo(String value) {
            addCriterion("charger_code <>", value, "chargerCode");
            return (Criteria) this;
        }

        public Criteria andChargerCodeGreaterThan(String value) {
            addCriterion("charger_code >", value, "chargerCode");
            return (Criteria) this;
        }

        public Criteria andChargerCodeGreaterThanOrEqualTo(String value) {
            addCriterion("charger_code >=", value, "chargerCode");
            return (Criteria) this;
        }

        public Criteria andChargerCodeLessThan(String value) {
            addCriterion("charger_code <", value, "chargerCode");
            return (Criteria) this;
        }

        public Criteria andChargerCodeLessThanOrEqualTo(String value) {
            addCriterion("charger_code <=", value, "chargerCode");
            return (Criteria) this;
        }

        public Criteria andChargerCodeLike(String value) {
            addCriterion("charger_code like", value, "chargerCode");
            return (Criteria) this;
        }

        public Criteria andChargerCodeNotLike(String value) {
            addCriterion("charger_code not like", value, "chargerCode");
            return (Criteria) this;
        }

        public Criteria andChargerCodeIn(List<String> values) {
            addCriterion("charger_code in", values, "chargerCode");
            return (Criteria) this;
        }

        public Criteria andChargerCodeNotIn(List<String> values) {
            addCriterion("charger_code not in", values, "chargerCode");
            return (Criteria) this;
        }

        public Criteria andChargerCodeBetween(String value1, String value2) {
            addCriterion("charger_code between", value1, value2, "chargerCode");
            return (Criteria) this;
        }

        public Criteria andChargerCodeNotBetween(String value1, String value2) {
            addCriterion("charger_code not between", value1, value2, "chargerCode");
            return (Criteria) this;
        }


        public Criteria andNameIsNull() {
            addCriterion("name is null");
            return (Criteria) this;
        }

        public Criteria andNameIsNotNull() {
            addCriterion("name is not null");
            return (Criteria) this;
        }

        public Criteria andNameEqualTo(String value) {
            addCriterion("name =", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotEqualTo(String value) {
            addCriterion("name <>", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThan(String value) {
            addCriterion("name >", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThanOrEqualTo(String value) {
            addCriterion("name >=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThan(String value) {
            addCriterion("name <", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThanOrEqualTo(String value) {
            addCriterion("name <=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLike(String value) {
            addCriterion("name like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotLike(String value) {
            addCriterion("name not like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameIn(List<String> values) {
            addCriterion("name in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotIn(List<String> values) {
            addCriterion("name not in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameBetween(String value1, String value2) {
            addCriterion("name between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotBetween(String value1, String value2) {
            addCriterion("name not between", value1, value2, "name");
            return (Criteria) this;
        }


        public Criteria andChargerTypeIsNull() {
            addCriterion("charger_type is null");
            return (Criteria) this;
        }

        public Criteria andChargerTypeIsNotNull() {
            addCriterion("charger_type is not null");
            return (Criteria) this;
        }

        public Criteria andChargerTypeEqualTo(Integer value) {
            addCriterion("charger_type =", value, "chargerType");
            return (Criteria) this;
        }

        public Criteria andChargerTypeNotEqualTo(Integer value) {
            addCriterion("charger_type <>", value, "chargerType");
            return (Criteria) this;
        }

        public Criteria andChargerTypeGreaterThan(Integer value) {
            addCriterion("charger_type >", value, "chargerType");
            return (Criteria) this;
        }

        public Criteria andChargerTypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("charger_type >=", value, "chargerType");
            return (Criteria) this;
        }

        public Criteria andChargerTypeLessThan(Integer value) {
            addCriterion("charger_type <", value, "chargerType");
            return (Criteria) this;
        }

        public Criteria andChargerTypeLessThanOrEqualTo(Integer value) {
            addCriterion("charger_type <=", value, "chargerType");
            return (Criteria) this;
        }

        public Criteria andChargerTypeLike(Integer value) {
            addCriterion("charger_type like", value, "chargerType");
            return (Criteria) this;
        }

        public Criteria andChargerTypeNotLike(Integer value) {
            addCriterion("charger_type not like", value, "chargerType");
            return (Criteria) this;
        }

        public Criteria andChargerTypeIn(List<Integer> values) {
            addCriterion("charger_type in", values, "chargerType");
            return (Criteria) this;
        }

        public Criteria andChargerTypeNotIn(List<Integer> values) {
            addCriterion("charger_type not in", values, "chargerType");
            return (Criteria) this;
        }

        public Criteria andChargerTypeBetween(Integer value1, Integer value2) {
            addCriterion("charger_type between", value1, value2, "chargerType");
            return (Criteria) this;
        }

        public Criteria andChargerTypeNotBetween(Integer value1, Integer value2) {
            addCriterion("charger_type not between", value1, value2, "chargerType");
            return (Criteria) this;
        }


        public Criteria andStatusIsNull() {
            addCriterion("status is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("status is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(Integer value) {
            addCriterion("status =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(Integer value) {
            addCriterion("status <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(Integer value) {
            addCriterion("status >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("status >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(Integer value) {
            addCriterion("status <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(Integer value) {
            addCriterion("status <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLike(Integer value) {
            addCriterion("status like", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotLike(Integer value) {
            addCriterion("status not like", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<Integer> values) {
            addCriterion("status in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<Integer> values) {
            addCriterion("status not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(Integer value1, Integer value2) {
            addCriterion("status between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(Integer value1, Integer value2) {
            addCriterion("status not between", value1, value2, "status");
            return (Criteria) this;
        }


        public Criteria andParkNoIsNull() {
            addCriterion("park_no is null");
            return (Criteria) this;
        }

        public Criteria andParkNoIsNotNull() {
            addCriterion("park_no is not null");
            return (Criteria) this;
        }

        public Criteria andParkNoEqualTo(String value) {
            addCriterion("park_no =", value, "parkNo");
            return (Criteria) this;
        }

        public Criteria andParkNoNotEqualTo(String value) {
            addCriterion("park_no <>", value, "parkNo");
            return (Criteria) this;
        }

        public Criteria andParkNoGreaterThan(String value) {
            addCriterion("park_no >", value, "parkNo");
            return (Criteria) this;
        }

        public Criteria andParkNoGreaterThanOrEqualTo(String value) {
            addCriterion("park_no >=", value, "parkNo");
            return (Criteria) this;
        }

        public Criteria andParkNoLessThan(String value) {
            addCriterion("park_no <", value, "parkNo");
            return (Criteria) this;
        }

        public Criteria andParkNoLessThanOrEqualTo(String value) {
            addCriterion("park_no <=", value, "parkNo");
            return (Criteria) this;
        }

        public Criteria andParkNoLike(String value) {
            addCriterion("park_no like", value, "parkNo");
            return (Criteria) this;
        }

        public Criteria andParkNoNotLike(String value) {
            addCriterion("park_no not like", value, "parkNo");
            return (Criteria) this;
        }

        public Criteria andParkNoIn(List<String> values) {
            addCriterion("park_no in", values, "parkNo");
            return (Criteria) this;
        }

        public Criteria andParkNoNotIn(List<String> values) {
            addCriterion("park_no not in", values, "parkNo");
            return (Criteria) this;
        }

        public Criteria andParkNoBetween(String value1, String value2) {
            addCriterion("park_no between", value1, value2, "parkNo");
            return (Criteria) this;
        }

        public Criteria andParkNoNotBetween(String value1, String value2) {
            addCriterion("park_no not between", value1, value2, "parkNo");
            return (Criteria) this;
        }


        public Criteria andManufacturerIsNull() {
            addCriterion("manufacturer is null");
            return (Criteria) this;
        }

        public Criteria andManufacturerIsNotNull() {
            addCriterion("manufacturer is not null");
            return (Criteria) this;
        }

        public Criteria andManufacturerEqualTo(String value) {
            addCriterion("manufacturer =", value, "manufacturer");
            return (Criteria) this;
        }

        public Criteria andManufacturerNotEqualTo(String value) {
            addCriterion("manufacturer <>", value, "manufacturer");
            return (Criteria) this;
        }

        public Criteria andManufacturerGreaterThan(String value) {
            addCriterion("manufacturer >", value, "manufacturer");
            return (Criteria) this;
        }

        public Criteria andManufacturerGreaterThanOrEqualTo(String value) {
            addCriterion("manufacturer >=", value, "manufacturer");
            return (Criteria) this;
        }

        public Criteria andManufacturerLessThan(String value) {
            addCriterion("manufacturer <", value, "manufacturer");
            return (Criteria) this;
        }

        public Criteria andManufacturerLessThanOrEqualTo(String value) {
            addCriterion("manufacturer <=", value, "manufacturer");
            return (Criteria) this;
        }

        public Criteria andManufacturerLike(String value) {
            addCriterion("manufacturer like", value, "manufacturer");
            return (Criteria) this;
        }

        public Criteria andManufacturerNotLike(String value) {
            addCriterion("manufacturer not like", value, "manufacturer");
            return (Criteria) this;
        }

        public Criteria andManufacturerIn(List<String> values) {
            addCriterion("manufacturer in", values, "manufacturer");
            return (Criteria) this;
        }

        public Criteria andManufacturerNotIn(List<String> values) {
            addCriterion("manufacturer not in", values, "manufacturer");
            return (Criteria) this;
        }

        public Criteria andManufacturerBetween(String value1, String value2) {
            addCriterion("manufacturer between", value1, value2, "manufacturer");
            return (Criteria) this;
        }

        public Criteria andManufacturerNotBetween(String value1, String value2) {
            addCriterion("manufacturer not between", value1, value2, "manufacturer");
            return (Criteria) this;
        }


        public Criteria andVoltageIsNull() {
            addCriterion("voltage is null");
            return (Criteria) this;
        }

        public Criteria andVoltageIsNotNull() {
            addCriterion("voltage is not null");
            return (Criteria) this;
        }

        public Criteria andVoltageEqualTo(String value) {
            addCriterion("voltage =", value, "voltage");
            return (Criteria) this;
        }

        public Criteria andVoltageNotEqualTo(String value) {
            addCriterion("voltage <>", value, "voltage");
            return (Criteria) this;
        }

        public Criteria andVoltageGreaterThan(String value) {
            addCriterion("voltage >", value, "voltage");
            return (Criteria) this;
        }

        public Criteria andVoltageGreaterThanOrEqualTo(String value) {
            addCriterion("voltage >=", value, "voltage");
            return (Criteria) this;
        }

        public Criteria andVoltageLessThan(String value) {
            addCriterion("voltage <", value, "voltage");
            return (Criteria) this;
        }

        public Criteria andVoltageLessThanOrEqualTo(String value) {
            addCriterion("voltage <=", value, "voltage");
            return (Criteria) this;
        }

        public Criteria andVoltageLike(String value) {
            addCriterion("voltage like", value, "voltage");
            return (Criteria) this;
        }

        public Criteria andVoltageNotLike(String value) {
            addCriterion("voltage not like", value, "voltage");
            return (Criteria) this;
        }

        public Criteria andVoltageIn(List<String> values) {
            addCriterion("voltage in", values, "voltage");
            return (Criteria) this;
        }

        public Criteria andVoltageNotIn(List<String> values) {
            addCriterion("voltage not in", values, "voltage");
            return (Criteria) this;
        }

        public Criteria andVoltageBetween(String value1, String value2) {
            addCriterion("voltage between", value1, value2, "voltage");
            return (Criteria) this;
        }

        public Criteria andVoltageNotBetween(String value1, String value2) {
            addCriterion("voltage not between", value1, value2, "voltage");
            return (Criteria) this;
        }


        public Criteria andChargerModelIsNull() {
            addCriterion("charger_model is null");
            return (Criteria) this;
        }

        public Criteria andChargerModelIsNotNull() {
            addCriterion("charger_model is not null");
            return (Criteria) this;
        }

        public Criteria andChargerModelEqualTo(String value) {
            addCriterion("charger_model =", value, "chargerModel");
            return (Criteria) this;
        }

        public Criteria andChargerModelNotEqualTo(String value) {
            addCriterion("charger_model <>", value, "chargerModel");
            return (Criteria) this;
        }

        public Criteria andChargerModelGreaterThan(String value) {
            addCriterion("charger_model >", value, "chargerModel");
            return (Criteria) this;
        }

        public Criteria andChargerModelGreaterThanOrEqualTo(String value) {
            addCriterion("charger_model >=", value, "chargerModel");
            return (Criteria) this;
        }

        public Criteria andChargerModelLessThan(String value) {
            addCriterion("charger_model <", value, "chargerModel");
            return (Criteria) this;
        }

        public Criteria andChargerModelLessThanOrEqualTo(String value) {
            addCriterion("charger_model <=", value, "chargerModel");
            return (Criteria) this;
        }

        public Criteria andChargerModelLike(String value) {
            addCriterion("charger_model like", value, "chargerModel");
            return (Criteria) this;
        }

        public Criteria andChargerModelNotLike(String value) {
            addCriterion("charger_model not like", value, "chargerModel");
            return (Criteria) this;
        }

        public Criteria andChargerModelIn(List<String> values) {
            addCriterion("charger_model in", values, "chargerModel");
            return (Criteria) this;
        }

        public Criteria andChargerModelNotIn(List<String> values) {
            addCriterion("charger_model not in", values, "chargerModel");
            return (Criteria) this;
        }

        public Criteria andChargerModelBetween(String value1, String value2) {
            addCriterion("charger_model between", value1, value2, "chargerModel");
            return (Criteria) this;
        }

        public Criteria andChargerModelNotBetween(String value1, String value2) {
            addCriterion("charger_model not between", value1, value2, "chargerModel");
            return (Criteria) this;
        }


        public Criteria andElectricityPriceIdIsNull() {
            addCriterion("electricity_price_id is null");
            return (Criteria) this;
        }

        public Criteria andElectricityPriceIdIsNotNull() {
            addCriterion("electricity_price_id is not null");
            return (Criteria) this;
        }

        public Criteria andElectricityPriceIdEqualTo(Integer value) {
            addCriterion("electricity_price_id =", value, "electricityPriceId");
            return (Criteria) this;
        }

        public Criteria andElectricityPriceIdNotEqualTo(Integer value) {
            addCriterion("electricity_price_id <>", value, "electricityPriceId");
            return (Criteria) this;
        }

        public Criteria andElectricityPriceIdGreaterThan(Integer value) {
            addCriterion("electricity_price_id >", value, "electricityPriceId");
            return (Criteria) this;
        }

        public Criteria andElectricityPriceIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("electricity_price_id >=", value, "electricityPriceId");
            return (Criteria) this;
        }

        public Criteria andElectricityPriceIdLessThan(Integer value) {
            addCriterion("electricity_price_id <", value, "electricityPriceId");
            return (Criteria) this;
        }

        public Criteria andElectricityPriceIdLessThanOrEqualTo(Integer value) {
            addCriterion("electricity_price_id <=", value, "electricityPriceId");
            return (Criteria) this;
        }

        public Criteria andElectricityPriceIdLike(Integer value) {
            addCriterion("electricity_price_id like", value, "electricityPriceId");
            return (Criteria) this;
        }

        public Criteria andElectricityPriceIdNotLike(Integer value) {
            addCriterion("electricity_price_id not like", value, "electricityPriceId");
            return (Criteria) this;
        }

        public Criteria andElectricityPriceIdIn(List<Integer> values) {
            addCriterion("electricity_price_id in", values, "electricityPriceId");
            return (Criteria) this;
        }

        public Criteria andElectricityPriceIdNotIn(List<Integer> values) {
            addCriterion("electricity_price_id not in", values, "electricityPriceId");
            return (Criteria) this;
        }

        public Criteria andElectricityPriceIdBetween(Integer value1, Integer value2) {
            addCriterion("electricity_price_id between", value1, value2, "electricityPriceId");
            return (Criteria) this;
        }

        public Criteria andElectricityPriceIdNotBetween(Integer value1, Integer value2) {
            addCriterion("electricity_price_id not between", value1, value2, "electricityPriceId");
            return (Criteria) this;
        }


        public Criteria andPowerIsNull() {
            addCriterion("power is null");
            return (Criteria) this;
        }

        public Criteria andPowerIsNotNull() {
            addCriterion("power is not null");
            return (Criteria) this;
        }

        public Criteria andPowerEqualTo(String value) {
            addCriterion("power =", value, "power");
            return (Criteria) this;
        }

        public Criteria andPowerNotEqualTo(String value) {
            addCriterion("power <>", value, "power");
            return (Criteria) this;
        }

        public Criteria andPowerGreaterThan(String value) {
            addCriterion("power >", value, "power");
            return (Criteria) this;
        }

        public Criteria andPowerGreaterThanOrEqualTo(String value) {
            addCriterion("power >=", value, "power");
            return (Criteria) this;
        }

        public Criteria andPowerLessThan(String value) {
            addCriterion("power <", value, "power");
            return (Criteria) this;
        }

        public Criteria andPowerLessThanOrEqualTo(String value) {
            addCriterion("power <=", value, "power");
            return (Criteria) this;
        }

        public Criteria andPowerLike(String value) {
            addCriterion("power like", value, "power");
            return (Criteria) this;
        }

        public Criteria andPowerNotLike(String value) {
            addCriterion("power not like", value, "power");
            return (Criteria) this;
        }

        public Criteria andPowerIn(List<String> values) {
            addCriterion("power in", values, "power");
            return (Criteria) this;
        }

        public Criteria andPowerNotIn(List<String> values) {
            addCriterion("power not in", values, "power");
            return (Criteria) this;
        }

        public Criteria andPowerBetween(String value1, String value2) {
            addCriterion("power between", value1, value2, "power");
            return (Criteria) this;
        }

        public Criteria andPowerNotBetween(String value1, String value2) {
            addCriterion("power not between", value1, value2, "power");
            return (Criteria) this;
        }


        public Criteria andSupportCarsIsNull() {
            addCriterion("support_cars is null");
            return (Criteria) this;
        }

        public Criteria andSupportCarsIsNotNull() {
            addCriterion("support_cars is not null");
            return (Criteria) this;
        }

        public Criteria andSupportCarsEqualTo(String value) {
            addCriterion("support_cars =", value, "supportCars");
            return (Criteria) this;
        }

        public Criteria andSupportCarsNotEqualTo(String value) {
            addCriterion("support_cars <>", value, "supportCars");
            return (Criteria) this;
        }

        public Criteria andSupportCarsGreaterThan(String value) {
            addCriterion("support_cars >", value, "supportCars");
            return (Criteria) this;
        }

        public Criteria andSupportCarsGreaterThanOrEqualTo(String value) {
            addCriterion("support_cars >=", value, "supportCars");
            return (Criteria) this;
        }

        public Criteria andSupportCarsLessThan(String value) {
            addCriterion("support_cars <", value, "supportCars");
            return (Criteria) this;
        }

        public Criteria andSupportCarsLessThanOrEqualTo(String value) {
            addCriterion("support_cars <=", value, "supportCars");
            return (Criteria) this;
        }

        public Criteria andSupportCarsLike(String value) {
            addCriterion("support_cars like", value, "supportCars");
            return (Criteria) this;
        }

        public Criteria andSupportCarsNotLike(String value) {
            addCriterion("support_cars not like", value, "supportCars");
            return (Criteria) this;
        }

        public Criteria andSupportCarsIn(List<String> values) {
            addCriterion("support_cars in", values, "supportCars");
            return (Criteria) this;
        }

        public Criteria andSupportCarsNotIn(List<String> values) {
            addCriterion("support_cars not in", values, "supportCars");
            return (Criteria) this;
        }

        public Criteria andSupportCarsBetween(String value1, String value2) {
            addCriterion("support_cars between", value1, value2, "supportCars");
            return (Criteria) this;
        }

        public Criteria andSupportCarsNotBetween(String value1, String value2) {
            addCriterion("support_cars not between", value1, value2, "supportCars");
            return (Criteria) this;
        }


        public Criteria andPriceIsNull() {
            addCriterion("price is null");
            return (Criteria) this;
        }

        public Criteria andPriceIsNotNull() {
            addCriterion("price is not null");
            return (Criteria) this;
        }

        public Criteria andPriceEqualTo(BigDecimal value) {
            addCriterion("price =", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceNotEqualTo(BigDecimal value) {
            addCriterion("price <>", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceGreaterThan(BigDecimal value) {
            addCriterion("price >", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("price >=", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceLessThan(BigDecimal value) {
            addCriterion("price <", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceLessThanOrEqualTo(BigDecimal value) {
            addCriterion("price <=", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceLike(BigDecimal value) {
            addCriterion("price like", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceNotLike(BigDecimal value) {
            addCriterion("price not like", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceIn(List<BigDecimal> values) {
            addCriterion("price in", values, "price");
            return (Criteria) this;
        }

        public Criteria andPriceNotIn(List<BigDecimal> values) {
            addCriterion("price not in", values, "price");
            return (Criteria) this;
        }

        public Criteria andPriceBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("price between", value1, value2, "price");
            return (Criteria) this;
        }

        public Criteria andPriceNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("price not between", value1, value2, "price");
            return (Criteria) this;
        }


        public Criteria andDescriptionIsNull() {
            addCriterion("description is null");
            return (Criteria) this;
        }

        public Criteria andDescriptionIsNotNull() {
            addCriterion("description is not null");
            return (Criteria) this;
        }

        public Criteria andDescriptionEqualTo(String value) {
            addCriterion("description =", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionNotEqualTo(String value) {
            addCriterion("description <>", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionGreaterThan(String value) {
            addCriterion("description >", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionGreaterThanOrEqualTo(String value) {
            addCriterion("description >=", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionLessThan(String value) {
            addCriterion("description <", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionLessThanOrEqualTo(String value) {
            addCriterion("description <=", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionLike(String value) {
            addCriterion("description like", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionNotLike(String value) {
            addCriterion("description not like", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionIn(List<String> values) {
            addCriterion("description in", values, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionNotIn(List<String> values) {
            addCriterion("description not in", values, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionBetween(String value1, String value2) {
            addCriterion("description between", value1, value2, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionNotBetween(String value1, String value2) {
            addCriterion("description not between", value1, value2, "description");
            return (Criteria) this;
        }


        public Criteria andQrcodeIsNull() {
            addCriterion("qrcode is null");
            return (Criteria) this;
        }

        public Criteria andQrcodeIsNotNull() {
            addCriterion("qrcode is not null");
            return (Criteria) this;
        }

        public Criteria andQrcodeEqualTo(String value) {
            addCriterion("qrcode =", value, "qrcode");
            return (Criteria) this;
        }

        public Criteria andQrcodeNotEqualTo(String value) {
            addCriterion("qrcode <>", value, "qrcode");
            return (Criteria) this;
        }

        public Criteria andQrcodeGreaterThan(String value) {
            addCriterion("qrcode >", value, "qrcode");
            return (Criteria) this;
        }

        public Criteria andQrcodeGreaterThanOrEqualTo(String value) {
            addCriterion("qrcode >=", value, "qrcode");
            return (Criteria) this;
        }

        public Criteria andQrcodeLessThan(String value) {
            addCriterion("qrcode <", value, "qrcode");
            return (Criteria) this;
        }

        public Criteria andQrcodeLessThanOrEqualTo(String value) {
            addCriterion("qrcode <=", value, "qrcode");
            return (Criteria) this;
        }

        public Criteria andQrcodeLike(String value) {
            addCriterion("qrcode like", value, "qrcode");
            return (Criteria) this;
        }

        public Criteria andQrcodeNotLike(String value) {
            addCriterion("qrcode not like", value, "qrcode");
            return (Criteria) this;
        }

        public Criteria andQrcodeIn(List<String> values) {
            addCriterion("qrcode in", values, "qrcode");
            return (Criteria) this;
        }

        public Criteria andQrcodeNotIn(List<String> values) {
            addCriterion("qrcode not in", values, "qrcode");
            return (Criteria) this;
        }

        public Criteria andQrcodeBetween(String value1, String value2) {
            addCriterion("qrcode between", value1, value2, "qrcode");
            return (Criteria) this;
        }

        public Criteria andQrcodeNotBetween(String value1, String value2) {
            addCriterion("qrcode not between", value1, value2, "qrcode");
            return (Criteria) this;
        }


        public Criteria andLikeQuery(Charger record) {
            List<String> list= new ArrayList<String>();
            List<String> list2= new ArrayList<String>();
            StringBuffer buffer=new StringBuffer();
            if(record.getId()!=null&&StrUtil.isNotEmpty(record.getId().toString())) {
                list.add("ifnull(id,'')");
            }
            if(record.getStationId()!=null&&StrUtil.isNotEmpty(record.getStationId().toString())) {
                list.add("ifnull(station_id,'')");
            }
            if(record.getChargerCode()!=null&&StrUtil.isNotEmpty(record.getChargerCode().toString())) {
                list.add("ifnull(charger_code,'')");
            }
            if(record.getName()!=null&&StrUtil.isNotEmpty(record.getName().toString())) {
                list.add("ifnull(name,'')");
            }
            if(record.getChargerType()!=null&&StrUtil.isNotEmpty(record.getChargerType().toString())) {
                list.add("ifnull(charger_type,'')");
            }
            if(record.getStatus()!=null&&StrUtil.isNotEmpty(record.getStatus().toString())) {
                list.add("ifnull(status,'')");
            }
            if(record.getParkNo()!=null&&StrUtil.isNotEmpty(record.getParkNo().toString())) {
                list.add("ifnull(park_no,'')");
            }
            if(record.getManufacturer()!=null&&StrUtil.isNotEmpty(record.getManufacturer().toString())) {
                list.add("ifnull(manufacturer,'')");
            }
            if(record.getVoltage()!=null&&StrUtil.isNotEmpty(record.getVoltage().toString())) {
                list.add("ifnull(voltage,'')");
            }
            if(record.getChargerModel()!=null&&StrUtil.isNotEmpty(record.getChargerModel().toString())) {
                list.add("ifnull(charger_model,'')");
            }
            if(record.getElectricityPriceId()!=null&&StrUtil.isNotEmpty(record.getElectricityPriceId().toString())) {
                list.add("ifnull(electricity_price_id,'')");
            }
            if(record.getPower()!=null&&StrUtil.isNotEmpty(record.getPower().toString())) {
                list.add("ifnull(power,'')");
            }
            if(record.getSupportCars()!=null&&StrUtil.isNotEmpty(record.getSupportCars().toString())) {
                list.add("ifnull(support_cars,'')");
            }
            if(record.getPrice()!=null&&StrUtil.isNotEmpty(record.getPrice().toString())) {
                list.add("ifnull(price,'')");
            }
            if(record.getDescription()!=null&&StrUtil.isNotEmpty(record.getDescription().toString())) {
                list.add("ifnull(description,'')");
            }
            if(record.getQrcode()!=null&&StrUtil.isNotEmpty(record.getQrcode().toString())) {
                list.add("ifnull(qrcode,'')");
            }
            if(record.getId()!=null&&StrUtil.isNotEmpty(record.getId().toString())) {
                list2.add("'%"+record.getId()+"%'");
            }
            if(record.getStationId()!=null&&StrUtil.isNotEmpty(record.getStationId().toString())) {
                list2.add("'%"+record.getStationId()+"%'");
            }
            if(record.getChargerCode()!=null&&StrUtil.isNotEmpty(record.getChargerCode().toString())) {
                list2.add("'%"+record.getChargerCode()+"%'");
            }
            if(record.getName()!=null&&StrUtil.isNotEmpty(record.getName().toString())) {
                list2.add("'%"+record.getName()+"%'");
            }
            if(record.getChargerType()!=null&&StrUtil.isNotEmpty(record.getChargerType().toString())) {
                list2.add("'%"+record.getChargerType()+"%'");
            }
            if(record.getStatus()!=null&&StrUtil.isNotEmpty(record.getStatus().toString())) {
                list2.add("'%"+record.getStatus()+"%'");
            }
            if(record.getParkNo()!=null&&StrUtil.isNotEmpty(record.getParkNo().toString())) {
                list2.add("'%"+record.getParkNo()+"%'");
            }
            if(record.getManufacturer()!=null&&StrUtil.isNotEmpty(record.getManufacturer().toString())) {
                list2.add("'%"+record.getManufacturer()+"%'");
            }
            if(record.getVoltage()!=null&&StrUtil.isNotEmpty(record.getVoltage().toString())) {
                list2.add("'%"+record.getVoltage()+"%'");
            }
            if(record.getChargerModel()!=null&&StrUtil.isNotEmpty(record.getChargerModel().toString())) {
                list2.add("'%"+record.getChargerModel()+"%'");
            }
            if(record.getElectricityPriceId()!=null&&StrUtil.isNotEmpty(record.getElectricityPriceId().toString())) {
                list2.add("'%"+record.getElectricityPriceId()+"%'");
            }
            if(record.getPower()!=null&&StrUtil.isNotEmpty(record.getPower().toString())) {
                list2.add("'%"+record.getPower()+"%'");
            }
            if(record.getSupportCars()!=null&&StrUtil.isNotEmpty(record.getSupportCars().toString())) {
                list2.add("'%"+record.getSupportCars()+"%'");
            }
            if(record.getPrice()!=null&&StrUtil.isNotEmpty(record.getPrice().toString())) {
                list2.add("'%"+record.getPrice()+"%'");
            }
            if(record.getDescription()!=null&&StrUtil.isNotEmpty(record.getDescription().toString())) {
                list2.add("'%"+record.getDescription()+"%'");
            }
            if(record.getQrcode()!=null&&StrUtil.isNotEmpty(record.getQrcode().toString())) {
                list2.add("'%"+record.getQrcode()+"%'");
            }
            buffer.append(" CONCAT(");
            buffer.append(StrUtil.join(",",list));
            buffer.append(")");
            buffer.append(" like CONCAT(");
            buffer.append(StrUtil.join(",",list2));
            buffer.append(")");
            if(!" CONCAT() like CONCAT()".equals(buffer.toString())) {
                addCriterion(buffer.toString());
            }
            return (Criteria) this;
        }

        public Criteria andLikeQuery2(String searchText) {
            List<String> list= new ArrayList<String>();
            StringBuffer buffer=new StringBuffer();
            list.add("ifnull(id,'')");
            list.add("ifnull(station_id,'')");
            list.add("ifnull(charger_code,'')");
            list.add("ifnull(name,'')");
            list.add("ifnull(charger_type,'')");
            list.add("ifnull(status,'')");
            list.add("ifnull(park_no,'')");
            list.add("ifnull(manufacturer,'')");
            list.add("ifnull(voltage,'')");
            list.add("ifnull(charger_model,'')");
            list.add("ifnull(electricity_price_id,'')");
            list.add("ifnull(power,'')");
            list.add("ifnull(support_cars,'')");
            list.add("ifnull(price,'')");
            list.add("ifnull(description,'')");
            list.add("ifnull(qrcode,'')");
            buffer.append(" CONCAT(");
            buffer.append(StrUtil.join(",",list));
            buffer.append(")");
            buffer.append("like '%");
            buffer.append(searchText);
            buffer.append("%'");
            addCriterion(buffer.toString());
            return (Criteria) this;
        }

    }

    public static class Criteria extends GeneratedCriteria {
        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}