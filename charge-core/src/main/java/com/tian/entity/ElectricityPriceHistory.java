package com.tian.entity;

import cn.hutool.core.date.DateUtil;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class ElectricityPriceHistory implements Serializable {
    private static final long serialVersionUID = 1L;

	
	@ApiModelProperty(value = "")
	private Integer id;
	
	@ApiModelProperty(value = "")
	private BigDecimal prcie;
	
	@ApiModelProperty(value = "")
	private Integer unitType;
	
	@ApiModelProperty(value = "")
	private BigDecimal unitValue;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	@ApiModelProperty(value = "")
	private Date startTime;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	@ApiModelProperty(value = "")
	private Date endTime;
	
	@JsonProperty("id")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id =  id;
	}
	@JsonProperty("prcie")
	public BigDecimal getPrcie() {
		return prcie;
	}

	public void setPrcie(BigDecimal prcie) {
		this.prcie =  prcie;
	}
	@JsonProperty("unitType")
	public Integer getUnitType() {
		return unitType;
	}

	public void setUnitType(Integer unitType) {
		this.unitType =  unitType;
	}
	@JsonProperty("unitValue")
	public BigDecimal getUnitValue() {
		return unitValue;
	}

	public void setUnitValue(BigDecimal unitValue) {
		this.unitValue =  unitValue;
	}
	@JsonProperty("startTime")
	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime =  startTime;
	}
	@JsonProperty("endTime")
	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime =  endTime;
	}

												
	public ElectricityPriceHistory(Integer id,BigDecimal prcie,Integer unitType,BigDecimal unitValue,Date startTime,Date endTime) {
				
		this.id = id;
				
		this.prcie = prcie;
				
		this.unitType = unitType;
				
		this.unitValue = unitValue;
				
		this.startTime = startTime;
				
		this.endTime = endTime;
				
	}

	public ElectricityPriceHistory() {
	    super();
	}

	public String dateToStringConvert(Date date) {
		if(date!=null) {
			return DateUtil.format(date, "yyyy-MM-dd HH:mm:ss");
		}
		return "";
	}
	

}