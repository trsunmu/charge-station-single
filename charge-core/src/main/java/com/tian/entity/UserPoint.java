package com.tian.entity;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

@Data
@ToString
public class UserPoint implements Serializable {
    private Long id;

    private Long userId;

    private Integer points;

    private Integer status;

    private Date createTime;

    private static final long serialVersionUID = 1L;

}