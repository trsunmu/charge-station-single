package com.tian.entity;

import cn.hutool.core.date.DateUtil;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class InvitedRegisterRecord implements Serializable {
    private static final long serialVersionUID = 1L;

	
	@ApiModelProperty(value = "")
	private Integer id;
	
	@ApiModelProperty(value = "")
	private Integer userId;
	
	@ApiModelProperty(value = "")
	private Integer invitationUserId;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	@ApiModelProperty(value = "")
	private Date createTime;
	
	@ApiModelProperty(value = "")
	private BigDecimal income;
	
	@JsonProperty("id")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id =  id;
	}
	@JsonProperty("userId")
	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId =  userId;
	}
	@JsonProperty("invitationUserId")
	public Integer getInvitationUserId() {
		return invitationUserId;
	}

	public void setInvitationUserId(Integer invitationUserId) {
		this.invitationUserId =  invitationUserId;
	}
	@JsonProperty("createTime")
	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime =  createTime;
	}
	@JsonProperty("income")
	public BigDecimal getIncome() {
		return income;
	}

	public void setIncome(BigDecimal income) {
		this.income =  income;
	}

										
	public InvitedRegisterRecord(Integer id,Integer userId,Integer invitationUserId,Date createTime,BigDecimal income) {
				
		this.id = id;
				
		this.userId = userId;
				
		this.invitationUserId = invitationUserId;
				
		this.createTime = createTime;
				
		this.income = income;
				
	}

	public InvitedRegisterRecord() {
	    super();
	}

	public String dateToStringConvert(Date date) {
		if(date!=null) {
			return DateUtil.format(date, "yyyy-MM-dd HH:mm:ss");
		}
		return "";
	}
	

}