package com.tian.entity;

import cn.hutool.core.date.DateUtil;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.Date;

public class Activity implements Serializable {
    private static final long serialVersionUID = 1L;

	
	@ApiModelProperty(value = "活动ID")
	private Integer id;
	
	@ApiModelProperty(value = "站点id")
	private Long stationsId;
	
	@ApiModelProperty(value = "活动名称")
	private String name;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	@ApiModelProperty(value = "活动开始时间")
	private Date startTime;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	@ApiModelProperty(value = "活动结束时间")
	private Date endTime;
	
	@ApiModelProperty(value = "活动描述")
	private String description;
	
	@ApiModelProperty(value = "活动状态，1表示进行中，0表示已结束")
	private Integer status;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	@ApiModelProperty(value = "")
	private Date createTime;
	
	@ApiModelProperty(value = "")
	private Integer type;
	
	@JsonProperty("id")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id =  id;
	}
	@JsonProperty("stationsId")
	public Long getStationsId() {
		return stationsId;
	}

	public void setStationsId(Long stationsId) {
		this.stationsId =  stationsId;
	}
	@JsonProperty("name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name =  name;
	}
	@JsonProperty("startTime")
	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime =  startTime;
	}
	@JsonProperty("endTime")
	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime =  endTime;
	}
	@JsonProperty("description")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description =  description;
	}
	@JsonProperty("status")
	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status =  status;
	}
	@JsonProperty("createTime")
	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime =  createTime;
	}
	@JsonProperty("type")
	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type =  type;
	}

																		
	public Activity(Integer id,Long stationsId,String name,Date startTime,Date endTime,String description,Integer status,Date createTime,Integer type) {
				
		this.id = id;
				
		this.stationsId = stationsId;
				
		this.name = name;
				
		this.startTime = startTime;
				
		this.endTime = endTime;
				
		this.description = description;
				
		this.status = status;
				
		this.createTime = createTime;
				
		this.type = type;
				
	}

	public Activity() {
	    super();
	}

	public String dateToStringConvert(Date date) {
		if(date!=null) {
			return DateUtil.format(date, "yyyy-MM-dd HH:mm:ss");
		}
		return "";
	}
	

}