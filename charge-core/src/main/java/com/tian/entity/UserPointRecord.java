package com.tian.entity;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

@Data
@ToString
public class UserPointRecord implements Serializable {
    private Long id;

    private Long userId;

    private Integer point;

    private Integer status;

    private String orderNo;

    private Integer type;

    private Date createTime;

    private Date updateTime;

    private static final long serialVersionUID = 1L;

}