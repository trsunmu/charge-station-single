package com.tian.entity;

import cn.hutool.core.date.DateUtil;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.Date;

public class ExchangeGoodsRecord implements Serializable {
    private static final long serialVersionUID = 1L;

	
	@ApiModelProperty(value = "")
	private Integer id;
	
	@ApiModelProperty(value = "")
	private Integer userId;
	
	@ApiModelProperty(value = "")
	private Integer activityGoodsId;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	@ApiModelProperty(value = "")
	private Date createTime;
	
	@JsonProperty("id")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id =  id;
	}
	@JsonProperty("userId")
	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId =  userId;
	}
	@JsonProperty("activityGoodsId")
	public Integer getActivityGoodsId() {
		return activityGoodsId;
	}

	public void setActivityGoodsId(Integer activityGoodsId) {
		this.activityGoodsId =  activityGoodsId;
	}
	@JsonProperty("createTime")
	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime =  createTime;
	}

								
	public ExchangeGoodsRecord(Integer id,Integer userId,Integer activityGoodsId,Date createTime) {
				
		this.id = id;
				
		this.userId = userId;
				
		this.activityGoodsId = activityGoodsId;
				
		this.createTime = createTime;
				
	}

	public ExchangeGoodsRecord() {
	    super();
	}

	public String dateToStringConvert(Date date) {
		if(date!=null) {
			return DateUtil.format(date, "yyyy-MM-dd HH:mm:ss");
		}
		return "";
	}
	

}