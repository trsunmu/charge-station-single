package com.tian.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

public class InvestorDetail implements Serializable {
    private static final long serialVersionUID = 1L;

	
	@ApiModelProperty(value = "")
	private Integer id;
	
	@ApiModelProperty(value = "用户表主键")
	private Integer userId;
	
	@ApiModelProperty(value = "")
	private String businessLicenseUrl;
	
	@ApiModelProperty(value = "")
	private String contractUrl;
	
	@ApiModelProperty(value = "")
	private String idCradFrontUrl;
	
	@ApiModelProperty(value = "")
	private String idCradReverseUrl;
	
	@ApiModelProperty(value = "")
	private String bankCardNo;
	
	@ApiModelProperty(value = "")
	private String bankAccount;
	
	@ApiModelProperty(value = "")
	private String openBank;
	
	@ApiModelProperty(value = "")
	private Integer status;
	
	@JsonProperty("id")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id =  id;
	}
	@JsonProperty("userId")
	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId =  userId;
	}
	@JsonProperty("businessLicenseUrl")
	public String getBusinessLicenseUrl() {
		return businessLicenseUrl;
	}

	public void setBusinessLicenseUrl(String businessLicenseUrl) {
		this.businessLicenseUrl =  businessLicenseUrl;
	}
	@JsonProperty("contractUrl")
	public String getContractUrl() {
		return contractUrl;
	}

	public void setContractUrl(String contractUrl) {
		this.contractUrl =  contractUrl;
	}
	@JsonProperty("idCradFrontUrl")
	public String getIdCradFrontUrl() {
		return idCradFrontUrl;
	}

	public void setIdCradFrontUrl(String idCradFrontUrl) {
		this.idCradFrontUrl =  idCradFrontUrl;
	}
	@JsonProperty("idCradReverseUrl")
	public String getIdCradReverseUrl() {
		return idCradReverseUrl;
	}

	public void setIdCradReverseUrl(String idCradReverseUrl) {
		this.idCradReverseUrl =  idCradReverseUrl;
	}
	@JsonProperty("bankCardNo")
	public String getBankCardNo() {
		return bankCardNo;
	}

	public void setBankCardNo(String bankCardNo) {
		this.bankCardNo =  bankCardNo;
	}
	@JsonProperty("bankAccount")
	public String getBankAccount() {
		return bankAccount;
	}

	public void setBankAccount(String bankAccount) {
		this.bankAccount =  bankAccount;
	}
	@JsonProperty("openBank")
	public String getOpenBank() {
		return openBank;
	}

	public void setOpenBank(String openBank) {
		this.openBank =  openBank;
	}
	@JsonProperty("status")
	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status =  status;
	}

																				
	public InvestorDetail(Integer id,Integer userId,String businessLicenseUrl,String contractUrl,String idCradFrontUrl,String idCradReverseUrl,String bankCardNo,String bankAccount,String openBank,Integer status) {
				
		this.id = id;
				
		this.userId = userId;
				
		this.businessLicenseUrl = businessLicenseUrl;
				
		this.contractUrl = contractUrl;
				
		this.idCradFrontUrl = idCradFrontUrl;
				
		this.idCradReverseUrl = idCradReverseUrl;
				
		this.bankCardNo = bankCardNo;
				
		this.bankAccount = bankAccount;
				
		this.openBank = openBank;
				
		this.status = status;
				
	}

	public InvestorDetail() {
	    super();
	}

	

}