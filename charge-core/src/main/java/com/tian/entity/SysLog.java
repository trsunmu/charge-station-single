package com.tian.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class SysLog implements Serializable {
    private Long id;

    private String methodDesc;

    private Date createTime;

    private String methodName;

    private Long costTime;

    private String requestParam;

    private String responseParam;

    private String url;

    private String ip;

    private static final long serialVersionUID = 1L;

}