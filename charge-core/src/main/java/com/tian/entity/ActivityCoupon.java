package com.tian.entity;

import cn.hutool.core.date.DateUtil;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.Date;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年11月11日 15:49
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 */
public class ActivityCoupon implements Serializable {
    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "")
    private Integer id;

    @ApiModelProperty(value = "")
    private Integer couponId;

    @ApiModelProperty(value = "")
    private Integer activityId;

    @ApiModelProperty(value = "")
    private Integer limitPerUser;

    @ApiModelProperty(value = "")
    private Integer totalCount;

    @ApiModelProperty(value = "")
    private Integer remainCount;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    @ApiModelProperty(value = "")
    private Date createTime;

    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id =  id;
    }
    @JsonProperty("couponId")
    public Integer getCouponId() {
        return couponId;
    }

    public void setCouponId(Integer couponId) {
        this.couponId =  couponId;
    }
    @JsonProperty("activityId")
    public Integer getActivityId() {
        return activityId;
    }

    public void setActivityId(Integer activityId) {
        this.activityId =  activityId;
    }
    @JsonProperty("limitPerUser")
    public Integer getLimitPerUser() {
        return limitPerUser;
    }

    public void setLimitPerUser(Integer limitPerUser) {
        this.limitPerUser =  limitPerUser;
    }
    @JsonProperty("totalCount")
    public Integer getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Integer totalCount) {
        this.totalCount =  totalCount;
    }
    @JsonProperty("remainCount")
    public Integer getRemainCount() {
        return remainCount;
    }

    public void setRemainCount(Integer remainCount) {
        this.remainCount =  remainCount;
    }
    @JsonProperty("createTime")
    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime =  createTime;
    }


    public ActivityCoupon(Integer id,Integer couponId,Integer activityId,Integer limitPerUser,Integer totalCount,Integer remainCount,Date createTime) {

        this.id = id;

        this.couponId = couponId;

        this.activityId = activityId;

        this.limitPerUser = limitPerUser;

        this.totalCount = totalCount;

        this.remainCount = remainCount;

        this.createTime = createTime;

    }

    public ActivityCoupon() {
        super();
    }

    public String dateToStringConvert(Date date) {
        if(date!=null) {
            return DateUtil.format(date, "yyyy-MM-dd HH:mm:ss");
        }
        return "";
    }


}