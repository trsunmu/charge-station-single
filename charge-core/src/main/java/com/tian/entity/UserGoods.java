package com.tian.entity;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

@ToString
@Data
public class UserGoods implements Serializable {
    private Integer id;

    private Integer goodsId;

    private Integer userId;

    private Integer count;

    private Integer status;

    private String goodsName;

    private Date createTime;

    private static final long serialVersionUID = 1L;

}