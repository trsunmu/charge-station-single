package com.tian.entity;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
public class User implements Serializable {
    private Integer id;
    private String nickName;
    private String realName;
    private String userPassword;
    private Integer userType;
    private Integer gender;
    private Date birthday;
    private String avatarUrl;
    private String twoDimensionalCodeUrl;
    private String phone;
    private String idNo;
    private String province;
    private String city;
    private String area;
    private String detailAddress;
    private Integer status;
    private String email;
    private BigDecimal money;
    private Integer points;
    private String invitationCode;
    private String invitedCode;
    private Date createTime;
}