package com.tian.entity;

import cn.hutool.core.date.DateUtil;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.Date;

public class ExceptionalRetroaction implements Serializable {
    private static final long serialVersionUID = 1L;

	
	@ApiModelProperty(value = "")
	private Long id;
	
	@ApiModelProperty(value = "")
	private Integer exceptionalType;
	
	@ApiModelProperty(value = "")
	private Long userId;
	
	@ApiModelProperty(value = "")
	private String orderNo;
	
	@ApiModelProperty(value = "")
	private String deviceNo;
	
	@ApiModelProperty(value = "")
	private String exceptionMessage;
	
	@ApiModelProperty(value = "")
	private Integer status;
	
	@ApiModelProperty(value = "")
	private String url1;
	
	@ApiModelProperty(value = "")
	private String url2;
	
	@ApiModelProperty(value = "")
	private String url3;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	@ApiModelProperty(value = "")
	private Date createTime;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	@ApiModelProperty(value = "")
	private Date updateTime;
	
	@JsonProperty("id")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id =  id;
	}
	@JsonProperty("exceptionalType")
	public Integer getExceptionalType() {
		return exceptionalType;
	}

	public void setExceptionalType(Integer exceptionalType) {
		this.exceptionalType =  exceptionalType;
	}
	@JsonProperty("userId")
	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId =  userId;
	}
	@JsonProperty("orderNo")
	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo =  orderNo;
	}
	@JsonProperty("deviceNo")
	public String getDeviceNo() {
		return deviceNo;
	}

	public void setDeviceNo(String deviceNo) {
		this.deviceNo =  deviceNo;
	}
	@JsonProperty("exceptionMessage")
	public String getExceptionMessage() {
		return exceptionMessage;
	}

	public void setExceptionMessage(String exceptionMessage) {
		this.exceptionMessage =  exceptionMessage;
	}
	@JsonProperty("status")
	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status =  status;
	}
	@JsonProperty("url1")
	public String getUrl1() {
		return url1;
	}

	public void setUrl1(String url1) {
		this.url1 =  url1;
	}
	@JsonProperty("url2")
	public String getUrl2() {
		return url2;
	}

	public void setUrl2(String url2) {
		this.url2 =  url2;
	}
	@JsonProperty("url3")
	public String getUrl3() {
		return url3;
	}

	public void setUrl3(String url3) {
		this.url3 =  url3;
	}
	@JsonProperty("createTime")
	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime =  createTime;
	}
	@JsonProperty("updateTime")
	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime =  updateTime;
	}

																								
	public ExceptionalRetroaction(Long id,Integer exceptionalType,Long userId,String orderNo,String deviceNo,String exceptionMessage,Integer status,String url1,String url2,String url3,Date createTime,Date updateTime) {
				
		this.id = id;
				
		this.exceptionalType = exceptionalType;
				
		this.userId = userId;
				
		this.orderNo = orderNo;
				
		this.deviceNo = deviceNo;
				
		this.exceptionMessage = exceptionMessage;
				
		this.status = status;
				
		this.url1 = url1;
				
		this.url2 = url2;
				
		this.url3 = url3;
				
		this.createTime = createTime;
				
		this.updateTime = updateTime;
				
	}

	public ExceptionalRetroaction() {
	    super();
	}

	public String dateToStringConvert(Date date) {
		if(date!=null) {
			return DateUtil.format(date, "yyyy-MM-dd HH:mm:ss");
		}
		return "";
	}
	

}