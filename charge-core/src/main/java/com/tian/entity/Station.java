package com.tian.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年11月10日 20:04
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 */
public class Station implements Serializable {
    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "")
    private Integer id;

    @ApiModelProperty(value = "")
    private Integer investorId;

    @ApiModelProperty(value = "")
    private String investorName;

    @ApiModelProperty(value = "")
    private String name;

    @ApiModelProperty(value = "")
    private String stationCode;

    @ApiModelProperty(value = "")
    private String province;

    @ApiModelProperty(value = "")
    private String city;

    @ApiModelProperty(value = "")
    private String area;

    @ApiModelProperty(value = "")
    private String address;

    @ApiModelProperty(value = "")
    private BigDecimal longitude;

    @ApiModelProperty(value = "")
    private BigDecimal latitude;

    @ApiModelProperty(value = "")
    private Integer stationType;

    @ApiModelProperty(value = "")
    private Integer operatingStatus;

    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id =  id;
    }
    @JsonProperty("investorId")
    public Integer getInvestorId() {
        return investorId;
    }

    public void setInvestorId(Integer investorId) {
        this.investorId =  investorId;
    }
    @JsonProperty("name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name =  name;
    }
    @JsonProperty("stationCode")
    public String getStationCode() {
        return stationCode;
    }

    public void setStationCode(String stationCode) {
        this.stationCode =  stationCode;
    }
    @JsonProperty("province")
    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province =  province;
    }
    @JsonProperty("city")
    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city =  city;
    }
    @JsonProperty("area")
    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area =  area;
    }
    @JsonProperty("address")
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address =  address;
    }
    @JsonProperty("longitude")
    public BigDecimal getLongitude() {
        return longitude;
    }

    public void setLongitude(BigDecimal longitude) {
        this.longitude =  longitude;
    }
    @JsonProperty("latitude")
    public BigDecimal getLatitude() {
        return latitude;
    }

    public void setLatitude(BigDecimal latitude) {
        this.latitude =  latitude;
    }
    @JsonProperty("stationType")
    public Integer getStationType() {
        return stationType;
    }

    public void setStationType(Integer stationType) {
        this.stationType =  stationType;
    }
    @JsonProperty("operatingStatus")
    public Integer getOperatingStatus() {
        return operatingStatus;
    }

    public void setOperatingStatus(Integer operatingStatus) {
        this.operatingStatus =  operatingStatus;
    }

    public String getInvestorName() {
        return investorName;
    }

    public void setInvestorName(String investorName) {
        this.investorName = investorName;
    }

    public Station(Integer id, Integer investorId, String name, String stationCode, String province, String city, String area, String address, BigDecimal longitude, BigDecimal latitude, Integer stationType, Integer operatingStatus) {

        this.id = id;

        this.investorId = investorId;

        this.name = name;

        this.stationCode = stationCode;

        this.province = province;

        this.city = city;

        this.area = area;

        this.address = address;

        this.longitude = longitude;

        this.latitude = latitude;

        this.stationType = stationType;

        this.operatingStatus = operatingStatus;

    }

    public Station() {
        super();
    }



}
