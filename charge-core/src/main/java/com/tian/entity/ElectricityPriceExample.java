package com.tian.entity;

import cn.hutool.core.util.StrUtil;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 电价 ElectricityPriceExample
 * @author tianwc_自动生成
 * @date 2023-11-14 10:26:58
 */
public class ElectricityPriceExample {

    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public ElectricityPriceExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }
        
				
        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLike(Integer value) {
            addCriterion("id like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotLike(Integer value) {
            addCriterion("id not like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }
        
				
        public Criteria andPrcieIsNull() {
            addCriterion("prcie is null");
            return (Criteria) this;
        }

        public Criteria andPrcieIsNotNull() {
            addCriterion("prcie is not null");
            return (Criteria) this;
        }

        public Criteria andPrcieEqualTo(BigDecimal value) {
            addCriterion("prcie =", value, "prcie");
            return (Criteria) this;
        }

        public Criteria andPrcieNotEqualTo(BigDecimal value) {
            addCriterion("prcie <>", value, "prcie");
            return (Criteria) this;
        }

        public Criteria andPrcieGreaterThan(BigDecimal value) {
            addCriterion("prcie >", value, "prcie");
            return (Criteria) this;
        }

        public Criteria andPrcieGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("prcie >=", value, "prcie");
            return (Criteria) this;
        }

        public Criteria andPrcieLessThan(BigDecimal value) {
            addCriterion("prcie <", value, "prcie");
            return (Criteria) this;
        }

        public Criteria andPrcieLessThanOrEqualTo(BigDecimal value) {
            addCriterion("prcie <=", value, "prcie");
            return (Criteria) this;
        }

        public Criteria andPrcieLike(BigDecimal value) {
            addCriterion("prcie like", value, "prcie");
            return (Criteria) this;
        }

        public Criteria andPrcieNotLike(BigDecimal value) {
            addCriterion("prcie not like", value, "prcie");
            return (Criteria) this;
        }

        public Criteria andPrcieIn(List<BigDecimal> values) {
            addCriterion("prcie in", values, "prcie");
            return (Criteria) this;
        }

        public Criteria andPrcieNotIn(List<BigDecimal> values) {
            addCriterion("prcie not in", values, "prcie");
            return (Criteria) this;
        }

        public Criteria andPrcieBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("prcie between", value1, value2, "prcie");
            return (Criteria) this;
        }

        public Criteria andPrcieNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("prcie not between", value1, value2, "prcie");
            return (Criteria) this;
        }
        
				
        public Criteria andUnitTypeIsNull() {
            addCriterion("unit_type is null");
            return (Criteria) this;
        }

        public Criteria andUnitTypeIsNotNull() {
            addCriterion("unit_type is not null");
            return (Criteria) this;
        }

        public Criteria andUnitTypeEqualTo(Integer value) {
            addCriterion("unit_type =", value, "unitType");
            return (Criteria) this;
        }

        public Criteria andUnitTypeNotEqualTo(Integer value) {
            addCriterion("unit_type <>", value, "unitType");
            return (Criteria) this;
        }

        public Criteria andUnitTypeGreaterThan(Integer value) {
            addCriterion("unit_type >", value, "unitType");
            return (Criteria) this;
        }

        public Criteria andUnitTypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("unit_type >=", value, "unitType");
            return (Criteria) this;
        }

        public Criteria andUnitTypeLessThan(Integer value) {
            addCriterion("unit_type <", value, "unitType");
            return (Criteria) this;
        }

        public Criteria andUnitTypeLessThanOrEqualTo(Integer value) {
            addCriterion("unit_type <=", value, "unitType");
            return (Criteria) this;
        }

        public Criteria andUnitTypeLike(Integer value) {
            addCriterion("unit_type like", value, "unitType");
            return (Criteria) this;
        }

        public Criteria andUnitTypeNotLike(Integer value) {
            addCriterion("unit_type not like", value, "unitType");
            return (Criteria) this;
        }

        public Criteria andUnitTypeIn(List<Integer> values) {
            addCriterion("unit_type in", values, "unitType");
            return (Criteria) this;
        }

        public Criteria andUnitTypeNotIn(List<Integer> values) {
            addCriterion("unit_type not in", values, "unitType");
            return (Criteria) this;
        }

        public Criteria andUnitTypeBetween(Integer value1, Integer value2) {
            addCriterion("unit_type between", value1, value2, "unitType");
            return (Criteria) this;
        }

        public Criteria andUnitTypeNotBetween(Integer value1, Integer value2) {
            addCriterion("unit_type not between", value1, value2, "unitType");
            return (Criteria) this;
        }
        
				
        public Criteria andUnitValueIsNull() {
            addCriterion("unit_value is null");
            return (Criteria) this;
        }

        public Criteria andUnitValueIsNotNull() {
            addCriterion("unit_value is not null");
            return (Criteria) this;
        }

        public Criteria andUnitValueEqualTo(BigDecimal value) {
            addCriterion("unit_value =", value, "unitValue");
            return (Criteria) this;
        }

        public Criteria andUnitValueNotEqualTo(BigDecimal value) {
            addCriterion("unit_value <>", value, "unitValue");
            return (Criteria) this;
        }

        public Criteria andUnitValueGreaterThan(BigDecimal value) {
            addCriterion("unit_value >", value, "unitValue");
            return (Criteria) this;
        }

        public Criteria andUnitValueGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("unit_value >=", value, "unitValue");
            return (Criteria) this;
        }

        public Criteria andUnitValueLessThan(BigDecimal value) {
            addCriterion("unit_value <", value, "unitValue");
            return (Criteria) this;
        }

        public Criteria andUnitValueLessThanOrEqualTo(BigDecimal value) {
            addCriterion("unit_value <=", value, "unitValue");
            return (Criteria) this;
        }

        public Criteria andUnitValueLike(BigDecimal value) {
            addCriterion("unit_value like", value, "unitValue");
            return (Criteria) this;
        }

        public Criteria andUnitValueNotLike(BigDecimal value) {
            addCriterion("unit_value not like", value, "unitValue");
            return (Criteria) this;
        }

        public Criteria andUnitValueIn(List<BigDecimal> values) {
            addCriterion("unit_value in", values, "unitValue");
            return (Criteria) this;
        }

        public Criteria andUnitValueNotIn(List<BigDecimal> values) {
            addCriterion("unit_value not in", values, "unitValue");
            return (Criteria) this;
        }

        public Criteria andUnitValueBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("unit_value between", value1, value2, "unitValue");
            return (Criteria) this;
        }

        public Criteria andUnitValueNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("unit_value not between", value1, value2, "unitValue");
            return (Criteria) this;
        }
        
				
        public Criteria andCreateTimeIsNull() {
            addCriterion("create_time is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("create_time is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Date value) {
            addCriterion("create_time =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Date value) {
            addCriterion("create_time <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Date value) {
            addCriterion("create_time >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("create_time >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Date value) {
            addCriterion("create_time <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("create_time <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLike(Date value) {
            addCriterion("create_time like", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotLike(Date value) {
            addCriterion("create_time not like", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Date> values) {
            addCriterion("create_time in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Date> values) {
            addCriterion("create_time not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Date value1, Date value2) {
            addCriterion("create_time between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("create_time not between", value1, value2, "createTime");
            return (Criteria) this;
        }
        
				
        public Criteria andLastUpdateTimeIsNull() {
            addCriterion("last_update_time is null");
            return (Criteria) this;
        }

        public Criteria andLastUpdateTimeIsNotNull() {
            addCriterion("last_update_time is not null");
            return (Criteria) this;
        }

        public Criteria andLastUpdateTimeEqualTo(Date value) {
            addCriterion("last_update_time =", value, "lastUpdateTime");
            return (Criteria) this;
        }

        public Criteria andLastUpdateTimeNotEqualTo(Date value) {
            addCriterion("last_update_time <>", value, "lastUpdateTime");
            return (Criteria) this;
        }

        public Criteria andLastUpdateTimeGreaterThan(Date value) {
            addCriterion("last_update_time >", value, "lastUpdateTime");
            return (Criteria) this;
        }

        public Criteria andLastUpdateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("last_update_time >=", value, "lastUpdateTime");
            return (Criteria) this;
        }

        public Criteria andLastUpdateTimeLessThan(Date value) {
            addCriterion("last_update_time <", value, "lastUpdateTime");
            return (Criteria) this;
        }

        public Criteria andLastUpdateTimeLessThanOrEqualTo(Date value) {
            addCriterion("last_update_time <=", value, "lastUpdateTime");
            return (Criteria) this;
        }

        public Criteria andLastUpdateTimeLike(Date value) {
            addCriterion("last_update_time like", value, "lastUpdateTime");
            return (Criteria) this;
        }

        public Criteria andLastUpdateTimeNotLike(Date value) {
            addCriterion("last_update_time not like", value, "lastUpdateTime");
            return (Criteria) this;
        }

        public Criteria andLastUpdateTimeIn(List<Date> values) {
            addCriterion("last_update_time in", values, "lastUpdateTime");
            return (Criteria) this;
        }

        public Criteria andLastUpdateTimeNotIn(List<Date> values) {
            addCriterion("last_update_time not in", values, "lastUpdateTime");
            return (Criteria) this;
        }

        public Criteria andLastUpdateTimeBetween(Date value1, Date value2) {
            addCriterion("last_update_time between", value1, value2, "lastUpdateTime");
            return (Criteria) this;
        }

        public Criteria andLastUpdateTimeNotBetween(Date value1, Date value2) {
            addCriterion("last_update_time not between", value1, value2, "lastUpdateTime");
            return (Criteria) this;
        }
        
			
		 public Criteria andLikeQuery(ElectricityPrice record) {
		 	List<String> list= new ArrayList<String>();
		 	List<String> list2= new ArrayList<String>();
        	StringBuffer buffer=new StringBuffer();
			if(record.getId()!=null&&StrUtil.isNotEmpty(record.getId().toString())) {
    			 list.add("ifnull(id,'')");
    		}
			if(record.getPrcie()!=null&&StrUtil.isNotEmpty(record.getPrcie().toString())) {
    			 list.add("ifnull(prcie,'')");
    		}
			if(record.getUnitType()!=null&&StrUtil.isNotEmpty(record.getUnitType().toString())) {
    			 list.add("ifnull(unit_type,'')");
    		}
			if(record.getUnitValue()!=null&&StrUtil.isNotEmpty(record.getUnitValue().toString())) {
    			 list.add("ifnull(unit_value,'')");
    		}
			if(record.getCreateTime()!=null&&StrUtil.isNotEmpty(record.getCreateTime().toString())) {
    			 list.add("ifnull(create_time,'')");
    		}
			if(record.getLastUpdateTime()!=null&&StrUtil.isNotEmpty(record.getLastUpdateTime().toString())) {
    			 list.add("ifnull(last_update_time,'')");
    		}
			if(record.getId()!=null&&StrUtil.isNotEmpty(record.getId().toString())) {
    			list2.add("'%"+record.getId()+"%'");
    		}
			if(record.getPrcie()!=null&&StrUtil.isNotEmpty(record.getPrcie().toString())) {
    			list2.add("'%"+record.getPrcie()+"%'");
    		}
			if(record.getUnitType()!=null&&StrUtil.isNotEmpty(record.getUnitType().toString())) {
    			list2.add("'%"+record.getUnitType()+"%'");
    		}
			if(record.getUnitValue()!=null&&StrUtil.isNotEmpty(record.getUnitValue().toString())) {
    			list2.add("'%"+record.getUnitValue()+"%'");
    		}
			if(record.getCreateTime()!=null&&StrUtil.isNotEmpty(record.getCreateTime().toString())) {
    			list2.add("'%"+record.getCreateTime()+"%'");
    		}
			if(record.getLastUpdateTime()!=null&&StrUtil.isNotEmpty(record.getLastUpdateTime().toString())) {
    			list2.add("'%"+record.getLastUpdateTime()+"%'");
    		}
        	buffer.append(" CONCAT(");
	        buffer.append(StrUtil.join(",",list));
        	buffer.append(")");
        	buffer.append(" like CONCAT(");
        	buffer.append(StrUtil.join(",",list2));
        	buffer.append(")");
        	if(!" CONCAT() like CONCAT()".equals(buffer.toString())) {
        		addCriterion(buffer.toString());
        	}
        	return (Criteria) this;
        }
        
        public Criteria andLikeQuery2(String searchText) {
		 	List<String> list= new ArrayList<String>();
        	StringBuffer buffer=new StringBuffer();
    		list.add("ifnull(id,'')");
    		list.add("ifnull(prcie,'')");
    		list.add("ifnull(unit_type,'')");
    		list.add("ifnull(unit_value,'')");
    		list.add("ifnull(create_time,'')");
    		list.add("ifnull(last_update_time,'')");
        	buffer.append(" CONCAT(");
	        buffer.append(StrUtil.join(",",list));
        	buffer.append(")");
        	buffer.append("like '%");
        	buffer.append(searchText);
        	buffer.append("%'");
        	addCriterion(buffer.toString());
        	return (Criteria) this;
        }
        
}
	
    public static class Criteria extends GeneratedCriteria {
        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}