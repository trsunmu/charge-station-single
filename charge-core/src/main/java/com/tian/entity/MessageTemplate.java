package com.tian.entity;

import cn.hutool.core.date.DateUtil;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.Date;

public class MessageTemplate implements Serializable {
    private static final long serialVersionUID = 1L;

	
	@ApiModelProperty(value = "")
	private Integer id;
	
	@ApiModelProperty(value = "")
	private String templateId;
	
	@ApiModelProperty(value = "")
	private Integer status;
	
	@ApiModelProperty(value = "")
	private String describe;
	
	@ApiModelProperty(value = "")
	private String template;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	@ApiModelProperty(value = "")
	private Date createTime;
	
	@ApiModelProperty(value = "")
	private String manufacturer;
	
	@JsonProperty("id")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id =  id;
	}
	@JsonProperty("templateId")
	public String getTemplateId() {
		return templateId;
	}

	public void setTemplateId(String templateId) {
		this.templateId =  templateId;
	}
	@JsonProperty("status")
	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status =  status;
	}
	@JsonProperty("describe")
	public String getDescribe() {
		return describe;
	}

	public void setDescribe(String describe) {
		this.describe =  describe;
	}
	@JsonProperty("template")
	public String getTemplate() {
		return template;
	}

	public void setTemplate(String template) {
		this.template =  template;
	}
	@JsonProperty("createTime")
	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime =  createTime;
	}
	@JsonProperty("manufacturer")
	public String getManufacturer() {
		return manufacturer;
	}

	public void setManufacturer(String manufacturer) {
		this.manufacturer =  manufacturer;
	}

														
	public MessageTemplate(Integer id,String templateId,Integer status,String describe,String template,Date createTime,String manufacturer) {
				
		this.id = id;
				
		this.templateId = templateId;
				
		this.status = status;
				
		this.describe = describe;
				
		this.template = template;
				
		this.createTime = createTime;
				
		this.manufacturer = manufacturer;
				
	}

	public MessageTemplate() {
	    super();
	}

	public String dateToStringConvert(Date date) {
		if(date!=null) {
			return DateUtil.format(date, "yyyy-MM-dd HH:mm:ss");
		}
		return "";
	}
	

}