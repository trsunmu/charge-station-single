package com.tian.entity;

import cn.hutool.core.date.DateUtil;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.Date;

/**
 *
 */
public class GoodsInfo implements Serializable {
    private static final long serialVersionUID = 1L;

	
	@ApiModelProperty(value = "主键id")
	private Integer id;
	
	@ApiModelProperty(value = "商品名称")
	private String name;
	
	@ApiModelProperty(value = "商品数量")
	private Integer stock;
	
	@ApiModelProperty(value = "状态")
	private Integer status;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	@ApiModelProperty(value = "上架时间")
	private Date createTime;
	
	@ApiModelProperty(value = "图片")
	private String imagUrl;
	
	@ApiModelProperty(value = "类型")
	private Integer type;
	
	@ApiModelProperty(value = "所需积分")
	private Integer point;
	
	@ApiModelProperty(value = "")
	private Integer activityId;
	
	@JsonProperty("id")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id =  id;
	}
	@JsonProperty("name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name =  name;
	}
	@JsonProperty("stock")
	public Integer getStock() {
		return stock;
	}

	public void setStock(Integer stock) {
		this.stock =  stock;
	}
	@JsonProperty("status")
	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status =  status;
	}
	@JsonProperty("createTime")
	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime =  createTime;
	}
	@JsonProperty("imagUrl")
	public String getImagUrl() {
		return imagUrl;
	}

	public void setImagUrl(String imagUrl) {
		this.imagUrl =  imagUrl;
	}
	@JsonProperty("type")
	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type =  type;
	}
	@JsonProperty("point")
	public Integer getPoint() {
		return point;
	}

	public void setPoint(Integer point) {
		this.point =  point;
	}
	@JsonProperty("activityId")
	public Integer getActivityId() {
		return activityId;
	}

	public void setActivityId(Integer activityId) {
		this.activityId =  activityId;
	}

																		
	public GoodsInfo(Integer id,String name,Integer stock,Integer status,Date createTime,String imagUrl,Integer type,Integer point,Integer activityId) {
				
		this.id = id;
				
		this.name = name;
				
		this.stock = stock;
				
		this.status = status;
				
		this.createTime = createTime;
				
		this.imagUrl = imagUrl;
				
		this.type = type;
				
		this.point = point;
				
		this.activityId = activityId;
				
	}

	public GoodsInfo() {
	    super();
	}

	public String dateToStringConvert(Date date) {
		if(date!=null) {
			return DateUtil.format(date, "yyyy-MM-dd HH:mm:ss");
		}
		return "";
	}
	

}