package com.tian.entity;

import cn.hutool.core.date.DateUtil;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.Date;

public class CarInfo implements Serializable {
    private static final long serialVersionUID = 1L;


	@ApiModelProperty(value = "")
	private Integer id;

	@ApiModelProperty(value = "")
	private Integer userId;

	@ApiModelProperty(value = "")
	private String carBrand;

	@ApiModelProperty(value = "")
	private String carType;

	@ApiModelProperty(value = "")
	private String frameNumber;

	@ApiModelProperty(value = "")
	private String engineNumber;

	@ApiModelProperty(value = "")
	private String plateNumber;

	@ApiModelProperty(value = "")
	private Integer carIdentifyStatus;
	private String carIdentifyStatusStr;

	@ApiModelProperty(value = "")
	private String drivingLicenseUrl;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	@ApiModelProperty(value = "")
	private Date createTime;

	@JsonProperty("id")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id =  id;
	}

	@JsonProperty("userId")
	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId =  userId;
	}

	@JsonProperty("carBrand")
	public String getCarBrand() {
		return carBrand;
	}

	public void setCarBrand(String carBrand) {
		this.carBrand =  carBrand;
	}

	@JsonProperty("carType")
	public String getCarType() {
		return carType;
	}

	public void setCarType(String carType) {
		this.carType =  carType;
	}

	@JsonProperty("frameNumber")
	public String getFrameNumber() {
		return frameNumber;
	}

	public void setFrameNumber(String frameNumber) {
		this.frameNumber =  frameNumber;
	}

	@JsonProperty("engineNumber")
	public String getEngineNumber() {
		return engineNumber;
	}

	public void setEngineNumber(String engineNumber) {
		this.engineNumber =  engineNumber;
	}

	@JsonProperty("plateNumber")
	public String getPlateNumber() {
		return plateNumber;
	}

	public void setPlateNumber(String plateNumber) {
		this.plateNumber =  plateNumber;
	}

	@JsonProperty("carIdentifyStatus")
	public Integer getCarIdentifyStatus() {
		return carIdentifyStatus;
	}

	public void setCarIdentifyStatus(Integer carIdentifyStatus) {
		this.carIdentifyStatus =  carIdentifyStatus;
	}

	@JsonProperty("drivingLicenseUrl")
	public String getDrivingLicenseUrl() {
		return drivingLicenseUrl;
	}

	public void setDrivingLicenseUrl(String drivingLicenseUrl) {
		this.drivingLicenseUrl =  drivingLicenseUrl;
	}

	@JsonProperty("createTime")
	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime =  createTime;
	}

	public String getCarIdentifyStatusStr() {
		return carIdentifyStatusStr;
	}

	public void setCarIdentifyStatusStr(String carIdentifyStatusStr) {
		this.carIdentifyStatusStr = carIdentifyStatusStr;
	}

	public CarInfo(Integer id, Integer userId, String carBrand, String carType, String frameNumber, String engineNumber, String plateNumber, Integer carIdentifyStatus, String carIdentifyStatusStr, String drivingLicenseUrl, Date createTime) {
		this.id = id;
		this.userId = userId;
		this.carBrand = carBrand;
		this.carType = carType;
		this.frameNumber = frameNumber;
		this.engineNumber = engineNumber;
		this.plateNumber = plateNumber;
		this.carIdentifyStatus = carIdentifyStatus;
		this.carIdentifyStatusStr = carIdentifyStatusStr;
		this.drivingLicenseUrl = drivingLicenseUrl;
		this.createTime = createTime;
	}

	public CarInfo() {
		super();
	}

	public String dateToStringConvert(Date date) {
		if(date!=null) {
			return DateUtil.format(date, "yyyy-MM-dd HH:mm:ss");
		}
		return "";
	}

	public static void main(String[] args) {
		System.out.println(DateUtil.format(new Date(),"yyyyMMddHHmmss"));
	}
}