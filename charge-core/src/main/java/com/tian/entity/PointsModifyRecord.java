package com.tian.entity;

import cn.hutool.core.date.DateUtil;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.Date;

public class PointsModifyRecord implements Serializable {
    private static final long serialVersionUID = 1L;

	
	@ApiModelProperty(value = "")
	private Integer id;
	
	@ApiModelProperty(value = "")
	private Integer userId;
	
	@ApiModelProperty(value = "")
	private Integer points;
	
	@ApiModelProperty(value = "")
	private Integer type;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	@ApiModelProperty(value = "")
	private Date createTime;
	
	@JsonProperty("id")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id =  id;
	}
	@JsonProperty("userId")
	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId =  userId;
	}
	@JsonProperty("points")
	public Integer getPoints() {
		return points;
	}

	public void setPoints(Integer points) {
		this.points =  points;
	}
	@JsonProperty("type")
	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type =  type;
	}
	@JsonProperty("createTime")
	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime =  createTime;
	}

										
	public PointsModifyRecord(Integer id,Integer userId,Integer points,Integer type,Date createTime) {
				
		this.id = id;
				
		this.userId = userId;
				
		this.points = points;
				
		this.type = type;
				
		this.createTime = createTime;
				
	}

	public PointsModifyRecord() {
	    super();
	}

	public String dateToStringConvert(Date date) {
		if(date!=null) {
			return DateUtil.format(date, "yyyy-MM-dd HH:mm:ss");
		}
		return "";
	}
	

}