package com.tian.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class InvitedIncomeRecord implements Serializable {
    private Long id;

    private Long userId;

    private BigDecimal income;

    private Long invitedUserRecordId;

    private Date createTime;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public BigDecimal getIncome() {
        return income;
    }

    public void setIncome(BigDecimal income) {
        this.income = income;
    }

    public Long getInvitedUserRecordId() {
        return invitedUserRecordId;
    }

    public void setInvitedUserRecordId(Long invitedUserRecordId) {
        this.invitedUserRecordId = invitedUserRecordId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", userId=").append(userId);
        sb.append(", income=").append(income);
        sb.append(", tbInvitedUserRecordId=").append(invitedUserRecordId);
        sb.append(", createTime=").append(createTime);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}