package com.tian.mongo.repository;

import com.tian.mongo.domain.UserMessage;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * {@code @description:} TODO
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024/1/13 11:18
 * {@code @version:} 1.0
 */
@Repository
public interface UserMessageRepository extends MongoRepository<UserMessage, Long> {
    @Query("{ 'receiverId' : ?0 }")
    List<UserMessage> findByReceiverId(Integer receiverId);
}

