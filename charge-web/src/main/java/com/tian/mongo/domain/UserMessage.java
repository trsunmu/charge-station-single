package com.tian.mongo.domain;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

/**
 * {@code @description:} 用户站内信
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024/1/13 11:14
 * {@code @version:} 1.0
 */
@Document(collection = "user-message")
@Data
public class UserMessage {

    @Id
    private Long id;
    private Integer receiverId;
    private String title;
    private Integer status;
    private String message;
    private Date createTime;
}

