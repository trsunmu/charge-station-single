package com.tian.mongo.ext;

import org.springframework.data.domain.Sort;

import java.util.List;

/**
 * {@code @description:} TODO
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024/1/13 20:04
 * {@code @version:} 1.0
 */
public class ChargeStationSort extends Sort {
    public ChargeStationSort(List<Order> orders) {
        super(orders);
    }
}
