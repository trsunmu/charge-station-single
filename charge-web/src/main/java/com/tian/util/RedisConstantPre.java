package com.tian.util;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * @date 2023年05月11日 17:23
 * 博客地址：<a href="http://woaijava.cc/">博客地址</a>
 */
public interface RedisConstantPre {

    String SEND_CODE_LOGIN_PRE ="send.code.redis.login_";
    String SEND_CODE_REGISTER_PRE ="send.code.redis.register_";
    String SEND_CODE_PASSWORD_PRE ="send.code.redis.password_";
    String SEND_CODE_LOGIN_VALID_PRE="send.code.valid.times.redis.login_";
    String ORDER_NO_KEY="order_no_key";
    String BALANCE_LOCK_PRE="balance_lock_pre";
    String INVITED_USER_REGISTER_PRE="invited_user_register_pre.";
    String USER_COUPON_PRE="user_coupon_pre_";
    String USE_USER_COUPON_PRE="use_user_coupon_pre_";
    String MESSAGE_TEMPLATE_KEY_PRE="message.template.key_pre";
    String MESSAGE_LIMIT_KEY_PRE="message_limit_key_pre";
    String USER_POINT_PRE="user_point_key_pre";
    String USER_POINT_LOCK_PRE="user_point_lock_key_pre";
    String GOODS_INFO="goods_info_key_pre";
    String GOODS_INFO_STOCK="goods_info_stock_lock_pre";
    String GLOBAL_PROPERTY_PRE="global_property_pre.";
    String SYSTEM_MESSAGE_PRE="system_message_pre..";
}
