package com.tian.util;

import com.tian.entity.User;
import com.tian.enums.ResultCode;
import com.tian.exception.BusinessException;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年11月03日 09:11
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 */
public class UserCacheUtil {

    /**
     * 创建ThreadLocal变量
     */
    private static final ThreadLocal<User> USER_THREAD_LOCAL = new ThreadLocal<>();

    public static void setUser(User user) {
        USER_THREAD_LOCAL.set(user);
    }

    public static User getUser() {
        User user = USER_THREAD_LOCAL.get();
        if (user == null) {
            throw new BusinessException(ResultCode.UNAUTHORIZED);
        }
        return user;
    }

    public static void clearUser() {
        USER_THREAD_LOCAL.remove();
    }
}
