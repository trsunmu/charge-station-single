package com.tian.util;

import java.util.Random;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * @date 2023年10月08日 16:49
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 */
public class RandomNumberGenerator {
    public static void main(String[] args) {
        for (int i = 0; i < 100; i++) {
            int randomNumber = generateRandomNumber();
            System.out.println("随机4位数：" + randomNumber);
        }
    }

    public static int generateRandomNumber() {
        Random random = new Random();
        int[] digits = new int[4];
        // 第一位数字为1-9的随机数
        digits[0] = random.nextInt(9) + 1;

        for (int i = 1; i < 4; i++) {
            // 生成0-9的随机数
            int digit = random.nextInt(10);
            // 检查是否已经包含该数字
            while (contains(digits, digit)) {
                digit = random.nextInt(10);
            }
            digits[i] = digit;
        }

        int randomNumber = 0;
        for (int i = 0; i < 4; i++) {
            randomNumber += digits[i] * Math.pow(10, 3 - i);
        }

        return randomNumber;
    }

    public static boolean contains(int[] arr, int target) {
        for (int num : arr) {
            if (num == target) {
                return true;
            }
        }
        return false;
    }
}
