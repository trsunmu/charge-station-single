package com.tian.util;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年11月08日 09:26
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 */
public class DateTimeUtil {

    // 日期格式
    public static final String DATE_FORMAT = "yyyy-MM-dd";
    public static final String DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
    public static final String DATE_TIME_MS_FORMAT = "yyyy-MM-dd HH:mm:ss.SSS";

    /**
     * 将字符串转换为日期对象
     * @param dateStr 日期字符串
     * @param format 日期格式
     * @return 日期对象
     */
    public static Date parseDate(String dateStr, String format) {
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        Date date = null;
        try {
            date = sdf.parse(dateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    /**
     * 将日期对象转换为字符串
     * @param date 日期对象
     * @param format 日期格式
     * @return 日期字符串
     */
    public static String formatDate(Date date, String format) {
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.format(date);
    }

    /**
     * 获取当前日期
     * @return 当前日期对象
     */
    public static Date getCurrentDate() {
        return new Date();
    }

    /**
     * 获取当前时间戳（毫秒）
     * @return 当前时间戳（毫秒）
     */
    public static long getCurrentTimeMillis() {
        return System.currentTimeMillis();
    }

    /**
     * 计算两个日期之间的天数差
     * @param startDate 开始日期
     * @param endDate 结束日期
     * @return 天数差
     */
    public static int getDaysBetween(Date startDate, Date endDate) {
        Calendar calendar1 = Calendar.getInstance();
        Calendar calendar2 = Calendar.getInstance();
        calendar1.setTime(startDate);
        calendar2.setTime(endDate);
        int days = 0;
        while (calendar1.before(calendar2)) {
            calendar1.add(Calendar.DAY_OF_MONTH, 1);
            days++;
        }
        return days;
    }

    /**
     * 计算两个日期之间的小时差
     * @param startDate 开始日期
     * @param endDate 结束日期
     * @return 小时差
     */
    public static int getHoursBetween(Date startDate, Date endDate) {
        Calendar calendar1 = Calendar.getInstance();
        Calendar calendar2 = Calendar.getInstance();
        calendar1.setTime(startDate);
        calendar2.setTime(endDate);
        int hours = 0;
        while (calendar1.before(calendar2)) {
            calendar1.add(Calendar.HOUR_OF_DAY, 1);
            hours++;
        }
        return hours;
    }

    /**
     * 计算两个日期之间的分钟差
     * @param startDate 开始日期
     * @param endDate 结束日期
     * @return 分钟差
     */
    public static int getMinutesBetween(Date startDate, Date endDate) {
        Calendar calendar1 = Calendar.getInstance();
        Calendar calendar2 = Calendar.getInstance();
        calendar1.setTime(startDate);
        calendar2.setTime(endDate);
        int minutes = 0;
        while (calendar1.before(calendar2)) {
            calendar1.add(Calendar.MINUTE, 1);
            minutes++;
        }
        return minutes;
    }
}

