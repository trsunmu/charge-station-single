package com.tian.util;

import com.tian.enums.ResultCode;
import com.tian.exception.BusinessException;

import java.util.Collection;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * @date 2023年06月11日 22:56
 * 博客地址：<a href="http://woaijava.cc/">博客地址</a>
 * <p>
 * 参数验证
 * <p>
 * 参考 org.apache.commons.lang3.Validate
 */
public class ParamValidate {

    public static void isNull(Object object, String message) {
        if (object == null) {
            throw new BusinessException(ResultCode.PARAMETER_EMPTY, message);
        }
    }

    public static void isTrue(boolean expression, String message) {
        if (!expression) {
            throw new BusinessException(ResultCode.PARAMETER_ERROR, message);
        }
    }

    public static void notEmpty(Collection collection, String message) {
        isNull(collection, message);
        if (collection.size() == 0) {
            throw new BusinessException(ResultCode.PARAMETER_ERROR, message);
        }
    }
}
