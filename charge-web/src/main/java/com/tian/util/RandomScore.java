package com.tian.util;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * @date 2023年06月06日 15:53
 * 博客地址：<a href="http://woaijava.cc/">博客地址</a>
 */
public class RandomScore {
    public static final long MAX_INTEGER = Integer.MAX_VALUE;
    public static final double TIME_CONSTANT = Math.pow(10, -13);

    public static double calculateScore(int contribution, long timestamp) {
        return contribution + (MAX_INTEGER - timestamp) * TIME_CONSTANT;
    }

    public static void main(String[] args) {
        int contribution = 100;
        long timestamp = System.currentTimeMillis();
        double score = RandomScore.calculateScore(contribution, timestamp);
        System.out.println(score);
        //99.8316109412776
        //99.8316109345264
        //99.8316109320034
    }
}
