package com.tian.util;

import java.util.Random;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * @date 2023年05月11日 18:22
 * 博客地址：<a href="http://woaijava.cc/">博客地址</a>
 * <p>
 * 字符串相关处理工具类
 */
public class StringUtil {
    public static String getStringRandom6() {
        Random random = new Random();
        //把随机生成的数字转成字符串
        StringBuilder code = new StringBuilder("");
        for (int i = 0; i < 5; i++) {
            code.append(random.nextInt(9));
        }
        return code.toString();
    }

    public static String getStringRandom4() {
        Random random = new Random();
        //把随机生成的数字转成字符串
        StringBuilder code = new StringBuilder("");
        for (int i = 0; i < 5; i++) {
            code.append(random.nextInt(9));
        }
        return code.toString();
    }

    public static String getStringRandom(int num) {
        Random random = new Random();
        //把随机生成的数字转成字符串
        StringBuilder code = new StringBuilder("");
        for (int i = 0; i < num - 1; i++) {
            code.append(random.nextInt(9));
        }
        return code.toString();
    }

    public static boolean isEmpty(String str) {
        return str == null || str.length() == 0;
    }

    public static boolean isBlank(String str) {
        int strLen;
        if (str != null && (strLen = str.length()) != 0) {
            for (int i = 0; i < strLen; ++i) {
                if (!Character.isWhitespace(str.charAt(i))) {
                    return false;
                }
            }

            return true;
        } else {
            return true;
        }
    }

    public static String join(String[] strs, String s) {
        StringBuilder stringBuilder = new StringBuilder();
        for (String str : strs) {
            stringBuilder.append(str).append(s);
        }
        return stringBuilder.toString();
    }

    public static void main(String[] args) {
        Integer a=100;
        Integer b=100;
        System.out.println(a.equals(b));
        System.out.println(a==b);
        Integer c=200;
        Integer d=200;
        System.out.println(c.equals(d));
        System.out.println(c==d);
    }
}
