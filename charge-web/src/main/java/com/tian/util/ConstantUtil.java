package com.tian.util;

/**
 * @author tianwc 公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * @date 2023年05月12日 17:00
 * 博客地址：<a href="http://woaijava.cc/">博客地址</a>
 */
public interface ConstantUtil {
    String MONEY = "100";
    String ORDER_PRE = "WX_PAY_";

    String DEFAULT_PASSWORD="123456";
    String DEFAULT_NICK="A驾";
}
