package com.tian.util;

import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Objects;
import java.util.UUID;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * @date 2023年05月27日 09:13
 * 博客地址：<a href="http://woaijava.cc/">博客地址</a>
 */
@Component
public class LimiterUtil {
    @Resource
    private RedisTemplate<String, String> redisTemplate;

    /**
     * 孤单窗口限流算法
     *
     * @return true 限流  false 放行
     */
    public boolean fixedWindow(String key, int count) {
        long countCache = redisTemplate.opsForValue().increment(key);
        return countCache > count;
    }

    /**
     * intervalTime 时间内 最多只能访问5次
     *
     * @param key          缓存key
     * @param currentTime  当前时间  new Date().getTime();
     * @param intervalTime 有效期
     * @param count        限流次数
     * @return true 限流 false 放行
     */
    public boolean slidingWindow(String key, Long currentTime, Long intervalTime, int count) {
        redisTemplate.opsForZSet().add(key, UUID.randomUUID().toString(), currentTime);
        // intervalTime是限流的时间
        int countCache = Objects.requireNonNull(redisTemplate.opsForZSet().rangeByScore(key, currentTime - intervalTime, currentTime)).size();
        return countCache > count;
    }
}
