package com.tian.util;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * @date 2023年04月25日 20:34
 * 博客地址：<a href="http://woaijava.cc/">博客地址</a>
 * <p>
 * json数据实体
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DataGridView {

    private Integer code = 0;
    private String msg = "";
    private Long count = 0L;
    private Object data;

    public DataGridView(Long count, Object data) {
        super();
        this.count = count;
        this.data = data;
    }

    public DataGridView(Integer count, Object data) {
        super();
        this.count = Long.parseLong(count.toString());
        this.data = data;
    }

    public DataGridView(Object data) {
        super();
        this.data = data;
    }
}