package com.tian.annotation;


import com.tian.enums.SysLogTypeEnum;

import java.lang.annotation.*;

/**
 * @author tianwc 公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * <p>
 * 自定义注解
 * <p>
 * 用于方法出入参打印
 * <p>
 * 使用案例： @SysLogAnnotation(methodDescription = "测试",type = SysLogTypeEnum.DB)
 */
@Target(ElementType.METHOD)
@Documented
@Retention(RetentionPolicy.RUNTIME)
public @interface SysLogAnnotation {
    /**
     * 方法描述
     */
    String methodDescription() default "";

    /**
     * {@link SysLogTypeEnum}
     */
    SysLogTypeEnum type() default SysLogTypeEnum.FILE;
}
