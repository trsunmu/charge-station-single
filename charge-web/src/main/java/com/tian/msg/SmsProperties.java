package com.tian.msg;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * @date 2023年07月20日 10:34
 * 在线刷题 1200+题和1000+篇干货文章：<a href="http://woaijava.cc/">博客地址</a>
 *
 * 发送短信配置属性
 */
@ConfigurationProperties(prefix = SmsProperties.SMS_PREFIX)
@Data
public class SmsProperties {

    /**
     * sms prefix.
     */
    static final String SMS_PREFIX = "sms";
    /**
     * aliyun
     */
    private Aliyun aliyun;
    /**
     * baidu
     */
    private Baidu baidu;
    /**
     * huawei
     */
    private Huawei huawei;
    /**
     * aliyun  sms config.
     */
    @Data
    public static class Aliyun {
        /**
         * aliyun sms sign name must not be null
         */
        private String signName;
        /**
         * aliyun sms access key
         */
        private String accessKeyId;
        /**
         * aliyun sms access key secret
         */
        private String accessKeySecret;

    }

    /**
     * baidu  sms config.
     */
    @Data
    public static class Baidu {
        /**
         * baidu sms accessKeyId
         */
        private String accessKeyId;
        /**
         * baidu sms secretAccessKey
         */
        private String secretAccessKey;
        /**
         * baidu sms signatureId
         */
        private String signatureId;

    }

    /**
     * huawei sms config.
     */
    @Data
    public static class Huawei {
        /**
         *  endpoint
         */
        private String endpoint;
        /**
         *  accessKeyId
         */
        private String accessKeyId;
        /**
         *  secretAccessKey
         */
        private String secretAccessKey;
        /**
         *  signName
         */
        private String signName;
    }

}
