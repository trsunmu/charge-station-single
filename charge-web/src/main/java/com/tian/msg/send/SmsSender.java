package com.tian.msg.send;

import com.tian.msg.SmsResult;

import java.util.LinkedHashMap;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * @date 2023年07月20日 10:39
 * 在线刷题 1200+题和1000+篇干货文章：<a href="http://woaijava.cc/">博客地址</a>
 *
 *
 */
public interface SmsSender {

    /**
     * 发送短信息
     *
     * @param smsTemplate the smsTemplateEnum
     * @param params          the params
     * @param phoneNumber     the phone number
     * @return the sms result
     */
    SmsResult send(String smsTemplate, LinkedHashMap<String, String> params, String phoneNumber);

}
