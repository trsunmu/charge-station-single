package com.tian.msg.send;

import com.tian.msg.SmsResult;
import lombok.extern.slf4j.Slf4j;

import java.util.LinkedHashMap;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * @date 2023年07月20日 10:41
 * 在线刷题 1200+题和1000+篇干货文章：<a href="http://woaijava.cc/">博客地址</a>
 * <p>
 * 发送短信模板类
 */
@Slf4j
public abstract class AbstractSmsSender implements SmsSender {

    /**
     * Instantiates a new Abstract sms sender.
     */
    public AbstractSmsSender() {
    }

    /**
     * 模板方法 算法估计
     */
    @Override
    public SmsResult send(String smsTemplate, LinkedHashMap<String, String> params, String phoneNumber) {
        SmsResult smsResult = new SmsResult();
        final String successCode = "0";
        String result = doSend(smsTemplate, params, phoneNumber);
        smsResult.setSuccessful(successCode.equals(result));
        return smsResult;
    }


    /**
     * 模板方法模式 钩子方法 子类自己去实现自己的业务逻辑
     *
     * @param templateCode the template code
     * @param params       the params
     * @param phoneNumber  the phone number
     * @return the string
     */
    protected abstract String doSend(String templateCode, LinkedHashMap<String, String> params, String phoneNumber);
}