package com.tian.msg.configuration;
import com.tian.msg.SmsProperties;
import com.tian.msg.send.HuaweiSmsSender;
import com.tian.msg.send.SmsSender;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;


/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * @date 2023年07月20日 15:39
 * 在线刷题 1200+题和1000+篇干货文章：<a href="http://woaijava.cc/">博客地址</a>
 *
 * 对接华为短信发送
 */
@EnableConfigurationProperties(SmsProperties.class)
@Configuration
public class HuaweiSMSconfiguration {

    @Bean
    public SmsSender huaweiSmsSender(SmsProperties smsProperties) {
        SmsProperties.Huawei huawei = smsProperties.getHuawei();
        RestTemplate restTemplate = new RestTemplate();
        return new HuaweiSmsSender(restTemplate, huawei);
    }
}