package com.tian.msg.configuration;

import com.tian.msg.SmsProperties;
import com.tian.msg.send.AliSmsSender;
import com.tian.msg.send.SmsSender;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * @date 2023年07月20日 10:33
 * 在线刷题 1200+题和1000+篇干货文章：<a href="http://woaijava.cc/">博客地址</a>
 * <p>
 * 对接阿里云短信发送
 */
@EnableConfigurationProperties(SmsProperties.class)
@Configuration
public class AliyunSmsAutoConfiguration {
    @Bean
    public SmsSender aliyunSmsSender(SmsProperties smsProperties) {
        SmsProperties.Aliyun aliyun = smsProperties.getAliyun();
        return new AliSmsSender(aliyun.getSignName(), aliyun.getAccessKeyId(), aliyun.getAccessKeySecret());
    }
}
