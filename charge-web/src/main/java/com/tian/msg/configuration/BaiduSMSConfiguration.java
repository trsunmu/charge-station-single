package com.tian.msg.configuration;

import com.tian.msg.SmsProperties;
import com.tian.msg.send.BaiduSmsSender;
import com.tian.msg.send.SmsSender;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * @date 2023年07月20日 15:34
 * 在线刷题 1200+题和1000+篇干货文章：<a href="http://woaijava.cc/">博客地址</a>
 *
 * 对接百度短信发送
 */
@EnableConfigurationProperties(SmsProperties.class)
@Configuration
public class BaiduSMSConfiguration {

    @Bean
    public SmsSender baiduSmsSender(SmsProperties smsProperties) {
        SmsProperties.Baidu baidu = smsProperties.getBaidu();
        return new BaiduSmsSender( baidu.getAccessKeyId(), baidu.getSecretAccessKey(), baidu.getSignatureId());
    }
}