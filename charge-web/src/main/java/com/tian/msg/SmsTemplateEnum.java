package com.tian.msg;

/**
 * @author tianwc 公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * @date 2023年07月20日 10:40
 * 在线刷题 1200+题和1000+篇干货文章：<a href="http://woaijava.cc/">博客地址</a>
 *
 * 短信模板
 */
public interface SmsTemplateEnum {
    String templateId();
}
