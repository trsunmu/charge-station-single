package com.tian.job;

import com.tian.entity.PayOrder;
import com.tian.enums.WxPayStatusEnum;
import com.tian.service.PayOrderAsynchronousService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 * <p>
 * 支付订单查询定时任务,处理支付未完成的
 * <p>
 * 在线编辑 Core 表达式 https://www.pppet.net/
 */
@Slf4j
@Component
public class PayOrderQueryTask {
    @Resource
    private PayOrderAsynchronousService payOrderAsynchronousService;

    /**
     * 每5分钟执行一次
     */
    @Scheduled(cron = "0 0/5 * * * ? ")
    public void queryPayOrder() {
        int count = payOrderAsynchronousService.countByStatus(WxPayStatusEnum.INIT.getStatus());
        if (count == 0) {
            log.info("-------不存在支付中的订单-------");
            return;
        }
        int start = 0;
        int pageSize = 10;
        int page = count / pageSize;
        for (int i = 0; start <= page; i++) {
            List<PayOrder> payOrders = payOrderAsynchronousService.selectByStatus(start, start, pageSize);
            for (PayOrder payOrder : payOrders) {
                payOrderAsynchronousService.processe(payOrder);
            }
            start = pageSize * i;
        }
        log.info("-------支付中的订单 异步处理完成-------");
    }
}