package com.tian.mqtt.publish;

import lombok.extern.slf4j.Slf4j;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年11月07日 19:26
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 */
@Component
@Slf4j
public class ChargeReadyStartPublish {
    @Resource
    private MqttClient client4Publish;

    public void process(String topic, MqttMessage message) {
        try {
            client4Publish.publish(topic, message);
        } catch (MqttException e) {
            log.error("发送mqtt失败", e);
        }
    }
}
