package com.tian.mqtt;

import com.tian.mqtt.subscribe.ChargerStartSubscribe;
import com.tian.mqtt.subscribe.ChargerStopSubscribe;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年11月06日 20:31
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 */
public class MqttSubscribeProcess {

    public static void process(ConfigurableApplicationContext context) {
        doProcess(context);
    }

    private static void doProcess(ConfigurableApplicationContext context) {

        ChargerStopSubscribe chargeServer = context.getBean(ChargerStopSubscribe.class);
        chargeServer.process();

        ChargerStartSubscribe chargerStartListener = context.getBean(ChargerStartSubscribe.class);
        chargerStartListener.process();
    }
}
