package com.tian.mqtt.subscribe;

import com.tian.mqtt.enums.MqttTopicEnum;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年11月06日 20:25
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 * <p>
 * 开始充电
 */
@Component
public class ChargerStartSubscribe {
    @Resource
    private MqttClient client4Subscribe;

    public void process() {
        try {
            client4Subscribe.subscribe(MqttTopicEnum.CHARGER_START.getTopicName());
        } catch (MqttException e) {
            throw new RuntimeException(e);
        }
    }

}
