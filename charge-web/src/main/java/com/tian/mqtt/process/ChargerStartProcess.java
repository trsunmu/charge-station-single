package com.tian.mqtt.process;

import com.alibaba.fastjson.JSON;
import com.tian.dto.charger.ChargerRecordAddReqDto;
import com.tian.mqtt.message.ChargerStartMessage;
import com.tian.service.ChargerRecordService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.nio.charset.StandardCharsets;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年11月07日 09:50
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 */
@Slf4j
@Component
public class ChargerStartProcess extends BaseProcess {

    @Resource
    private ChargerRecordService chargerRecordService;

    @Override
    public void run() {
        log.info("充电开始，订阅到消息msg={}", mqttMessage);
        ChargerStartMessage chargerStartMessage = JSON.parseObject(new String(mqttMessage.getPayload(), StandardCharsets.UTF_8), ChargerStartMessage.class);
        ChargerRecordAddReqDto chargerRecordAddReqDto=new ChargerRecordAddReqDto();
        BeanUtils.copyProperties(chargerStartMessage,chargerRecordAddReqDto);
        chargerRecordService.doStartCharge(chargerRecordAddReqDto);
    }
}
