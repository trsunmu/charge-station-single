package com.tian.mqtt.enums;

/**
 * @author tianwc 公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年11月06日 19:46
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 */
public enum MqttTopicEnum {
    /**
     * 开始充电--topic--beanClass
     */
    CHARGER_START("charger-start", "充电开始", "chargerStartProcess"),
    /**
     * 开始结束,设备端订阅这个topic
     */
    CHARGER_STOP_CHARGER_GUN("charger-ready-stop", "充电结束", null),
    /**
     * 准备充电
     */
    CHARGER_READY_CHARGE("charger-ready-charge", "准备充电", null),
    /**
     * 停止充电--topic--beanClass
     */
    CHARGER_STOP("charger-stop", "充电结束", "chargerStopProcess");

    private final String topicName;
    private final String topicNameDesc;
    private final String processBeanName;

    MqttTopicEnum(String topicName, String topicNameDesc, String beanClass) {
        this.topicName = topicName;
        this.topicNameDesc = topicNameDesc;
        this.processBeanName = beanClass;
    }

    public String getTopicName() {
        return topicName;
    }

    public String getTopicNameDesc() {
        return topicNameDesc;
    }

    public String getProcessBeanName() {
        return processBeanName;
    }

    /**
     * 通过topic name 查询对应 beanClass
     *
     * @param topicName topic
     * @return processBeanName
     */
    public static String getBeanClassByTopicName(String topicName) {
        for (MqttTopicEnum value : MqttTopicEnum.values()) {
            if (value.getTopicName().equals(topicName)) {
                return value.getProcessBeanName();
            }
        }
        return null;
    }
}
