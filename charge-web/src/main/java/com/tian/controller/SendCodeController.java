package com.tian.controller;

import com.tian.annotation.SysLogAnnotation;
import com.tian.dto.SendCodeReqDto;
import com.tian.service.SendCodeService;
import com.tian.util.CommonResult;
import com.tian.util.RedisConstantPre;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * @date 2023年10月15日 11:43
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 */
@Api("发送短信")
@RestController
@RequestMapping("/send")
public class SendCodeController {
    @Resource
    private SendCodeService sendCodeService;
    @ApiOperation("发送")
    @SysLogAnnotation
    @PostMapping("/code")
    public CommonResult<Boolean> sendCode4Login(@RequestBody SendCodeReqDto sendCodeReqDto) {
        sendCodeReqDto.setMsgTemplateId(1);
        return sendCodeService.sendCode(sendCodeReqDto);
    }

}
