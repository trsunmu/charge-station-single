package com.tian.controller.charger;

import com.tian.dto.charger.*;
import com.tian.service.ChargerRecordService;
import com.tian.util.CommonResult;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @author tianwc 公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年11月01日 11:14
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 */
@RestController
@RequestMapping("/charger/record")
public class ChargerRecordController {
    @Resource
    private ChargerRecordService chargerRecordService;



    @GetMapping("/{id}")
    public CommonResult<ChargerRecordRespDto> selectById(@PathVariable Integer id) {
        return chargerRecordService.selectById(id);
    }

    @GetMapping("/page")
    public CommonResult<ChargerRecordPageRespDto> page(@RequestBody ChargerRecordPageReqDto chargerRecordPageReqDto) {
        return chargerRecordService.page(chargerRecordPageReqDto);
    }
}
