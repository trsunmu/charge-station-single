package com.tian.controller;

import com.tian.dto.price.*;
import com.tian.service.ElectricityPriceService;
import com.tian.util.CommonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年11月01日 15:55
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 * <p>
 * 电量价格
 */
@Api("电价信息")
@RestController
@RequestMapping("/electricity/price")
public class ElectricityPriceController {
    @Resource
    private ElectricityPriceService electricityPriceService;

    @ApiOperation("获取电价详细")
    @GetMapping("/{id}")
    @ApiImplicitParam(name = "id", value = "电价信息id", required = true, dataType = "int", paramType = "path")
    public CommonResult<ElectricityPriceRespDto> selectById(@PathVariable Integer id) {
        return electricityPriceService.selectById(id);
    }

    @ApiOperation("获取电价详细列表")
    @ApiImplicitParam(name = "electricityPricePageReqDto", value = "电价信息", dataType = "UserEntity")
    @GetMapping("/page")
    public CommonResult<ElectricityPricePageRespDto> page(@RequestBody ElectricityPricePageReqDto electricityPricePageReqDto) {
        return electricityPriceService.page(electricityPricePageReqDto);
    }
}
