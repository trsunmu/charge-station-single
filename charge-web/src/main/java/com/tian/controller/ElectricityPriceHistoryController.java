package com.tian.controller;

import com.tian.dto.price.*;
import com.tian.service.ElectricityPriceHistoryService;
import com.tian.util.CommonResult;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年11月01日 15:55
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 * <p>
 * 电量价格变动历史记录表
 */
@RestController
@RequestMapping("/electricity/price/history")
public class ElectricityPriceHistoryController {
    @Resource
    private ElectricityPriceHistoryService electricityPriceHistoryService;

    @PostMapping("/add")
    public CommonResult<ElectricityPriceHistoryAddRespDto> add(@RequestBody ElectricityPriceHistoryAddReqDto electricityPriceHistoryAddReqDto) {
        return electricityPriceHistoryService.add(electricityPriceHistoryAddReqDto);
    }

    @PostMapping("/update")
    public CommonResult<ElectricityPriceHistoryUpdateRespDto> update(@RequestBody ElectricityPriceHistoryUpdateReqDto electricityPriceHistoryUpdateReqDto) {
        return electricityPriceHistoryService.update(electricityPriceHistoryUpdateReqDto);
    }

    @GetMapping("/{id}")
    public CommonResult<ElectricityPriceHistoryRespDto> selectById(@PathVariable Integer id) {
        return electricityPriceHistoryService.selectById(id);
    }

    @GetMapping("/page")
    public CommonResult<ElectricityPriceHistoryPageRespDto> page(@RequestBody ElectricityPriceHistoryPageReqDto electricityPriceHistoryPageReqDto) {
        return electricityPriceHistoryService.page(electricityPriceHistoryPageReqDto);
    }
}
