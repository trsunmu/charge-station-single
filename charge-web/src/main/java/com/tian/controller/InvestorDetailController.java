package com.tian.controller;

import com.tian.annotation.LoginCheckAnnotation;
import com.tian.dto.investDetail.*;
import com.tian.service.InvestorDetailService;
import com.tian.util.CommonResult;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年11月03日 16:34
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 */
@RestController
@RequestMapping("/investor/detail")
public class InvestorDetailController {
    @Resource
    private InvestorDetailService investorDetailService;

    @PostMapping("/add")
    @LoginCheckAnnotation
    public CommonResult<InvestorDetailAddRespDto> add(@RequestBody InvestorDetailAddReqDto investorDetailAddReqDto) {
        return investorDetailService.add(investorDetailAddReqDto);
    }

    @PostMapping("/update")
    public CommonResult<InvestorDetailUpdateRespDto> update(@RequestBody InvestorDetailUpdateReqDto investorDetailUpdateReqDto) {
        return investorDetailService.update(investorDetailUpdateReqDto);
    }

    @GetMapping("/page")
    public CommonResult<InvestorDetailPageRespDto> page(@RequestBody InvestorDetailPageReqDto investorDetailPageReqDto) {
        return investorDetailService.page(investorDetailPageReqDto);
    }
}
