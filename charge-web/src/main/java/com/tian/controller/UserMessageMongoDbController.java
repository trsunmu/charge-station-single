package com.tian.controller;

import com.tian.annotation.LoginCheckAnnotation;
import com.tian.asyn.SystemUserMessageAsynchronous;
import com.tian.dto.message.*;
import com.tian.mongo.domain.UserMessage;
import com.tian.mongo.service.UserMessageService;
import com.tian.util.CommonResult;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * {@code @description:} MongoDB版存储站内信
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024/1/13 11:22
 * {@code @version:} 1.0
 */
@RestController
@RequestMapping("/user/message")
public class UserMessageMongoDbController {
    @Resource
    private UserMessageService userMessageService;
    @Resource
    private SystemUserMessageAsynchronous systemUserMessageAsynchronous;

    @GetMapping("/test")
    public void createUser() {
        for (int i = 0; i < 100; i++) {
            try {
                Thread.sleep(2);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            systemUserMessageAsynchronous.sendSystemUserMessageNoParamsMongoDb(2, 1, null);
        }
    }

    @GetMapping("/{id}")
    @LoginCheckAnnotation
    public CommonResult<SystemUserMessageRespDto> queryById(@PathVariable("id") Long id) {
        return userMessageService.queryById(id);
    }

    /**
     * 我的站内信列表
     *
     * @param messageRecordPageReqDto 查询参数
     * @return 站内信列表
     */
    @PostMapping("/page")
    @LoginCheckAnnotation
    public CommonResult<SystemUserMessagePageRespDto> page(@RequestBody SystemUserMessageRecordPageReqDto messageRecordPageReqDto) {
        return userMessageService.page(messageRecordPageReqDto);
    }

    @PostMapping("/read")
    @LoginCheckAnnotation
    public CommonResult<SystemUserMessageUpdateRespDto> read(@RequestBody SystemUserMessageRecordUpdateReqDto recordUpdateReqDto) {
        return userMessageService.read(recordUpdateReqDto);
    }

    @PostMapping("/read/all")
    @LoginCheckAnnotation
    public CommonResult<SystemUserMessageUpdateRespDto> readAll(@RequestBody SystemUserMessageRecordUpdateReqDto recordUpdateReqDto) {
        return userMessageService.readAll();
    }

    /**
     * 删除部分站内信
     */
    @PostMapping("/delete")
    @LoginCheckAnnotation
    public CommonResult<SystemUserMessageUpdateRespDto> delete(@RequestBody SystemUserMessageRecordUpdateReqDto recordUpdateReqDto) {
        return userMessageService.read(recordUpdateReqDto);
    }
}
