package com.tian.controller;

import com.tian.dto.GoodsInfoDto;
import com.tian.service.GoodsInfoService;
import com.tian.util.CommonResult;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;


/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * @date 2023年06月10日 19:54
 * 博客地址：<a href="http://woaijava.cc/">博客地址</a>
 * <p>
 * 商品信息管理
 */
@RestController
@RequestMapping("/goods/info")
public class GoodsInfoController {

    @Resource
    private GoodsInfoService goodsInfoService;


    /**
     * 查询单个商品信息
     *
     * @param id 商品id
     * @return 商品信息
     */
    @GetMapping("/{id}")
    public CommonResult<GoodsInfoDto> getById(@PathVariable("id") Integer id) {
        return goodsInfoService.getById(id);
    }
}
