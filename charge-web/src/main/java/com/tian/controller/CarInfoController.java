package com.tian.controller;

import com.tian.annotation.LoginCheckAnnotation;
import com.tian.dto.car.*;
import com.tian.service.CarInfoService;
import com.tian.util.CommonResult;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年10月31日 22:18
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 * <p>
 * 我的汽车信息 包括认证
 */
@RestController
@RequestMapping("/car/info")
public class CarInfoController {
    @Resource
    private CarInfoService carInfoService;


    @PostMapping("/add")
    @LoginCheckAnnotation
    public CommonResult<CarInfoAddRespDto> addCarInfo(@RequestBody CarInfoAddReqDto carInfoAddReqDto) {
        return carInfoService.add(carInfoAddReqDto);
    }

    @PostMapping("/update")
    public CommonResult<CarInfoUpdateRespDto> updateCarInfo(@RequestBody CarInfoUpdateReqDto carInfoUpdateReqDto) {
        return carInfoService.updateCarInfo(carInfoUpdateReqDto);
    }

    @GetMapping("/get/{id}")
    public CommonResult<CarInfoRespDto> getCarInfoById(@PathVariable int id) {
        return carInfoService.getCarInfoById(id);
    }
}
