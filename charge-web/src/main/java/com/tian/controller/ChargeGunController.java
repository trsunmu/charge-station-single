package com.tian.controller;

import com.tian.dto.gun.*;
import com.tian.service.ChargeGunService;
import com.tian.util.CommonResult;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年11月01日 10:23
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 */
@RestController
@RequestMapping("/gun")
public class ChargeGunController {
    @Resource
    private ChargeGunService chargeGunService;

    @PostMapping("/add")
    public CommonResult<ChargeGunAddRespDto> add(@RequestBody ChargeGunAddReqDto chargeGunAddReqDto) {
        return chargeGunService.add(chargeGunAddReqDto);
    }

    @PostMapping("/update")
    public CommonResult<ChargeGunUpdateRespDto> update(@RequestBody ChargeGunUpdateReqDto chargeGunUpdateReqDto) {
        return chargeGunService.update(chargeGunUpdateReqDto);
    }

    @GetMapping("/{id}")
    public CommonResult<ChargeGunRespDto> selectById(@PathVariable Integer id) {
        return chargeGunService.selectById(id);
    }

    @GetMapping("/page")
    public CommonResult<ChargeGunPageRespDto> page(@RequestBody ChargeGunPageReqDto chargeGunPageReqDto) {
        return chargeGunService.page(chargeGunPageReqDto);
    }
}
