package com.tian.controller.activity;

import com.tian.dto.coupon.ActivityCouponDto;
import com.tian.dto.coupon.ActivityCouponListRespDto;
import com.tian.dto.coupon.ActivityCouponReqDto;
import com.tian.dto.user.UserCouponAddDto;
import com.tian.entity.ActivityCoupon;
import com.tian.service.ActivityCouponService;
import com.tian.util.CommonResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * @date 2023年05月15日 15:29
 * 博客地址：<a href="http://woaijava.cc/">博客地址</a>
 * <p>
 * 运营人 给活动关联优惠券
 */
@RestController
@RequestMapping("/activity/coupon")
public class ActivityCouponController {

    @Resource
    private ActivityCouponService activityCouponService;

    /**
     * 活动关联优惠券
     */
    @PostMapping("/add")
    public CommonResult<Boolean> add(@RequestBody ActivityCouponDto activityCouponDto) {
        return activityCouponService.add(activityCouponDto);
    }

    /**
     * 活动关联优惠券
     */
    @PostMapping("/list")
    public CommonResult<ActivityCouponListRespDto> list(@RequestBody ActivityCouponReqDto activityCouponReqDto) {
        return activityCouponService.list(activityCouponReqDto);
    }

    @PostMapping("/check")
    public CommonResult<ActivityCoupon> check(@RequestBody UserCouponAddDto userCouponAddDto) {
        return activityCouponService.check(userCouponAddDto);
    }
}
