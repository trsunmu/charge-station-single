package com.tian.controller.activity;

import com.tian.dto.activityGoods.*;
import com.tian.service.ActivityGoodsService;
import com.tian.util.CommonResult;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年10月31日 15:43
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 * <p>
 * 活动商品
 */

@RestController
@RequestMapping("/activityGoods")
public class ActivityGoodsController {
    @Resource
    private ActivityGoodsService activityGoodsService;

    @PostMapping("/add")
    public CommonResult<ActivityGoodsAddRespDto> add(@RequestBody ActivityGoodsAddReqDto activityGoodsAddReqDto) {
        return activityGoodsService.add(activityGoodsAddReqDto);
    }

    @PutMapping("/update")
    public CommonResult<ActivityGoodsUpdateRespDto> update(@RequestBody ActivityGoodsUpdateReqDto activityGoodsUpdateReqDto) {
        return activityGoodsService.update(activityGoodsUpdateReqDto);
    }

    @GetMapping("/{id}")
    public CommonResult<ActivityGoodsRespDto> selectById(@PathVariable("id") Integer id) {
        return activityGoodsService.selectById(id);
    }

    @GetMapping("/page")
    public CommonResult<ActivityGoodsPageRespDto> page(ActivityGoodsPageReqDto activityGoodsPageReqDto) {
        return activityGoodsService.page(activityGoodsPageReqDto);
    }
}
