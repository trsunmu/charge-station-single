package com.tian.controller.activity;

import com.tian.dto.activity.ActivityDto;
import com.tian.dto.activity.ActivityListRespDto;
import com.tian.service.ActivityService;
import com.tian.util.CommonResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * @date 2023年05月15日 15:27
 * 博客地址：<a href="http://woaijava.cc/">博客地址</a>
 * <p>
 * 运营人新增活动、查询活动列表
 */
@RestController
@RequestMapping("/activity")
public class ActivityController {

    @Resource
    private ActivityService activityService;

    /**
     * 新增活动
     *
     * @param activityDto 活动信息
     * @return 新增成功
     */
    @PostMapping("/add")
    public CommonResult<Boolean> add(@RequestBody ActivityDto activityDto) {
        return activityService.add(activityDto);
    }

    /**
     * 查询活动列表
     *
     * @param activityDto 查询条件
     * @return 活动信息列表
     */
    @PostMapping("/list")
    public CommonResult<ActivityListRespDto> list(@RequestBody ActivityDto activityDto) {
        return activityService.list(activityDto);
    }
}
