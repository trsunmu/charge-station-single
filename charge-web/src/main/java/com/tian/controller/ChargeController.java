package com.tian.controller;

import com.tian.annotation.LoginCheckAnnotation;
import com.tian.dto.charger.ChargerRecordUpdateReqDto;
import com.tian.dto.charger.ChargerRecordUpdateRespDto;
import com.tian.service.ChargeService;
import com.tian.service.ChargerRecordService;
import com.tian.util.CommonResult;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * @date 2023年06月20日 08:56
 * 博客地址：<a href="http://woaijava.cc/">博客地址</a>
 * <p>
 * 充电桩充电
 */
@RestController
@RequestMapping("/charge")
public class ChargeController {

    @Resource
    private ChargeService chargeService;
    @Resource
    private ChargerRecordService chargerRecordService;

    /**
     * 开启充电服务
     */
    @GetMapping(value = "/start/{gunNo}")
    @LoginCheckAnnotation
    public CommonResult<Boolean> start(@PathVariable("gunNo") String gunNo) {
        return chargeService.start(gunNo);
    }

    /**
     * 用户手动启发关闭充电
     */
    @PostMapping("/stop")
    @LoginCheckAnnotation
    public CommonResult<ChargerRecordUpdateRespDto> stop(@RequestBody ChargerRecordUpdateReqDto chargerRecordUpdateReqDto) {
        return chargerRecordService.readyStopCharge(chargerRecordUpdateReqDto);
    }
}
