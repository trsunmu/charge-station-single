package com.tian.controller;

import com.tian.config.AliyunOssService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

/**
 * {@code @description:} 用户头像上传、行驶证、驾驶证等信息上传
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024/1/9 9:31
 * {@code @version:} 1.0
 */
@Controller
@RequestMapping("/file")
public class FileUpDownController {
    @Resource
    private AliyunOssService aliyunOssService;

    @RequestMapping("/blogs/md/uploadfile")
    @ResponseBody
    public Map<String, Object> imgUpdate(@RequestParam(value = "editormd-image-file", required = false) MultipartFile file) throws IOException {

        System.out.println("文件上传");


        InputStream is = new ByteArrayInputStream(file.getBytes());

        //文件名
        String trueFileName = file.getOriginalFilename();
        //后缀名
        String suffix = trueFileName.substring(trueFileName.lastIndexOf("."));
        //重新命名
        String fileName = System.currentTimeMillis() + suffix;
        //图片OSS完整地址
        String result = aliyunOssService.uploadFile(fileName, is);
        Map<String,Object> res= new HashMap<>();
        if (!result.equals("error")) {
            res.put("success", 0);
            res.put("message", "upload success!");
            res.put("url", result);
        } else {
            res.put("success", 1);
            res.put("message", "upload fail!");
        }
        return res;
    }
}
