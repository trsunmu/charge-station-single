package com.tian.controller.user;

import com.tian.dto.user.UserGoodsDto;
import com.tian.dto.user.UserGoodsReqDto;
import com.tian.dto.user.UserGoodsRespDto;
import com.tian.service.UserGoodsService;
import com.tian.util.CommonResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * @date 2023年06月10日 20:11
 * 博客地址：<a href="http://woaijava.cc/">博客地址</a>
 * <p>
 * 用户积分兑换商品
 */
@RestController
@RequestMapping("/user/goods")
public class UserGoodsController {
    @Resource
    private UserGoodsService userGoodsService;

    /**
     * 用户使用积分兑换商品
     *
     * @param userGoodsDto 领取商品
     * @return 领取成功
     */
    @PostMapping("/exchange")
    public CommonResult<Boolean> exchange(UserGoodsDto userGoodsDto) {
        return userGoodsService.exchange(userGoodsDto);
    }

    /**
     * 我的商品
     *
     * @param userGoodsReqDto 查询条件
     * @return 用户已领取商品列表
     */
    @GetMapping("/list")
    public CommonResult<UserGoodsRespDto> list(UserGoodsReqDto userGoodsReqDto) {
        return userGoodsService.list(userGoodsReqDto);
    }
}
