package com.tian.controller.user;

import com.tian.annotation.LoginCheckAnnotation;
import com.tian.dto.coupon.UserActivityCouponPageReqDto;
import com.tian.dto.user.UserCouponAddDto;
import com.tian.dto.user.UserCouponRecordListRespDto;
import com.tian.service.UserCouponService;
import com.tian.util.CommonResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年11月15日 14:25
 * 博客地址：<a href="http://woaijava.cc/">博客地址</a>
 * <p>
 * 我的优惠券
 */
@RestController("/user/coupon")
public class UserCouponController {


    @Resource(name = "userCouponService")
    private UserCouponService userCouponService;
    @Resource(name = "decoratorCouponService")
    private UserCouponService decoratorCouponService;

    /**
     * 用户领取优惠券
     */
    @PostMapping("/receive")
    @LoginCheckAnnotation
    public CommonResult<Boolean> receive(@RequestBody UserCouponAddDto userCouponAddDto) {
        return userCouponService.receive(userCouponAddDto);
    }

    /**
     * 用户已领取优惠券列表（全部、未使用、已使用、过期）
     */
    @PostMapping("/list")
    @LoginCheckAnnotation
    public CommonResult<UserCouponRecordListRespDto> list(@RequestBody UserActivityCouponPageReqDto userActivityCouponPageReqDto) {
        return userCouponService.list(userActivityCouponPageReqDto);
    }
}
