package com.tian.controller.user;

import com.tian.annotation.LoginCheckAnnotation;
import com.tian.annotation.SysLogAnnotation;
import com.tian.dto.InvitedDto;
import com.tian.dto.user.UserLoginReqDto;
import com.tian.dto.user.UserLoginResDto;
import com.tian.dto.user.UserUpdatePasswordReqDto;
import com.tian.entity.User;
import com.tian.service.UserService;
import com.tian.util.CommonResult;
import com.tian.util.RedisConstantPre;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 博客地址：<a href="http://woaijava.cc/">博客地址</a>
 * <p>
 * 用户电脑端登录
 */
@Slf4j
@RestController
@RequestMapping("/user")
public class UserWebController {

    @Resource
    private UserService userService;

    /**
     * 用户采用 手机验证码登录
     */
    @PostMapping("/login")
    public CommonResult<UserLoginResDto> login(@RequestBody UserLoginReqDto userLoginReqDto) {
        return userService.login(userLoginReqDto, RedisConstantPre.SEND_CODE_LOGIN_PRE);
    }

    /**
     * 手机验证码注册
     *
     * @param invitedDto 注册参数--手机号 openId 验证码
     * @return 是否成功
     */
    @PostMapping("/register")
    @SysLogAnnotation
    public CommonResult<User> register(@RequestBody InvitedDto invitedDto) {
        return userService.register(invitedDto, RedisConstantPre.SEND_CODE_REGISTER_PRE);
    }

    @GetMapping("/registerPage")
    @SysLogAnnotation
    public CommonResult<User> registerPage(String invitedCode) {
        log.info(invitedCode);
        return CommonResult.success();
    }

    @PostMapping("/update")
    @SysLogAnnotation
    @LoginCheckAnnotation
    public CommonResult<User> update(@RequestBody InvitedDto invitedDto) {
        return userService.update(invitedDto, RedisConstantPre.SEND_CODE_REGISTER_PRE);
    }

    @PostMapping("/update/password")
    @SysLogAnnotation
    @LoginCheckAnnotation
    public CommonResult<User> updatePassword(@RequestBody UserUpdatePasswordReqDto userUpdatePasswordReqDto) {
        return userService.updatePassword(userUpdatePasswordReqDto, RedisConstantPre.SEND_CODE_PASSWORD_PRE);
    }
}
