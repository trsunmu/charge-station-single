package com.tian.controller.user;

import com.tian.annotation.LoginCheckAnnotation;
import com.tian.dto.user.UserPointDto;
import com.tian.dto.user.UserPointUpdateDto;
import com.tian.dto.user.UserRankResDto;
import com.tian.service.UserPointService;
import com.tian.util.CommonResult;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * @date 2023年06月05日 16:39
 * 博客地址：<a href="http://woaijava.cc/">博客地址</a>
 * <p>
 * 用户积分
 */
@RestController
@RequestMapping("/user/point")
public class UserPointController {

    @Resource
    private UserPointService userPointService;

    @PostMapping("/init")
    @LoginCheckAnnotation
    public CommonResult<Boolean> init(@RequestBody UserPointDto userPointDto) {
        return userPointService.add(userPointDto.getPoints());
    }

    @PostMapping("/update")
    @LoginCheckAnnotation
    public CommonResult<Boolean> update(@RequestBody UserPointUpdateDto userPointUpdateDto) {
        return userPointService.update(userPointUpdateDto);
    }

    @GetMapping("/rank/{rankCount}")
    public CommonResult<Boolean> userPointRank(@PathVariable Integer rankCount) {
        return userPointService.userPointRank(rankCount);
    }

    @GetMapping("/rank/user/{userId}")
    public CommonResult<Long> getUserPointRank(@PathVariable Long userId) {
        return userPointService.getUserPointRank(userId);
    }

    @GetMapping("/rank/list/{rankCount}")
    public CommonResult<UserRankResDto> rankList(@PathVariable Integer rankCount) {
        return userPointService.rankList(rankCount);
    }
}
