package com.tian.controller;

import com.tian.dto.charger.*;
import com.tian.service.ChargerService;
import com.tian.util.CommonResult;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年11月01日 10:49
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 */
@RestController
@RequestMapping("/charger")
public class ChargerController {
    @Resource
    private ChargerService chargerService;

    @GetMapping("/{id}")
    public CommonResult<ChargerRespDto> selectById(@PathVariable Integer id) {
        return chargerService.selectById(id);
    }

    @GetMapping("/page")
    public CommonResult<ChargerPageRespDto> page(@RequestBody ChargerPageReqDto chargerPageReqDto) {
        return chargerService.page(chargerPageReqDto);
    }
}
