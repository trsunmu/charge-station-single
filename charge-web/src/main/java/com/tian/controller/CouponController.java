package com.tian.controller;

import com.tian.dto.coupon.CouponInfoReqDto;
import com.tian.dto.coupon.CouponListRespDto;
import com.tian.service.CouponService;
import com.tian.util.CommonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * @date 2023年05月15日 14:27
 * 博客地址：<a href="http://woaijava.cc/">博客地址</a>
 * <p>
 * 查询优惠券列表
 */
@Api("优惠券")
@RestController
@RequestMapping("/coupon")
public class CouponController {

    @Resource
    private CouponService couponService;

    /**
     * 查询优惠券列表
     */
    @ApiOperation("查询优惠券列表")
    @PostMapping("/list")
    public CommonResult<CouponListRespDto> list(@RequestBody CouponInfoReqDto couponInfoReqDto) {
        return couponService.list(couponInfoReqDto);
    }
}
