package com.tian.controller;

import com.tian.dto.MessageTemplateDto;
import com.tian.service.MessageTemplateService;
import com.tian.util.CommonResult;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * @date 2023年05月26日 20:24
 * 博客地址：<a href="http://woaijava.cc/">博客地址</a>
 * <p>
 * 短信模板操作
 */
@RestController
@RequestMapping("/message/template")
public class MessageTemplateController {


    @Resource
    private MessageTemplateService messageTemplateService;

    /**
     * 新增短信模板
     *
     * @param messageTemplateDto 模板信息
     * @return 更新成功
     */
    @PostMapping("/add")
    public CommonResult<Boolean> add(MessageTemplateDto messageTemplateDto) {
        return messageTemplateService.add(messageTemplateDto);
    }

    /**
     * 更新短信模板
     *
     * @param messageTemplateDto 模板信息
     * @return 更新成功
     */
    @PostMapping("/update")
    public CommonResult<Boolean> update(MessageTemplateDto messageTemplateDto) {
        return messageTemplateService.update(messageTemplateDto);
    }

    /**
     * 通过类型查询
     *
     * @param messageType 短信模板类型
     * @return 短信模板信息
     */
    @GetMapping("/query/type")
    public CommonResult<MessageTemplateDto> queryByMessageType(@RequestParam("messageType") Integer messageType) {
        return messageTemplateService.queryByMessageType(messageType);
    }
}
