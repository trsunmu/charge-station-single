package com.tian.controller;

import com.tian.annotation.LoginCheckAnnotation;
import com.tian.asyn.SystemUserMessageAsynchronous;
import com.tian.dto.message.*;
import com.tian.service.SystemUserMessageService;
import com.tian.util.CommonResult;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * {@code @description:} 我的站内信
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024/1/12 10:01
 * {@code @version:} 1.0
 */
@RestController
@RequestMapping("/system/message")
public class UserMessageMySQLController {
    @Resource
    private SystemUserMessageService systemUserMessageService;
    @Resource
    private SystemUserMessageAsynchronous systemUserMessageAsynchronous;

    /**
     * 查询站内信内容详情
     *
     * @param id 站内信id
     * @return 站内信内容
     */
    @GetMapping("/{id}")
    @LoginCheckAnnotation
    public CommonResult<SystemUserMessageRespDto> queryById(@PathVariable("id") Long id) {
        return systemUserMessageService.queryById(id);
    }

    /**
     * 我的站内信列表
     *
     * @param messageRecordPageReqDto 查询参数
     * @return 站内信列表
     */
    @PostMapping("/page")
    @LoginCheckAnnotation
    public CommonResult<SystemUserMessagePageRespDto> page(@RequestBody SystemUserMessageRecordPageReqDto messageRecordPageReqDto) {
        return systemUserMessageService.page(messageRecordPageReqDto);
    }

    /**
     * 阅读站内信内容---全部阅读---清楚未读
     *
     * @return 阅读成功
     */
    @PostMapping("/read/all")
    @LoginCheckAnnotation
    public CommonResult<SystemUserMessageUpdateRespDto> readAll() {
        return systemUserMessageService.readAll();
    }

    /**
     * 阅读站内信内容---单条阅读
     *
     * @param recordUpdateReqDto 请求参数
     * @return 阅读成功
     */
    @PostMapping("/read")
    @LoginCheckAnnotation
    public CommonResult<SystemUserMessageUpdateRespDto> read(@RequestBody SystemUserMessageRecordUpdateReqDto recordUpdateReqDto) {
        return systemUserMessageService.read(recordUpdateReqDto);
    }

    /**
     * 删除部分站内信
     */
    @PostMapping("/delete")
    @LoginCheckAnnotation
    public CommonResult<SystemUserMessageUpdateRespDto> delete(@RequestBody SystemUserMessageRecordUpdateReqDto recordUpdateReqDto) {
        return systemUserMessageService.read(recordUpdateReqDto);
    }


    @GetMapping("/test")
    public void test() {
        systemUserMessageAsynchronous.sendSystemUserMessageNoParams(1, 1);
    }
}