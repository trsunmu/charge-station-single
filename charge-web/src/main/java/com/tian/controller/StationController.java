package com.tian.controller;

import com.tian.dto.station.*;
import com.tian.service.StationService;
import com.tian.util.CommonResult;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * @date 2023年05月12日 12:50
 * 博客地址：<a href="http://woaijava.cc/">博客地址</a>
 * <p>
 * 充电桩信息
 */
@RestController
@RequestMapping("/station")
public class StationController {
    @Resource
    private StationService stationService;

    @GetMapping("/{id}")
    public CommonResult<StationRespDto> selectById(@PathVariable("id") Integer id) {
        return stationService.getStationById(id);
    }

    @GetMapping("/page")
    public CommonResult<StationPageRespDto> page(@RequestBody StationPageReqDto StationPageReqDto) {
        return stationService.page(StationPageReqDto);
    }
}
