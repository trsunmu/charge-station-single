package com.tian.enums;

/**
 * @author tianwc 公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * @date 2023年05月26日 20:40
 * 博客地址：<a href="http://woaijava.cc/">博客地址</a>
 */
public enum InsertUpdateDateBaseFlagEnum {
    SUCCESS(1),
    FAILED(0);
    private int flag;

    InsertUpdateDateBaseFlagEnum(int flag) {
        this.flag = flag;
    }

    public int getFlag() {
        return flag;
    }
}
