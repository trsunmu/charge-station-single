package com.tian.enums;

/**
 * @author tianwc 公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * @date 2023年10月15日 17:58
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 * <p>
 * 交易状态.
 * trade_state
 * 是
 * String(32)
 * SUCCESS
 * SUCCESS—支付成功,REFUND—转入退款,NOTPAY—未支付,CLOSED—已关闭,REVOKED—已撤销（刷卡支付）,USERPAYING--用户支付中,PAYERROR--支付失败(其他原因，如银行返回失败)
 */
public enum WxTradeStateEnum {
    /**
     * SUCCESS—支付成功
     */
    SUCCESS("SUCCESS","支付成功"),
    /**
     * REFUND—转入退款
     */
    REFUND("REFUND","转入退款"),
    /**
     * NOTPAY—未支付
     */
    NOTPAY("NOTPAY","未支付"),
    /**
     * CLOSED—已关闭
     */
    CLOSED("CLOSED","已关闭"),
    /**
     * REVOKED—已撤销（刷卡支付）
     */
    REVOKED("REVOKED","已撤销（刷卡支付）"),
    /**
     * USERPAYING--用户支付中
     */
    USERPAYING("USERPAYING","用户支付中"),
    /**
     * PAYERROR--支付失败(其他原因，如银行返回失败)
     */
    PAYERROR("PAYERROR","支付失败(其他原因，如银行返回失败)"),;
    private String state;
    private String desc;

    WxTradeStateEnum(String state, String desc) {
        this.state = state;
        this.desc = desc;
    }

    public String getState() {
        return state;
    }

    public String getDesc() {
        return desc;
    }
}
