package com.tian.enums;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * @date 2022年10月28日 11:11
 * <p>
 * 枚举了一些常用API操作码
 * <p>
 * 200000  操作成功
 * 500000---599999  系统出错
 * 400001---499999  参数问题
 */
public enum ResultCode {
    /**
     * 成功
     */
    SUCCESS(200000, "操作成功"),
    /**
     * 失败
     */
    FAILED(500000, "操作失败"),
    /**
     * 服务熔断
     */
    FALLBACK_FAILED(500001, "服务熔断"),
    /**
     * 参数校验失败
     */
    VALIDATE_FAILED(400001, "参数检验失败"),
    /**
     * 验证码已过期
     */
    CODE_EXPIRE(400002, "验证码已过期"),
    /**
     * token过期
     */
    UNAUTHORIZED(400003, "暂未登录或token已经过期"),
    /**
     * 没权限
     */
    FORBIDDEN(400004, "没有相关权限"),
    /**
     * 验证码错误
     */
    CODE_ERROR(400005, "验证码错误"),

    /**
     * 验证码错误验证次数
     */
    CODE_ERROR_VALID_TIMES(400006, "验证码错误"),
    /**
     * 参数为空
     */
    PARAMETER_EMPTY(400007, "参数为空"),
    /**
     * 参数有误
     */
    PARAMETER_ERROR(400008, "参数有误"),
    UN_AUTHENTICATION(400009, "用户未认证"),

    /**
     * 活动已下架
     */
    ACTIVITY_DELETE(400010, "活动已下架"),
    /**
     * 活动没有此奖项
     */
    ACTIVITY_NO_COUPON(400011, "活动没有此奖项"),
    /**
     * 用户积分不够
     */
    ACTIVITY_NO_ENOUGH(400011, "用户积分不够"),
    /**
     * 活动已失效
     */
    ACTIVITY_EXPIRED(400012, "活动已失效"),
    /**
     * 用户领取此时已上线
     */
    USER_COUPON_LIMITER(400013, "用户领取已上线"),
    /**
     * 活动优惠券已领完
     */
    TREMAIN_COUNT_LIMITER(400014, "活动优惠券已领完"),
    /**
     *
     */
    GOODS_STOCK_NO_ENOUGH(400016, "商品库存不足"),
    /**
     * 短信发送太频繁
     */
    SEND_MESSAGE_LIMIT(400015, "短信发送太频繁~"),
    /**
     * topic 不存在
     */
    TOPIC_NOT_EXIST(400016, "topic 不存在~");

    private final int code;
    private final String message;

    ResultCode(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}