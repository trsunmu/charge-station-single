package com.tian.enums;

import java.util.HashMap;
import java.util.Map;

/**
 * @author tianwc 公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * @date 2023年09月23日 16:04
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 */
public enum BusiTypeEnum {
    /**
     * 用户注册成功送积分（注册人获取积分）
     */
    REGISTER_TO_POINTS(1, "用户注册成功送积分"),
    /**
     * 邀请他人注册送积分（邀请人获取积分）
     */
    INVITED_REGISTER_TO_POINTS(2, "邀请注册送积分"),
    INVITED_REGISTER_TO_BALANCE(3, "邀请用户注册获取收益"),
    LOGIN_TO_POINTS(4, "登录送积分"),
    CHARGE_BALANCE_TO_POINTS(5, "充值送积分"),
    CHARGE_TO_POINTS(6, "充电送积分"),
    /**
     * 用户余额到多少时，提示用户充值
     */
    REMIND_TO_CHARGE_BALANCE(6, "提示用户充值");

    private static Map<Integer, BusiTypeEnum> cacheMap = new HashMap<>();

    static {
        for (BusiTypeEnum value : BusiTypeEnum.values()) {
            cacheMap.put(value.type, value);
        }
    }

    private final int type;
    private final String desc;

    BusiTypeEnum(int type, String desc) {
        this.type = type;
        this.desc = desc;
    }

    public int getType() {
        return type;
    }

    public String getDesc() {
        return desc;
    }

    public static BusiTypeEnum findByBusiType(int type) {
        return cacheMap.get(type);
    }
}
