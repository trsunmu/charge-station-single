package com.tian.enums;

/**
 * @author tianwc 公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * @date 2023年06月05日 17:12
 * 博客地址：<a href="http://woaijava.cc/">博客地址</a>
 */
public enum StatusEnums {
    /**
     * 初始化
     */
    INIT(0, "初始化"),
    /**
     * 成功
     */
    SUCCESS(1, "成功"),
    /**
     * 失败
     */
    FAILED(2, "失败");

    private int status;
    private String desc;

    StatusEnums(int status, String desc) {
        this.status = status;
        this.desc = desc;
    }

    public int getStatus() {
        return status;
    }

    public String getDesc() {
        return desc;
    }
}
