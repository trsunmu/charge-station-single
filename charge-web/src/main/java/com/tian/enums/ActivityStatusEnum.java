package com.tian.enums;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * @date 2023年05月15日 18:06
 * 博客地址：<a href="http://woaijava.cc/">博客地址</a>
 */
public enum ActivityStatusEnum {
    /**
     * 活动正常状态
     */
    INIT(0, "初始状态"),
    /**
     * 特殊情况下架
     */
    DELETE(1, "下架");
    private final int status;
    private final String desc;

    ActivityStatusEnum(int status, String desc) {
        this.status = status;
        this.desc = desc;
    }

    public int getStatus() {
        return status;
    }

    public String getDesc() {
        return desc;
    }
}
