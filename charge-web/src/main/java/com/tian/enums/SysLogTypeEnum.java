package com.tian.enums;

/**
 * @author tianwc 公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 博客地址：<a href="http://woaijava.cc/">博客地址</a>
 * <p>
 * 日志存储格式
 */
public enum SysLogTypeEnum {
    /**
     * 日志存储格式为文件
     */
    FILE(0),
    /**
     * 日志存储格式为数据库
     */
    DB(1),
    /**
     * 日志存储格式为ES
     */
    ES(2);

    private final int type;

    SysLogTypeEnum(int type) {
        this.type = type;
    }

    public int getType() {
        return type;
    }
}
