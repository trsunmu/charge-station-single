package com.tian.enums;

/**
 * @author tianwc 公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * @date 2023年10月15日 19:39
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 * <p>
 * 充电桩设备状态
 */
public enum StationPileStatusEnum {
    /**
     * 设备空着 能用
     */
    SUCCESS(1, "正常"),
    /**
     * 设备在充电中
     */
    DOING(2, "充电桩"),
    /**
     * 设备不可用
     */
    BROKEN(3, "设备维修中");
    private final int status;
    private final String desc;

    StationPileStatusEnum(int status, String desc) {
        this.status = status;
        this.desc = desc;
    }

    public int getStatus() {
        return status;
    }

    public String getDesc() {
        return desc;
    }
}
