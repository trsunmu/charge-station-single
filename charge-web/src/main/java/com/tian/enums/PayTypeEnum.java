package com.tian.enums;

/**
 * @author tianwc 公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * @date 2023年05月12日 16:19
 * 博客地址：<a href="http://woaijava.cc/">博客地址</a>
 */
public enum PayTypeEnum {
    /**
     * 暂定两种支付方式
     */
    WX_PAY(0, "微信支付"),
    ZFB_PAY(1, "支付宝支付");
    private final int payType;
    private final String desc;

    PayTypeEnum(int payType, String desc) {
        this.payType = payType;
        this.desc = desc;
    }

    public int getPayType() {
        return payType;
    }

    public String getDesc() {
        return desc;
    }
}
