package com.tian.enums;

/**
 * @author tianwc 公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * @date 2023年05月26日 20:40
 * 博客地址：<a href="http://woaijava.cc/">博客地址</a>
 * 积分类型
 */
public enum UserPointRecordTypeEnum {
    ADD(0),
    DELETE(1);
    private int type;

    UserPointRecordTypeEnum(int flag) {
        this.type = flag;
    }

    public int getType() {
        return type;
    }
}
