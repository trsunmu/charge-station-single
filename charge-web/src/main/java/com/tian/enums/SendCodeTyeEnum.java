package com.tian.enums;

/**
 * @author tianwc 公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * @date 2023年10月14日 15:20
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 */
public enum SendCodeTyeEnum {
    /**
     * 阿里
     */
    ALI(0),
    /**
     * 百度
     */
    BAIDU(1),
    /**
     * 华为
     */
    HUAWIE(2);

    private final int type;

    SendCodeTyeEnum(int type) {
        this.type = type;
    }

    public int getType() {
        return type;
    }
}
