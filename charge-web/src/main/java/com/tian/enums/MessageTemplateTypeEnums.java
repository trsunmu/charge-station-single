package com.tian.enums;

/**
 * @author tianwc 公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * @date 2023年05月26日 23:41
 * 博客地址：<a href="http://woaijava.cc/">博客地址</a>
 * <p>
 * 短信验证码模板
 */
public enum MessageTemplateTypeEnums {
    /**
     * 登录短信模板
     */
    LOGIN_MESSAGE_TEMPLATE(1, "登录短信模板"),
    /**
     * 支付成功短信模板
     */
    PAY_SUCCESS_TEMPLATE(2, "支付成功短信模板");

    private final int type;
    private final String desc;

    MessageTemplateTypeEnums(int type, String desc) {
        this.type = type;
        this.desc = desc;
    }

    public int getType() {
        return type;
    }

    public String getDesc() {
        return desc;
    }
}
