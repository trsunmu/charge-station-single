package com.tian.enums;

/**
 * @author tianwc 公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年11月08日 10:01
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 *
 * 充电记录 充电状态
 */
public enum ChargerStatusEnum {
    CHARGEING(0,"充电中"),
    CHARGER_FINISH(1,"充电完成");
    private final int chargerStatus;
    private final String desc;

    ChargerStatusEnum(int chargerStatus, String desc) {
        this.chargerStatus = chargerStatus;
        this.desc = desc;
    }

    public int getChargerStatus() {
        return chargerStatus;
    }

    public String getDesc() {
        return desc;
    }
}
