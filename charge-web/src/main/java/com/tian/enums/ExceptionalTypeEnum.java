package com.tian.enums;

/**
 * @author tianwc 公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * @date 2023年09月24日 21:49
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 * <p>
 * 异常反馈--异常类型
 */
public enum ExceptionalTypeEnum {
    DEVICE_UNDER_LINE(1,"设备离线"),
    CHARGE_FAILED(2,"充不进电"),
    PLUG_DISCONNECT(3,"插座异常断开"),
    ORDER_SETTLEMENT_FAILED(4,"订单结算异常");

    private final int type;
    private final String desc;

    ExceptionalTypeEnum(int type, String desc) {
        this.type = type;
        this.desc = desc;
    }

    public int getType() {
        return type;
    }

    public String getDesc() {
        return desc;
    }
}
