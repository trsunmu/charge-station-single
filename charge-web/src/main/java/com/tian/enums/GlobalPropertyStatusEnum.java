package com.tian.enums;

/**
 * @author tianwc 公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * @date 2023年09月22日 23:03
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 */
public enum GlobalPropertyStatusEnum {
    INIT(0),
    DELETE(1);
    private int status;

    GlobalPropertyStatusEnum(int flag) {
        this.status = flag;
    }

    public int getStatus() {
        return status;
    }
}
