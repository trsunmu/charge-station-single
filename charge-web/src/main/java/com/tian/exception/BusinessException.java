package com.tian.exception;

import com.tian.enums.ResultCode;
import lombok.Getter;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * @date 2023年06月11日 22:41
 * 博客地址：<a href="http://woaijava.cc/">博客地址</a>
 * <p>
 * 自定义异常
 */
@Getter
public class BusinessException extends RuntimeException {
    /**
     * http状态码
     */
    private Integer code;

    private Object object;

    public BusinessException(String message, Integer code, Object object) {
        super(message);
        this.code = code;
        this.object = object;
    }

    public BusinessException(ResultCode resultCode) {
        super(resultCode.getMessage());
        this.code = resultCode.getCode();
    }

    public BusinessException(ResultCode resultCode, String message) {
        super(resultCode.getMessage());
        this.code = resultCode.getCode();
    }

    public BusinessException(String message) {
        super(message);
    }
}
