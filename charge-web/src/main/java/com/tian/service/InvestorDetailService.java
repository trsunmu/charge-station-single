package com.tian.service;

import com.tian.dto.investDetail.*;
import com.tian.util.CommonResult;

/**
 * @author tianwc 公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年10月31日 15:18
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 * <p>
 * 投资人详情
 */
public interface InvestorDetailService {

    /**
     * 分页查询
     *
     * @param investorDetailPageReqDto 查询条件
     * @return 投资人详情信息列表
     */
    CommonResult<InvestorDetailPageRespDto> page(InvestorDetailPageReqDto investorDetailPageReqDto);

    /**
     *  新增 投资人详情
     * @param investorDetailAddReqDto 投资人详情信息
     * @return 是否迟早成功
     */
    CommonResult<InvestorDetailAddRespDto> add(InvestorDetailAddReqDto investorDetailAddReqDto);
    /**
     *  修改 投资人详情
     * @param investorDetailUpdateReqDto 投资人详情信息
     * @return 是否迟早成功
     */
    CommonResult<InvestorDetailUpdateRespDto> update(InvestorDetailUpdateReqDto investorDetailUpdateReqDto);
}
