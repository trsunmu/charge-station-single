package com.tian.service;

import com.tian.dto.user.UserGoodsDto;
import com.tian.dto.user.UserGoodsReqDto;
import com.tian.dto.user.UserGoodsRespDto;
import com.tian.util.CommonResult;

/**
 * @author tianwc 公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * @date 2023年06月09日 20:21
 * 博客地址：<a href="http://woaijava.cc/">博客地址</a>
 */
public interface UserGoodsService {
    /**
     * 用户使用积分兑换商品
     *
     * @param userGoodsDto 领取商品
     * @return 领取成功
     */
    CommonResult<Boolean> exchange(UserGoodsDto userGoodsDto);

    /**
     * 领取列表查询
     *
     * @param userGoodsReqDto 查询条件
     * @return 用户已领取商品列表
     */
    CommonResult<UserGoodsRespDto> list(UserGoodsReqDto userGoodsReqDto);
}
