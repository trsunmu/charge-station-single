package com.tian.service;

import com.tian.dto.user.UserPointDto;
import com.tian.dto.user.UserPointUpdateDto;
import com.tian.dto.user.UserRankResDto;
import com.tian.util.CommonResult;

/**
 * @author tianwc 公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * @date 2023年06月05日 16:40
 * 博客地址：<a href="http://woaijava.cc/">博客地址</a>
 */
public interface UserPointService {

    /**
     * 积分新增
     *
     * @param points 积分参数
     * @return 新增是否成功
     */
    CommonResult<Boolean> add(Integer points);

    /**
     * 查询
     *
     * @param userId 用户唯一id
     * @return 用户积分
     */
    CommonResult<UserPointDto> queryByUserId(Long userId);

    /**
     * 积分修改---积分增加、扣减
     *
     * @param userPointUpdateDto 积分变化参数
     * @return 修改是否成功
     */
    CommonResult<Boolean> update(UserPointUpdateDto userPointUpdateDto);

    /**
     * 用户积分排行榜
     *
     * @param rankCount 需要多少排名
     * @return 返回积分排行榜
     */
    CommonResult<Boolean> userPointRank(Integer rankCount);

    /**
     * 个人积分男排行榜
     *
     * @param userId 用户id
     * @return 个人排名 -1 表示不再排行榜中
     */
    CommonResult<Long> getUserPointRank(Long userId);

    /**
     * 获取积分排行榜列表
     *
     * @param rankCount 多少人的排行榜
     * @return 排行榜
     */
    CommonResult<UserRankResDto> rankList(Integer rankCount);
}
