package com.tian.service;

import com.tian.dto.activity.ActivityDto;
import com.tian.dto.activity.ActivityListRespDto;
import com.tian.util.CommonResult;

/**
 * @author tianwc 公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * @date 2023年05月15日 15:26
 * 博客地址：<a href="http://woaijava.cc/">博客地址</a>
 */
public interface ActivityService {

    /**
     * 新增活动
     *
     * @param activityDto 活动信息
     * @return 新增是否成功
     */
    CommonResult<Boolean> add(ActivityDto activityDto);

    /**
     * 查询活动列表
     *
     * @param activityDto 查询参数
     * @return 活动列表
     */
    CommonResult<ActivityListRespDto> list(ActivityDto activityDto);
}
