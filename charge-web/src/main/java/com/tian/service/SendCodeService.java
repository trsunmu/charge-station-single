package com.tian.service;

import com.tian.dto.SendCodeReqDto;
import com.tian.util.CommonResult;

/**
 * @author tianwc 公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * @date 2023年05月11日 16:47
 * 博客地址：<a href="http://woaijava.cc/">博客地址</a>
 * <p>
 * 短信发送和验证
 */
public interface SendCodeService {

    /**
     * 发送手机验证码
     *
     * @param sendCodeReqDto 短信类型  手机号  缓存前缀 等
     * @return 发送成功
     */
    CommonResult<Boolean> sendCode(SendCodeReqDto sendCodeReqDto);

    /**
     * 验证 验证码
     * * @param phone 手机号、验证码
     * * @param code 验证码
     * * @param codeCachePre 缓存前缀
     *
     * @return 验证是否成功
     */
    CommonResult<Boolean> codeValid(String phone, String code, String codeCachePre);
}
