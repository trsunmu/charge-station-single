package com.tian.service;

import com.tian.dto.message.*;
import com.tian.util.CommonResult;

/**
 * {@code @description:} 我的站内信
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024/1/12 9:17
 * {@code @version:} 1.0
 */
public interface SystemUserMessageService {

    /**
     * 我的列表
     *
     * @param recordPageReqDto 请求参数
     * @return 站内信列表
     */
    CommonResult<SystemUserMessagePageRespDto> page(SystemUserMessageRecordPageReqDto recordPageReqDto);

    /**
     * 阅读站内信
     *
     * @param recordUpdateReqDto 请求参数
     * @return 是否阅读成功
     */
    CommonResult<SystemUserMessageUpdateRespDto> read(SystemUserMessageRecordUpdateReqDto recordUpdateReqDto);

    /**
     * 全部阅读
     */
    CommonResult<SystemUserMessageUpdateRespDto> readAll();
    CommonResult<SystemUserMessageUpdateRespDto> delete(SystemUserMessageRecordUpdateReqDto recordUpdateReqDto);

    /**
     * 通过主键id查询站内信内容
     *
     * @param id 主键id
     * @return 站内信内容
     */
    CommonResult<SystemUserMessageRespDto> queryById(Long id);
}
