package com.tian.service;

import com.tian.dto.price.*;
import com.tian.util.CommonResult;

/**
 * @author tianwc 公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年11月01日 16:13
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 * <p>
 * 电量价格变动历史记录表
 */
public interface ElectricityPriceHistoryService {
    CommonResult<ElectricityPriceHistoryAddRespDto> add(ElectricityPriceHistoryAddReqDto electricityPriceHistoryAddReqDto);

    CommonResult<ElectricityPriceHistoryUpdateRespDto> update(ElectricityPriceHistoryUpdateReqDto electricityPriceHistoryUpdateReqDto);

    CommonResult<ElectricityPriceHistoryRespDto> selectById(Integer id);

    CommonResult<ElectricityPriceHistoryPageRespDto> page(ElectricityPriceHistoryPageReqDto electricityPriceHistoryPageReqDto);
}
