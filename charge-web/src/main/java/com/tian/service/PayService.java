package com.tian.service;

import com.github.binarywang.wxpay.bean.order.WxPayMpOrderResult;
import com.tian.dto.WxPrePayReqDto;
import com.tian.util.CommonResult;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author tianwc 公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * @date 2023年05月12日 18:05
 * 博客地址：<a href="http://woaijava.cc/">博客地址</a>
 */
public interface PayService {
    CommonResult<WxPayMpOrderResult> prePay(WxPrePayReqDto wxPrePayReqDto, HttpServletRequest request);

    String payNotify(HttpServletRequest request, HttpServletResponse response);
}
