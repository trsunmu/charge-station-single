package com.tian.service;

import com.tian.dto.charger.*;
import com.tian.mqtt.message.ChargerStopSubscribeMessage;
import com.tian.util.CommonResult;

/**
 * @author tianwc 公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年11月01日 11:14
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 *
 * 充电记录
 */
public interface ChargerRecordService {
    /**
     * 开始充电
     * @param chargerRecordAddReqDto mqtt获取的信息
     */
    void doStartCharge(ChargerRecordAddReqDto chargerRecordAddReqDto);

    /**
     * 用户手动停止充电
     * @param chargerRecordUpdateReqDto 充电记录
     * @return 是否发错停止充电成功
     */
    CommonResult<ChargerRecordUpdateRespDto> readyStopCharge(ChargerRecordUpdateReqDto chargerRecordUpdateReqDto);

    /**
     * 真正的收到 mqtt 充电结束
     * @param chargerStopSubscribeMessage mqtt 回调信息
     */
    void doStopCharge(ChargerStopSubscribeMessage chargerStopSubscribeMessage);

    CommonResult<ChargerRecordRespDto> selectById(Integer id);

    CommonResult<ChargerRecordPageRespDto> page(ChargerRecordPageReqDto chargerRecordPageReqDto);
}
