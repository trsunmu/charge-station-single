package com.tian.service;

import com.tian.dto.order.*;
import com.tian.util.CommonResult;

/**
 * @author tianwc 公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年11月01日 19:20
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 */
public interface PayOrderService {
    CommonResult<PayOrderAddRespDto> add(PayOrderAddReqDto payOrderAddReqDto);

    CommonResult<PayOrderUpdateRespDto> update(PayOrderUpdateReqDto payOrderUpdateReqDto);

    CommonResult<PayOrderRespDto> selectById(Integer id);

    CommonResult<PayOrderPageRespDto> page(PayOrderPageReqDto payOrderPageReqDto);
}