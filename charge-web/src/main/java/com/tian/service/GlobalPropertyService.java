package com.tian.service;

import com.tian.entity.GlobalProperty;

import java.util.List;

/**
 * @author tianwc 公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * @date 2023年09月22日 22:55
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 */
public interface GlobalPropertyService {

    /**
     * 添加全局配置项
     *
     * @param globalProperty 新增配置项
     * @return 添加是否成功
     */
    boolean add(GlobalProperty globalProperty);

    /**
     * 修改全局配置项
     *
     * @param globalProperty 修改配置项
     * @return 修改是否成功
     */
    boolean update(GlobalProperty globalProperty);

    /**
     * 按照分页查询配置项
     *
     * @param globalProperty 查询条件
     * @param currPage       当前页
     * @param pageSize       每页记录数
     * @return 配置项列表
     */
    List<GlobalProperty> queryByPage(GlobalProperty globalProperty, Integer currPage, Integer pageSize);
}
