package com.tian.service;

import com.tian.dto.points.*;
import com.tian.util.CommonResult;

/**
 * @author tianwc 公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年11月01日 19:33
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 * <p>
 * 积分变更记录表
 */
public interface PointsModifyRecordService {
    CommonResult<PointsModifyRecordAddRespDto> add(PointsModifyRecordAddReqDto pointsModifyRecordAddReqDto);

    CommonResult<PointsModifyRecordUpdateRespDto> update(PointsModifyRecordUpdateReqDto pointsModifyRecordUpdateReqDto);

    CommonResult<PointsModifyRecordRespDto> selectById(Integer id);

    CommonResult<PointsModifyRecordPageRespDto> page(PointsModifyRecordPageReqDto pointsModifyRecordPageReqDto);
}
