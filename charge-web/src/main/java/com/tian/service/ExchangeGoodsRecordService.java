package com.tian.service;

import com.tian.dto.exchange.*;
import com.tian.util.CommonResult;

/**
 * @author tianwc 公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年11月01日 17:20
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 * <p>
 * 兑换商品记录表
 */
public interface ExchangeGoodsRecordService {
    CommonResult<ExchangeGoodsRecordAddRespDto> add(ExchangeGoodsRecordAddReqDto exchangeGoodsRecordAddReqDto);

    CommonResult<ExchangeGoodsRecordUpdateRespDto> update(ExchangeGoodsRecordUpdateReqDto exchangeGoodsRecordUpdateReqDto);

    CommonResult<ExchangeGoodsRecordRespDto> selectById(Integer id);

    CommonResult<ExchangeGoodsRecordPageRespDto> page(ExchangeGoodsRecordPageReqDto exchangeGoodsRecordPageReqDto);
}
