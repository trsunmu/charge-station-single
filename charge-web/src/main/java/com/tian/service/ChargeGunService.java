package com.tian.service;

import com.tian.dto.gun.*;
import com.tian.util.CommonResult;

/**
 * @author tianwc 公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年11月01日 09:59
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 * <p>
 * 充电枪
 */
public interface ChargeGunService {

    CommonResult<ChargeGunAddRespDto> add(ChargeGunAddReqDto chargeGunAddReqDto);

    CommonResult<ChargeGunUpdateRespDto> update(ChargeGunUpdateReqDto chargeGunUpdateReqDto);

    CommonResult<ChargeGunRespDto> selectById(Integer id);

    CommonResult<ChargeGunPageRespDto> page(ChargeGunPageReqDto chargeGunPageReqDto);
}
