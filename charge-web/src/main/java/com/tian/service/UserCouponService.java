package com.tian.service;

import com.tian.dto.coupon.UserActivityCouponPageReqDto;
import com.tian.dto.user.UserCouponAddDto;
import com.tian.dto.user.UserCouponRecordListRespDto;
import com.tian.dto.user.UserCouponRecordRespDto;
import com.tian.util.CommonResult;

/**
 * @author tianwc 公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * @date 2023年05月15日 15:08
 * 博客地址：<a href="http://woaijava.cc/">博客地址</a>
 */
public interface UserCouponService {

    /**
     * 领取优惠券
     *
     * @param userCouponAddDto 活动 优惠券 用户信息
     * @return 领取是否成功
     */
    CommonResult<Boolean> receive(UserCouponAddDto userCouponAddDto);

    /**
     * 查询个人已领取优惠券列表
     * @param userActivityCouponPageReqDto 查询条件
     * @return 个人已领取优惠券列表
     */
    CommonResult<UserCouponRecordListRespDto> list(UserActivityCouponPageReqDto userActivityCouponPageReqDto);
}
