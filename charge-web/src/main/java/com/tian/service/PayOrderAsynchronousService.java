package com.tian.service;

import com.tian.entity.PayOrder;

import java.util.List;

/**
 * @author tianwc 公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年11月20日 09:59
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 */
public interface PayOrderAsynchronousService {
    /**
     * 异步处理
     *
     * @param payOrder 支付订单信息
     */
    void processe(PayOrder payOrder);

    /**
     * 统计
     *
     * @param status 订单状态
     * @return 数量
     */
    int countByStatus(Integer status);

    /**
     * 分页查询
     *
     * @param status   状态
     * @param start    起始
     * @param pageSize 页大小
     * @return 分页列表
     */
    List<PayOrder> selectByStatus(Integer status, int start, int pageSize);
}
