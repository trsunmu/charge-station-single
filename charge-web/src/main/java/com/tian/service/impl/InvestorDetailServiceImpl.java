package com.tian.service.impl;

import com.tian.dto.investDetail.*;
import com.tian.entity.InvestorDetail;
import com.tian.entity.User;
import com.tian.mapper.InvestorDetailMapper;
import com.tian.service.InvestorDetailService;
import com.tian.util.CommonResult;
import com.tian.util.UserCacheUtil;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年10月31日 15:27
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 */
@Service
public class InvestorDetailServiceImpl implements InvestorDetailService {

    @Resource
    private InvestorDetailMapper investorDetailMapper;

    @Override
    public CommonResult<InvestorDetailPageRespDto> page(InvestorDetailPageReqDto investorDetailPageReqDto) {
        int count = investorDetailMapper.count(investorDetailPageReqDto);
        if (count == 0) {
            return CommonResult.success();
        }
        List<InvestorDetail> investorDetailList = investorDetailMapper.page(investorDetailPageReqDto);
        InvestorDetailPageRespDto investorDetailPageRespDto = InvestorDetailConvert.convert4Page(investorDetailList);
        return CommonResult.success(investorDetailPageRespDto);
    }

    @Override
    public CommonResult<InvestorDetailAddRespDto> add(InvestorDetailAddReqDto investorDetailAddReqDto) {
        InvestorDetail investorDetail = InvestorDetailConvert.convert4Add(investorDetailAddReqDto);
        User user = UserCacheUtil.getUser();
        investorDetail.setUserId(user.getId());
        investorDetail.setStatus(0);
        investorDetailMapper.insert(investorDetail);
        return CommonResult.success();
    }

    @Override
    public CommonResult<InvestorDetailUpdateRespDto> update(InvestorDetailUpdateReqDto investorDetailUpdateReqDto) {
        return null;
    }
}
