package com.tian.service.impl;

import com.tian.dto.gun.*;
import com.tian.entity.ChargeGun;
import com.tian.mapper.ChargeGunMapper;
import com.tian.service.ChargeGunService;
import com.tian.util.CommonResult;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

import java.util.List;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年11月01日 10:05
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 */

@Service
public class ChargeGunServiceImpl implements ChargeGunService {

    @Resource
    private ChargeGunMapper chargeGunMapper;

    @Override
    public CommonResult<ChargeGunAddRespDto> add(ChargeGunAddReqDto chargeGunAddReqDto) {
        ChargeGun chargeGun = ChargeGunConvert.convert4Add(chargeGunAddReqDto);
        chargeGun.setStatus(0);
        chargeGunMapper.insert(chargeGun);
        return CommonResult.success();
    }

    @Override
    public CommonResult<ChargeGunUpdateRespDto> update(ChargeGunUpdateReqDto chargeGunUpdateReqDto) {
        ChargeGun chargeGun = ChargeGunConvert.convert4Update(chargeGunUpdateReqDto);
        chargeGunMapper.updateByPrimaryKey(chargeGun);
        return CommonResult.success();
    }

    @Override
    public CommonResult<ChargeGunRespDto> selectById(Integer id) {
        ChargeGun chargeGun = chargeGunMapper.selectByPrimaryKey(id);
        ChargeGunRespDto chargeGunRespDto = ChargeGunConvert.convert4SelectById(chargeGun);
        return CommonResult.success(chargeGunRespDto);
    }

    @Override
    public CommonResult<ChargeGunPageRespDto> page(ChargeGunPageReqDto chargeGunPageReqDto) {
        int count = chargeGunMapper.count(chargeGunPageReqDto);
        if (count == 0) {
            return CommonResult.success();
        }
        List<ChargeGun> chargeGunList = chargeGunMapper.page(chargeGunPageReqDto);
        ChargeGunPageRespDto chargeGunPageRespDto = ChargeGunConvert.convert4Page(chargeGunList);
        return CommonResult.success(chargeGunPageRespDto);
    }
}
