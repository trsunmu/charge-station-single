package com.tian.service.impl;

import com.tian.dto.activityGoods.*;
import com.tian.entity.ActivityGoods;
import com.tian.mapper.ActivityGoodsMapper;
import com.tian.service.ActivityGoodsService;
import com.tian.util.CommonResult;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年10月31日 15:42
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 */
@Service
public class ActivityGoodsServiceImpl implements ActivityGoodsService {
    @Resource
    private ActivityGoodsMapper activityGoodsMapper;


    @Override
    public CommonResult<ActivityGoodsAddRespDto> add(ActivityGoodsAddReqDto activityGoodsAddReqDto) {
        ActivityGoods activityGoods = ActivityGoodsConvert.convert4Add(activityGoodsAddReqDto);
        activityGoodsMapper.insert(activityGoods);
        return CommonResult.success();
    }

    @Override
    public CommonResult<ActivityGoodsUpdateRespDto> update(ActivityGoodsUpdateReqDto activityGoodsUpdateReqDto) {
        ActivityGoods activityGoods = ActivityGoodsConvert.convert4Update(activityGoodsUpdateReqDto);
        activityGoodsMapper.update(activityGoods);
        return CommonResult.success();
    }

    @Override
    public CommonResult<ActivityGoodsRespDto> selectById(Integer id) {
        ActivityGoods activityGoods = activityGoodsMapper.selectById(id);
        ActivityGoodsRespDto activityGoodsRespDto = ActivityGoodsConvert.convert4SelectById(activityGoods);
        return CommonResult.success(activityGoodsRespDto);
    }

    @Override
    public CommonResult<ActivityGoodsPageRespDto> page(ActivityGoodsPageReqDto activityGoodsPageReqDto) {
        int count = activityGoodsMapper.count(activityGoodsPageReqDto);
        if (count == 0) {
            return CommonResult.success();
        }
        List<ActivityGoods> activityGoodsList = activityGoodsMapper.page(activityGoodsPageReqDto);
        ActivityGoodsPageRespDto activityGoodsPageRespDto = ActivityGoodsConvert.convert4Response(activityGoodsList);
        return CommonResult.success(activityGoodsPageRespDto);
    }
}