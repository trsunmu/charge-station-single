package com.tian.service.impl;

import com.tian.dto.coupon.*;
import com.tian.entity.UserActivityCoupon;
import com.tian.mapper.UserActivityCouponMapper;
import com.tian.service.UserActivityCouponService;
import com.tian.util.CommonResult;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年11月01日 21:15
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 */
@Service
public class UserActivityCouponServiceImpl implements UserActivityCouponService {
    @Resource
    private UserActivityCouponMapper userActivityCouponMapper;

    @Override
    public CommonResult<UserActivityCouponAddRespDto> add(UserActivityCouponAddReqDto userActivityCouponAddReqDto) {
        UserActivityCoupon userActivityCoupon = UserActivityCouponConvert.convert4Add(userActivityCouponAddReqDto);
        userActivityCouponMapper.insert(userActivityCoupon);
        return CommonResult.success();
    }

    @Override
    public CommonResult<UserActivityCouponUpdateRespDto> update(UserActivityCouponUpdateReqDto userActivityCouponUpdateReqDto) {
        UserActivityCoupon userActivityCoupon = UserActivityCouponConvert.convert4Update(userActivityCouponUpdateReqDto);
        userActivityCouponMapper.updateByPrimaryKey(userActivityCoupon);
        return CommonResult.success();
    }

    @Override
    public CommonResult<UserActivityCouponRespDto> selectById(Integer id) {
        UserActivityCoupon userActivityCoupon = userActivityCouponMapper.selectByPrimaryKey(id);
        if (userActivityCoupon == null) {
            return CommonResult.success();
        }
        UserActivityCouponRespDto userActivityCouponRespDto = UserActivityCouponConvert.convert4SelectById(userActivityCoupon);
        return CommonResult.success(userActivityCouponRespDto);
    }

    @Override
    public CommonResult<UserActivityCouponPageRespDto> page(UserActivityCouponPageReqDto userActivityCouponPageReqDto) {
        long count = userActivityCouponMapper.count(userActivityCouponPageReqDto);
        if (count == 0) {
            return CommonResult.success();
        }
        List<UserActivityCoupon> userActivityCoupons = userActivityCouponMapper.page(userActivityCouponPageReqDto);
        UserActivityCouponPageRespDto userActivityCouponPageRespDto = UserActivityCouponConvert.convert4Page(userActivityCoupons);
        return CommonResult.success(userActivityCouponPageRespDto);
    }
}
