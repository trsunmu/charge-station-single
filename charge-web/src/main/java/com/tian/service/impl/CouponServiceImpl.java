package com.tian.service.impl;

import com.tian.dto.coupon.CouponConvertUtil;
import com.tian.dto.coupon.CouponAddReqDto;
import com.tian.dto.coupon.CouponInfoReqDto;
import com.tian.dto.coupon.CouponListRespDto;
import com.tian.entity.Coupon;
import com.tian.enums.ResultCode;
import com.tian.mapper.CouponMapper;
import com.tian.service.CouponService;
import com.tian.util.CommonResult;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * @date 2023年05月15日 14:35
 * 博客地址：<a href="http://woaijava.cc/">博客地址</a>
 * <p>
 * 优惠券
 */
@Service
public class CouponServiceImpl implements CouponService {

    @Resource
    private CouponMapper couponMapper;


    @Override
    public CommonResult<CouponListRespDto> list(CouponInfoReqDto request) {
        int count = couponMapper.count(request);
        if (count == 0) {
            return CommonResult.success(new CouponListRespDto());
        }
        List<Coupon> couponList = couponMapper.page(request);
        List<CouponInfoReqDto> couponInfoReqDtoList=new ArrayList<>();
        for (Coupon coupon : couponList) {
            CouponInfoReqDto couponInfoReqDto=new CouponInfoReqDto();
            couponInfoReqDto.setId(coupon.getId());
            couponInfoReqDtoList.add(couponInfoReqDto);
        }
        CouponListRespDto couponListRespDto=new CouponListRespDto();

        couponListRespDto.setList(couponInfoReqDtoList);

        couponListRespDto.setPage(request.getPage());
        couponListRespDto.setPageSize(request.getPageSize());

        return CommonResult.success(couponListRespDto);
    }
}
