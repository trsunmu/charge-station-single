package com.tian.service.impl;

import com.tian.dto.station.*;
import com.tian.entity.Station;
import com.tian.mapper.StationMapper;
import com.tian.service.StationService;
import com.tian.util.CommonResult;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年11月07日 15:43
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 */
@Service
public class StationServiceImpl implements StationService {
    @Resource
    private StationMapper stationMapper;

    @Override
    public CommonResult<StationRespDto> getStationById(Integer id) {
        Station station = stationMapper.selectByPrimaryKey(id);
        StationRespDto stationRespDto = StationConvert.convert4SelectById(station);
        return CommonResult.success(stationRespDto);
    }

    @Override
    public CommonResult<StationPageRespDto> page(StationPageReqDto stationPageReqDto) {
        int count = stationMapper.count(stationPageReqDto);
        if (count == 0) {
            return CommonResult.success();
        }
        List<Station> stationList = stationMapper.page(stationPageReqDto);
        StationPageRespDto stationPageRespDto = StationConvert.convert4Page(stationList);
        return CommonResult.success(stationPageRespDto);
    }
}
