package com.tian.service.impl;

import com.alibaba.fastjson.JSON;
import com.tian.config.RedisConfig;
import com.tian.dto.GoodsInfoDto;
import com.tian.entity.GoodsInfo;
import com.tian.enums.InsertUpdateDateBaseFlagEnum;
import com.tian.enums.ResultCode;
import com.tian.mapper.GoodsInfoMapper;
import com.tian.service.GoodsInfoService;
import com.tian.util.CommonResult;
import com.tian.util.RedisConstantPre;
import com.tian.util.StringUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * @date 2023年06月09日 19:45
 * 博客地址：<a href="http://woaijava.cc/">博客地址</a>
 * <p>
 * 商品信息表
 * <p>
 * 缓存数据一致性采用 延迟双删
 */
@Slf4j
@Service
public class GoodsInfoServiceImpl implements GoodsInfoService {
    @Resource
    private GoodsInfoMapper goodsInfoMapper;
    @Resource
    private RedisConfig redisConfig;

    @Override
    public CommonResult<Boolean> update(GoodsInfoDto goodsInfoDto) {
        redisConfig.delete(RedisConstantPre.GOODS_INFO + goodsInfoDto.getId());
        GoodsInfo goodsInfo = goodsInfoMapper.selectByPrimaryKey(goodsInfoDto.getId());
        goodsInfo.setName(goodsInfoDto.getName());
        goodsInfo.setPoint(goodsInfoDto.getPoint());
        goodsInfo.setStock(goodsInfoDto.getStock());
        goodsInfo.setImagUrl(goodsInfoDto.getImagUrl());
        goodsInfo.setActivityId(goodsInfoDto.getActivityId());
        goodsInfo.setImagUrl(goodsInfoDto.getImagUrl());
        int flag = goodsInfoMapper.updateByPrimaryKey(goodsInfo);
        if (flag == InsertUpdateDateBaseFlagEnum.SUCCESS.getFlag()) {
            try {
                //延迟双删
                Thread.sleep(2000L);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            redisConfig.delete(RedisConstantPre.GOODS_INFO + goodsInfoDto.getId());
            return CommonResult.success(Boolean.TRUE);
        }
        return CommonResult.failed();
    }

    @Override
    public CommonResult<GoodsInfoDto> getById(Integer id) {
        String key = RedisConstantPre.GOODS_INFO + id;
        String cache = redisConfig.get(key);
        if (StringUtil.isBlank(cache)) {
            GoodsInfo goodsInfo = goodsInfoMapper.selectByPrimaryKey(id);
            if (goodsInfo == null) {
                redisConfig.set(key, JSON.toJSONString(new GoodsInfo()));
                return CommonResult.failed(ResultCode.PARAMETER_ERROR);
            }
            redisConfig.set(key, JSON.toJSONString(goodsInfo));
            GoodsInfoDto goodsInfoDto = new GoodsInfoDto();
            //todo  goodsInfo---》 goodsInfoDto
            return CommonResult.success(goodsInfoDto);
        }
        GoodsInfo goodsInfo = JSON.parseObject(cache, GoodsInfo.class);
        if (goodsInfo == null || goodsInfo.getId() == null) {
            return CommonResult.failed(ResultCode.PARAMETER_ERROR);
        }
        GoodsInfoDto goodsInfoDto = new GoodsInfoDto();
        //todo  goodsInfo---》 goodsInfoDto
        return CommonResult.success(goodsInfoDto);
    }
}