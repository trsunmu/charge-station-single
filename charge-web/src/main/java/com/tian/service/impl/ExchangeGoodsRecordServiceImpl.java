package com.tian.service.impl;

import com.tian.dto.exchange.*;
import com.tian.entity.ExchangeGoodsRecord;
import com.tian.mapper.ExchangeGoodsRecordMapper;
import com.tian.service.ExchangeGoodsRecordService;
import com.tian.util.CommonResult;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年11月01日 17:24
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 */
@Service
public class ExchangeGoodsRecordServiceImpl implements ExchangeGoodsRecordService {
    @Resource
    private ExchangeGoodsRecordMapper exchangeGoodsRecordMapper;

    @Override
    public CommonResult<ExchangeGoodsRecordAddRespDto> add(ExchangeGoodsRecordAddReqDto exchangeGoodsRecordAddReqDto) {
        ExchangeGoodsRecord exchangeGoodsRecord = ExchangeGoodsRecordConvert.convert4Add(exchangeGoodsRecordAddReqDto);
        exchangeGoodsRecordMapper.insert(exchangeGoodsRecord);
        return CommonResult.success();
    }

    @Override
    public CommonResult<ExchangeGoodsRecordUpdateRespDto> update(ExchangeGoodsRecordUpdateReqDto exchangeGoodsRecordUpdateReqDto) {
        ExchangeGoodsRecord exchangeGoodsRecord = ExchangeGoodsRecordConvert.convert4Update(exchangeGoodsRecordUpdateReqDto);
        exchangeGoodsRecordMapper.updateByPrimaryKey(exchangeGoodsRecord);
        return CommonResult.success();
    }

    @Override
    public CommonResult<ExchangeGoodsRecordRespDto> selectById(Integer id) {
        ExchangeGoodsRecord exchangeGoodsRecord = exchangeGoodsRecordMapper.selectByPrimaryKey(id);
        if (exchangeGoodsRecord == null) {
            return CommonResult.success();
        }
        ExchangeGoodsRecordRespDto exchangeGoodsRecordRespDto = ExchangeGoodsRecordConvert.convert4SelectById(exchangeGoodsRecord);
        return CommonResult.success(exchangeGoodsRecordRespDto);
    }

    @Override
    public CommonResult<ExchangeGoodsRecordPageRespDto> page(ExchangeGoodsRecordPageReqDto exchangeGoodsRecordPageReqDto) {
        int count = exchangeGoodsRecordMapper.count(exchangeGoodsRecordPageReqDto);
        if (count == 0) {
            return CommonResult.success();
        }
        List<ExchangeGoodsRecord> exchangeGoodsRecordList = exchangeGoodsRecordMapper.page(exchangeGoodsRecordPageReqDto);
        ExchangeGoodsRecordPageRespDto exchangeGoodsRecordPageRespDto = ExchangeGoodsRecordConvert.convert4Page(exchangeGoodsRecordList);
        return CommonResult.success(exchangeGoodsRecordPageRespDto);
    }
}
