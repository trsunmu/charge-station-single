package com.tian.service.impl;

import com.tian.entity.ExceptionalRetroaction;
import com.tian.mapper.ExceptionalRetroactionMapper;
import com.tian.service.ExceptionalRetractionService;
import com.tian.util.CommonResult;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * @date 2023年09月24日 11:55
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 * <p>
 * 异常反馈
 */
@Service
public class ExceptionalRetractionImpl implements ExceptionalRetractionService {

    @Resource
    private ExceptionalRetroactionMapper exceptionalRetroactionMapper;

    @Override
    public CommonResult add(ExceptionalRetroaction exceptionalRetraction) {
        exceptionalRetroactionMapper.insert(exceptionalRetraction);
        System.out.println("按照当前人查询");
        System.out.println("按照部门名称查询");
        return null;
    }
}
