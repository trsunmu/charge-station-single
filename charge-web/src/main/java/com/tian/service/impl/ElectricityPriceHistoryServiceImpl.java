package com.tian.service.impl;

import com.tian.dto.price.*;
import com.tian.entity.ElectricityPriceHistory;
import com.tian.mapper.ElectricityPriceHistoryMapper;
import com.tian.service.ElectricityPriceHistoryService;
import com.tian.util.CommonResult;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年11月01日 16:44
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 *
 */
@Service
public class ElectricityPriceHistoryServiceImpl implements ElectricityPriceHistoryService {

    @Resource
    private ElectricityPriceHistoryMapper electricityPriceHistoryMapper;

    @Override
    public CommonResult<ElectricityPriceHistoryAddRespDto> add(ElectricityPriceHistoryAddReqDto electricityPriceHistoryAddReqDto) {
        ElectricityPriceHistory electricityPriceHistory = ElectricityPriceHistoryConvert.convert4Add(electricityPriceHistoryAddReqDto);
        electricityPriceHistoryMapper.insert(electricityPriceHistory);
        return CommonResult.success();
    }

    @Override
    public CommonResult<ElectricityPriceHistoryUpdateRespDto> update(ElectricityPriceHistoryUpdateReqDto electricityPriceHistoryUpdateReqDto) {
        ElectricityPriceHistory electricityPriceHistory = ElectricityPriceHistoryConvert.convert4Update(electricityPriceHistoryUpdateReqDto);
        electricityPriceHistoryMapper.insert(electricityPriceHistory);
        return CommonResult.success();
    }

    @Override
    public CommonResult<ElectricityPriceHistoryRespDto> selectById(Integer id) {
        ElectricityPriceHistory electricityPrice = electricityPriceHistoryMapper.selectByPrimaryKey(id);
        if (electricityPrice == null) {
            return CommonResult.success();
        }
        ElectricityPriceHistoryRespDto electricityPriceRespDto = ElectricityPriceHistoryConvert.convert4SelectById(electricityPrice);
        return CommonResult.success(electricityPriceRespDto);
    }

    @Override
    public CommonResult<ElectricityPriceHistoryPageRespDto> page(ElectricityPriceHistoryPageReqDto electricityPriceHistoryPageReqDto) {
        int count = electricityPriceHistoryMapper.count(electricityPriceHistoryPageReqDto);
        if (count == 0) {
            return CommonResult.success();
        }
        List<ElectricityPriceHistory> electricityPriceList = electricityPriceHistoryMapper.page(electricityPriceHistoryPageReqDto);
        ElectricityPriceHistoryPageRespDto electricityPricePageRespDto = ElectricityPriceHistoryConvert.convert4Page(electricityPriceList);
        return CommonResult.success(electricityPricePageRespDto);
    }
}
