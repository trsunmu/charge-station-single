package com.tian.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.tian.dto.message.*;
import com.tian.entity.SystemUserMessage;
import com.tian.entity.User;
import com.tian.enums.ResultCode;
import com.tian.enums.SystemUserMessageStatusEnum;
import com.tian.exception.BusinessException;
import com.tian.mapper.SystemUserMessageMapper;
import com.tian.service.SystemUserMessageService;
import com.tian.util.CommonResult;
import com.tian.util.UserCacheUtil;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Objects;

/**
 * {@code @description:} 我的站内信
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024/1/12 9:17
 * {@code @version:} 1.0
 */
@Service
public class SystemUserMessageServiceImpl implements SystemUserMessageService {
    @Resource
    private SystemUserMessageMapper systemUserMessageMapper;

    @Override
    public CommonResult<SystemUserMessagePageRespDto> page(SystemUserMessageRecordPageReqDto recordPageReqDto) {
        User user = UserCacheUtil.getUser();
        recordPageReqDto.setReceiverId(user.getId());


        SystemUserMessagePageRespDto systemUserMessagePageRespDto = new SystemUserMessagePageRespDto();


        Page page = PageHelper.startPage(recordPageReqDto.getPage(), recordPageReqDto.getPageSize());

        List<SystemUserMessage> systemUserMessageList = systemUserMessageMapper.page(recordPageReqDto);
        systemUserMessagePageRespDto.setTotal(page.getTotal());
        List<SystemUserMessageRespDto> systemUserMessageRespDtoList = SystemUserMessageRecordConvert.convert4Page(systemUserMessageList);
        systemUserMessagePageRespDto.setDataList(systemUserMessageRespDtoList);

        return CommonResult.success(systemUserMessagePageRespDto);
    }

    @Override
    public CommonResult<SystemUserMessageUpdateRespDto> read(SystemUserMessageRecordUpdateReqDto recordUpdateReqDto) {
        SystemUserMessage systemUserMessage = systemUserMessageMapper.selectByPrimaryKey(recordUpdateReqDto.getId());
        User user = UserCacheUtil.getUser();
        if (!Objects.equals(systemUserMessage.getReceiverId(), user.getId())) {
            throw new BusinessException(ResultCode.FORBIDDEN);
        }
        systemUserMessage.setStatus(SystemUserMessageStatusEnum.READ.getStatus());
        systemUserMessageMapper.updateStatusById(systemUserMessage);
        return CommonResult.success();
    }

    @Override
    public CommonResult<SystemUserMessageUpdateRespDto> readAll() {
        User user = UserCacheUtil.getUser();
        systemUserMessageMapper.readAll(user.getId());
        return CommonResult.success();
    }

    @Override
    public CommonResult<SystemUserMessageUpdateRespDto> delete(SystemUserMessageRecordUpdateReqDto recordUpdateReqDto) {
        SystemUserMessage systemUserMessage = systemUserMessageMapper.selectByPrimaryKey(recordUpdateReqDto.getId());
        User user = UserCacheUtil.getUser();
        if (!Objects.equals(systemUserMessage.getReceiverId(), user.getId())) {
            throw new BusinessException(ResultCode.FORBIDDEN);
        }
        systemUserMessage.setStatus(SystemUserMessageStatusEnum.DELETE.getStatus());
        systemUserMessageMapper.updateStatusById(systemUserMessage);
        return CommonResult.success();
    }

    @Override
    public CommonResult<SystemUserMessageRespDto> queryById(Long id) {
        User user = UserCacheUtil.getUser();
        SystemUserMessage systemUserMessage = systemUserMessageMapper.selectByPrimaryKey(id);
        if (!Objects.equals(systemUserMessage.getReceiverId(), user.getId())) {
            throw new BusinessException(ResultCode.FORBIDDEN);
        }
        SystemUserMessageRespDto systemUserMessageRespDto = SystemUserMessageRecordConvert.convert4SelectById(systemUserMessage);
        return CommonResult.success(systemUserMessageRespDto);
    }
}
