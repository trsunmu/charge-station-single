package com.tian.service.impl;

import com.tian.dto.price.ElectricityPriceConvert;
import com.tian.dto.price.ElectricityPricePageReqDto;
import com.tian.dto.price.ElectricityPricePageRespDto;
import com.tian.dto.price.ElectricityPriceRespDto;
import com.tian.entity.ElectricityPrice;
import com.tian.mapper.ElectricityPriceMapper;
import com.tian.service.ElectricityPriceService;
import com.tian.util.CommonResult;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年11月01日 12:05
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 * <p>
 * 电量价格
 */
@Service
public class ElectricityPriceServiceImpl implements ElectricityPriceService {

    @Resource
    private ElectricityPriceMapper electricityPriceMapper;

    @Override
    public CommonResult<ElectricityPriceRespDto> selectById(Integer id) {
        ElectricityPrice electricityPrice = electricityPriceMapper.selectByPrimaryKey(id);
        if (electricityPrice == null) {
            return CommonResult.success();
        }
        ElectricityPriceRespDto electricityPriceRespDto = ElectricityPriceConvert.convert4SelectById(electricityPrice);
        return CommonResult.success(electricityPriceRespDto);
    }

    @Override
    public CommonResult<ElectricityPricePageRespDto> page(ElectricityPricePageReqDto electricityPricePageReqDto) {
        int count = electricityPriceMapper.count(electricityPricePageReqDto);
        if (count == 0) {
            return CommonResult.success();
        }
        List<ElectricityPrice> electricityPriceList = electricityPriceMapper.page(electricityPricePageReqDto);
        ElectricityPricePageRespDto electricityPricePageRespDto = ElectricityPriceConvert.convert4Page(electricityPriceList);
        return CommonResult.success(electricityPricePageRespDto);
    }
}
