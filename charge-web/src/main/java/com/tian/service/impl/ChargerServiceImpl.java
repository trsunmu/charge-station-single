package com.tian.service.impl;

import com.tian.dto.charger.*;
import com.tian.entity.Charger;
import com.tian.mapper.ChargerMapper;
import com.tian.service.ChargerService;
import com.tian.util.CommonResult;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年11月01日 10:42
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 */
@Service
public class ChargerServiceImpl implements ChargerService {

    @Resource
    private ChargerMapper chargerMapper;

    @Override
    public CommonResult<ChargerAddRespDto> add(ChargerAddReqDto chargerAddReqDto) {
        Charger charger = ChargerConvert.convert4Add(chargerAddReqDto);
        chargerMapper.insert(charger);
        return CommonResult.success();
    }

    @Override
    public CommonResult<ChargerUpdateRespDto> update(ChargerUpdateReqDto chargerUpdateReqDto) {
        Charger charger = ChargerConvert.convert4Update(chargerUpdateReqDto);
        chargerMapper.updateByPrimaryKey(charger);
        return CommonResult.success();
    }

    @Override
    public CommonResult<ChargerRespDto> selectById(Integer id) {
        Charger charger = chargerMapper.selectByPrimaryKey(id);
        ChargerRespDto chargerRespDto = ChargerConvert.convert4SelectById(charger);
        return CommonResult.success(chargerRespDto);
    }

    @Override
    public CommonResult<ChargerPageRespDto> page(ChargerPageReqDto chargerPageReqDto) {
        int count = chargerMapper.count(chargerPageReqDto);
        if (count == 0) {
            return CommonResult.success();
        }
        List<Charger> chargerList = chargerMapper.page(chargerPageReqDto);
        ChargerPageRespDto chargerPageRespDto = ChargerConvert.convert4Page(chargerList);
        return CommonResult.success(chargerPageRespDto);
    }
}
