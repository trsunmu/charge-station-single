package com.tian.service.impl;

import com.tian.dto.order.*;
import com.tian.entity.PayOrder;
import com.tian.mapper.PayOrderMapper;
import com.tian.service.PayOrderService;
import com.tian.util.CommonResult;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年11月01日 19:22
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 * <p>
 * 支付订单
 */
@Service
public class PayOrderServiceImpl implements PayOrderService {
    @Resource
    private PayOrderMapper payOrderMapper;

    @Override
    public CommonResult<PayOrderAddRespDto> add(PayOrderAddReqDto payOrderAddReqDto) {
        PayOrder payOrder = PayOrderConvert.convert4Add(payOrderAddReqDto);
        payOrderMapper.insert(payOrder);
        return CommonResult.success();
    }

    @Override
    public CommonResult<PayOrderUpdateRespDto> update(PayOrderUpdateReqDto payOrderUpdateReqDto) {
        PayOrder payOrder = PayOrderConvert.convert4Update(payOrderUpdateReqDto);
        payOrderMapper.updateByPrimaryKey(payOrder);
        return CommonResult.success();
    }

    @Override
    public CommonResult<PayOrderRespDto> selectById(Integer id) {
        PayOrder payOrder = payOrderMapper.selectByPrimaryKey(id);
        if (payOrder == null) {
            return CommonResult.success();
        }
        PayOrderRespDto payOrderRespDto = PayOrderConvert.convert4SelectById(payOrder);
        return CommonResult.success(payOrderRespDto);
    }

    @Override
    public CommonResult<PayOrderPageRespDto> page(PayOrderPageReqDto payOrderPageReqDto) {
        int count = payOrderMapper.count(payOrderPageReqDto);
        if (count == 0) {
            return CommonResult.success();
        }
        List<PayOrder> payOrderList = payOrderMapper.page(payOrderPageReqDto);
        PayOrderPageRespDto payOrderPageRespDto = PayOrderConvert.convert4Page(payOrderList);
        return CommonResult.success(payOrderPageRespDto);
    }
}
