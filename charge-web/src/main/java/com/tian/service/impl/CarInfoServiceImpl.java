package com.tian.service.impl;

import com.tian.dto.car.*;
import com.tian.entity.CarInfo;
import com.tian.entity.User;
import com.tian.enums.CarIdentifyStatus;
import com.tian.mapper.CarInfoMapper;
import com.tian.service.CarInfoService;
import com.tian.util.CommonResult;
import com.tian.util.StringUtil;
import com.tian.util.UserCacheUtil;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import javax.annotation.Resource;
import java.util.Date;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年10月31日 19:22
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 *
 * 用户汽车认证
 */
@Service
public class CarInfoServiceImpl implements CarInfoService {
    @Resource
    private CarInfoMapper carInfoMapper;

    @Override
    public CommonResult<CarInfoAddRespDto> add(@RequestBody CarInfoAddReqDto carInfoAddReqDto) {
        User user = UserCacheUtil.getUser();
        CarInfo carInfo = CarInfoConvert.convert4Add(carInfoAddReqDto);
        carInfo.setUserId(user.getId());
        carInfo.setCreateTime(new Date());
        int ststus = CarIdentifyStatus.CHECKING.getStatus();
        if(StringUtil.isEmpty(carInfoAddReqDto.getDrivingLicenseUrl())){
            ststus=CarIdentifyStatus.INIT.getStatus();
        }
        carInfo.setCarIdentifyStatus(ststus);
        carInfoMapper.insertCarInfo(carInfo);
        return CommonResult.success();
    }

    @Override
    public CommonResult<CarInfoUpdateRespDto> updateCarInfo(CarInfoUpdateReqDto carInfoUpdateReqDto) {
        CarInfo carInfo = CarInfoConvert.convert4Update(carInfoUpdateReqDto);
        carInfoMapper.updateCarInfo(carInfo);
        return CommonResult.success();
    }

    @Override
    public CommonResult<CarInfoRespDto> getCarInfoById(Integer id) {
        CarInfo carInfo = carInfoMapper.getCarInfoById(id);
        CarInfoRespDto carInfoRespDto = CarInfoConvert.convert4SelectById(carInfo);
        return CommonResult.success(carInfoRespDto);
    }
}
