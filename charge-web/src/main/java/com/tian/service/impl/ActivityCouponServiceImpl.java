package com.tian.service.impl;

import com.tian.dto.coupon.ActivityCouponConvertUtil;
import com.tian.dto.coupon.ActivityCouponDto;
import com.tian.dto.coupon.ActivityCouponListRespDto;
import com.tian.dto.coupon.ActivityCouponReqDto;
import com.tian.dto.user.UserCouponAddDto;
import com.tian.entity.Activity;
import com.tian.entity.ActivityCoupon;
import com.tian.entity.Coupon;
import com.tian.enums.ActivityStatusEnum;
import com.tian.enums.ResultCode;
import com.tian.mapper.ActivityCouponMapper;
import com.tian.mapper.ActivityMapper;
import com.tian.mapper.CouponMapper;
import com.tian.service.ActivityCouponService;
import com.tian.util.CommonResult;
import com.tian.util.DateUtil;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * @date 2023年05月15日 15:36
 * 博客地址：<a href="http://woaijava.cc/">博客地址</a>
 */
@Service
public class ActivityCouponServiceImpl implements ActivityCouponService {

    @Resource
    private ActivityCouponMapper activityCouponMapper;
    @Resource
    private ActivityMapper activityMapper;
    @Resource
    private CouponMapper couponMapper;

    @Override
    public CommonResult<Boolean> add(ActivityCouponDto activityCouponDto) {
        ActivityCoupon activityCoupon = new ActivityCoupon();
        int flag = activityCouponMapper.insert(activityCoupon);
        if (flag == 1) {
            return CommonResult.success(Boolean.TRUE);
        }
        return CommonResult.failed();
    }

    @Override
    public CommonResult<ActivityCouponListRespDto> list(ActivityCouponReqDto activityCouponReqDto) {
        int count = activityCouponMapper.count(activityCouponReqDto);
        if (count == 0) {
            return CommonResult.success(new ActivityCouponListRespDto(activityCouponReqDto.getStart(), activityCouponReqDto.getPageSize(), 0));
        }
        List<ActivityCoupon> activityCouponList = activityCouponMapper.list(activityCouponReqDto);

        ActivityCouponListRespDto activityCouponListRespDto = new ActivityCouponListRespDto(activityCouponReqDto.getStart(), activityCouponReqDto.getPageSize(), count);
        activityCouponListRespDto.setList(ActivityCouponConvertUtil.convert(activityCouponList));

        return CommonResult.success(activityCouponListRespDto);
    }

    @Override
    public CommonResult<ActivityCoupon> check(UserCouponAddDto userCouponAddDto) {
        Activity activity = activityMapper.selectByPrimaryKey(userCouponAddDto.getActivityId());
        //活动状态校验
        if (activity.getStatus() != ActivityStatusEnum.INIT.getStatus()) {
            return CommonResult.failed(ResultCode.ACTIVITY_DELETE);
        }
        Date currentdate = new Date();
        //活动已失效
        if (DateUtil.compare(currentdate, activity.getStartTime()) && DateUtil.compare(activity.getEndTime(), currentdate)) {
            return CommonResult.failed(ResultCode.ACTIVITY_EXPIRED);
        }
        Coupon coupon = couponMapper.selectByPrimaryKey(userCouponAddDto.getCouponId());

        ActivityCoupon activityCoupon = activityCouponMapper.selectByPrimaryKey(userCouponAddDto.getActivityCouponId());
        //校验活动是否有优惠券
        if (activityCoupon == null) {
            return CommonResult.failed(ResultCode.ACTIVITY_NO_ENOUGH);
        }
        return CommonResult.success(activityCoupon);
    }
}
