package com.tian.service.impl;

import com.alibaba.fastjson.JSON;
import com.tian.dto.charge.ChargeStartAddReqDto;
import com.tian.entity.User;
import com.tian.mqtt.enums.MqttTopicEnum;
import com.tian.mqtt.publish.ChargeReadyStartPublish;
import com.tian.service.ChargeService;
import com.tian.util.CommonResult;
import com.tian.util.UserCacheUtil;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年11月07日 19:20
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 */
@Service
public class ChargeServiceImpl implements ChargeService {
    @Resource
    private ChargeReadyStartPublish chargeReadyStartPublish;

    /**
     * 在mqtt上模拟 订阅 这个topic
     *
     * @param gunNo 充电枪编号
     */
    @Override
    public CommonResult<Boolean> start(String gunNo) {
        ChargeStartAddReqDto chargeStartAddReqDto = new ChargeStartAddReqDto();
        chargeStartAddReqDto.setGunNo(gunNo);
        User user = UserCacheUtil.getUser();
        chargeStartAddReqDto.setUserId(user.getId());
        MqttMessage message = new MqttMessage();
        message.setPayload(JSON.toJSONString(chargeStartAddReqDto).getBytes());
        chargeReadyStartPublish.process(MqttTopicEnum.CHARGER_READY_CHARGE.getTopicName(), message);
        return CommonResult.success(Boolean.TRUE);
    }
}
