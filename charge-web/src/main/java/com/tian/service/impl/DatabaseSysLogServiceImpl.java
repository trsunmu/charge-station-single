package com.tian.service.impl;

import com.tian.dto.SysLogMessage;
import com.tian.service.SysLogService;
import org.springframework.stereotype.Service;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 博客地址：<a href="http://woaijava.cc/">博客地址</a>
 * <p>
 * 日志保存到数据库中
 */
@Service
public class DatabaseSysLogServiceImpl implements SysLogService {
    @Override
    public void logRecord(SysLogMessage sysLogMessage) {
    }
}
