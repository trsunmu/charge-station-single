package com.tian.service.impl;

import com.tian.dto.user.UserPointRecordDto;
import com.tian.service.UserPointRecordService;
import com.tian.util.CommonResult;
import org.springframework.stereotype.Service;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * @date 2023年06月05日 16:54
 * 博客地址：<a href="http://woaijava.cc/">博客地址</a>
 * <p>
 * 积分变化记录
 */
@Service
public class UserPointRecordServiceImpl implements UserPointRecordService {

    /*@Resource
    private UserPointRecordMapper userPointRecordMapper;*/
    @Override
    public CommonResult<Boolean> add(UserPointRecordDto userPointRecordDto) {
       /*int flag= userPointRecordMapper.insert(userPointRecordDto);
       if(flag== InsertUpdateDateBaseFlagEnum.SUCCESS.getFlag()){
           return CommonResult.success(Boolean.TRUE);
       }*/
        return CommonResult.failed("新增积分变化记录失败");
    }

    @Override
    public CommonResult page() {
        return null;
    }
}
