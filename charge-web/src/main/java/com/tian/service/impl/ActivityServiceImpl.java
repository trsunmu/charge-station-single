package com.tian.service.impl;

import com.tian.dto.activity.ActivityConvertUtil;
import com.tian.dto.activity.ActivityDto;
import com.tian.dto.activity.ActivityListRespDto;
import com.tian.dto.activity.ActivityRespDto;
import com.tian.entity.Activity;
import com.tian.mapper.ActivityMapper;
import com.tian.service.ActivityService;
import com.tian.util.CommonResult;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * @date 2023年05月15日 16:52
 * 博客地址：<a href="http://woaijava.cc/">博客地址</a>
 */
@Service
public class ActivityServiceImpl implements ActivityService {

    @Resource
    private ActivityMapper activityMapper;

    @Override
    public CommonResult<Boolean> add(ActivityDto activityDto) {
        Activity record = ActivityConvertUtil.convert(activityDto);
        int flag = activityMapper.insert(record);
        if (flag == 1) {
            return CommonResult.success(Boolean.TRUE);
        }
        return CommonResult.failed();
    }

    @Override
    public CommonResult<ActivityListRespDto> list(ActivityDto activityDto) {
        int count = activityMapper.count(activityDto);
        if (count == 0) {
            return CommonResult.success(new ActivityListRespDto(activityDto.getStart(), activityDto.getPageSize()));
        }
        List<Activity> activityList = activityMapper.list(activityDto);

        List<ActivityRespDto> activityRespDtoList = ActivityConvertUtil.convert(activityList);

        ActivityListRespDto activityListRespDto = new ActivityListRespDto(activityDto.getStart(), activityDto.getPageSize());
        activityListRespDto.setList(activityRespDtoList);

        return CommonResult.success(activityListRespDto);
    }
}
