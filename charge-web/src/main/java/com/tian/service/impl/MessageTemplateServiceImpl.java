package com.tian.service.impl;

import com.alibaba.fastjson.JSON;
import com.tian.config.RedisConfig;
import com.tian.dto.MessageTemplateDto;
import com.tian.entity.MessageTemplate;
import com.tian.enums.InsertUpdateDateBaseFlagEnum;
import com.tian.enums.ResultCode;
import com.tian.mapper.MessageTemplateMapper;
import com.tian.service.MessageTemplateService;
import com.tian.util.CommonResult;
import com.tian.util.RedisConstantPre;
import com.tian.util.StringUtil;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * @date 2023年05月26日 20:30
 * 博客地址：<a href="http://woaijava.cc/">博客地址</a>
 * <p>
 * 短信模板
 * 缓存：key=前缀+表注解id
 */
@Service
public class MessageTemplateServiceImpl implements MessageTemplateService {

    @Resource
    private MessageTemplateMapper messageTemplateMapper;

    @Resource
    private RedisConfig redisConfig;

    @Resource
    private ThreadDelayTaskService threadTaskService;

    @Override
    public CommonResult<Boolean> add(MessageTemplateDto messageTemplateDto) {
        MessageTemplate messageTemplate = new MessageTemplate();
        int flag = messageTemplateMapper.insert(messageTemplate);
        if (flag == InsertUpdateDateBaseFlagEnum.SUCCESS.getFlag()) {
            redisConfig.delete(getKey(RedisConstantPre.MESSAGE_TEMPLATE_KEY_PRE, messageTemplateDto.getId()));
            return CommonResult.success(Boolean.TRUE);
        }
        return CommonResult.failed();
    }

    private static String getKey(String pre, Integer id) {
        return pre + "_" + id;
    }

    @Override
    public CommonResult<Boolean> update(MessageTemplateDto messageTemplateDto) {
        String key = getKey(RedisConstantPre.MESSAGE_TEMPLATE_KEY_PRE, messageTemplateDto.getId());
        MessageTemplate messageTemplate = messageTemplateMapper.selectByPrimaryKey(messageTemplateDto.getId());
        if (messageTemplate == null) {
            return CommonResult.failed(ResultCode.PARAMETER_ERROR);
        }
        redisConfig.delete(key);
        messageTemplate.setStatus(messageTemplateDto.getStatus());
        messageTemplate.setTemplate(messageTemplateDto.getTemplate());
        messageTemplate.setId(messageTemplateDto.getId());

        int flag = messageTemplateMapper.updateByPrimaryKey(messageTemplate);
        if (flag == InsertUpdateDateBaseFlagEnum.SUCCESS.getFlag()) {
            threadTaskService.delayDeleteCache(key, ThreadDelayTaskService.DELAY_TIME);
            return CommonResult.success(Boolean.TRUE);
        }
        return CommonResult.failed();
    }

    @Override
    public CommonResult<MessageTemplateDto> queryByMessageType(Integer id) {
        String key = getKey(RedisConstantPre.MESSAGE_TEMPLATE_KEY_PRE, id);
        String cacheValue = redisConfig.get(key);
        //不存在的,直接存空对象
        if (StringUtil.isBlank(cacheValue)) {
            MessageTemplate messageTemplate = messageTemplateMapper.selectByPrimaryKey(id);
            if (messageTemplate == null) {
                redisConfig.set(key, JSON.toJSONString(new MessageTemplate()));
                return CommonResult.failed(ResultCode.PARAMETER_ERROR);
            }
            MessageTemplateDto messageTemplateDto = new MessageTemplateDto();
            messageTemplateDto.setTemplate(messageTemplate.getTemplate());
            messageTemplateDto.setId(messageTemplate.getId());
            messageTemplateDto.setStatus(messageTemplate.getStatus());
            messageTemplateDto.setCreateTime(messageTemplate.getCreateTime());
            redisConfig.set(key, JSON.toJSONString(messageTemplate));
            return CommonResult.success(messageTemplateDto);
        }
        if (JSON.parseObject(cacheValue, MessageTemplate.class).getId() == null) {
            return CommonResult.failed(ResultCode.PARAMETER_ERROR);
        }

        MessageTemplate messageTemplate = JSON.parseObject(cacheValue, MessageTemplate.class);

        MessageTemplateDto messageTemplateDto = new MessageTemplateDto();
        messageTemplateDto.setTemplate(messageTemplate.getTemplate());
        messageTemplateDto.setId(messageTemplate.getId());
        messageTemplateDto.setStatus(messageTemplate.getStatus());
        messageTemplateDto.setCreateTime(messageTemplate.getCreateTime());

        return CommonResult.success(messageTemplateDto);
    }
}
