package com.tian.service.impl;

import com.tian.dto.invited.*;
import com.tian.entity.InvitedRegisterRecord;
import com.tian.mapper.InvitedRegisterRecordMapper;
import com.tian.service.InvitedRegisterRecordService;
import com.tian.util.CommonResult;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年11月01日 19:03
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 * <p>
 * 邀请用户注册
 */
@Service
public class InvitedRegisterRecordServiceImpl implements InvitedRegisterRecordService {

    @Resource
    private InvitedRegisterRecordMapper invitedRegisterRecordMapper;

    @Override
    public CommonResult<InvitedRegisterRecordAddRespDto> add(InvitedRegisterRecordAddReqDto invitedRegisterRecordAddReqDto) {
        InvitedRegisterRecord invitedRegisterRecord = InvitedRegisterRecordConvert.convert4Add(invitedRegisterRecordAddReqDto);
        invitedRegisterRecordMapper.insert(invitedRegisterRecord);
        return CommonResult.success();
    }

    @Override
    public CommonResult<InvitedRegisterRecordUpdateRespDto> update(InvitedRegisterRecordUpdateReqDto invitedRegisterRecordUpdateReqDto) {
        InvitedRegisterRecord invitedRegisterRecord = InvitedRegisterRecordConvert.convert4Update(invitedRegisterRecordUpdateReqDto);
        invitedRegisterRecordMapper.updateByPrimaryKey(invitedRegisterRecord);
        return CommonResult.success();
    }

    @Override
    public CommonResult<InvitedRegisterRecordRespDto> selectById(Integer id) {
        InvitedRegisterRecord invitedRegisterRecord = invitedRegisterRecordMapper.selectByPrimaryKey(id);
        InvitedRegisterRecordRespDto invitedRegisterRecordRespDto = InvitedRegisterRecordConvert.convert4SelectById(invitedRegisterRecord);
        return CommonResult.success(invitedRegisterRecordRespDto);
    }

    @Override
    public CommonResult<InvitedRegisterRecordPageRespDto> page(InvitedRegisterRecordPageReqDto invitedRegisterRecordPageReqDto) {
        int count = invitedRegisterRecordMapper.count(invitedRegisterRecordPageReqDto);
        if (count == 0) {
            return CommonResult.success();
        }
        List<InvitedRegisterRecord> invitedRegisterRecordList = invitedRegisterRecordMapper.page(invitedRegisterRecordPageReqDto);
        InvitedRegisterRecordPageRespDto invitedRegisterRecordPageRespDto = InvitedRegisterRecordConvert.convert4Page(invitedRegisterRecordList);
        return CommonResult.success(invitedRegisterRecordPageRespDto);
    }
}
