package com.tian.service.impl;

import com.tian.dto.points.*;
import com.tian.entity.PointsModifyRecord;
import com.tian.mapper.PointsModifyRecordMapper;
import com.tian.service.PointsModifyRecordService;
import com.tian.util.CommonResult;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年11月01日 19:35
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 */
@Service
public class PointsModifyRecordServiceImpl implements PointsModifyRecordService {
    @Resource
    private PointsModifyRecordMapper pointsModifyRecordMapper;

    @Override
    public CommonResult<PointsModifyRecordAddRespDto> add(PointsModifyRecordAddReqDto pointsModifyRecordAddReqDto) {
        PointsModifyRecord pointsModifyRecord = PointsModifyRecordConvert.convert4Add(pointsModifyRecordAddReqDto);
        pointsModifyRecordMapper.insert(pointsModifyRecord);
        return CommonResult.success();
    }

    @Override
    public CommonResult<PointsModifyRecordUpdateRespDto> update(PointsModifyRecordUpdateReqDto pointsModifyRecordUpdateReqDto) {
        PointsModifyRecord pointsModifyRecord = PointsModifyRecordConvert.convert4Update(pointsModifyRecordUpdateReqDto);
        pointsModifyRecordMapper.updateByPrimaryKey(pointsModifyRecord);
        return CommonResult.success();
    }

    @Override
    public CommonResult<PointsModifyRecordRespDto> selectById(Integer id) {
        PointsModifyRecord pointsModifyRecord = pointsModifyRecordMapper.selectByPrimaryKey(id);
        if (pointsModifyRecord == null) {
            return CommonResult.success();
        }
        PointsModifyRecordRespDto pointsModifyRecordRespDto = PointsModifyRecordConvert.convert4SelectById(pointsModifyRecord);
        return CommonResult.success(pointsModifyRecordRespDto);
    }

    @Override
    public CommonResult<PointsModifyRecordPageRespDto> page(PointsModifyRecordPageReqDto pointsModifyRecordPageReqDto) {
        int count = pointsModifyRecordMapper.count(pointsModifyRecordPageReqDto);
        if (count == 0) {
            return CommonResult.success();
        }
        List<PointsModifyRecord> pointsModifyRecordList = pointsModifyRecordMapper.page(pointsModifyRecordPageReqDto);
        PointsModifyRecordPageRespDto pointsModifyRecordPageRespDto = PointsModifyRecordConvert.convert4Page(pointsModifyRecordList);
        return CommonResult.success(pointsModifyRecordPageRespDto);
    }
}
