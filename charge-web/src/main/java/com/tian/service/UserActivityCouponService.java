package com.tian.service;

import com.tian.dto.coupon.*;
import com.tian.util.CommonResult;

/**
 * @author tianwc 公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年11月01日 21:12
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 */
public interface UserActivityCouponService {
    CommonResult<UserActivityCouponAddRespDto> add(UserActivityCouponAddReqDto userActivityCouponAddReqDto);

    CommonResult<UserActivityCouponUpdateRespDto> update(UserActivityCouponUpdateReqDto userActivityCouponUpdateReqDto);

    CommonResult<UserActivityCouponRespDto> selectById(Integer id);

    CommonResult<UserActivityCouponPageRespDto> page(UserActivityCouponPageReqDto userActivityCouponPageReqDto);
}
