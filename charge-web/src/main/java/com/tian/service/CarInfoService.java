package com.tian.service;

import com.tian.dto.car.*;
import com.tian.util.CommonResult;

/**
 * @author tianwc 公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年10月31日 19:21
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 */
public interface CarInfoService {
    /**
     * 新增 汽车信息
     *
     * @param carInfoAddReqDto
     * @return
     */
    CommonResult<CarInfoAddRespDto> add(CarInfoAddReqDto carInfoAddReqDto);

    /**
     * 修改 汽车信息
     *
     * @param carInfoUpdateReqDto
     * @return
     */
    CommonResult<CarInfoUpdateRespDto> updateCarInfo(CarInfoUpdateReqDto carInfoUpdateReqDto);

    /**
     * 使用id查询
     *
     * @param id 主键id
     * @return
     */
    CommonResult<CarInfoRespDto> getCarInfoById(Integer id);
}
