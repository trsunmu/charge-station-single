package com.tian.service;
import com.tian.dto.activityGoods.*;
import com.tian.util.CommonResult;

/**
 * @author tianwc 公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年10月31日 15:41
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 *
 * 活动商品
 */

public interface ActivityGoodsService {
    /**
     * 新增
     * @param activityGoodsAddReqDto 活动商品信息
     * @return 新增是否成功已经返回参数
     */
    CommonResult<ActivityGoodsAddRespDto> add(ActivityGoodsAddReqDto activityGoodsAddReqDto);
    /**
     * 更新
     * @param activityGoodsUpdateReqDto 活动商品信息
     * @return 更新是否成功已经返回参数
     */
    CommonResult<ActivityGoodsUpdateRespDto> update(ActivityGoodsUpdateReqDto activityGoodsUpdateReqDto);

    CommonResult<ActivityGoodsRespDto> selectById(Integer id);
    CommonResult<ActivityGoodsPageRespDto> page(ActivityGoodsPageReqDto activityGoodsPageReqDto);
}
