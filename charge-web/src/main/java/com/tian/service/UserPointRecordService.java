package com.tian.service;

import com.tian.dto.user.UserPointRecordDto;
import com.tian.util.CommonResult;

/**
 * @author tianwc 公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * @date 2023年06月05日 16:53
 * 博客地址：<a href="http://woaijava.cc/">博客地址</a>
 */
public interface UserPointRecordService {

    CommonResult<Boolean> add(UserPointRecordDto userPointRecordDto);

    CommonResult page();
}
