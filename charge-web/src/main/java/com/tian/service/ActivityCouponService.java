package com.tian.service;

import com.tian.dto.coupon.ActivityCouponDto;
import com.tian.dto.coupon.ActivityCouponListRespDto;
import com.tian.dto.coupon.ActivityCouponReqDto;
import com.tian.dto.user.UserCouponAddDto;
import com.tian.entity.ActivityCoupon;
import com.tian.util.CommonResult;

/**
 * @author tianwc 公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * @date 2023年05月15日 15:34
 * 博客地址：<a href="http://woaijava.cc/">博客地址</a>
 */
public interface ActivityCouponService {

    /**
     * 给活动新增优惠券
     *
     * @param activityCouponDto 活动和优惠券信息
     * @return 新增是否成
     */
    CommonResult<Boolean> add(ActivityCouponDto activityCouponDto);

    /**
     * 查询活动优惠券列表
     *
     * @param activityCouponReqDto 查询条件
     * @return 活动优惠券列表
     */
    CommonResult<ActivityCouponListRespDto> list(ActivityCouponReqDto activityCouponReqDto);

    /**
     * 领取校验
     * @param userCouponAddDto 活动信息
     * @return  是否通过
     */
    CommonResult<ActivityCoupon> check(UserCouponAddDto userCouponAddDto);
}
