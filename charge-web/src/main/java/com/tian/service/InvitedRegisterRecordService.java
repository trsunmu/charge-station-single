package com.tian.service;

import com.tian.dto.invited.*;
import com.tian.util.CommonResult;

/**
 * @author tianwc 公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年11月01日 19:02
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 * <p>
 * 邀请用户注册
 */
public interface InvitedRegisterRecordService {
    CommonResult<InvitedRegisterRecordAddRespDto> add(InvitedRegisterRecordAddReqDto invitedRegisterRecordAddReqDto);

    CommonResult<InvitedRegisterRecordUpdateRespDto> update(InvitedRegisterRecordUpdateReqDto invitedRegisterRecordUpdateReqDto);

    CommonResult<InvitedRegisterRecordRespDto> selectById(Integer id);

    CommonResult<InvitedRegisterRecordPageRespDto> page(InvitedRegisterRecordPageReqDto invitedRegisterRecordPageReqDto);
}
