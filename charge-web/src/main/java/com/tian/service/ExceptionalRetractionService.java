package com.tian.service;

import com.tian.entity.ExceptionalRetroaction;
import com.tian.util.CommonResult;

/**
 * @author tianwc 公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * @date 2023年09月24日 11:55
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 */
public interface ExceptionalRetractionService {

    CommonResult add(ExceptionalRetroaction exceptionalRetraction);
}
