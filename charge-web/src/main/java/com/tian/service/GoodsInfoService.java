package com.tian.service;

import com.tian.dto.GoodsInfoDto;
import com.tian.util.CommonResult;

/**
 * @author tianwc 公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * @date 2023年06月09日 19:44
 * 博客地址：<a href="http://woaijava.cc/">博客地址</a>
 */
public interface GoodsInfoService {


    /**
     * 修改商品信息
     *
     * @param goodsInfoDto 商品信息
     * @return 修改成功
     */
    CommonResult<Boolean> update(GoodsInfoDto goodsInfoDto);

    /**
     * 查询单个商品信息
     *
     * @param id 商品id
     * @return 商品信息
     */
    CommonResult<GoodsInfoDto> getById(Integer id);
}
