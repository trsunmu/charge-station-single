package com.tian.service;

import com.tian.dto.station.*;
import com.tian.util.CommonResult;

/**
 * @author tianwc 公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年11月07日 15:28
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 */
public interface StationService {
    /**
     * 使用id查询
     *
     * @param id 主键id
     * @return
     */
    CommonResult<StationRespDto> getStationById(Integer id);

    CommonResult<StationPageRespDto> page(StationPageReqDto stationPageReqDto);
}
