package com.tian.service;

import com.tian.util.CommonResult;

/**
 * @author tianwc 公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年11月07日 19:11
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 */
public interface ChargeService {
    /**
     * 用户扫描，可以进行充电概念了
     * @param gunNo 充电枪编号
     */
    CommonResult<Boolean> start(String gunNo);
}
