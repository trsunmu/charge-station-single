package com.tian.service;

import com.tian.dto.coupon.CouponInfoReqDto;
import com.tian.dto.coupon.CouponListRespDto;
import com.tian.util.CommonResult;

/**
 * @author tianwc 公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * @date 2023年05月15日 14:29
 * 博客地址：<a href="http://woaijava.cc/">博客地址</a>
 * <p>
 * 优惠券
 */
public interface CouponService {

    /**
     * 分页查询优惠券
     *
     * @param couponInfoReqDto 查询参数
     * @return 优惠券列表
     */
    CommonResult<CouponListRespDto> list(CouponInfoReqDto couponInfoReqDto);
}
