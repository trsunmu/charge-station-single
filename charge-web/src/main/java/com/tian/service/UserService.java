package com.tian.service;

import com.tian.dto.*;
import com.tian.dto.user.*;
import com.tian.entity.User;
import com.tian.util.CommonResult;
import com.tian.util.DataGridView;

/**
 * @author tianwc 公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 博客地址：<a href="http://woaijava.cc/">博客地址</a>
 * <p>
 * 用户信息相关操作
 */
public interface UserService {

    /**
     * 通过id查询用户信息
     *
     * @param id user表主键id
     * @return 用户信息
     */
    CommonResult<UsersRespDto> selectById(Integer id);

    /**
     * 邀请成功增加收益
     *
     * @param invitedDto   邀请增加收益
     * @param codeCachePre 缓存key前缀
     * @return 邀请成功
     */
    CommonResult<User> register(InvitedDto invitedDto, String codeCachePre);

    /**
     * 通过手机号查询用户信息
     *
     * @param queryUserReqDto 用户手机号
     * @return 用户信息
     */
    CommonResult<QueryUserRespDto> selectByPhone(QueryUserReqDto queryUserReqDto);

    /**
     * 用户登录
     *
     * @param userLoginReqDto 手机号+验证码
     * @param codeCachePre    验证码缓存前缀
     * @return 用户信息
     */
    CommonResult<UserLoginResDto> login(UserLoginReqDto userLoginReqDto, String codeCachePre);

    CommonResult<User> update( InvitedDto invitedDto, String sendCodeRegisterPre);

    CommonResult<User> updatePassword(UserUpdatePasswordReqDto userUpdatePasswordReqDto, String sendCodePasswordPre);
}
