package com.tian.service;

import com.tian.dto.MessageTemplateDto;
import com.tian.util.CommonResult;

/**
 * @author tianwc 公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * @date 2023年05月26日 20:30
 * 博客地址：<a href="http://woaijava.cc/">博客地址</a>
 * <p>
 * 短信模板
 */
public interface MessageTemplateService {

    /**
     * 新增短信模板
     *
     * @param messageTemplateDto 模板信息
     * @return 更新成功
     */
    CommonResult<Boolean> add(MessageTemplateDto messageTemplateDto);

    /**
     * 更新短信模板
     *
     * @param messageTemplateDto 模板信息
     * @return 更新成功
     */
    CommonResult<Boolean> update(MessageTemplateDto messageTemplateDto);

    /**
     * 通过类型查询
     *
     * @param type 短信模板类型
     * @return 短信模板信息
     */
    CommonResult<MessageTemplateDto> queryByMessageType(Integer type);
}
