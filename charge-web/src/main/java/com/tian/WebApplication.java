package com.tian;

import com.tian.factory.ApplicationContextFactory;
import com.tian.mqtt.MqttSubscribeProcess;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 * <p>
 * 单体版
 * <p>
 * 开启定时任务注解  https://blog.csdn.net/m0_62854966/article/details/131963712
 * <p>
 * MQTT
 * 启动命令：emqx start
 * 看板地址
 * http://localhost:18083/
 * admin
 * tian123,
 * <p>
 * 文档 https://www.emqx.io/docs/en/latest/dashboard/introduction.html
 */
@EnableScheduling
@SpringBootApplication
public class WebApplication {
    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(WebApplication.class, args);
        ApplicationContextFactory.setApplicationContext(context);
        //启动 mqtt 订阅topic
        MqttSubscribeProcess.process(context);
    }
}
