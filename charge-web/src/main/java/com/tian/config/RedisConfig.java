package com.tian.config;

import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Collection;
import java.util.concurrent.TimeUnit;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * @date 2023年04月22日 15:51
 * 博客地址：<a href="http://woaijava.cc/">博客地址</a>
 */
@Component
public class RedisConfig {

    @Resource
    private RedisTemplate<String, String> redisTemplate;

    /**
     * 设置键值对，不设置过期时间
     *
     * @param key   键
     * @param value 值
     */
    public void set(String key, String value) {
        redisTemplate.opsForValue().set(key, value);
    }

    public Object incr(String key) {
        return redisTemplate.opsForValue().increment(key);
    }

    /**
     * 设置键值对，并设置过期时间
     *
     * @param key    键
     * @param value  值
     * @param expire 过期时间（单位：秒）
     */
    public void set(String key, String value, long expire) {
        redisTemplate.opsForValue().set(key, value, expire, TimeUnit.SECONDS);
    }

    /**
     * 获取键对应的值
     *
     * @param key 键
     * @return 值
     */
    public String get(String key) {
        return redisTemplate.opsForValue().get(key);
    }

    /**
     * 删除指定的键值对
     *
     * @param key 键
     */
    public void delete(String key) {
        redisTemplate.delete(key);
    }

    /**
     * 批量删除指定的键值对
     *
     * @param keys 键的集合
     */
    public void delete(Collection<String> keys) {
        redisTemplate.delete(keys);
    }

    /**
     * 判断键是否存在
     *
     * @param key 键
     * @return 是否存在
     */
    public boolean exists(String key) {
        return redisTemplate.hasKey(key);
    }

    /**
     * 设置键的过期时间
     *
     * @param key    键
     * @param expire 过期时间（单位：秒）
     * @return 是否成功
     */
    public boolean expire(String key, long expire) {
        return redisTemplate.expire(key, expire, TimeUnit.SECONDS);
    }

    /**
     * 获取键的过期时间
     *
     * @param key 键
     * @return 过期时间（单位：秒），返回-2表示键不存在，返回-1表示键存在但没有设置过期时间
     */
    public long ttl(String key) {
        return redisTemplate.getExpire(key, TimeUnit.SECONDS);
    }
}
