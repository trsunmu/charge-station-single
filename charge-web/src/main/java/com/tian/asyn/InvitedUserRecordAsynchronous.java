package com.tian.asyn;

import com.tian.entity.InvitedRegisterRecord;
import com.tian.mapper.InvitedRegisterRecordMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * @date 2023年10月14日 22:16
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 */
@Slf4j
@Component
public class InvitedUserRecordAsynchronous {

    @Resource
    private InvitedRegisterRecordMapper invitedRegisterRecordMapper;

    /**
     * 邀请记录
     *
     * @param userId        注册人id
     * @param createTime    时间
     * @param invitedUserId 邀请人id
     */
    @Async("asyncThreadPool")
    public void insert(Integer userId, Date createTime, Integer invitedUserId) {
        log.info("邀请记录异步落库：注册人id={},时间={},邀请人id={}", userId, createTime, invitedUserId);
        //邀请用户注册记录
        InvitedRegisterRecord invitedRegisterRecord=new InvitedRegisterRecord();
        invitedRegisterRecord.setUserId(userId);
        invitedRegisterRecord.setInvitationUserId(invitedUserId);
        invitedRegisterRecord.setCreateTime(new Date());
        // TODO: 2023/11/2 这里从数据库里中运营人员配置表中获取
        invitedRegisterRecord.setIncome(new BigDecimal("10.05"));
        invitedRegisterRecordMapper.insert(invitedRegisterRecord);
    }
}
