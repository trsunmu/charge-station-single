package com.tian.asyn;

import com.tian.entity.User;
import com.tian.mapper.UserMapper;
import com.tian.util.TwoDimensionalCodeGenerator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Map;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年11月22日 10:14
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 * <p>
 * 异步生成邀请注册二维码 也可以使用前端技术生成
 */
@Slf4j
@Component
public class InvitedCodeAsynchronous {
    @Resource
    private UserMapper userMapper;

    @Async("asyncThreadPool")
    public void getTwoDimensionalCode(Integer userId, String url, Map<String, Object> paramMap) {
        // TODO: 2023/11/22 可以改造成 oss 存储
        String path = TwoDimensionalCodeGenerator.getTwoDimensionalCode(url, paramMap);
        User user = userMapper.getUserById(userId);
        user.setTwoDimensionalCodeUrl(path);
        userMapper.updateUser(user);
    }
}
