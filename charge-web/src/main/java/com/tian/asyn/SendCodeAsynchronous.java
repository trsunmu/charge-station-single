package com.tian.asyn;

import com.tian.enums.SendCodeTyeEnum;
import com.tian.factory.SendCodeStrategyFactory;
import com.tian.msg.send.SmsSender;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.LinkedHashMap;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * @date 2023年10月14日 11:57
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 * <p>
 * 单体版 采用线程池+@Async 实现异步
 */
@Slf4j
@Component
public class SendCodeAsynchronous {
    @Resource
    private SendCodeStrategyFactory sendCodeStrategyFactory;

    @Async("asyncThreadPool")
    public void sendCode(String smsTemplate, String phone, LinkedHashMap<String, String> params) {
        log.info("SendCodeAsynchronous：异步发送短信验证码,phone={},短信模板={}", phone, smsTemplate);
        SmsSender smsSender = sendCodeStrategyFactory.getInvokeStrategy(SendCodeTyeEnum.BAIDU.getType());
        smsSender.send(smsTemplate, params, phone);
    }
}
