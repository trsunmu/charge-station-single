package com.tian.mapper;

import com.tian.dto.charger.ChargerPageReqDto;
import com.tian.entity.Charger;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ChargerMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Charger record);

    Charger selectByPrimaryKey(Integer id);

    List<Charger> selectAll();

    int updateByPrimaryKey(Charger record);

    List<Charger> page(ChargerPageReqDto chargerPageReqDto);

    int count(ChargerPageReqDto chargerPageReqDto);
}