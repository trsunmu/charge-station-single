package com.tian.mapper;

import com.tian.entity.GlobalProperty;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface GlobalPropertyMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(GlobalProperty record);

    GlobalProperty selectByPrimaryKey(Integer id);

    List<GlobalProperty> selectAll();
    List<GlobalProperty> selectByBusiType(Integer busiType);

    int updateByPrimaryKey(GlobalProperty record);
}