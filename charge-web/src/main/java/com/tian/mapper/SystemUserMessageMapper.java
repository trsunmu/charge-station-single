package com.tian.mapper;

import com.tian.dto.message.SystemUserMessageRecordPageReqDto;
import com.tian.entity.SystemUserMessage;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * {@code @description:} 站内信持久层
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024/1/12 9:17
 * {@code @version:} 1.0
 */
@Mapper
public interface SystemUserMessageMapper {
    /**
     * 根据id查询站内信内容
     *
     * @param id 表主键id
     * @return 站内心内容
     */
    SystemUserMessage selectByPrimaryKey(Long id);

    /**
     * 新增站内信
     *
     * @param systemUserMessage 站内信内容
     */
    int insert(SystemUserMessage systemUserMessage);

    /**
     * 根据id修改站内信内容
     *
     * @param record 站内信新内容
     * @return 修改是否成功
     */
    int updateByPrimaryKey(SystemUserMessage record);

    /**
     * 删除
     * @param record 站内信信息
     * @return 删除成功
     */
    int updateStatusById(SystemUserMessage record);

    /**
     * 分页查询站内信
     *
     * @param recordPageReqDto 查询参数
     * @return 站内信类别
     */
    List<SystemUserMessage> page(@Param("recordPageReqDto") SystemUserMessageRecordPageReqDto recordPageReqDto);

    /**
     * 站内信数量统计
     *
     * @param recordPageReqDto 查询参数
     * @return 站内信数量
     */
    int count(@Param("recordPageReqDto") SystemUserMessageRecordPageReqDto recordPageReqDto);

    /**
     * 全部阅读
     * @param userId 用户id
     * @return 阅读成功
     */
    int readAll(Integer userId);
}