package com.tian.mapper;

import com.tian.dto.gun.ChargeGunPageReqDto;
import com.tian.entity.ChargeGun;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ChargeGunMapper {

    int insert(ChargeGun record);

    ChargeGun selectByPrimaryKey(Integer id);
    ChargeGun selectByGunNo(String gunNo);

    List<ChargeGun> selectAll();

    int updateByPrimaryKey(ChargeGun record);

    List<ChargeGun> page(ChargeGunPageReqDto chargeGunPageReqDto);

    int count(ChargeGunPageReqDto chargeGunPageReqDto);
}