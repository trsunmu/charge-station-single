package com.tian.mapper;

import com.tian.entity.SystemMessageTemplate;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface SystemMessageTemplateMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(SystemMessageTemplate record);

    SystemMessageTemplate selectByPrimaryKey(Integer id);

    List<SystemMessageTemplate> selectAll();

    int updateByPrimaryKey(SystemMessageTemplate record);
}