package com.tian.mapper;

import com.tian.dto.order.PayOrderPageReqDto;
import com.tian.entity.PayOrder;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface PayOrderMapper {

    int insert(PayOrder record);

    PayOrder selectByPrimaryKey(Integer id);

    PayOrder selectByOrderNo(String orderNo);

    List<PayOrder> selectByStatus(@Param("status") Integer status, @Param("start") int start, @Param("pageSize") int pageSize);

    int countByStatus(@Param("status") Integer status);

    List<PayOrder> selectAll();

    int updateByPrimaryKey(PayOrder record);

    List<PayOrder> page(PayOrderPageReqDto payOrderPageReqDto);

    int count(PayOrderPageReqDto payOrderPageReqDto);
}