package com.tian.mapper;

import com.tian.dto.activityGoods.ActivityGoodsPageReqDto;
import com.tian.entity.ActivityGoods;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author tianwc 公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年10月31日 15:40
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 */
@Mapper
public interface ActivityGoodsMapper {
    void insert(ActivityGoods activityGoods);

    void update(ActivityGoods activityGoods);

    void delete(Integer id);

    ActivityGoods selectById(Integer id);

    List<ActivityGoods> selectAll();

    List<ActivityGoods> page(ActivityGoodsPageReqDto activityGoodsPageReqDto);
    int  count(ActivityGoodsPageReqDto activityGoodsPageReqDto);
}
