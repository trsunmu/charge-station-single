package com.tian.mapper;

import com.tian.entity.InvitedIncomeRecord;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface InvitedIncomeRecordMapper {
    int deleteByPrimaryKey(Long id);

    int insert(InvitedIncomeRecord record);

    InvitedIncomeRecord selectByPrimaryKey(Long id);

    List<InvitedIncomeRecord> selectAll();

    int updateByPrimaryKey(InvitedIncomeRecord record);
}