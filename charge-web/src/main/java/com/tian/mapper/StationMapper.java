package com.tian.mapper;

import com.tian.dto.station.StationPageReqDto;
import com.tian.entity.Station;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface StationMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Station record);

    Station selectByPrimaryKey(Integer id);

    List<Station> selectAll();

    int updateByPrimaryKey(Station record);

    int count(StationPageReqDto stationPageReqDto);

    List<Station> page(StationPageReqDto stationPageReqDto);
}