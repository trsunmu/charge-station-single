package com.tian.mapper;

import com.tian.dto.points.PointsModifyRecordPageReqDto;
import com.tian.entity.PointsModifyRecord;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface PointsModifyRecordMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(PointsModifyRecord record);

    PointsModifyRecord selectByPrimaryKey(Integer id);

    List<PointsModifyRecord> selectAll();

    int updateByPrimaryKey(PointsModifyRecord record);

    int count(PointsModifyRecordPageReqDto pointsModifyRecordPageReqDto);

    List<PointsModifyRecord> page(PointsModifyRecordPageReqDto pointsModifyRecordPageReqDto);
}