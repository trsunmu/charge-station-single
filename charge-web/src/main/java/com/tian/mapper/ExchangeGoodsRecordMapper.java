package com.tian.mapper;

import com.tian.dto.exchange.ExchangeGoodsRecordPageReqDto;
import com.tian.entity.ExchangeGoodsRecord;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ExchangeGoodsRecordMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ExchangeGoodsRecord record);

    ExchangeGoodsRecord selectByPrimaryKey(Integer id);

    List<ExchangeGoodsRecord> selectAll();

    int updateByPrimaryKey(ExchangeGoodsRecord record);

    int count(ExchangeGoodsRecordPageReqDto exchangeGoodsRecordPageReqDto);

    List<ExchangeGoodsRecord> page(ExchangeGoodsRecordPageReqDto exchangeGoodsRecordPageReqDto);
}