package com.tian.mapper;

import com.tian.dto.price.ElectricityPricePageReqDto;
import com.tian.entity.ElectricityPrice;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ElectricityPriceMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ElectricityPrice record);

    ElectricityPrice selectByPrimaryKey(Integer id);

    List<ElectricityPrice> selectAll();

    int updateByPrimaryKey(ElectricityPrice record);

    int count(ElectricityPricePageReqDto electricityPricePageReqDto);

    List<ElectricityPrice> page(ElectricityPricePageReqDto electricityPricePageReqDto);
}