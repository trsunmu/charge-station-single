package com.tian.mapper;

import com.tian.dto.invited.InvitedRegisterRecordPageReqDto;
import com.tian.entity.InvitedRegisterRecord;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface InvitedRegisterRecordMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(InvitedRegisterRecord record);

    InvitedRegisterRecord selectByPrimaryKey(Integer id);

    List<InvitedRegisterRecord> selectAll();

    int updateByPrimaryKey(InvitedRegisterRecord record);

    int count(InvitedRegisterRecordPageReqDto invitedRegisterRecordPageReqDto);

    List<InvitedRegisterRecord> page(InvitedRegisterRecordPageReqDto invitedRegisterRecordPageReqDto);
}