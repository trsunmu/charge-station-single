package com.tian.mapper;

import com.tian.dto.investDetail.InvestorDetailPageReqDto;
import com.tian.entity.InvitedUserRecord;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
@Mapper
public interface InvitedUserRecordMapper {
    int deleteByPrimaryKey(Long id);

    int insert(InvitedUserRecord record);

    InvitedUserRecord selectByPrimaryKey(Long id);

    List<InvitedUserRecord> selectAll();

    int updateByPrimaryKey(InvitedUserRecord record);

    int count(InvestorDetailPageReqDto investorDetailPageReqDto);

}