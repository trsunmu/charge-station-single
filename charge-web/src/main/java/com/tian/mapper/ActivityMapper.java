package com.tian.mapper;

import com.tian.dto.activity.ActivityDto;
import com.tian.entity.Activity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
@Mapper
public interface ActivityMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Activity record);

    Activity selectByPrimaryKey(Integer id);

    List<Activity> selectAll();

    int updateByPrimaryKey(Activity record);

    int count(ActivityDto activityDto);

    List<Activity> list(ActivityDto activityDto);
}