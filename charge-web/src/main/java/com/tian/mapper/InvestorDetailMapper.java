package com.tian.mapper;

import com.tian.dto.investDetail.InvestorDetailPageReqDto;
import com.tian.entity.InvestorDetail;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface InvestorDetailMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(InvestorDetail record);

    InvestorDetail selectByPrimaryKey(Integer id);

    List<InvestorDetail> selectAll();

    int updateByPrimaryKey(InvestorDetail record);

    int count(InvestorDetailPageReqDto investorDetailPageReqDto);

    List<InvestorDetail> page(InvestorDetailPageReqDto investorDetailPageReqDto);
}