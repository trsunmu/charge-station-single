package com.tian.mapper;

import com.tian.dto.car.CarInfoPageReqDto;
import com.tian.entity.CarInfo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author tianwc 公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年10月31日 19:19
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 */
@Mapper
public interface CarInfoMapper {
    void insertCarInfo(CarInfo carInfo);

    void updateCarInfo(CarInfo carInfo);

    void deleteCarInfo(int id);

    CarInfo getCarInfoById(int id);

    List<CarInfo> getAllCarInfo();

    List<CarInfo> page(CarInfoPageReqDto carInfoPageReqDto);

    int count(CarInfoPageReqDto carInfoPageReqDto);
}
