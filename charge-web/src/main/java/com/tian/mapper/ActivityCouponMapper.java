package com.tian.mapper;

import com.tian.dto.coupon.ActivityCouponReqDto;
import com.tian.entity.ActivityCoupon;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface ActivityCouponMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ActivityCoupon record);

    ActivityCoupon selectByPrimaryKey(Integer id);

    List<ActivityCoupon> selectAll();

    int updateByPrimaryKey(ActivityCoupon record);

    int count(ActivityCouponReqDto activityCouponReqDto);

    List<ActivityCoupon> list(ActivityCouponReqDto activityCouponReqDto);

    ActivityCoupon selectByActivityCoupon(@Param("activityId") Integer activityId,@Param("activityId")  Integer couponId);
}