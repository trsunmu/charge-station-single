package com.tian.mapper;

import com.tian.dto.coupon.UserActivityCouponPageReqDto;
import com.tian.entity.UserActivityCoupon;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface UserActivityCouponMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(UserActivityCoupon record);

    UserActivityCoupon selectByPrimaryKey(Integer id);

    List<UserActivityCoupon> selectAll();

    int updateByPrimaryKey(UserActivityCoupon record);

    long count(UserActivityCouponPageReqDto userActivityCouponPageReqDto);

    List<UserActivityCoupon> page(UserActivityCouponPageReqDto userActivityCouponPageReqDto);

    int countByUserIdAndCouponId(@Param("userId") Long userId, @Param("couponId") Integer couponId);
}