package com.tian.mapper;

import com.tian.dto.charger.ChargerRecordPageReqDto;
import com.tian.entity.ChargerRecord;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ChargerRecordMapper {

    int insert(ChargerRecord record);

    ChargerRecord selectByPrimaryKey(Integer id);
    ChargerRecord selectByUserId(Integer userId);
    ChargerRecord selectByChargeGunId(Integer chargerGunId);

    List<ChargerRecord> selectAll();

    int updateByPrimaryKey(ChargerRecord record);

    List<ChargerRecord> page(ChargerRecordPageReqDto chargerRecordPageReqDto);

    int count(ChargerRecordPageReqDto chargerRecordPageReqDto);
}