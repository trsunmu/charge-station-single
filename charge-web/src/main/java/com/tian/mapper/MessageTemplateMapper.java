package com.tian.mapper;

import com.tian.entity.MessageTemplate;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface MessageTemplateMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(MessageTemplate record);

    MessageTemplate selectByPrimaryKey(Integer id);

    List<MessageTemplate> selectAll();

    int updateByPrimaryKey(MessageTemplate record);
}