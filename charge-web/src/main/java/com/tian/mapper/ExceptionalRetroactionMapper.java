package com.tian.mapper;

import com.tian.entity.ExceptionalRetroaction;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ExceptionalRetroactionMapper {
    int deleteByPrimaryKey(Long id);

    int insert(ExceptionalRetroaction record);

    ExceptionalRetroaction selectByPrimaryKey(Long id);

    List<ExceptionalRetroaction> selectAll();

    int updateByPrimaryKey(ExceptionalRetroaction record);
}