package com.tian.mapper;

import com.tian.dto.user.UserQueryListReqDto;
import com.tian.entity.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface UserMapper {
    void insertUser(User user);

    void updateUser(User user);

    User getUserById(Integer id);

    List<User> getAllUsers();

    User selectByInvitationCode(String invitedCode);

    User selectByPhone(String phone);

    int count(UserQueryListReqDto userQueryListReqDto);

    List<User> page(@Param("dto") UserQueryListReqDto userQueryListReqDto);

    int removeById(int id);
}