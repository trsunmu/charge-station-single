package com.tian.mapper;

import com.tian.entity.GoodsInfo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface GoodsInfoMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(GoodsInfo record);

    GoodsInfo selectByPrimaryKey(Integer id);

    List<GoodsInfo> selectAll();

    int updateByPrimaryKey(GoodsInfo record);
}