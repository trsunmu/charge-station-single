package com.tian.mapper;

import com.tian.dto.price.ElectricityPriceHistoryPageReqDto;
import com.tian.entity.ElectricityPriceHistory;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ElectricityPriceHistoryMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ElectricityPriceHistory record);

    ElectricityPriceHistory selectByPrimaryKey(Integer id);

    List<ElectricityPriceHistory> selectAll();

    int updateByPrimaryKey(ElectricityPriceHistory record);

    int count(ElectricityPriceHistoryPageReqDto electricityPriceHistoryPageReqDto);

    List<ElectricityPriceHistory>  page(ElectricityPriceHistoryPageReqDto electricityPriceHistoryPageReqDto);
}