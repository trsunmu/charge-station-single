package com.tian.mapper;

import com.tian.entity.UserActivityGoods;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
@Mapper
public interface UserActivityGoodsMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(UserActivityGoods record);

    UserActivityGoods selectByPrimaryKey(Integer id);

    List<UserActivityGoods> selectAll();

    int updateByPrimaryKey(UserActivityGoods record);
}