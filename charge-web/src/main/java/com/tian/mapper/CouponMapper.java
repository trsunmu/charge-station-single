package com.tian.mapper;

import com.tian.dto.coupon.CouponInfoReqDto;
import com.tian.entity.Coupon;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface CouponMapper {

    Coupon selectByPrimaryKey(Integer id);
    List<Coupon> selectByIdList(List<Integer> couponIdList);

    int updateByPrimaryKey(Coupon record);

    int count(CouponInfoReqDto request);

    List<Coupon> page(CouponInfoReqDto request);
}