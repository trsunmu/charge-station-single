package com.tian.dto.exchange;

import com.tian.entity.ExchangeGoodsRecord;

import java.util.List;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年10月31日 21:53
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 */
public class ExchangeGoodsRecordConvert {
    public static ExchangeGoodsRecord convert4Add(ExchangeGoodsRecordAddReqDto carInfoAddReqDto){
        ExchangeGoodsRecord exchangeGoodsRecord=new ExchangeGoodsRecord();
        return exchangeGoodsRecord;
    }
    public static ExchangeGoodsRecord convert4Update(ExchangeGoodsRecordUpdateReqDto exchangeGoodsRecordUpdateReqDto){
        ExchangeGoodsRecord exchangeGoodsRecord=new ExchangeGoodsRecord();
        return exchangeGoodsRecord;
    }
    public static ExchangeGoodsRecordRespDto convert4SelectById(ExchangeGoodsRecord exchangeGoodsRecord){
        ExchangeGoodsRecordRespDto exchangeGoodsRecordRespDto=new ExchangeGoodsRecordRespDto();
        return exchangeGoodsRecordRespDto;
    }
    public static ExchangeGoodsRecordPageRespDto convert4Page(List<ExchangeGoodsRecord> exchangeGoodsRecordList){
        ExchangeGoodsRecordPageRespDto exchangeGoodsRecordPageRespDto=new ExchangeGoodsRecordPageRespDto();
        return exchangeGoodsRecordPageRespDto;
    }
}
