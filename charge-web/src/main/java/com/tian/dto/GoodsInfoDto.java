package com.tian.dto;

import com.tian.entity.GoodsInfo;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * @date 2023年06月09日 19:44
 * 博客地址：<a href="http://woaijava.cc/">博客地址</a>
 */
public class GoodsInfoDto extends GoodsInfo {
}
