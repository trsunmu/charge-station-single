package com.tian.dto.investDetail;

import lombok.Data;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年10月31日 15:19
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 */
@Data
public class InvestorDetailAddReqDto {
    private String businessLicenseUrl;

    private String contractUrl;

    private String idCradFrontUrl;

    private String idCradReverseUrl;

    private String bankCardNo;

    private String bankAccount;

    private String openBank;
}
