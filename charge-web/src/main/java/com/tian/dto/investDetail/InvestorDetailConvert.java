package com.tian.dto.investDetail;

import com.tian.entity.InvestorDetail;
import org.springframework.beans.BeanUtils;

import java.util.List;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年10月31日 15:21
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 */
public class InvestorDetailConvert {
    public static InvestorDetail convert4Add(InvestorDetailAddReqDto investorDetailAddReqDto) {
        InvestorDetail investorDetail = new InvestorDetail();
        BeanUtils.copyProperties(investorDetailAddReqDto, investorDetail);
        return investorDetail;
    }

    public static InvestorDetail convert4Update(InvestorDetailUpdateReqDto investorDetailUpdateReqDto) {
        InvestorDetail investorDetail = new InvestorDetail();

        return investorDetail;
    }

    public static InvestorDetailPageRespDto convert4Page(List<InvestorDetail> investorDetailList) {
        InvestorDetailPageRespDto investorDetailPageRespDto = new InvestorDetailPageRespDto();

        return investorDetailPageRespDto;
    }

    public static InvestorDetailRespDto convert4SelectById(InvestorDetail investorDetail) {
        InvestorDetailRespDto investorDetailRespDto = new InvestorDetailRespDto();

        return investorDetailRespDto;
    }
}
