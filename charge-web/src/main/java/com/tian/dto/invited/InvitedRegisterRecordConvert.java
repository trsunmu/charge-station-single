package com.tian.dto.invited;

import com.tian.entity.InvitedRegisterRecord;

import java.util.List;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年11月01日 10:06
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 */
public class InvitedRegisterRecordConvert {

    public static InvitedRegisterRecord convert4Add(InvitedRegisterRecordAddReqDto chargeGunAddReqDto) {
        InvitedRegisterRecord invitedRegisterRecord=new InvitedRegisterRecord();

        return invitedRegisterRecord;
    }

    public static InvitedRegisterRecord convert4Update(InvitedRegisterRecordUpdateReqDto invitedRegisterRecordUpdateReqDto) {
        InvitedRegisterRecord invitedRegisterRecord=new InvitedRegisterRecord();

        return invitedRegisterRecord;
    }

    public static InvitedRegisterRecordPageRespDto convert4Page(List<InvitedRegisterRecord> invitedRegisterRecords) {
        InvitedRegisterRecordPageRespDto invitedRegisterRecordPageRespDto=new InvitedRegisterRecordPageRespDto();
        return invitedRegisterRecordPageRespDto;
    }

    public static InvitedRegisterRecordRespDto convert4SelectById(InvitedRegisterRecord invitedRegisterRecord) {
        InvitedRegisterRecordRespDto invitedRegisterRecordRespDto=new InvitedRegisterRecordRespDto();

        return invitedRegisterRecordRespDto;
    }
}
