package com.tian.dto.activityGoods;

import com.tian.entity.ActivityGoods;

import java.util.List;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年10月31日 16:39
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 */
public class ActivityGoodsConvert {

    public static ActivityGoods convert4Add(ActivityGoodsAddReqDto activityGoodsAddReqDto) {
        ActivityGoods activityGoods = new ActivityGoods();
        // TODO: 2023/10/31  set get 参数
        return activityGoods;
    }
    public static ActivityGoods convert4Update(ActivityGoodsUpdateReqDto activityGoodsUpdateReqDto) {
        ActivityGoods activityGoods = new ActivityGoods();
        // TODO: 2023/10/31  set get 参数
        return activityGoods;
    }
    public static ActivityGoodsRespDto convert4SelectById(ActivityGoods activityGoods) {
        ActivityGoodsRespDto activityGoodsRespDto = new ActivityGoodsRespDto();
        // TODO: 2023/10/31  set get 参数
        return activityGoodsRespDto;
    }
    public static ActivityGoodsPageRespDto convert4Response(List<ActivityGoods> activityGoodsList) {
        ActivityGoodsPageRespDto activityGoodsPageRespDto = new ActivityGoodsPageRespDto();
        // TODO: 2023/10/31  set get 参数
        return activityGoodsPageRespDto;
    }
}
