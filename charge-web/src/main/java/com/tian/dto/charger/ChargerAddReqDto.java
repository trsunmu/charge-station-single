package com.tian.dto.charger;

import lombok.Data;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年11月01日 10:34
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 */
@Data
public class ChargerAddReqDto {

    private Integer stationId;

    private String chargerCode;

    private String name;

    private Integer chargerType;

    private Integer status;

    private String parkNo;

    private String manufacturer;

    private String voltage;

    private String chargerModel;

    private Integer electricityPriceId;

    private String power;

    private String supportCars;

    private String description;

    private String qrcode;
}
