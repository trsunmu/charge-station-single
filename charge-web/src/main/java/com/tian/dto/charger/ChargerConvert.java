package com.tian.dto.charger;

import com.tian.entity.Charger;
import org.springframework.beans.BeanUtils;

import java.util.List;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年11月01日 10:37
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 */
public class ChargerConvert {

    public static Charger convert4Add(ChargerAddReqDto chargerAddReqDto) {
        Charger charger = new Charger();
        BeanUtils.copyProperties(chargerAddReqDto, charger);
        return charger;
    }

    public static Charger convert4Update(ChargerUpdateReqDto chargerUpdateReqDto) {
        Charger charger = new Charger();
        return charger;
    }

    public static ChargerPageRespDto convert4Page(List<Charger> chargerList) {
        ChargerPageRespDto chargerPageRespDto = new ChargerPageRespDto();
        return chargerPageRespDto;
    }

    public static ChargerRespDto convert4SelectById(Charger charger) {
        ChargerRespDto chargerRespDto = new ChargerRespDto();
        return chargerRespDto;
    }
}
