package com.tian.dto.charger;

import com.tian.entity.ChargerRecord;

import java.util.List;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年11月01日 11:08
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 */
public class ChargerRecordConvert {

    public static ChargerRecord convert4Add(ChargerRecordAddReqDto chargerRecordAddReqDto) {
        ChargerRecord chargerRecord = new ChargerRecord();

        return chargerRecord;
    }

    public static ChargerRecord convert4Update(ChargerRecordUpdateReqDto chargerRecordUpdateReqDto) {
        ChargerRecord chargerRecord = new ChargerRecord();

        return chargerRecord;
    }

    public static ChargerRecordPageRespDto convert4Page(List<ChargerRecord> chargerRecordList) {
        ChargerRecordPageRespDto chargerRecordPageRespDto=new ChargerRecordPageRespDto();

        return chargerRecordPageRespDto;
    }

    public static ChargerRecordRespDto convert4SelectById(ChargerRecord chargerRecord) {
        ChargerRecordRespDto chargerRecordRespDto=new ChargerRecordRespDto();

        return chargerRecordRespDto;
    }
}
