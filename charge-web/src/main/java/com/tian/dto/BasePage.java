package com.tian.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 * <p>
 * 分页
 */
@Data
public abstract class BasePage {
    @ApiModelProperty(value = "分页大小")
    protected int pageSize;
    @ApiModelProperty(value = "当前页")
    protected int page;
    @ApiModelProperty(value = "总记录数")
    protected long total;
    protected int start;
}
