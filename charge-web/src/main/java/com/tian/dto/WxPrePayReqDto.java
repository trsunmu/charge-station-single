package com.tian.dto;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * @date 2023年05月12日 16:06
 * 博客地址：<a href="http://woaijava.cc/">博客地址</a>
 */
@Data
public class WxPrePayReqDto {
    private Integer userId;
    private String openId;
    private String orderNo;
    private BigDecimal amount;
    private String ip;
    private Integer userCouponId;
}
