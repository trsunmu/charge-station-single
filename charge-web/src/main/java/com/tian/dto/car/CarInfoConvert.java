package com.tian.dto.car;

import com.tian.entity.CarInfo;
import org.springframework.beans.BeanUtils;

import java.util.List;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年10月31日 21:53
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 */
public class CarInfoConvert {

    public static CarInfo convert4Add(CarInfoAddReqDto carInfoAddReqDto){
        CarInfo carInfo=new CarInfo();
        carInfo.setCarBrand(carInfoAddReqDto.getCarBrand());
        carInfo.setCarType(carInfoAddReqDto.getCarType());
        carInfo.setCarIdentifyStatus(0);
        carInfo.setDrivingLicenseUrl(carInfoAddReqDto.getDrivingLicenseUrl());
        carInfo.setEngineNumber(carInfoAddReqDto.getEngineNumber());
        carInfo.setFrameNumber(carInfoAddReqDto.getFrameNumber());
        carInfo.setPlateNumber(carInfoAddReqDto.getPlateNumber());
        return carInfo;
    }
    public static CarInfo convert4Update(CarInfoUpdateReqDto carInfoUpdateReqDto){
        CarInfo carInfo=new CarInfo();
        BeanUtils.copyProperties(carInfoUpdateReqDto,carInfo);
        return carInfo;
    }
    public static CarInfoRespDto convert4SelectById(CarInfo carInfo){
        CarInfoRespDto carInfoRespDto=new CarInfoRespDto();
        return carInfoRespDto;
    }
    public static CarInfoPageRespDto convert4Page(List<CarInfo> carInfoList){
        CarInfoPageRespDto carInfoPageRespDto=new CarInfoPageRespDto();
        return carInfoPageRespDto;
    }
}
