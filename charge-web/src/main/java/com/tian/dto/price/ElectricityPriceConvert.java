package com.tian.dto.price;

import com.tian.entity.ElectricityPrice;

import java.util.List;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年11月01日 12:09
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 */
public class ElectricityPriceConvert {
    public static ElectricityPrice convert4Add(ElectricityPriceAddReqDto electricityPriceAddReqDto) {
        ElectricityPrice electricityPrice = new ElectricityPrice();

        return electricityPrice;
    }
    public static ElectricityPrice convert4Update(ElectricityPriceUpdateReqDto electricityPriceUpdateReqDto) {
        ElectricityPrice electricityPrice = new ElectricityPrice();

        return electricityPrice;
    }

    public static ElectricityPriceRespDto convert4SelectById(ElectricityPrice electricityPrice) {
        ElectricityPriceRespDto electricityPriceRespDto=new ElectricityPriceRespDto();

        return electricityPriceRespDto;
    }

    public static ElectricityPricePageRespDto convert4Page(List<ElectricityPrice> electricityPriceList) {
        ElectricityPricePageRespDto electricityPricePageRespDto=new ElectricityPricePageRespDto();

        return electricityPricePageRespDto;
    }
}
