package com.tian.dto.price;

import com.tian.entity.ElectricityPriceHistory;

import java.util.List;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年11月01日 12:09
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 */
public class ElectricityPriceHistoryConvert {
    public static ElectricityPriceHistory convert4Add(ElectricityPriceHistoryAddReqDto electricityPriceAddReqDto) {
        ElectricityPriceHistory electricityPriceHistory = new ElectricityPriceHistory();
        return electricityPriceHistory;
    }

    public static ElectricityPriceHistory convert4Update(ElectricityPriceHistoryUpdateReqDto electricityPriceUpdateReqDto) {
        ElectricityPriceHistory electricityPriceHistory = new ElectricityPriceHistory();

        return electricityPriceHistory;
    }

    public static ElectricityPriceHistoryRespDto convert4SelectById(ElectricityPriceHistory electricityPrice) {
        ElectricityPriceHistoryRespDto electricityPriceHistoryRespDto = new ElectricityPriceHistoryRespDto();

        return electricityPriceHistoryRespDto;
    }

    public static ElectricityPriceHistoryPageRespDto convert4Page(List<ElectricityPriceHistory> electricityPriceHistoryList) {
        ElectricityPriceHistoryPageRespDto electricityPriceHistoryPageRespDto = new ElectricityPriceHistoryPageRespDto();

        return electricityPriceHistoryPageRespDto;
    }
}
