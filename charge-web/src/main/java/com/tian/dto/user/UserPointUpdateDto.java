package com.tian.dto.user;

import lombok.Data;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * @date 2023年06月05日 16:42
 * 博客地址：<a href="http://woaijava.cc/">博客地址</a>
 * <p>
 * 积分变更
 */
@Data
public class UserPointUpdateDto {

    /**
     * 本次变更的唯一标识
     * 充值：前缀 CHARGE_{orderNo}
     * CHARGE_1000001121312
     * 用户注册：前缀 USER_REGISTER_{orderNo}
     * 注册式 order 直接使用userId
     */
    private String orderNo;
    /**
     * 用户id
     */
    private Long userId;
    /**
     * 变更积分数量
     */
    private Integer points;
    /**
     * 积分变动类型：0：增加  1：扣减
     */
    private Integer type;
}
