package com.tian.dto.user;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年11月02日 09:45
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 */
@Data
public class UsersAddReqDto {
    private Integer id;
    private String nickName;
    private String realName;
    private String userPassword;
    private Integer userType;
    private Integer gender;
    private Date birthday;
    private String avatarUrl;
    private String phone;
    private String idNo;
    private String province;
    private String city;
    private String area;
    private String detailAddress;
    private Integer status;
    private String email;
    private BigDecimal money;
    private Integer points;
    private String invitationCode;
    private String invitedCode;
    private Date createTime;
}
