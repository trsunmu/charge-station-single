package com.tian.dto.user;

import com.tian.dto.QueryUserRespDto;
import com.tian.entity.User;
import org.springframework.beans.BeanUtils;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年11月20日 16:19
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 */
public class UserConvert {

    public static QueryUserRespDto convert4selectByPhone(User user) {
        QueryUserRespDto queryUserRespDto = new QueryUserRespDto();
        BeanUtils.copyProperties(user,queryUserRespDto);
        return queryUserRespDto;
    }
}
