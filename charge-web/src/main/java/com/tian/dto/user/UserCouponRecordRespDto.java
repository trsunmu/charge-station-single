package com.tian.dto.user;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * @date 2023年05月15日 19:55
 * 博客地址：<a href="http://woaijava.cc/">博客地址</a>
 */
@Data
public class UserCouponRecordRespDto {

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    @ApiModelProperty(value = "")
    private Date startDate;

    private String startDateStr;

    private String name;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    @ApiModelProperty(value = "")
    private Date endDate;

    private String endDateStr;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    @ApiModelProperty(value = "")
    private Date createTime;

    private Long id;

    private Integer userId;

    private Integer activityId;

    private Integer couponId;

    private Integer status;

}
