package com.tian.dto.user;

import com.tian.dto.PageBase;
import lombok.Data;

import java.util.Date;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * @date 2023年06月09日 20:25
 * 博客地址：<a href="http://woaijava.cc/">博客地址</a>
 */
@Data
public class UserGoodsReqDto extends PageBase {
    private Long id;

    private Long goodsId;

    private Long userId;

    private Integer count;

    private Integer status;

    private String goodsName;

    private Date createTime;
}
