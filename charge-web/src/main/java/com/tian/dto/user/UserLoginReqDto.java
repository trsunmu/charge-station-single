package com.tian.dto.user;

import lombok.Data;

import java.io.Serializable;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * @date 2023年10月15日 11:56
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 * <p>
 * 登录请求参数
 */
@Data
public class UserLoginReqDto implements Serializable {
    /**
     * 手机号码
     */
    private String phone;
    /**
     * 验证码
     */
    private String code;
}
