package com.tian.dto.user;

import lombok.Data;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * @date 2023年05月11日 18:44
 * 博客地址：<a href="http://woaijava.cc/">博客地址</a>
 * <p>
 * 用户登录返回信息
 */
@Data
public class UserLoginResDto {
    /**
     * 用户Id
     */
    private Integer userId;
    /**
     * 用户昵称
     */
    private String nickName;
    /**
     * 用户登录成功后的 token
     */
    private String token;
    /**
     * 用户头像
     */
    private String avatarUrl;
    /**
     * 用户邀请码
     */
    private String invitedCode;
    /**
     * 用户性别
     */
    private Integer gender;
}
