package com.tian.dto.user;

import lombok.Data;
import lombok.ToString;

import java.util.List;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * @date 2023年06月09日 20:22
 * 博客地址：<a href="http://woaijava.cc/">博客地址</a>
 */

@ToString
@Data
public class UserGoodsRespDto {

    private List<UserGoodsDto> list;
    private long start;
    private int pageSize;
    private int count;
}
