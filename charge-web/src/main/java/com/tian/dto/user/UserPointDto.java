package com.tian.dto.user;

import com.tian.entity.UserPoint;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * @date 2023年06月05日 16:42
 * 博客地址：<a href="http://woaijava.cc/">博客地址</a>
 * <p>
 * 新增
 */
public class UserPointDto extends UserPoint {
}
