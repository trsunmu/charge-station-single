package com.tian.dto.user;

import com.tian.entity.Coupon;
import com.tian.entity.UserActivityCoupon;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年11月20日 11:19
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 */
public class UserCouponConvert {
    public static List<UserCouponRecordRespDto> convert4Page(List<UserActivityCoupon> userCouponRecordList, Map<Integer, Coupon> couponMap) {
        List<UserCouponRecordRespDto> userCouponRecordRespDtoList = new ArrayList<>();
        for (UserActivityCoupon userActivityCoupon : userCouponRecordList) {

            Coupon coupon=couponMap.get(userActivityCoupon.getCouponId());

            UserCouponRecordRespDto userCouponRecordRespDto = new UserCouponRecordRespDto();
            userCouponRecordRespDto.setCouponId(userActivityCoupon.getCouponId());
            userCouponRecordRespDto.setName(coupon.getName());
            userCouponRecordRespDto.setStatus(userActivityCoupon.getStatus());
            userCouponRecordRespDto.setStartDate(coupon.getStartDate());
            userCouponRecordRespDto.setEndDate(coupon.getEndDate());
            userCouponRecordRespDto.setId(userActivityCoupon.getId());

            userCouponRecordRespDtoList.add(userCouponRecordRespDto);
        }
        return userCouponRecordRespDtoList;
    }
}
