package com.tian.dto.user;

import com.tian.dto.BasePage;
import lombok.Data;

import java.util.List;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * @date 2023年05月15日 19:55
 * 博客地址：<a href="http://woaijava.cc/">博客地址</a>
 */
@Data
public class UserCouponRecordListRespDto extends BasePage {
    private List<UserCouponRecordRespDto> list;
}
