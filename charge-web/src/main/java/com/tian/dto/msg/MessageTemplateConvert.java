package com.tian.dto.msg;

import com.tian.entity.MessageTemplate;

import java.util.List;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年10月31日 21:53
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 */
public class MessageTemplateConvert {
    public static MessageTemplate convert4Add(MessageTemplateAddReqDto messageTemplateAddReqDto) {
        MessageTemplate messageTemplate = new MessageTemplate();
        return messageTemplate;
    }

    public static MessageTemplate convert4Update(MessageTemplateUpdateReqDto messageTemplateUpdateReqDto) {
        MessageTemplate messageTemplate = new MessageTemplate();
        return messageTemplate;
    }

    public static MessageTemplateRespDto convert4SelectById(MessageTemplate messageTemplate) {
        MessageTemplateRespDto messageTemplateRespDto = new MessageTemplateRespDto();
        return messageTemplateRespDto;
    }

    public static MessageTemplatePageRespDto convert4Page(List<MessageTemplate> messageTemplates) {
        MessageTemplatePageRespDto messageTemplatePageRespDto = new MessageTemplatePageRespDto();
        return messageTemplatePageRespDto;
    }
}
