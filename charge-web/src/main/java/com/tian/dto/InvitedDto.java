package com.tian.dto;

import lombok.Data;

import java.util.Date;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * @date 2023年06月06日 10:53
 * 博客地址：<a href="http://woaijava.cc/">博客地址</a>
 */
@Data
public class InvitedDto {

    private String invitedCode;

    private Date createTime;

    private String phone;

    private String code;
    
    private String password;

    private String realName;

    private Date birthday;

    private String avatarUrl;

    private String idNo;

    private String province;

    private String city;

    private String area;

    private String detailAddress;

    private String email;

    private Integer gender;
}
