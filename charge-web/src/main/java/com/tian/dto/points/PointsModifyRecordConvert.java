package com.tian.dto.points;

import com.tian.entity.PointsModifyRecord;

import java.util.List;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年10月31日 21:53
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 */
public class PointsModifyRecordConvert {
    public static PointsModifyRecord convert4Add(PointsModifyRecordAddReqDto pointsModifyRecordAddReqDto) {
        PointsModifyRecord pointsModifyRecord = new PointsModifyRecord();
        return pointsModifyRecord;
    }

    public static PointsModifyRecord convert4Update(PointsModifyRecordUpdateReqDto pointsModifyRecordUpdateReqDto) {
        PointsModifyRecord pointsModifyRecord = new PointsModifyRecord();
        return pointsModifyRecord;
    }

    public static PointsModifyRecordRespDto convert4SelectById(PointsModifyRecord pointsModifyRecord) {
        PointsModifyRecordRespDto pointsModifyRecordRespDto = new PointsModifyRecordRespDto();
        return pointsModifyRecordRespDto;
    }

    public static PointsModifyRecordPageRespDto convert4Page(List<PointsModifyRecord> pointsModifyRecordList) {
        PointsModifyRecordPageRespDto pointsModifyRecordPageRespDto = new PointsModifyRecordPageRespDto();
        return pointsModifyRecordPageRespDto;
    }
}
