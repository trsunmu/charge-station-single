package com.tian.dto;

import lombok.Data;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * @date 2023年05月11日 19:40
 * 博客地址：<a href="http://woaijava.cc/">博客地址</a>
 */
@Data
public class QueryUserReqDto {
    private String phone;
}
