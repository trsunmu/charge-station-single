package com.tian.dto.balance;

import lombok.Data;

import java.util.Date;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * @date 2023年05月12日 20:13
 * 博客地址：<a href="http://woaijava.cc/">博客地址</a>
 */
@Data
public class ChargeBalanceRecordDto {
    private Long id;

    private String orderNo;

    private Long userId;

    private Integer balance;

    private Integer status;

    private Integer payType;

    private Date createTime;

    private static final long serialVersionUID = 1L;
}
