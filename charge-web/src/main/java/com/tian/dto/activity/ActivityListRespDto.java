package com.tian.dto.activity;

import com.tian.entity.Activity;
import lombok.Data;

import java.util.List;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * @date 2023年05月15日 15:26
 * 博客地址：<a href="http://woaijava.cc/">博客地址</a>
 */
@Data
public class ActivityListRespDto extends Activity {
    private List<ActivityRespDto> list;
    private long start;
    private int pageSize;

    public ActivityListRespDto(long start, int pageSize) {
        this.start = start;
        this.pageSize = pageSize;
    }

    public ActivityListRespDto() {
    }
}
