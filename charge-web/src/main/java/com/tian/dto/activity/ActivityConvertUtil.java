package com.tian.dto.activity;

import com.tian.entity.Activity;

import java.util.ArrayList;
import java.util.List;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 博客地址：<a href="http://woaijava.cc/">博客地址</a>
 */
public class ActivityConvertUtil {

    public static Activity convert(ActivityDto activityDto){
        Activity activity=new Activity();
        activity.setStationsId(activityDto.getStationsId());
        activity.setName(activityDto.getName());
        activity.setDescription(activityDto.getDescription());
        activity.setStatus(activityDto.getStatus());
        activity.setEndTime(activityDto.getEndTime());
        activity.setStartTime(activityDto.getStartTime());
        return activity;
    }

    public static List<ActivityRespDto> convert(List<Activity> activityList){
        List<ActivityRespDto> activityRespDtoList=new ArrayList<>();
        for (Activity activity : activityList) {
            ActivityRespDto activityRespDto=new ActivityRespDto();
            activityRespDto.setId(activity.getId());
            activityRespDtoList.add(activityRespDto);
        }
        return activityRespDtoList;
    }
}
