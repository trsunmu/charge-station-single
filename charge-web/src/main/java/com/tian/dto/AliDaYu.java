package com.tian.dto;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 * @author tianwc 公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * @date 2023年06月12日 22:34
 * 博客地址：<a href="http://woaijava.cc/">博客地址</a>
 * 阿里大鱼配置信息
 */
@Data
@Configuration
public class AliDaYu {
    @Value("{accessKeyId}")
    private String accessKeyId;
    @Value("{accessKeyId}")
    private String accessKeySecret;
    @Value("{accessKeyId}")
    private String signName;
}
