package com.tian.dto.pile;

import com.tian.dto.BasePage;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * @date 2023年10月15日 19:51
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 */
@Data
public class StationPileQueryListRespDto  extends BasePage implements Serializable {

    private List<StationPileQueryRespDto> data;
}
