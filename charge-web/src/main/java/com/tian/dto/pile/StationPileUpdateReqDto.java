package com.tian.dto.pile;

import lombok.Data;

import java.io.Serializable;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * @date 2023年10月15日 19:46
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 */
@Data
public class StationPileUpdateReqDto implements Serializable {
    private Integer id;
    private String deviceNo;
    private Integer status;
}
