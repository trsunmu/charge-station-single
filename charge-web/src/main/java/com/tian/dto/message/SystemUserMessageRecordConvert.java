package com.tian.dto.message;

import com.tian.entity.SystemUserMessage;
import com.tian.enums.SystemUserMessageStatusEnum;
import org.springframework.beans.BeanUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年11月01日 10:06
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 */
public class SystemUserMessageRecordConvert {

    public static SystemUserMessage convert4Update(SystemUserMessageRecordUpdateReqDto messageRecordUpdateReqDto) {
        SystemUserMessage systemUserMessage = new SystemUserMessage();
        systemUserMessage.setStatus(messageRecordUpdateReqDto.getStatus());
        systemUserMessage.setId(messageRecordUpdateReqDto.getId());
        return systemUserMessage;
    }

    public static List<SystemUserMessageRespDto> convert4Page(List<SystemUserMessage> systemUserMessageList) {

        List<SystemUserMessageRespDto> systemUserMessageRespDtoList = new ArrayList<>();
        for (SystemUserMessage systemUserMessage : systemUserMessageList) {

            SystemUserMessageRespDto systemUserMessageRespDto = new SystemUserMessageRespDto();
            BeanUtils.copyProperties(systemUserMessage, systemUserMessageRespDto);

            String desc = SystemUserMessageStatusEnum.getSystemUserMessageStatusEnumByStatus(systemUserMessage.getStatus()).getDesc();
            systemUserMessageRespDto.setStatusStr(desc);

            systemUserMessageRespDtoList.add(systemUserMessageRespDto);
        }
        return systemUserMessageRespDtoList;
    }

    public static SystemUserMessageRespDto convert4SelectById(SystemUserMessage systemUserMessage) {
        SystemUserMessageRespDto systemUserMessageRespDto = new SystemUserMessageRespDto();
        BeanUtils.copyProperties(systemUserMessage, systemUserMessageRespDto);
        String desc = SystemUserMessageStatusEnum.getSystemUserMessageStatusEnumByStatus(systemUserMessage.getStatus()).getDesc();
        systemUserMessageRespDto.setStatusStr(desc);
        return systemUserMessageRespDto;
    }
}
