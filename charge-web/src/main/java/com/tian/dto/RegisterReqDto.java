package com.tian.dto;

import lombok.Data;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * @date 2023年05月13日 19:55
 * 博客地址：<a href="http://woaijava.cc/">博客地址</a>
 * <p>
 * 注册
 */
@Data
public class RegisterReqDto {
    private String phone;
    private String openId;
    /**
     * 注册邀请码
     */
    private String code;
}
