package com.tian.dto.station;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年11月07日 15:29
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 */
@Data
public class StationAddReqDto {

    private String name;

    private String stationCode;

    private String province;

    private String city;

    private String area;

    private String address;

    private BigDecimal longitude;

    private BigDecimal latitude;

    private Integer stationType;

    private Integer operatingStatus;
}
