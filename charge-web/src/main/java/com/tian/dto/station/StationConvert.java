package com.tian.dto.station;

import com.tian.entity.Station;
import org.springframework.beans.BeanUtils;

import java.util.List;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年11月07日 15:30
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 * <p>
 * 数据信息转换
 */
public class StationConvert {

    public static Station convert4Add(StationAddReqDto stationAddReqDto) {
        Station station = new Station();
        BeanUtils.copyProperties(stationAddReqDto, station);
        return station;
    }

    public static Station convert4Update(StationUpdateReqDto stationUpdateReqDto) {
        Station station = new Station();
        BeanUtils.copyProperties(stationUpdateReqDto, station);
        return station;
    }

    public static StationRespDto convert4SelectById(Station station) {
        StationRespDto stationRespDto = new StationRespDto();
        return stationRespDto;
    }

    public static StationPageRespDto convert4Page(List<Station> stationList) {
        StationPageRespDto stationPageRespDto = new StationPageRespDto();
        return stationPageRespDto;
    }
}
