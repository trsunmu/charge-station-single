package com.tian.dto.gun;

import lombok.Data;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年11月01日 10:00
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 */
@Data
public class ChargeGunAddReqDto {
    private Integer chargerId;

    private String name;

    private String gunNo;

    private String voltage;

    private String electricity;

    private String power;

    private String description;
}
