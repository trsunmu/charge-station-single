package com.tian.dto.gun;

import com.tian.entity.ChargeGun;
import org.springframework.beans.BeanUtils;

import java.util.List;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年11月01日 10:06
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 */
public class ChargeGunConvert {

    public static ChargeGun convert4Add(ChargeGunAddReqDto chargeGunAddReqDto) {
        ChargeGun chargeGun = new ChargeGun();
        BeanUtils.copyProperties(chargeGunAddReqDto,chargeGun);
        return chargeGun;
    }

    public static ChargeGun convert4Update(ChargeGunUpdateReqDto chargeGunUpdateReqDto) {
        ChargeGun chargeGun = new ChargeGun();

        return chargeGun;
    }

    public static ChargeGunPageRespDto convert4Page(List<ChargeGun> chargeGunList) {
        ChargeGunPageRespDto chargeGunPageRespDto = new ChargeGunPageRespDto();

        return chargeGunPageRespDto;
    }

    public static ChargeGunRespDto convert4SelectById(ChargeGun chargeGun) {
        ChargeGunRespDto chargeGunRespDto = new ChargeGunRespDto();

        return chargeGunRespDto;
    }
}
