package com.tian.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * @date 2023年05月11日 17:16
 * 博客地址：<a href="http://woaijava.cc/">博客地址</a>
 */
@Data
@ApiModel("请求参数")
public class SendCodeReqDto {
    /**
     * 手机号码
     */
    @ApiModelProperty(value = "手机号码")
    private String phone;
    /**
     * 缓存前缀 参照： RedisConstantPre
     */
    @ApiModelProperty(value = "缓存key")
    private String cacheKeyPre;
    /**
     * 模板类型
     */
    @ApiModelProperty(value = "模板id")
    private Integer msgTemplateId;
}
