package com.tian.dto.coupon;

import lombok.Data;

import java.util.List;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * @date 2023年05月15日 17:26
 * 博客地址：<a href="http://woaijava.cc/">博客地址</a>
 */
@Data
public class ActivityCouponListRespDto {
    List<ActivityCouponRespDto> list;

    private long start;
    private int pageSize;

    private long count;

    public ActivityCouponListRespDto() {
    }

    public ActivityCouponListRespDto(long start, int pageSize, long count) {
        this.start = start;
        this.pageSize = pageSize;
        this.count = count;
    }
}
