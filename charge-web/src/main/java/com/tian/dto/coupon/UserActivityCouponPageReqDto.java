package com.tian.dto.coupon;

import com.tian.dto.BasePage;
import lombok.Data;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年11月01日 17:15
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 */
@Data
public class UserActivityCouponPageReqDto extends BasePage {
    private Integer status;
    private Integer userId;
}
