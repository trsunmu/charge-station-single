package com.tian.dto.coupon;

import com.tian.dto.BasePage;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * @date 2023年05月15日 14:32
 * 博客地址：<a href="http://woaijava.cc/">博客地址</a>
 */
@Data
@ApiModel("返回参数")
public class CouponListRespDto extends BasePage {
    @ApiModelProperty(value = "优惠券列表")
    private List<CouponInfoReqDto> list;
}
