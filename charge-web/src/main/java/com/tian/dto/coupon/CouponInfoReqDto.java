package com.tian.dto.coupon;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.tian.dto.BasePage;
import com.tian.entity.Coupon;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * @date 2023年05月15日 14:32
 * 博客地址：<a href="http://woaijava.cc/">博客地址</a>
 */
@Data
@ApiModel("优惠券信息")
public class CouponInfoReqDto extends BasePage {
    @ApiModelProperty(value = "主键id")
    private Integer id;

    @ApiModelProperty(value = "优惠券类型")
    private Integer couponType;

    @ApiModelProperty(value = "优惠券编码")
    private String couponCode;

    @ApiModelProperty(value = "优惠券名称")
    private String name;

    @ApiModelProperty(value = "优惠券面值")
    private BigDecimal couponValue;

    @ApiModelProperty(value = "所需积分")
    private Integer points;

    @ApiModelProperty(value = "状态")
    private Integer status;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    @ApiModelProperty(value = "开始时间")
    private Date startDate;

    private String startDateStr;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    @ApiModelProperty(value = "截止时间")
    private Date endDate;

    private String endDateStr;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    @ApiModelProperty(value = "创建时间")
    private Date createTime;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    @ApiModelProperty(value = "更新时间")
    private Date updateTime;
}
