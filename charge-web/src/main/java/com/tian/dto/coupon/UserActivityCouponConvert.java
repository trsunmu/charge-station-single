package com.tian.dto.coupon;

import com.tian.entity.UserActivityCoupon;

import java.util.List;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年10月31日 21:53
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 */
public class UserActivityCouponConvert {
    public static UserActivityCoupon convert4Add(UserActivityCouponAddReqDto userActivityCouponAddReqDto) {
        UserActivityCoupon userActivityCoupon = new UserActivityCoupon();
        return userActivityCoupon;
    }

    public static UserActivityCoupon convert4Update(UserActivityCouponUpdateReqDto userActivityCouponUpdateReqDto) {
        UserActivityCoupon userActivityCoupon = new UserActivityCoupon();
        return userActivityCoupon;
    }

    public static UserActivityCouponRespDto convert4SelectById(UserActivityCoupon userActivityCoupon) {
        UserActivityCouponRespDto userActivityCouponRespDto = new UserActivityCouponRespDto();
        return userActivityCouponRespDto;
    }

    public static UserActivityCouponPageRespDto convert4Page(List<UserActivityCoupon> userActivityCoupons) {
        UserActivityCouponPageRespDto userActivityCouponPageRespDto = new UserActivityCouponPageRespDto();
        return userActivityCouponPageRespDto;
    }
}
