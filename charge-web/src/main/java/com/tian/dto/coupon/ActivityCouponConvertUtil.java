package com.tian.dto.coupon;

import com.tian.entity.ActivityCoupon;

import java.util.ArrayList;
import java.util.List;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * @date 2023年05月15日 17:43
 * 博客地址：<a href="http://woaijava.cc/">博客地址</a>
 * <p>
 * 活动优惠券数据转换
 */
public class ActivityCouponConvertUtil {

    public static List<ActivityCouponRespDto> convert(List<ActivityCoupon> activityCouponList) {
        List<ActivityCouponRespDto> activityCouponRespDtoList = new ArrayList<>();
        for (ActivityCoupon activityCoupon : activityCouponList) {
            ActivityCouponRespDto activityCouponRespDto = new ActivityCouponRespDto();
            activityCouponRespDto.setId(activityCoupon.getId());
            activityCouponRespDto.setActivityId(activityCoupon.getActivityId());
            activityCouponRespDto.setCouponId(activityCoupon.getCouponId());
            activityCouponRespDto.setCreateTime(activityCoupon.getCreateTime());
            activityCouponRespDto.setRemainCount(activityCoupon.getRemainCount());
            activityCouponRespDto.setLimitPerUser(activityCoupon.getLimitPerUser());
            activityCouponRespDto.setTotalCount(activityCoupon.getTotalCount());
            activityCouponRespDtoList.add(activityCouponRespDto);
        }
        return activityCouponRespDtoList;
    }
}
