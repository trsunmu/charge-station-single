package com.tian.dto.coupon;

import com.tian.entity.ActivityCoupon;
import lombok.Data;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * @date 2023年05月15日 15:35
 * 博客地址：<a href="http://woaijava.cc/">博客地址</a>
 */
@Data
public class ActivityCouponReqDto extends ActivityCoupon {
    private long start;
    private int pageSize;
}
