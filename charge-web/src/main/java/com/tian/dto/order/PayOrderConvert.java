package com.tian.dto.order;

import com.tian.entity.PayOrder;

import java.util.List;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年11月01日 10:06
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 */
public class PayOrderConvert {

    public static PayOrder convert4Add(PayOrderAddReqDto payOrderAddReqDto) {
        PayOrder PayOrder = new PayOrder();

        return PayOrder;
    }

    public static PayOrder convert4Update(PayOrderUpdateReqDto payOrderUpdateReqDto) {
        PayOrder PayOrder = new PayOrder();

        return PayOrder;
    }

    public static PayOrderPageRespDto convert4Page(List<PayOrder> payOrderList) {
        PayOrderPageRespDto payOrderPageRespDto = new PayOrderPageRespDto();
        return payOrderPageRespDto;
    }

    public static PayOrderRespDto convert4SelectById(PayOrder payOrder) {
        PayOrderRespDto payOrderRespDto = new PayOrderRespDto();

        return payOrderRespDto;
    }
}
