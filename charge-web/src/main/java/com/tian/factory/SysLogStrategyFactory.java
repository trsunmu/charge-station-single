package com.tian.factory;

import com.tian.enums.SysLogTypeEnum;
import com.tian.service.impl.DatabaseSysLogServiceImpl;
import com.tian.service.impl.ElasticSearchSysLogServiceImpl;
import com.tian.service.impl.FileSysLogServiceImpl;
import com.tian.service.SysLogService;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 博客地址：<a href="http://woaijava.cc/">博客地址</a>
 */
@Component
public class SysLogStrategyFactory {
    private static Map<Integer, Class> STRATEGY_MAP = new HashMap<>();
    @Resource
    private ApplicationContext applicationContext;
    public SysLogService getInvokeStrategy(int type) {
        return (SysLogService) applicationContext.getBean(STRATEGY_MAP.get(type));
    }
    static {
        STRATEGY_MAP.put(SysLogTypeEnum.FILE.getType(), FileSysLogServiceImpl.class);
        STRATEGY_MAP.put(SysLogTypeEnum.ES.getType(), ElasticSearchSysLogServiceImpl.class);
        STRATEGY_MAP.put(SysLogTypeEnum.DB.getType(), DatabaseSysLogServiceImpl.class);
    }
}
