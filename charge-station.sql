/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 80200
Source Host           : localhost:3306
Source Database       : charge-station

Target Server Type    : MYSQL
Target Server Version : 80200
File Encoding         : 65001

Date: 2024-01-08 10:01:30
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `activity`
-- ----------------------------
DROP TABLE IF EXISTS `activity`;
CREATE TABLE `activity` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '活动ID',
  `stations_id` bigint DEFAULT NULL COMMENT '站点id',
  `name` varchar(50) NOT NULL COMMENT '活动名称',
  `start_time` datetime NOT NULL COMMENT '活动开始时间',
  `end_time` datetime NOT NULL COMMENT '活动结束时间',
  `description` varchar(200) DEFAULT NULL COMMENT '活动描述',
  `status` int NOT NULL DEFAULT '1' COMMENT '活动状态，1表示进行中，0表示已结束',
  `create_time` datetime DEFAULT NULL,
  `type` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='活动表';

-- ----------------------------
-- Records of activity
-- ----------------------------

-- ----------------------------
-- Table structure for `activity_coupon`
-- ----------------------------
DROP TABLE IF EXISTS `activity_coupon`;
CREATE TABLE `activity_coupon` (
  `id` int NOT NULL AUTO_INCREMENT,
  `coupon_id` int DEFAULT NULL,
  `activity_id` int DEFAULT NULL,
  `limit_per_user` int DEFAULT NULL,
  `total_count` int DEFAULT NULL,
  `remain_count` int DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of activity_coupon
-- ----------------------------

-- ----------------------------
-- Table structure for `activity_goods`
-- ----------------------------
DROP TABLE IF EXISTS `activity_goods`;
CREATE TABLE `activity_goods` (
  `id` int NOT NULL AUTO_INCREMENT,
  `activity_id` int DEFAULT NULL,
  `goods_id` int DEFAULT NULL,
  `points` int DEFAULT NULL,
  `money` decimal(10,2) DEFAULT NULL,
  `type` int DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of activity_goods
-- ----------------------------

-- ----------------------------
-- Table structure for `car_info`
-- ----------------------------
DROP TABLE IF EXISTS `car_info`;
CREATE TABLE `car_info` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int DEFAULT NULL,
  `car_brand` varchar(255) DEFAULT NULL,
  `car_type` varchar(255) DEFAULT NULL,
  `frame_number` varchar(255) DEFAULT NULL,
  `engine_number` varchar(255) DEFAULT NULL,
  `plate_number` varchar(255) DEFAULT NULL,
  `car_identify_status` int DEFAULT NULL,
  `driving_license_url` varchar(255) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of car_info
-- ----------------------------
INSERT INTO `car_info` VALUES ('2', '6', '粤A503X2', '中大型', '235239', '235239', '235239', '0', 'http://www.dddd.ddddd/dd..', '2023-11-03 15:48:37');

-- ----------------------------
-- Table structure for `charger`
-- ----------------------------
DROP TABLE IF EXISTS `charger`;
CREATE TABLE `charger` (
  `id` int NOT NULL AUTO_INCREMENT,
  `station_id` int DEFAULT NULL,
  `charger_code` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `charger_type` int DEFAULT NULL,
  `status` int DEFAULT NULL,
  `park_no` varchar(255) DEFAULT NULL,
  `manufacturer` varchar(255) DEFAULT NULL,
  `voltage` varchar(255) DEFAULT NULL,
  `charger_model` varchar(255) DEFAULT NULL,
  `electricity_price_id` int DEFAULT NULL,
  `power` varchar(255) DEFAULT NULL,
  `support_cars` varchar(255) DEFAULT NULL,
  `price` decimal(10,2) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `qrcode` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of charger
-- ----------------------------
INSERT INTO `charger` VALUES ('1', '1', 'HDAHUI111', 'FFFFFFF33333', '0', '0', '235239374342343232', '中国xx电桩有限公司', '220', '11111', '1', '150', 'IX3', '1.50', '舍也不说', 'http://www.dddd.ddddd/dd..');

-- ----------------------------
-- Table structure for `charger_record`
-- ----------------------------
DROP TABLE IF EXISTS `charger_record`;
CREATE TABLE `charger_record` (
  `id` int NOT NULL AUTO_INCREMENT,
  `charger_gun_id` int DEFAULT NULL,
  `user_id` int DEFAULT NULL,
  `start_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `quantity` decimal(10,2) DEFAULT NULL,
  `price` decimal(10,2) DEFAULT NULL,
  `money` decimal(10,2) DEFAULT NULL,
  `pay_status` int DEFAULT NULL,
  `charge_status` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of charger_record
-- ----------------------------
INSERT INTO `charger_record` VALUES ('1', '3', '6', '2023-11-08 07:32:12', '2023-11-07 16:32:12', '20.55', null, '30.83', '0', '1');

-- ----------------------------
-- Table structure for `charge_gun`
-- ----------------------------
DROP TABLE IF EXISTS `charge_gun`;
CREATE TABLE `charge_gun` (
  `id` int NOT NULL AUTO_INCREMENT,
  `charger_id` int DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `gun_no` varchar(255) DEFAULT NULL,
  `voltage` varchar(255) DEFAULT NULL,
  `electricity` varchar(255) DEFAULT NULL,
  `power` varchar(255) DEFAULT NULL,
  `status` int DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of charge_gun
-- ----------------------------
INSERT INTO `charge_gun` VALUES ('3', '1', '小小充电枪200000001', 'GUN00001', '100', '150', '150', '0', 'ASDAS9');

-- ----------------------------
-- Table structure for `coupon`
-- ----------------------------
DROP TABLE IF EXISTS `coupon`;
CREATE TABLE `coupon` (
  `id` int NOT NULL AUTO_INCREMENT,
  `coupon_type` int DEFAULT NULL,
  `coupon_code` varchar(255) DEFAULT NULL,
  `coupon_value` decimal(10,2) DEFAULT NULL,
  `points` int DEFAULT NULL,
  `status` int DEFAULT NULL,
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of coupon
-- ----------------------------
INSERT INTO `coupon` VALUES ('1', '1', '1111', '1.00', '1', '0', '2023-11-13 19:28:43', '2023-11-14 19:28:49', '2023-11-11 03:54:29', '2023-11-11 03:54:29');
INSERT INTO `coupon` VALUES ('2', '1', '111', '11.00', '1', '0', '2023-11-14 19:28:51', '2023-11-18 19:28:57', '2023-11-11 04:04:08', '2023-11-11 04:04:08');

-- ----------------------------
-- Table structure for `electricity_price`
-- ----------------------------
DROP TABLE IF EXISTS `electricity_price`;
CREATE TABLE `electricity_price` (
  `id` int NOT NULL AUTO_INCREMENT,
  `prcie` decimal(10,2) DEFAULT NULL,
  `unit_type` int DEFAULT NULL,
  `unit_value` decimal(10,2) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `last_update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of electricity_price
-- ----------------------------

-- ----------------------------
-- Table structure for `electricity_price_history`
-- ----------------------------
DROP TABLE IF EXISTS `electricity_price_history`;
CREATE TABLE `electricity_price_history` (
  `id` int NOT NULL AUTO_INCREMENT,
  `prcie` decimal(10,2) DEFAULT NULL,
  `unit_type` int DEFAULT NULL,
  `unit_value` decimal(10,2) DEFAULT NULL,
  `start_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of electricity_price_history
-- ----------------------------

-- ----------------------------
-- Table structure for `exceptional_retroaction`
-- ----------------------------
DROP TABLE IF EXISTS `exceptional_retroaction`;
CREATE TABLE `exceptional_retroaction` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `exceptional_type` int NOT NULL,
  `user_id` bigint NOT NULL,
  `order_no` varchar(255) DEFAULT NULL,
  `device_no` varchar(255) DEFAULT NULL,
  `exception_message` varchar(255) DEFAULT NULL,
  `status` int NOT NULL DEFAULT '0',
  `url1` varchar(255) DEFAULT NULL,
  `url2` varchar(255) DEFAULT NULL,
  `url3` varchar(255) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of exceptional_retroaction
-- ----------------------------

-- ----------------------------
-- Table structure for `exchange_goods_record`
-- ----------------------------
DROP TABLE IF EXISTS `exchange_goods_record`;
CREATE TABLE `exchange_goods_record` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int DEFAULT NULL,
  `activity_goods_id` int DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of exchange_goods_record
-- ----------------------------

-- ----------------------------
-- Table structure for `global_property`
-- ----------------------------
DROP TABLE IF EXISTS `global_property`;
CREATE TABLE `global_property` (
  `id` int NOT NULL AUTO_INCREMENT,
  `busi_type` int DEFAULT NULL COMMENT '业务类型',
  `busi_default` int DEFAULT NULL,
  `busi_desc` varchar(255) DEFAULT NULL,
  `status` int NOT NULL DEFAULT '0',
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of global_property
-- ----------------------------

-- ----------------------------
-- Table structure for `goods_info`
-- ----------------------------
DROP TABLE IF EXISTS `goods_info`;
CREATE TABLE `goods_info` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `stock` int NOT NULL,
  `status` int NOT NULL,
  `create_time` datetime NOT NULL,
  `imag_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `type` int NOT NULL,
  `point` int NOT NULL DEFAULT '0',
  `activity_id` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of goods_info
-- ----------------------------
INSERT INTO `goods_info` VALUES ('1', '福利彩票', '10', '0', '2023-11-09 22:16:24', 'http:///asaasas', '1', '100', '1');

-- ----------------------------
-- Table structure for `investor_detail`
-- ----------------------------
DROP TABLE IF EXISTS `investor_detail`;
CREATE TABLE `investor_detail` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int DEFAULT NULL COMMENT '用户表主键',
  `business_license_url` varchar(255) DEFAULT NULL,
  `contract_url` varchar(255) DEFAULT NULL,
  `id_crad_front_url` varchar(255) DEFAULT NULL,
  `id_crad_reverse_url` varchar(255) DEFAULT NULL,
  `bank_card_no` varchar(255) DEFAULT NULL,
  `bank_account` varchar(255) DEFAULT NULL,
  `open_bank` varchar(255) DEFAULT NULL,
  `status` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='投资人详情';

-- ----------------------------
-- Records of investor_detail
-- ----------------------------
INSERT INTO `investor_detail` VALUES ('1', '6', 'http://www.dddd.ddddd/dd..', 'http://www.dddd.ddddd/dd..', 'http://www.dddd.ddddd/dd..', 'http://www.dddd.ddddd/dd..', '235239374342343232', 'http://www.dddd.ddddd/dd..', '中国银行XXX支行', '0');

-- ----------------------------
-- Table structure for `invited_register_record`
-- ----------------------------
DROP TABLE IF EXISTS `invited_register_record`;
CREATE TABLE `invited_register_record` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int DEFAULT NULL,
  `invitation_user_id` int DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `income` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of invited_register_record
-- ----------------------------

-- ----------------------------
-- Table structure for `message_template`
-- ----------------------------
DROP TABLE IF EXISTS `message_template`;
CREATE TABLE `message_template` (
  `id` int NOT NULL AUTO_INCREMENT,
  `template_id` varchar(255) DEFAULT NULL,
  `status` int DEFAULT NULL,
  `describe` varchar(255) DEFAULT NULL,
  `template` varchar(255) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `manufacturer` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of message_template
-- ----------------------------
INSERT INTO `message_template` VALUES ('1', 'baidu01100001', '1', 'sadsa', '你好！你的验证码:{code}', '2023-11-02 09:26:09', 'baidu');

-- ----------------------------
-- Table structure for `pay_order`
-- ----------------------------
DROP TABLE IF EXISTS `pay_order`;
CREATE TABLE `pay_order` (
  `id` int NOT NULL AUTO_INCREMENT,
  `order_no` varchar(255) DEFAULT NULL,
  `business_type` int DEFAULT NULL,
  `charger_record_id` int DEFAULT NULL,
  `pay_order` varchar(255) DEFAULT NULL,
  `money` decimal(10,2) DEFAULT NULL,
  `status` int DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of pay_order
-- ----------------------------

-- ----------------------------
-- Table structure for `points_modify_record`
-- ----------------------------
DROP TABLE IF EXISTS `points_modify_record`;
CREATE TABLE `points_modify_record` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int DEFAULT NULL,
  `points` int DEFAULT NULL,
  `type` int DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of points_modify_record
-- ----------------------------

-- ----------------------------
-- Table structure for `station`
-- ----------------------------
DROP TABLE IF EXISTS `station`;
CREATE TABLE `station` (
  `id` int NOT NULL AUTO_INCREMENT,
  `investor_id` int DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `station_code` varchar(255) DEFAULT NULL,
  `province` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `area` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `longitude` decimal(19,6) DEFAULT NULL,
  `latitude` decimal(19,6) DEFAULT NULL,
  `station_type` int DEFAULT NULL,
  `operating_status` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of station
-- ----------------------------
INSERT INTO `station` VALUES ('1', '1', '北京路1号', 'HDAHUI111', '广东省', '广州市', '白云区', 'XXXX路1号', '113.276500', '23.156390', '0', '0');

-- ----------------------------
-- Table structure for `t_sys_department`
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_department`;
CREATE TABLE `t_sys_department` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT 'id',
  `parent_id` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '父id',
  `dept_name` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '部门名称',
  `leader` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '部门负责人',
  `phone` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '电话',
  `email` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '邮箱',
  `status` int DEFAULT NULL COMMENT '状态',
  `order_num` int DEFAULT NULL COMMENT '排序',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb3 ROW_FORMAT=DYNAMIC COMMENT='部门表';

-- ----------------------------
-- Records of t_sys_department
-- ----------------------------
INSERT INTO `t_sys_department` VALUES ('1', '0', 'v2', 'v2', '13012345678', 'v2@qq.com', '1', '1');
INSERT INTO `t_sys_department` VALUES ('2', '1', '技术部门', 'x某某', '13012345678', 'v2@qq.com', '1', '2');
INSERT INTO `t_sys_department` VALUES ('3', '1', '人事部门', 'a某某', '13012345678', 'v2@qq.com', '1', '3');
INSERT INTO `t_sys_department` VALUES ('4', '2', '开发一小组', 'b某某', '13012345678', 'v2@qq.com', '1', '4');
INSERT INTO `t_sys_department` VALUES ('5', '3', '销售部门', 'd某某', '13012345678', 'v2@qq.com', '1', '5');
INSERT INTO `t_sys_department` VALUES ('6', '5', '销售一组', 'e某某', '13012345678', 'v2@qq.com', '1', '6');

-- ----------------------------
-- Table structure for `t_sys_dict_type`
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_dict_type`;
CREATE TABLE `t_sys_dict_type` (
  `id` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '主键',
  `dict_name` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT '' COMMENT '字典名称',
  `dict_type` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT '' COMMENT '字典类型',
  `status` char(1) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `dict_type` (`dict_type`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 ROW_FORMAT=DYNAMIC COMMENT='字典类型表';

-- ----------------------------
-- Records of t_sys_dict_type
-- ----------------------------
INSERT INTO `t_sys_dict_type` VALUES ('340079827459641344', '省份状态', 'sys_province_state', '0', 'admin', '2019-10-04 20:42:39', '', '2019-10-04 20:42:39', '省份状态');
INSERT INTO `t_sys_dict_type` VALUES ('373493952487231488', '拦截器类型', 'sys_inter_url_type', '0', 'admin', '2020-01-05 01:38:28', 'admin', '2020-03-29 23:23:43', '拦截器类型');
INSERT INTO `t_sys_dict_type` VALUES ('563746635880992768', '捐款类型', 'payment_type', '0', 'admin', '2021-06-12 17:34:42', '', '2021-06-12 17:34:42', '');
INSERT INTO `t_sys_dict_type` VALUES ('563747016396640256', '礼物类型', 'gift_type', '0', 'admin', '2021-06-12 17:36:13', '', '2021-06-12 17:36:13', '');
INSERT INTO `t_sys_dict_type` VALUES ('571365854613213184', '是与否', 'yes_or_no', '0', 'admin', '2021-07-03 18:10:45', '', '2021-07-03 18:10:45', '用于select');
INSERT INTO `t_sys_dict_type` VALUES ('6', '通知类型', 'sys_notice_type', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-12-27 14:26:42', '通知类型列表');

-- ----------------------------
-- Table structure for `t_sys_email`
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_email`;
CREATE TABLE `t_sys_email` (
  `id` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NOT NULL COMMENT '主键',
  `receivers_email` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL COMMENT '接收人电子邮件',
  `title` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL COMMENT '邮件标题',
  `content` text CHARACTER SET utf8mb3 COLLATE utf8mb3_bin COMMENT '内容',
  `send_user_id` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL COMMENT '发送人id',
  `send_user_name` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL COMMENT '发送人账号',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin ROW_FORMAT=DYNAMIC COMMENT='电子邮件';

-- ----------------------------
-- Records of t_sys_email
-- ----------------------------
INSERT INTO `t_sys_email` VALUES ('503928650819833856', '251965157@qq.com', '251965157@qq.com', 0x666666666666663C696D67207372633D22687474703A2F2F6C6F63616C686F73743A383038302F64656D6F2F7374617469632F636F6D706F6E656E742F6C617975692F696D616765732F666163652F32322E6769662220616C743D225BE5A794E5B1885D223E, '1', 'admin', '2020-12-29 15:59:23');
INSERT INTO `t_sys_email` VALUES ('503928914918379520', '251965157@qq.com', '251965157@qq.com', 0x737373737373667364667364667364663C696D67207372633D22687474703A2F2F6C6F63616C686F73743A383038302F64656D6F2F7374617469632F636F6D706F6E656E742F6C617975692F696D616765732F666163652F34322E6769662220616C743D225BE68A93E78B825D223E3C696D67207372633D22687474703A2F2F6C6F63616C686F73743A383038302F64656D6F2F7374617469632F636F6D706F6E656E742F6C617975692F696D616765732F666163652F37312E6769662220616C743D225BE89B8BE7B3955D223E, '1', 'admin', '2020-12-29 16:00:26');
INSERT INTO `t_sys_email` VALUES ('595001021625794560', '251965157@qq.com', 'springbootv2测试邮件', 0x3C703EE6B58BE8AF95E6B58BE6B58BE6B58B3C2F703E, '1', 'admin', '2019-06-30 21:21:38');

-- ----------------------------
-- Table structure for `t_sys_file`
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_file`;
CREATE TABLE `t_sys_file` (
  `id` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NOT NULL COMMENT '主键',
  `file_name` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL COMMENT '文件名字',
  `bucket_name` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL COMMENT '桶名',
  `file_size` bigint DEFAULT NULL COMMENT '文件大小',
  `file_suffix` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL COMMENT '后缀',
  `create_user_id` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL COMMENT '创建人id',
  `create_user_name` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL COMMENT '创建人名字',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_user_id` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL COMMENT '修改人',
  `update_user_name` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL COMMENT '修改人名字',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin ROW_FORMAT=DYNAMIC COMMENT='文件信息表';

-- ----------------------------
-- Records of t_sys_file
-- ----------------------------
INSERT INTO `t_sys_file` VALUES ('503885495013609472', '503895116063313920.png', 'v2-cloud', '48831', 'image/png', '1', 'admin', '2020-12-29 13:07:54', '1', 'admin', '2020-12-29 13:46:08');
INSERT INTO `t_sys_file` VALUES ('503885528857448448', '503895039806672896.png', 'v2-cloud', '71460', 'image/png', '1', 'admin', '2020-12-29 13:08:02', '1', 'admin', '2020-12-29 13:45:50');

-- ----------------------------
-- Table structure for `t_sys_inter_url`
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_inter_url`;
CREATE TABLE `t_sys_inter_url` (
  `id` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '主键',
  `inter_name` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '拦截名称',
  `url` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '拦截url',
  `type` int DEFAULT NULL COMMENT '类型',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 ROW_FORMAT=DYNAMIC COMMENT='拦截url表';

-- ----------------------------
-- Records of t_sys_inter_url
-- ----------------------------
INSERT INTO `t_sys_inter_url` VALUES ('411495038321823744', '字典表新增', '/DictDataController/add', '2');
INSERT INTO `t_sys_inter_url` VALUES ('506433268967673856', '字典表修改', '/DictDataController/edit', '2');
INSERT INTO `t_sys_inter_url` VALUES ('506434978159136768', '字典表删除', '/DictDataController/remove', '3');
INSERT INTO `t_sys_inter_url` VALUES ('506435565655298048', '字典表状态修改', '/DictDataController/updateDefault', '4');
INSERT INTO `t_sys_inter_url` VALUES ('506435921147727872', '字典表状态修改2', '/DictDataController/updateEnable', '4');
INSERT INTO `t_sys_inter_url` VALUES ('506436031403397120', '字典表类型新增', '/DictTypeController/add', '2');
INSERT INTO `t_sys_inter_url` VALUES ('506436148680331264', '字典表类型修改', '/DictTypeController/edit', '2');
INSERT INTO `t_sys_inter_url` VALUES ('506436165776314368', '字典表类型删除', '/DictTypeController/remove', '3');
INSERT INTO `t_sys_inter_url` VALUES ('506436180578013184', '字典表类型状态修改', '/DictTypeController/updateEnable', '4');
INSERT INTO `t_sys_inter_url` VALUES ('506436662134444032', '邮件新增', '/EmailController/add', '2');
INSERT INTO `t_sys_inter_url` VALUES ('506436757722632192', '邮件删除', '/EmailController/remove', '3');
INSERT INTO `t_sys_inter_url` VALUES ('506437010966319104', '日志删除', '/LogController/remove', '3');
INSERT INTO `t_sys_inter_url` VALUES ('506437420099702784', 'oss新增', '/oss/bucket/', '2');
INSERT INTO `t_sys_inter_url` VALUES ('506437439112482816', 'oss删除', '/oss/bucket/', '3');
INSERT INTO `t_sys_inter_url` VALUES ('506437964436475904', '权限新增', '/PermissionController/add', '2');
INSERT INTO `t_sys_inter_url` VALUES ('506438040823140352', '权限保存', '/PermissionController/edit', '2');
INSERT INTO `t_sys_inter_url` VALUES ('506438121399914496', '权限删除', '/PermissionController/remove', '3');
INSERT INTO `t_sys_inter_url` VALUES ('506438208599494656', '权限授权', '/PermissionController/saveRolePower', '4');
INSERT INTO `t_sys_inter_url` VALUES ('506438306276446208', '权限状态修改', '/PermissionController/updateVisible', '4');
INSERT INTO `t_sys_inter_url` VALUES ('506438447226032128', '定时器新增', '/SysQuartzJobController/add', '2');
INSERT INTO `t_sys_inter_url` VALUES ('506438589874311168', ' 任务调度状态修改', '/SysQuartzJobController/changeStatus', '4');
INSERT INTO `t_sys_inter_url` VALUES ('506438725388079104', '定时器保存', '/SysQuartzJobController/edit', '2');
INSERT INTO `t_sys_inter_url` VALUES ('506438870959788032', '定时器修改', '/SysQuartzJobController/remove', '3');
INSERT INTO `t_sys_inter_url` VALUES ('506439003516571648', '定时任务日志删除', '/SysQuartzJobLogController/remove', '3');
INSERT INTO `t_sys_inter_url` VALUES ('506439171481669632', '角色新增', '/RoleController/add', '2');
INSERT INTO `t_sys_inter_url` VALUES ('506439186778296320', '角色修改', '/RoleController/edit', '4');
INSERT INTO `t_sys_inter_url` VALUES ('506439297122045952', '角色删除', '/RoleController/remove', '3');
INSERT INTO `t_sys_inter_url` VALUES ('506439669773373440', '地区新增', '/SysAreaController/add', '2');
INSERT INTO `t_sys_inter_url` VALUES ('506439687859212288', '地区修改', '/SysAreaController/edit', '2');
INSERT INTO `t_sys_inter_url` VALUES ('506439835490324480', '地区删除', '/SysAreaController/remove', '3');
INSERT INTO `t_sys_inter_url` VALUES ('506440103976112128', 'City新增', '/SysCityController/add', '2');
INSERT INTO `t_sys_inter_url` VALUES ('506440145147400192', 'City修改', ' /SysCityController/edit', '2');
INSERT INTO `t_sys_inter_url` VALUES ('506440217188765696', 'City删除', '/SysCityController/remove', '3');
INSERT INTO `t_sys_inter_url` VALUES ('506440386873528320', '部门新增', '/SysDepartmentController/add', '2');
INSERT INTO `t_sys_inter_url` VALUES ('506440448223612928', '部门修改', '/SysDepartmentController/edit', '4');
INSERT INTO `t_sys_inter_url` VALUES ('506440515110178816', '部门删除', '/SysDepartmentController/remove', '3');
INSERT INTO `t_sys_inter_url` VALUES ('506440574635741184', '部门状态', '/SysDepartmentController/updateVisible', '4');
INSERT INTO `t_sys_inter_url` VALUES ('506440668508459008', '拦截器url新增', '/SysInterUrlController/add', '2');
INSERT INTO `t_sys_inter_url` VALUES ('506440708056551424', '拦截器url修改', '/SysInterUrlController/edit', '2');
INSERT INTO `t_sys_inter_url` VALUES ('506440802856210432', '拦截器url删除', '/SysInterUrlController/remove', '3');
INSERT INTO `t_sys_inter_url` VALUES ('506441001783660544', '公告新增', '/SysNoticeController/add', '2');
INSERT INTO `t_sys_inter_url` VALUES ('506441051263864832', '公告修改', '/SysNoticeController/edit', '2');
INSERT INTO `t_sys_inter_url` VALUES ('506441105743679488', '公告删除', '/SysNoticeController/remove', '3');
INSERT INTO `t_sys_inter_url` VALUES ('506441242591236096', '职位新增', '/SysPositionController/add', '2');
INSERT INTO `t_sys_inter_url` VALUES ('506441287038275584', '职位修改', '/SysPositionController/edit', '2');
INSERT INTO `t_sys_inter_url` VALUES ('506441350200299520', '职位删除', '/SysPositionController/remove', '3');
INSERT INTO `t_sys_inter_url` VALUES ('506441420677189632', '职位状态修改', '/SysPositionController/updateVisible', '4');
INSERT INTO `t_sys_inter_url` VALUES ('506441780003213312', '省份新增', '/SysProvinceController/add', '2');
INSERT INTO `t_sys_inter_url` VALUES ('506441807383629824', '省份修改', '/SysProvinceController/edit', '2');
INSERT INTO `t_sys_inter_url` VALUES ('506441871850082304', '省份删除', '/SysProvinceController/remove', '3');
INSERT INTO `t_sys_inter_url` VALUES ('506441980012793856', '街道新增', '/SysStreetController/add', '2');
INSERT INTO `t_sys_inter_url` VALUES ('506442015706320896', '街道修改', '/SysStreetController/edit', '2');
INSERT INTO `t_sys_inter_url` VALUES ('506442092445306880', '街道删除', '/SysStreetController/remove', '3');
INSERT INTO `t_sys_inter_url` VALUES ('506442186552905728', '用户新增', '/UserController/add', '2');
INSERT INTO `t_sys_inter_url` VALUES ('506442212696002560', '用户修改', '/UserController/edit', '2');
INSERT INTO `t_sys_inter_url` VALUES ('506442271252680704', '用户修改密码', '/UserController/editPwd', '2');
INSERT INTO `t_sys_inter_url` VALUES ('506442344443285504', '用户删除', '/UserController/remove', '3');
INSERT INTO `t_sys_inter_url` VALUES ('506444610625736704', '拦截器url复制', '/SysInterUrlController/copy/', '3');

-- ----------------------------
-- Table structure for `t_sys_notice`
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_notice`;
CREATE TABLE `t_sys_notice` (
  `id` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NOT NULL COMMENT '主键',
  `title` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '标题',
  `content` varchar(1000) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL COMMENT '内容',
  `type` int DEFAULT NULL COMMENT '类型',
  `create_id` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '创建人id',
  `create_username` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL COMMENT '创建人name',
  `create_time` datetime DEFAULT NULL COMMENT '发信时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 ROW_FORMAT=DYNAMIC COMMENT='公告';

-- ----------------------------
-- Records of t_sys_notice
-- ----------------------------
INSERT INTO `t_sys_notice` VALUES ('330381411007729664', '测试公告', '<p>啊啊啊<img src=\"http://img.baidu.com/hi/jx2/j_0002.gif\"/><img src=\"http://img.baidu.com/hi/jx2/j_0024.gif\"/></p>', '1', '1', 'admin', '2019-09-08 02:24:37');
INSERT INTO `t_sys_notice` VALUES ('330381806358630400', '鲜花视频', '<p>哈哈哈哈<img src=\"http://img.baidu.com/hi/jx2/j_0024.gif\"/></p>', '2', '1', 'admin', '2019-09-08 02:26:11');
INSERT INTO `t_sys_notice` VALUES ('373478036928073728', '顶顶顶顶顶顶顶顶顶', '<p>顶顶顶顶顶顶顶顶顶顶<img src=\"http://img.baidu.com/hi/jx2/j_0014.gif\"/></p>', '1', '1', 'admin', '2020-01-05 00:35:13');
INSERT INTO `t_sys_notice` VALUES ('884296401960439808', '项目里程碑', '项目第一版搞一个段落，后面就开始修复bug了', '2', '1', 'admin', '2023-11-14 06:45:02');

-- ----------------------------
-- Table structure for `t_sys_notice_user`
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_notice_user`;
CREATE TABLE `t_sys_notice_user` (
  `id` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '主键',
  `notice_id` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '公告id',
  `user_id` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '用户id',
  `state` int DEFAULT NULL COMMENT '0未阅读 1 阅读',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 ROW_FORMAT=DYNAMIC COMMENT='公告_用户外键';

-- ----------------------------
-- Records of t_sys_notice_user
-- ----------------------------
INSERT INTO `t_sys_notice_user` VALUES ('330381411037089792', '330381411007729664', '1', '1');
INSERT INTO `t_sys_notice_user` VALUES ('330381411045478400', '330381411007729664', '488294747442511872', '0');
INSERT INTO `t_sys_notice_user` VALUES ('330381806375407616', '330381806358630400', '1', '1');
INSERT INTO `t_sys_notice_user` VALUES ('330381806379601920', '330381806358630400', '488294747442511872', '0');
INSERT INTO `t_sys_notice_user` VALUES ('330622143622680576', '330622143597514752', '1', '1');
INSERT INTO `t_sys_notice_user` VALUES ('330622143626874880', '330622143597514752', '488294747442511872', '0');
INSERT INTO `t_sys_notice_user` VALUES ('354984345649418240', '354984345632641024', '1', '1');
INSERT INTO `t_sys_notice_user` VALUES ('373478037158760448', '373478036928073728', '1', '1');
INSERT INTO `t_sys_notice_user` VALUES ('373478037162954752', '373478036928073728', '368026921239449600', '0');
INSERT INTO `t_sys_notice_user` VALUES ('373478037171343360', '373478036928073728', '368026937181999104', '0');
INSERT INTO `t_sys_notice_user` VALUES ('373478037175537664', '373478036928073728', '368027013392502784', '0');
INSERT INTO `t_sys_notice_user` VALUES ('373478037183926272', '373478036928073728', '368027030899527680', '0');
INSERT INTO `t_sys_notice_user` VALUES ('373478037192314880', '373478036928073728', '368027048402358272', '0');
INSERT INTO `t_sys_notice_user` VALUES ('373478037204897792', '373478036928073728', '368027066563694592', '0');
INSERT INTO `t_sys_notice_user` VALUES ('373478037213286400', '373478036928073728', '368027087866564608', '0');
INSERT INTO `t_sys_notice_user` VALUES ('373478037217480704', '373478036928073728', '368027104895438848', '0');
INSERT INTO `t_sys_notice_user` VALUES ('373478037225869312', '373478036928073728', '368027130728157184', '0');
INSERT INTO `t_sys_notice_user` VALUES ('373478037230063616', '373478036928073728', '368027151624179712', '0');
INSERT INTO `t_sys_notice_user` VALUES ('373478037238452224', '373478036928073728', '368382463233363968', '0');
INSERT INTO `t_sys_notice_user` VALUES ('502750147499921408', '502750147395063808', '1', '0');
INSERT INTO `t_sys_notice_user` VALUES ('502750147508310016', '502750147395063808', '433236479427350528', '0');
INSERT INTO `t_sys_notice_user` VALUES ('502758207983325184', '502758207907827712', '1', '0');
INSERT INTO `t_sys_notice_user` VALUES ('502758207991713792', '502758207907827712', '433236479427350528', '0');
INSERT INTO `t_sys_notice_user` VALUES ('502820822130495488', '502820822042415104', '1', '0');
INSERT INTO `t_sys_notice_user` VALUES ('502820822138884096', '502820822042415104', '433236479427350528', '0');
INSERT INTO `t_sys_notice_user` VALUES ('884296402069491712', '884296401960439808', '1', '1');
INSERT INTO `t_sys_notice_user` VALUES ('884296402086268928', '884296401960439808', '433236479427350528', '0');
INSERT INTO `t_sys_notice_user` VALUES ('884296402098851840', '884296401960439808', '882517373159084032', '0');
INSERT INTO `t_sys_notice_user` VALUES ('884296402107240448', '884296401960439808', '884233123427127296', '0');
INSERT INTO `t_sys_notice_user` VALUES ('884296402115629056', '884296401960439808', '884292169618296832', '0');

-- ----------------------------
-- Table structure for `t_sys_oper_log`
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_oper_log`;
CREATE TABLE `t_sys_oper_log` (
  `id` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NOT NULL,
  `title` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL COMMENT '标题',
  `method` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL COMMENT '方法',
  `oper_name` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL COMMENT '操作人',
  `oper_url` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL COMMENT 'url',
  `oper_param` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL COMMENT '参数',
  `error_msg` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL,
  `oper_time` date DEFAULT NULL COMMENT '操作时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin ROW_FORMAT=DYNAMIC COMMENT='日志记录表';

-- ----------------------------
-- Records of t_sys_oper_log
-- ----------------------------
INSERT INTO `t_sys_oper_log` VALUES ('884233124144353280', '用户新增', 'com.tian.controller.admin.UserController.add()', 'admin', '/UserController/add', '{\"username\":[\"qqq\"],\"password\":[\"123456\"],\"nickname\":[\"运营人1\"],\"depId\":[\"1\"],\"selectParent_select_input\":[\"v2\"],\"posId\":[\"411477874382606336\"],\"roleIds\":[\"882910089013497856\"]}', null, '2023-11-14');
INSERT INTO `t_sys_oper_log` VALUES ('884292170176139264', '用户新增', 'com.tian.controller.admin.UserController.add()', 'admin', '/UserController/add', '{\"username\":[\"zhangmei\"],\"password\":[\"123456\"],\"nickname\":[\"张美\"],\"depId\":[\"5\"],\"selectParent_select_input\":[\"销售部门\"],\"posId\":[\"410792368778907648\"],\"roleIds\":[\"488305788310257664,882910089013497856\"]}', null, '2023-11-14');

-- ----------------------------
-- Table structure for `t_sys_permission`
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_permission`;
CREATE TABLE `t_sys_permission` (
  `id` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT 'id',
  `name` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '权限名称',
  `descripion` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '权限描述',
  `url` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '授权链接',
  `is_blank` int DEFAULT '0' COMMENT '是否跳转 0 不跳转 1跳转',
  `pid` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '父节点id',
  `perms` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '权限标识',
  `type` int DEFAULT NULL COMMENT '类型   0：目录   1：菜单   2：按钮',
  `icon` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '菜单图标',
  `order_num` int DEFAULT NULL COMMENT '排序',
  `visible` int DEFAULT NULL COMMENT '是否可见',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 ROW_FORMAT=DYNAMIC COMMENT='权限表';

-- ----------------------------
-- Records of t_sys_permission
-- ----------------------------
INSERT INTO `t_sys_permission` VALUES ('10', '角色集合', '角色集合', '/RoleController/list', '0', '9', 'system:role:list', '2', '', null, '0');
INSERT INTO `t_sys_permission` VALUES ('11', '角色添加', '角色添加', '/RoleController/add', '0', '9', 'system:role:add', '2', 'entypo-plus-squared', null, '0');
INSERT INTO `t_sys_permission` VALUES ('12', '角色删除', '角色删除', '/RoleController/remove', '0', '9', 'system:role:remove', '2', 'entypo-trash', null, '0');
INSERT INTO `t_sys_permission` VALUES ('13', '角色修改', '角色修改', '/RoleController/edit', '0', '9', 'system:role:edit', '2', 'fa fa-wrench', null, '0');
INSERT INTO `t_sys_permission` VALUES ('14', '权限展示', '权限展示', '/PermissionController/view', '0', '411522822607867904', 'system:permission:view', '1', 'fa fa-key', '3', '0');
INSERT INTO `t_sys_permission` VALUES ('15', '权限集合', '权限集合', '/PermissionController/list', '0', '14', 'system:permission:list', '2', '', null, '0');
INSERT INTO `t_sys_permission` VALUES ('16', '权限添加', '权限添加', '/permissionController/add', '0', '14', 'system:permission:add', '2', 'entypo-plus-squared', null, '0');
INSERT INTO `t_sys_permission` VALUES ('17', '权限删除', '权限删除', '/PermissionController/remove', '0', '14', 'system:permission:remove', '2', 'entypo-trash', null, '0');
INSERT INTO `t_sys_permission` VALUES ('18', '权限修改', '权限修改', '/PermissionController/edit', '0', '14', 'system:permission:edit', '2', 'fa fa-wrench', null, '0');
INSERT INTO `t_sys_permission` VALUES ('19', '文件管理', '文件管理', '/FileController/view', '0', '592059865673760768', 'system:file:view', '1', 'fa fa-file-image-o', '4', '0');
INSERT INTO `t_sys_permission` VALUES ('20', '文件添加', '文件添加', '/FileController/add', '0', '19', 'system:file:add', '2', 'entypo-plus-squared', null, '0');
INSERT INTO `t_sys_permission` VALUES ('21', '文件删除', '文件删除', '/FileController/remove', '0', '19', 'system:file:remove', '2', 'entypo-trash', null, '0');
INSERT INTO `t_sys_permission` VALUES ('22', '文件修改', '文件修改', '/FileController/edit', '0', '19', 'system:file:edit', '2', 'fa fa-wrench', null, '0');
INSERT INTO `t_sys_permission` VALUES ('23', '文件集合', '文件集合', '/FileController/list', '0', '19', 'system:file:list', '2', '', null, '0');
INSERT INTO `t_sys_permission` VALUES ('330365026642825216', '公告管理', '公告展示', '/SysNoticeController/view', '0', '592059865673760768', 'gen:sysNotice:view', '1', 'fa fa-telegram', '10', '0');
INSERT INTO `t_sys_permission` VALUES ('3303650266428252171', '公告集合', '公告集合', '/SysNoticeController/list', '0', '330365026642825216', 'gen:sysNotice:list', '2', '', null, '0');
INSERT INTO `t_sys_permission` VALUES ('3303650266428252182', '公告添加', '公告添加', '/SysNoticeController/add', '0', '330365026642825216', 'gen:sysNotice:add', '2', 'entypo-plus-squared', null, '0');
INSERT INTO `t_sys_permission` VALUES ('3303650266428252193', '公告删除', '公告删除', '/SysNoticeController/remove', '0', '330365026642825216', 'gen:sysNotice:remove', '2', 'entypo-trash', null, '0');
INSERT INTO `t_sys_permission` VALUES ('3303650266428252204', '公告修改', '公告修改', '/SysNoticeController/edit', '0', '330365026642825216', 'gen:sysNotice:edit', '2', 'fa fa-wrench', null, '0');
INSERT INTO `t_sys_permission` VALUES ('331778807298134016', '定时器表达式', null, 'https://www.bejson.com/othertools/cron/', '1', '617766548966211584', '#', '1', 'layui-icon fa fa-flash', '12', '0');
INSERT INTO `t_sys_permission` VALUES ('332157860920299520', '定时任务', '定时任务调度表展示', '/SysQuartzJobController/view', '0', '592059865673760768', 'gen:sysQuartzJob:view', '1', 'fa fa-hourglass-1', '13', '0');
INSERT INTO `t_sys_permission` VALUES ('3321578609202995211', '定时任务调度表集合', '定时任务调度表集合', '/SysQuartzJobController/list', '0', '332157860920299520', 'gen:sysQuartzJob:list', '2', '', null, '0');
INSERT INTO `t_sys_permission` VALUES ('3321578609202995222', '定时任务调度表添加', '定时任务调度表添加', '/SysQuartzJobController/add', '0', '332157860920299520', 'gen:sysQuartzJob:add', '2', 'entypo-plus-squared', null, '0');
INSERT INTO `t_sys_permission` VALUES ('3321578609202995233', '定时任务调度表删除', '定时任务调度表删除', '/SysQuartzJobController/remove', '0', '332157860920299520', 'gen:sysQuartzJob:remove', '2', 'entypo-trash', null, '0');
INSERT INTO `t_sys_permission` VALUES ('3321578609202995244', '定时任务调度表修改', '定时任务调度表修改', '/SysQuartzJobController/edit', '0', '332157860920299520', 'gen:sysQuartzJob:edit', '2', 'fa fa-wrench', null, '0');
INSERT INTO `t_sys_permission` VALUES ('332857281479839744', '定时任务日志', '定时任务日志', '/SysQuartzJobLogController/view', '0', '592059865673760768', 'gen:sysQuartzJobLog:view', '1', 'fa fa-database', '14', '0');
INSERT INTO `t_sys_permission` VALUES ('3328572814798397451', '定时任务调度日志表集合', '定时任务调度日志表集合', '/SysQuartzJobLogController/list', '0', '332857281479839744', 'gen:sysQuartzJobLog:list', '2', '', null, '0');
INSERT INTO `t_sys_permission` VALUES ('3328572814798397473', '定时任务调度日志表删除', '定时任务调度日志表删除', '/SysQuartzJobLogController/remove', '0', '332857281479839744', 'gen:sysQuartzJobLog:remove', '2', 'entypo-trash', null, '0');
INSERT INTO `t_sys_permission` VALUES ('335330315113467904', 'Json工具', null, 'https://www.bejson.com/jsonviewernew/', '1', '617766548966211584', '#', '1', 'layui-icon fa fa-retweet', '10', '0');
INSERT INTO `t_sys_permission` VALUES ('340067579836108800', '省份管理', null, '', '0', '0', '', '0', 'layui-icon layui-icon layui-icon-website', '4', '1');
INSERT INTO `t_sys_permission` VALUES ('340068151804956672', '省份表管理', '省份表展示', '/SysProvinceController/view', '0', '340067579836108800', 'gen:sysProvince:view', '1', 'fa fa-quora', '2', '0');
INSERT INTO `t_sys_permission` VALUES ('3400681518049566731', '省份表集合', '省份表集合', '/SysProvinceController/list', '0', '340068151804956672', 'gen:sysProvince:list', '2', '', null, '0');
INSERT INTO `t_sys_permission` VALUES ('3400681518049566742', '省份表添加', '省份表添加', '/SysProvinceController/add', '0', '340068151804956672', 'gen:sysProvince:add', '2', 'entypo-plus-squared', null, '0');
INSERT INTO `t_sys_permission` VALUES ('3400681518049566753', '省份表删除', '省份表删除', '/SysProvinceController/remove', '0', '340068151804956672', 'gen:sysProvince:remove', '2', 'entypo-trash', null, '0');
INSERT INTO `t_sys_permission` VALUES ('3400681518049566764', '省份表修改', '省份表修改', '/SysProvinceController/edit', '0', '340068151804956672', 'gen:sysProvince:edit', '2', 'fa fa-wrench', null, '0');
INSERT INTO `t_sys_permission` VALUES ('340088022018166784', '城市表管理', '城市设置展示', '/SysCityController/view', '0', '340067579836108800', 'gen:sysCity:view', '1', 'fa fa-quora', '3', '0');
INSERT INTO `t_sys_permission` VALUES ('3400880220181667851', '城市设置集合', '城市设置集合', '/SysCityController/list', '0', '340088022018166784', 'gen:sysCity:list', '2', '', null, '0');
INSERT INTO `t_sys_permission` VALUES ('3400880220181667862', '城市设置添加', '城市设置添加', '/SysCityController/add', '0', '340088022018166784', 'gen:sysCity:add', '2', 'entypo-plus-squared', null, '0');
INSERT INTO `t_sys_permission` VALUES ('3400880220181667873', '城市设置删除', '城市设置删除', '/SysCityController/remove', '0', '340088022018166784', 'gen:sysCity:remove', '2', 'entypo-trash', null, '0');
INSERT INTO `t_sys_permission` VALUES ('3400880220181667884', '城市设置修改', '城市设置修改', '/SysCityController/edit', '0', '340088022018166784', 'gen:sysCity:edit', '2', 'fa fa-wrench', null, '0');
INSERT INTO `t_sys_permission` VALUES ('340096183135506432', '地区设置管理', '地区设置展示', '/SysAreaController/view', '0', '340067579836108800', 'gen:sysArea:view', '1', 'fa fa-quora', '4', '0');
INSERT INTO `t_sys_permission` VALUES ('3400961831355064331', '地区设置集合', '地区设置集合', '/SysAreaController/list', '0', '340096183135506432', 'gen:sysArea:list', '2', '', null, '0');
INSERT INTO `t_sys_permission` VALUES ('3400961831355064342', '地区设置添加', '地区设置添加', '/SysAreaController/add', '0', '340096183135506432', 'gen:sysArea:add', '2', 'entypo-plus-squared', null, '0');
INSERT INTO `t_sys_permission` VALUES ('3400961831355064353', '地区设置删除', '地区设置删除', '/SysAreaController/remove', '0', '340096183135506432', 'gen:sysArea:remove', '2', 'entypo-trash', null, '0');
INSERT INTO `t_sys_permission` VALUES ('3400961831355064364', '地区设置修改', '地区设置修改', '/SysAreaController/edit', '0', '340096183135506432', 'gen:sysArea:edit', '2', 'fa fa-wrench', null, '0');
INSERT INTO `t_sys_permission` VALUES ('340127412270534656', '街道设置管理', '街道设置展示', '/SysStreetController/view', '0', '340067579836108800', 'gen:sysStreet:view', '1', 'fa fa-quora', '5', '0');
INSERT INTO `t_sys_permission` VALUES ('3401274122705346571', '街道设置集合', '街道设置集合', '/SysStreetController/list', '0', '340127412270534656', 'gen:sysStreet:list', '2', '', null, '0');
INSERT INTO `t_sys_permission` VALUES ('3401274122705346582', '街道设置添加', '街道设置添加', '/SysStreetController/add', '0', '340127412270534656', 'gen:sysStreet:add', '2', 'entypo-plus-squared', null, '0');
INSERT INTO `t_sys_permission` VALUES ('3401274122705346593', '街道设置删除', '街道设置删除', '/SysStreetController/remove', '0', '340127412270534656', 'gen:sysStreet:remove', '2', 'entypo-trash', null, '0');
INSERT INTO `t_sys_permission` VALUES ('3401274122705346604', '街道设置修改', '街道设置修改', '/SysStreetController/edit', '0', '340127412270534656', 'gen:sysStreet:edit', '2', 'fa fa-wrench', null, '0');
INSERT INTO `t_sys_permission` VALUES ('340301160042860544', '省份联动', '省份联动', '/ProvinceLinkageController/view', '0', '340067579836108800', '#', '1', 'fa fa-etsy', '1', '0');
INSERT INTO `t_sys_permission` VALUES ('340381240911859712', 'JavaScript格式化', null, '/static/tool/htmlformat/javascriptFormat.html', '0', '617766548966211584', '#', '1', 'layui-icon layui-icon fa fa-magic', '11', '0');
INSERT INTO `t_sys_permission` VALUES ('373489907429150720', 'URL拦截管理', '拦截url表展示', '/SysInterUrlController/view', '0', '617766548966211584', 'gen:sysInterUrl:view', '1', 'fa fa-hand-stop-o', '16', '0');
INSERT INTO `t_sys_permission` VALUES ('3734899074291507211', '拦截url表集合', '拦截url表集合', '/SysInterUrlController/list', '0', '373489907429150720', 'gen:sysInterUrl:list', '2', '', null, '0');
INSERT INTO `t_sys_permission` VALUES ('3734899074291507222', '拦截url表添加', '拦截url表添加', '/SysInterUrlController/add', '0', '373489907429150720', 'gen:sysInterUrl:add', '2', 'entypo-plus-squared', null, '0');
INSERT INTO `t_sys_permission` VALUES ('3734899074291507233', '拦截url表删除', '拦截url表删除', '/SysInterUrlController/remove', '0', '373489907429150720', 'gen:sysInterUrl:remove', '2', 'entypo-trash', null, '0');
INSERT INTO `t_sys_permission` VALUES ('3734899074291507244', '拦截url表修改', '拦截url表修改', '/SysInterUrlController/edit', '0', '373489907429150720', 'gen:sysInterUrl:edit', '2', 'fa fa-wrench', null, '0');
INSERT INTO `t_sys_permission` VALUES ('4', '运营人员管理', null, '/UserController/view', '0', '411522822607867904', 'system:userMessage:view', '1', 'layui-icon icon icon-userMessage', '1', '0');
INSERT INTO `t_sys_permission` VALUES ('410791701859405824', '岗位管理', '岗位展示', '/SysPositionController/view', '0', '411522822607867904', 'gen:sysPosition:view', '1', 'fa fa-vcard', '17', '0');
INSERT INTO `t_sys_permission` VALUES ('4107917018594058251', '岗位表集合', '岗位集合', '/SysPositionController/list', '0', '410791701859405824', 'gen:sysPosition:list', '2', '', null, '0');
INSERT INTO `t_sys_permission` VALUES ('4107917018594058262', '岗位表添加', '岗位添加', '/SysPositionController/add', '0', '410791701859405824', 'gen:sysPosition:add', '2', 'entypo-plus-squared', null, '0');
INSERT INTO `t_sys_permission` VALUES ('4107917018594058273', '岗位表删除', '岗位删除', '/SysPositionController/remove', '0', '410791701859405824', 'gen:sysPosition:remove', '2', 'entypo-trash', null, '0');
INSERT INTO `t_sys_permission` VALUES ('4107917018594058284', '岗位表修改', '岗位修改', '/SysPositionController/edit', '0', '410791701859405824', 'gen:sysPosition:edit', '2', 'fa fa-wrench', null, '0');
INSERT INTO `t_sys_permission` VALUES ('410989805699207168', '部门管理', '部门展示', '/SysDepartmentController/view', '0', '411522822607867904', 'gen:sysDepartment:view', '1', 'fa fa-odnoklassniki', '18', '0');
INSERT INTO `t_sys_permission` VALUES ('4109898056992071691', '部门集合', '部门集合', '/SysDepartmentController/list', '0', '410989805699207168', 'gen:sysDepartment:list', '2', '', null, '0');
INSERT INTO `t_sys_permission` VALUES ('4109898056992071702', '部门添加', '部门添加', '/SysDepartmentController/add', '0', '410989805699207168', 'gen:sysDepartment:add', '2', 'entypo-plus-squared', null, '0');
INSERT INTO `t_sys_permission` VALUES ('4109898056992071713', '部门删除', '部门删除', '/SysDepartmentController/remove', '0', '410989805699207168', 'gen:sysDepartment:remove', '2', 'entypo-trash', null, '0');
INSERT INTO `t_sys_permission` VALUES ('4109898056992071724', '部门修改', '部门修改', '/SysDepartmentController/edit', '0', '410989805699207168', 'gen:sysDepartment:edit', '2', 'fa fa-wrench', null, '0');
INSERT INTO `t_sys_permission` VALUES ('411522822607867904', '系统设置', null, '', '0', '0', '', '0', 'layui-icon layui-icon layui-icon layui-icon-userMessage', '3', '0');
INSERT INTO `t_sys_permission` VALUES ('486690002869157888', '用户密码修改', '用户密码修改', '/UserController/editPwd', '0', '4', 'system:userMessage:editPwd', '2', 'entypo-tools', '3', '0');
INSERT INTO `t_sys_permission` VALUES ('496126970468237312', '日志展示', '日志管理', '/LogController/view', '0', '592059865673760768', 'system:log:view', '1', 'fa fa-info', '9', '0');
INSERT INTO `t_sys_permission` VALUES ('496127240363311104', '日志删除', '日志删除', '/LogController/remove', '0', '496126970468237312', 'system:log:remove', '2', 'entypo-trash', null, '0');
INSERT INTO `t_sys_permission` VALUES ('496127794879660032', '日志集合', '日志集合', '/LogController/list', '0', '496126970468237312', 'system:log:list', '2', null, null, '0');
INSERT INTO `t_sys_permission` VALUES ('5', '用户集合', '用户集合', '/UserController/list', '0', '4', 'system:userMessage:list', '2', '', null, '0');
INSERT INTO `t_sys_permission` VALUES ('581541547099553792', 'druid监控', 'druid监控', '/druid/', '0', '617766548966211584', 'userMessage:list', '1', 'fa fa-line-chart', '6', '0');
INSERT INTO `t_sys_permission` VALUES ('583063272123531264', 'API文档', null, '/doc.html', '1', '617766548966211584', '--', '1', 'layui-icon fa fa-font', '8', '0');
INSERT INTO `t_sys_permission` VALUES ('586003694080753664', '表单构建', null, '/static/component/code/index.html', '0', '617766548966211584', 'system:tool:view', '1', 'layui-icon layui-icon fa fa-list-alt', '5', '0');
INSERT INTO `t_sys_permission` VALUES ('587453033487532032', '后台模板', null, 'https://www.layui.com/doc/', '1', '617766548966211584', '', '1', 'layui-icon layui-icon fa fa-telegram', '9', '0');
INSERT INTO `t_sys_permission` VALUES ('589559748521623552', '一级菜单', null, '', '0', '0', '', '0', 'layui-icon layui-icon layui-icon layui-icon-face-smile', '6', '1');
INSERT INTO `t_sys_permission` VALUES ('592059865673760768', '系统管理', null, '', '0', '0', '', '0', 'layui-icon layui-icon-home', '1', '0');
INSERT INTO `t_sys_permission` VALUES ('592067570522128384', '测试跳转', '测试跳转', 'http://www.baidu.com', '1', '589559748521623552', '#', '1', 'fa fa-address-book', null, '0');
INSERT INTO `t_sys_permission` VALUES ('592167738407911424', '系统监控', '系统监控', '/ServiceController/view', '0', '617766548966211584', 'system:service:view', '1', 'fa fa-video-camera', '7', '0');
INSERT INTO `t_sys_permission` VALUES ('594691026430459904', '电子邮件管理', '电子邮件展示', '/EmailController/view', '0', '592059865673760768', 'system:email:view', '1', 'fa fa-envelope', '8', '0');
INSERT INTO `t_sys_permission` VALUES ('5946910264304599041', '电子邮件集合', '电子邮件集合', '/EmailController/list', '0', '594691026430459904', 'system:email:list', '2', '', null, '0');
INSERT INTO `t_sys_permission` VALUES ('5946910264304599042', '电子邮件添加', '电子邮件添加', '/EmailController/add', '0', '594691026430459904', 'system:email:add', '2', 'entypo-plus-squared', null, '0');
INSERT INTO `t_sys_permission` VALUES ('5946910264304599043', '电子邮件删除', '电子邮件删除', '/EmailController/remove', '0', '594691026430459904', 'system:email:remove', '2', 'entypo-trash', null, '0');
INSERT INTO `t_sys_permission` VALUES ('5946910264304599044', '电子邮件修改', '电子邮件修改', '/EmailController/edit', '0', '594691026430459904', 'system:email:edit', '2', 'fa fa-wrench', null, '0');
INSERT INTO `t_sys_permission` VALUES ('6', '用户添加', '用户添加', '/UserController/add', '0', '4', 'system:userMessage:add', '2', 'entypo-plus-squared', null, '0');
INSERT INTO `t_sys_permission` VALUES ('610635485890478080', '代码生成', null, '', '0', '0', '', '0', 'layui-icon layui-icon layui-icon layui-icon-praise', '2', '0');
INSERT INTO `t_sys_permission` VALUES ('610635950447394816', '全局配置', '', '/autoCodeController/global', '0', '610635485890478080', 'system:autocode:global', '1', 'fa fa-university', null, '0');
INSERT INTO `t_sys_permission` VALUES ('617766548966211584', '系统工具', null, '', '0', '0', '', '0', 'layui-icon layui-icon layui-icon layui-icon layui-icon layui-icon layui-icon-util', '5', '0');
INSERT INTO `t_sys_permission` VALUES ('618918631769636864', '字典管理', '字典类型表展示', '/DictTypeController/view', '0', '592059865673760768', 'system:dictType:view', '1', 'fa fa-puzzle-piece', '11', '0');
INSERT INTO `t_sys_permission` VALUES ('6189186317738311681', '字典类型表集合', '字典类型表集合', '/DictTypeController/list', '0', '618918631769636864', 'system:dictType:list', '2', null, null, '0');
INSERT INTO `t_sys_permission` VALUES ('6189186317948026882', '字典类型表添加', '字典类型表添加', '/DictTypeController/add', '0', '618918631769636864', 'system:dictType:add', '2', null, null, '0');
INSERT INTO `t_sys_permission` VALUES ('6189186317948026883', '字典类型表删除', '字典类型表删除', '/DictTypeController/remove', '0', '618918631769636864', 'system:dictType:remove', '2', null, null, '0');
INSERT INTO `t_sys_permission` VALUES ('6189186317989969924', '字典类型表修改', '字典类型表修改', '/DictTypeController/edit', '0', '618918631769636864', 'system:dictType:edit', '2', null, null, '0');
INSERT INTO `t_sys_permission` VALUES ('6192095214866268161', '字典数据表集合', '字典数据表集合', '/DictDataController/list', '0', '618918631769636864', 'system:dictData:list', '2', null, null, '0');
INSERT INTO `t_sys_permission` VALUES ('6192095214866268162', '字典数据表添加', '字典数据表添加', '/DictDataController/add', '0', '618918631769636864', 'system:dictData:add', '2', null, null, '0');
INSERT INTO `t_sys_permission` VALUES ('6192095215075983363', '字典数据表删除', '字典数据表删除', '/DictDataController/remove', '0', '618918631769636864', 'system:dictData:remove', '2', null, null, '0');
INSERT INTO `t_sys_permission` VALUES ('6192095215075983364', '字典数据表修改', '字典数据表修改', '/DictDataController/edit', '0', '618918631769636864', 'system:dictData:edit', '2', null, null, '0');
INSERT INTO `t_sys_permission` VALUES ('619836559427895296', '字典数据视图', '字典数据视图', '/DictDataController/view', '0', '618918631769636864', 'system:dictData:view', '2', null, null, '0');
INSERT INTO `t_sys_permission` VALUES ('7', '用户删除', '用户删除', '/UserController/remove', '0', '4', 'system:userMessage:remove', '2', 'entypo-trash', null, '0');
INSERT INTO `t_sys_permission` VALUES ('8', '用户修改', '用户修改', '/UserController/edit', '0', '4', 'system:userMessage:edit', '2', 'fa fa-wrench', null, '0');
INSERT INTO `t_sys_permission` VALUES ('882492338633576448', '营销管理', null, '', '0', '0', '', '0', 'layui-icon layui-icon layui-icon layui-icon layui-icon-app', '7', '0');
INSERT INTO `t_sys_permission` VALUES ('882492763348799488', '商品信息管理', null, '/GoodsInfoController/view', '0', '882492338633576448', 'gen:goodsInfo:view', '1', 'layui-icon layui-icon layui-icon-diamond', '1', '0');
INSERT INTO `t_sys_permission` VALUES ('882528193872007168', '商品集合', null, '', '0', '882492763348799488', 'gen:goodsInfo:list', '2', 'layui-icon', null, '0');
INSERT INTO `t_sys_permission` VALUES ('882529697622265856', '商品添加', null, '', '0', '882492763348799488', 'gen:goodsInfo:add', '2', 'layui-icon', null, '0');
INSERT INTO `t_sys_permission` VALUES ('882529814760787968', '商品删除', null, '', '0', '882492763348799488', 'gen:goodsInfo:remove', '2', 'layui-icon', null, '0');
INSERT INTO `t_sys_permission` VALUES ('882529912307716096', '商品修改', null, '', '0', '882492763348799488', 'gen:goodsInfo:edit', '2', 'layui-icon', null, '0');
INSERT INTO `t_sys_permission` VALUES ('882600779091939328', '用户管理', null, '/UsersController/view', '0', '882600779091939328', 'gen:users:view', '1', 'layui-icon layui-icon layui-icon layui-icon-face-smile', null, '0');
INSERT INTO `t_sys_permission` VALUES ('882600779091939329', '用户表集合', '用户表集合', '/UsersController/list', '0', '882600779091939328', 'gen:users:list', '2', '', null, '0');
INSERT INTO `t_sys_permission` VALUES ('882600779091939330', '用户表添加', '用户表添加', '/UsersController/add', '0', '882600779091939328', 'gen:users:add', '2', 'layui-icon layui-icon-add-1', null, '0');
INSERT INTO `t_sys_permission` VALUES ('882600779091939331', '用户表删除', '用户表删除', '/UsersController/remove', '0', '882600779091939328', 'gen:users:remove', '2', 'layui-icon layui-icon-delete', null, '0');
INSERT INTO `t_sys_permission` VALUES ('882600779091939332', '用户表修改', '用户表修改', '/UsersController/edit', '0', '882600779091939328', 'gen:users:edit', '2', 'layui-icon layui-icon-edit', null, '0');
INSERT INTO `t_sys_permission` VALUES ('882607288454615040', '管理', '展示', '/UsersController/view', '0', '0', 'gen:users:view', '1', 'layui-icon layui-icon-face-smile', null, '0');
INSERT INTO `t_sys_permission` VALUES ('882607288454615041', '集合', '集合', '/UsersController/list', '0', '882607288454615040', 'gen:users:list', '2', '', null, '0');
INSERT INTO `t_sys_permission` VALUES ('882607288454615042', '添加', '添加', '/UsersController/add', '0', '882607288454615040', 'gen:users:add', '2', 'layui-icon layui-icon-add-1', null, '0');
INSERT INTO `t_sys_permission` VALUES ('882607288454615043', '删除', '删除', '/UsersController/remove', '0', '882607288454615040', 'gen:users:remove', '2', 'layui-icon layui-icon-delete', null, '0');
INSERT INTO `t_sys_permission` VALUES ('882607288454615044', '修改', '修改', '/UsersController/edit', '0', '882607288454615040', 'gen:users:edit', '2', 'layui-icon layui-icon-edit', null, '0');
INSERT INTO `t_sys_permission` VALUES ('882607722879651840', '用户管理', null, '', '0', '0', '', '0', 'layui-icon layui-icon layui-icon-username', '8', '0');
INSERT INTO `t_sys_permission` VALUES ('882608044100423680', '普通用户管理', null, '/UsersController/view', '0', '882607722879651840', 'gen:users:view', '1', 'layui-icon', '1', '0');
INSERT INTO `t_sys_permission` VALUES ('882608570519130112', '普通用户集合', null, '', '0', '882608044100423680', 'gen:users:list', '2', 'layui-icon', '1', '0');
INSERT INTO `t_sys_permission` VALUES ('882608812341727232', '普通用户新增', null, '', '0', '882608044100423680', 'gen:users:add', '2', 'layui-icon layui-icon', '3', '0');
INSERT INTO `t_sys_permission` VALUES ('882609124276310016', '普通用户修改', null, '', '0', '882608044100423680', 'gen:users:edit', '2', 'layui-icon', '4', '0');
INSERT INTO `t_sys_permission` VALUES ('882609412651487232', '普通用户删除', null, '', '0', '882608044100423680', 'gen:users:remove', '2', 'layui-icon', '5', '0');
INSERT INTO `t_sys_permission` VALUES ('882780788863668224', '充电管理', null, '', '0', '0', '', '0', 'layui-icon layui-icon layui-icon-home', '9', '0');
INSERT INTO `t_sys_permission` VALUES ('882877537343115264', '充电记录管理', null, '/ChargerRecordController/view', '0', '882780788863668224', 'gen:chargerRecord:view', '1', 'layui-icon layui-icon layui-icon-face-smile', null, '0');
INSERT INTO `t_sys_permission` VALUES ('882877537343115265', '集合', '集合', '/ChargerRecordController/list', '0', '882877537343115264', 'gen:chargerRecord:list', '2', '', null, '0');
INSERT INTO `t_sys_permission` VALUES ('882877537343115266', '添加', '添加', '/ChargerRecordController/add', '0', '882877537343115264', 'gen:chargerRecord:add', '2', 'layui-icon layui-icon-add-1', null, '0');
INSERT INTO `t_sys_permission` VALUES ('882877537343115267', '删除', '删除', '/ChargerRecordController/remove', '0', '882877537343115264', 'gen:chargerRecord:remove', '2', 'layui-icon layui-icon-delete', null, '0');
INSERT INTO `t_sys_permission` VALUES ('882877537343115268', '修改', '修改', '/ChargerRecordController/edit', '0', '882877537343115264', 'gen:chargerRecord:edit', '2', 'layui-icon layui-icon-edit', null, '0');
INSERT INTO `t_sys_permission` VALUES ('882879744083890176', '优惠券管理', null, '/CouponController/view', '0', '882492338633576448', 'gen:coupon:view', '1', 'layui-icon layui-icon layui-icon-face-smile', '2', '0');
INSERT INTO `t_sys_permission` VALUES ('882879744083890177', '优惠券集合', '优惠券集合', '/CouponController/list', '0', '882879744083890176', 'gen:coupon:list', '2', '', null, '0');
INSERT INTO `t_sys_permission` VALUES ('882879744083890178', '优惠券添加', '优惠券添加', '/CouponController/add', '0', '882879744083890176', 'gen:coupon:add', '2', 'layui-icon layui-icon-add-1', null, '0');
INSERT INTO `t_sys_permission` VALUES ('882879744083890179', '优惠券删除', '优惠券删除', '/CouponController/remove', '0', '882879744083890176', 'gen:coupon:remove', '2', 'layui-icon layui-icon-delete', null, '0');
INSERT INTO `t_sys_permission` VALUES ('882879744083890180', '优惠券修改', '优惠券修改', '/CouponController/edit', '0', '882879744083890176', 'gen:coupon:edit', '2', 'layui-icon layui-icon-edit', null, '0');
INSERT INTO `t_sys_permission` VALUES ('882884784056045568', '充电枪管理', '充电枪展示', '/ChargeGunController/view', '0', '882780788863668224', 'gen:chargeGun:view', '1', 'layui-icon layui-icon-face-smile', null, '0');
INSERT INTO `t_sys_permission` VALUES ('882884784056045569', '充电枪集合', '充电枪集合', '/ChargeGunController/list', '0', '882884784056045568', 'gen:chargeGun:list', '2', '', null, '0');
INSERT INTO `t_sys_permission` VALUES ('882884784056045570', '充电枪添加', '充电枪添加', '/ChargeGunController/add', '0', '882884784056045568', 'gen:chargeGun:add', '2', 'layui-icon layui-icon-add-1', null, '0');
INSERT INTO `t_sys_permission` VALUES ('882884784056045571', '充电枪删除', '充电枪删除', '/ChargeGunController/remove', '0', '882884784056045568', 'gen:chargeGun:remove', '2', 'layui-icon layui-icon-delete', null, '0');
INSERT INTO `t_sys_permission` VALUES ('882884784056045572', '充电枪修改', '充电枪修改', '/ChargeGunController/edit', '0', '882884784056045568', 'gen:chargeGun:edit', '2', 'layui-icon layui-icon-edit', null, '0');
INSERT INTO `t_sys_permission` VALUES ('882916565211811840', '充电桩管理', '充电桩展示', '/ChargerController/view', '0', '882780788863668224', 'gen:charger:view', '1', 'layui-icon layui-icon-face-smile', null, '0');
INSERT INTO `t_sys_permission` VALUES ('882916565211811841', '充电桩集合', '充电桩集合', '/ChargerController/list', '0', '882916565211811840', 'gen:charger:list', '2', '', null, '0');
INSERT INTO `t_sys_permission` VALUES ('882916565211811842', '充电桩添加', '充电桩添加', '/ChargerController/add', '0', '882916565211811840', 'gen:charger:add', '2', 'layui-icon layui-icon-add-1', null, '0');
INSERT INTO `t_sys_permission` VALUES ('882916565211811843', '充电桩删除', '充电桩删除', '/ChargerController/remove', '0', '882916565211811840', 'gen:charger:remove', '2', 'layui-icon layui-icon-delete', null, '0');
INSERT INTO `t_sys_permission` VALUES ('882916565211811844', '充电桩修改', '充电桩修改', '/ChargerController/edit', '0', '882916565211811840', 'gen:charger:edit', '2', 'layui-icon layui-icon-edit', null, '0');
INSERT INTO `t_sys_permission` VALUES ('882927147621158912', '充电站管理', '充电站展示', '/StationController/view', '0', '882780788863668224', 'gen:station:view', '1', 'layui-icon layui-icon-face-smile', null, '0');
INSERT INTO `t_sys_permission` VALUES ('882927147621158913', '充电站集合', '充电站集合', '/StationController/list', '0', '882927147621158912', 'gen:station:list', '2', '', null, '0');
INSERT INTO `t_sys_permission` VALUES ('882927147621158914', '充电站添加', '充电站添加', '/StationController/add', '0', '882927147621158912', 'gen:station:add', '2', 'layui-icon layui-icon-add-1', null, '0');
INSERT INTO `t_sys_permission` VALUES ('882927147621158915', '充电站删除', '充电站删除', '/StationController/remove', '0', '882927147621158912', 'gen:station:remove', '2', 'layui-icon layui-icon-delete', null, '0');
INSERT INTO `t_sys_permission` VALUES ('882927147621158916', '充电站修改', '充电站修改', '/StationController/edit', '0', '882927147621158912', 'gen:station:edit', '2', 'layui-icon layui-icon-edit', null, '0');
INSERT INTO `t_sys_permission` VALUES ('883225370877366272', '活动优惠券管理', null, '/ActivityCouponController/view', '0', '882492338633576448', 'gen:activityCoupon:view', '1', 'layui-icon layui-icon layui-icon-face-smile', '3', '0');
INSERT INTO `t_sys_permission` VALUES ('883225370877366273', '活动优惠券集合', '活动优惠券集合', '/ActivityCouponController/list', '0', '883225370877366272', 'gen:activityCoupon:list', '2', '', null, '0');
INSERT INTO `t_sys_permission` VALUES ('883225370877366274', '活动优惠券添加', '活动优惠券添加', '/ActivityCouponController/add', '0', '883225370877366272', 'gen:activityCoupon:add', '2', 'layui-icon layui-icon-add-1', null, '0');
INSERT INTO `t_sys_permission` VALUES ('883225370877366275', '活动优惠券删除', '活动优惠券删除', '/ActivityCouponController/remove', '0', '883225370877366272', 'gen:activityCoupon:remove', '2', 'layui-icon layui-icon-delete', null, '0');
INSERT INTO `t_sys_permission` VALUES ('883225370877366276', '活动优惠券修改', '活动优惠券修改', '/ActivityCouponController/edit', '0', '883225370877366272', 'gen:activityCoupon:edit', '2', 'layui-icon layui-icon-edit', null, '0');
INSERT INTO `t_sys_permission` VALUES ('884013484151541760', '支付管理', null, '', '0', '0', '', '0', 'layui-icon layui-icon-cellphone', '8', '0');
INSERT INTO `t_sys_permission` VALUES ('884014715003604992', '支付单管理', '支付单展示', '/PayOrderController/view', '0', '884013484151541760', 'gen:payOrder:view', '1', 'layui-icon layui-icon-face-smile', null, '0');
INSERT INTO `t_sys_permission` VALUES ('884014715003604993', '支付单集合', '支付单集合', '/PayOrderController/list', '0', '884014715003604992', 'gen:payOrder:list', '2', '', null, '0');
INSERT INTO `t_sys_permission` VALUES ('884014715003604994', '支付单添加', '支付单添加', '/PayOrderController/add', '0', '884014715003604992', 'gen:payOrder:add', '2', 'layui-icon layui-icon-add-1', null, '0');
INSERT INTO `t_sys_permission` VALUES ('884014715003604995', '支付单删除', '支付单删除', '/PayOrderController/remove', '0', '884014715003604992', 'gen:payOrder:remove', '2', 'layui-icon layui-icon-delete', null, '0');
INSERT INTO `t_sys_permission` VALUES ('884014715003604996', '支付单修改', '支付单修改', '/PayOrderController/edit', '0', '884014715003604992', 'gen:payOrder:edit', '2', 'layui-icon layui-icon-edit', null, '0');
INSERT INTO `t_sys_permission` VALUES ('884020401519333376', '异常监控', null, '', '0', '0', '', '0', 'layui-icon layui-icon-snowflake', '10', '0');
INSERT INTO `t_sys_permission` VALUES ('884025055770710016', '异常反馈管理', '异常反馈展示', '/ExceptionalRetroactionController/view', '0', '884020401519333376', 'gen:exceptionalRetroaction:view', '1', 'layui-icon layui-icon-face-smile', null, '0');
INSERT INTO `t_sys_permission` VALUES ('884025055770710017', '异常反馈集合', '异常反馈集合', '/ExceptionalRetroactionController/list', '0', '884025055770710016', 'gen:exceptionalRetroaction:list', '2', '', null, '0');
INSERT INTO `t_sys_permission` VALUES ('884025055770710018', '异常反馈添加', '异常反馈添加', '/ExceptionalRetroactionController/add', '0', '884025055770710016', 'gen:exceptionalRetroaction:add', '2', 'layui-icon layui-icon-add-1', null, '0');
INSERT INTO `t_sys_permission` VALUES ('884025055770710019', '异常反馈删除', '异常反馈删除', '/ExceptionalRetroactionController/remove', '0', '884025055770710016', 'gen:exceptionalRetroaction:remove', '2', 'layui-icon layui-icon-delete', null, '0');
INSERT INTO `t_sys_permission` VALUES ('884025055774904320', '异常反馈修改', '异常反馈修改', '/ExceptionalRetroactionController/edit', '0', '884025055770710016', 'gen:exceptionalRetroaction:edit', '2', 'layui-icon layui-icon-edit', null, '0');
INSERT INTO `t_sys_permission` VALUES ('884220364220141568', '短信模板管理管理', '短信模板管理展示', '/MessageTemplateController/view', '0', '592059865673760768', 'gen:messageTemplate:view', '1', 'layui-icon layui-icon-face-smile', null, '0');
INSERT INTO `t_sys_permission` VALUES ('884220364220141569', '短信模板管理集合', '短信模板管理集合', '/MessageTemplateController/list', '0', '884220364220141568', 'gen:messageTemplate:list', '2', '', null, '0');
INSERT INTO `t_sys_permission` VALUES ('884220364220141570', '短信模板管理添加', '短信模板管理添加', '/MessageTemplateController/add', '0', '884220364220141568', 'gen:messageTemplate:add', '2', 'layui-icon layui-icon-add-1', null, '0');
INSERT INTO `t_sys_permission` VALUES ('884220364220141571', '短信模板管理删除', '短信模板管理删除', '/MessageTemplateController/remove', '0', '884220364220141568', 'gen:messageTemplate:remove', '2', 'layui-icon layui-icon-delete', null, '0');
INSERT INTO `t_sys_permission` VALUES ('884220364220141572', '短信模板管理修改', '短信模板管理修改', '/MessageTemplateController/edit', '0', '884220364220141568', 'gen:messageTemplate:edit', '2', 'layui-icon layui-icon-edit', null, '0');
INSERT INTO `t_sys_permission` VALUES ('884223003284606976', '汽车信息管理', '汽车信息展示', '/CarInfoController/view', '0', '882607722879651840', 'gen:carInfo:view', '1', 'layui-icon layui-icon-face-smile', null, '0');
INSERT INTO `t_sys_permission` VALUES ('884223003284606977', '汽车信息集合', '汽车信息集合', '/CarInfoController/list', '0', '884223003284606976', 'gen:carInfo:list', '2', '', null, '0');
INSERT INTO `t_sys_permission` VALUES ('884223003284606978', '汽车信息添加', '汽车信息添加', '/CarInfoController/add', '0', '884223003284606976', 'gen:carInfo:add', '2', 'layui-icon layui-icon-add-1', null, '0');
INSERT INTO `t_sys_permission` VALUES ('884223003284606979', '汽车信息删除', '汽车信息删除', '/CarInfoController/remove', '0', '884223003284606976', 'gen:carInfo:remove', '2', 'layui-icon layui-icon-delete', null, '0');
INSERT INTO `t_sys_permission` VALUES ('884223003284606980', '汽车信息修改', '汽车信息修改', '/CarInfoController/edit', '0', '884223003284606976', 'gen:carInfo:edit', '2', 'layui-icon layui-icon-edit', null, '0');
INSERT INTO `t_sys_permission` VALUES ('884223679888756736', '活动信息管理', null, '/ActivityController/view', '0', '882492338633576448', 'gen:activity:view', '1', 'layui-icon layui-icon layui-icon-face-smile', null, '0');
INSERT INTO `t_sys_permission` VALUES ('884223679888756737', '活动信息集合', null, '', '0', '884223679888756736', 'gen:activity:list', '2', 'layui-icon', null, '0');
INSERT INTO `t_sys_permission` VALUES ('884223679888756738', '活动信息添加', null, '', '0', '884223679888756736', 'gen:activity:add', '2', 'layui-icon layui-icon layui-icon-add-1', null, '0');
INSERT INTO `t_sys_permission` VALUES ('884223679888756739', '活动信息删除', null, '', '0', '884223679888756736', 'gen:activity:remove', '2', 'layui-icon layui-icon layui-icon-delete', null, '0');
INSERT INTO `t_sys_permission` VALUES ('884223679888756740', '活动信息修改', null, '', '0', '884223679888756736', 'gen:activity:edit', '2', 'layui-icon layui-icon layui-icon-edit', null, '0');
INSERT INTO `t_sys_permission` VALUES ('884226785477267456', '用户积分变更管理', '用户积分变更展示', '/PointsModifyRecordController/view', '0', '882607722879651840', 'gen:pointsModifyRecord:view', '1', 'layui-icon layui-icon-face-smile', null, '0');
INSERT INTO `t_sys_permission` VALUES ('884226785477267457', '用户积分变更集合', '用户积分变更集合', '/PointsModifyRecordController/list', '0', '884226785477267456', 'gen:pointsModifyRecord:list', '2', '', null, '0');
INSERT INTO `t_sys_permission` VALUES ('884226785477267458', '用户积分变更添加', '用户积分变更添加', '/PointsModifyRecordController/add', '0', '884226785477267456', 'gen:pointsModifyRecord:add', '2', 'layui-icon layui-icon-add-1', null, '0');
INSERT INTO `t_sys_permission` VALUES ('884226785477267459', '用户积分变更删除', '用户积分变更删除', '/PointsModifyRecordController/remove', '0', '884226785477267456', 'gen:pointsModifyRecord:remove', '2', 'layui-icon layui-icon-delete', null, '0');
INSERT INTO `t_sys_permission` VALUES ('884226785477267460', '用户积分变更修改', '用户积分变更修改', '/PointsModifyRecordController/edit', '0', '884226785477267456', 'gen:pointsModifyRecord:edit', '2', 'layui-icon layui-icon-edit', null, '0');
INSERT INTO `t_sys_permission` VALUES ('884228539791052800', '商品兑换管理', '商品兑换展示', '/ExchangeGoodsRecordController/view', '0', '882492338633576448', 'gen:exchangeGoodsRecord:view', '1', 'layui-icon layui-icon-face-smile', null, '0');
INSERT INTO `t_sys_permission` VALUES ('884228539791052801', '商品兑换集合', '商品兑换集合', '/ExchangeGoodsRecordController/list', '0', '884228539791052800', 'gen:exchangeGoodsRecord:list', '2', '', null, '0');
INSERT INTO `t_sys_permission` VALUES ('884228539795247104', '商品兑换添加', '商品兑换添加', '/ExchangeGoodsRecordController/add', '0', '884228539791052800', 'gen:exchangeGoodsRecord:add', '2', 'layui-icon layui-icon-add-1', null, '0');
INSERT INTO `t_sys_permission` VALUES ('884228539795247105', '商品兑换删除', '商品兑换删除', '/ExchangeGoodsRecordController/remove', '0', '884228539791052800', 'gen:exchangeGoodsRecord:remove', '2', 'layui-icon layui-icon-delete', null, '0');
INSERT INTO `t_sys_permission` VALUES ('884228539795247106', '商品兑换修改', '商品兑换修改', '/ExchangeGoodsRecordController/edit', '0', '884228539791052800', 'gen:exchangeGoodsRecord:edit', '2', 'layui-icon layui-icon-edit', null, '0');
INSERT INTO `t_sys_permission` VALUES ('884229692402569216', '活动商品管理', '活动商品展示', '/ActivityGoodsController/view', '0', '882492338633576448', 'gen:activityGoods:view', '1', 'layui-icon layui-icon-face-smile', null, '0');
INSERT INTO `t_sys_permission` VALUES ('884229692402569217', '活动商品集合', '活动商品集合', '/ActivityGoodsController/list', '0', '884229692402569216', 'gen:activityGoods:list', '2', '', null, '0');
INSERT INTO `t_sys_permission` VALUES ('884229692402569218', '活动商品添加', '活动商品添加', '/ActivityGoodsController/add', '0', '884229692402569216', 'gen:activityGoods:add', '2', 'layui-icon layui-icon-add-1', null, '0');
INSERT INTO `t_sys_permission` VALUES ('884229692402569219', '活动商品删除', '活动商品删除', '/ActivityGoodsController/remove', '0', '884229692402569216', 'gen:activityGoods:remove', '2', 'layui-icon layui-icon-delete', null, '0');
INSERT INTO `t_sys_permission` VALUES ('884229692402569220', '活动商品修改', '活动商品修改', '/ActivityGoodsController/edit', '0', '884229692402569216', 'gen:activityGoods:edit', '2', 'layui-icon layui-icon-edit', null, '0');
INSERT INTO `t_sys_permission` VALUES ('884230589228650496', '用户优惠券管理', '用户优惠券展示', '/UserActivityCouponController/view', '0', '882492338633576448', 'gen:userActivityCoupon:view', '1', 'layui-icon layui-icon-face-smile', null, '0');
INSERT INTO `t_sys_permission` VALUES ('884230589228650497', '用户优惠券集合', '用户优惠券集合', '/UserActivityCouponController/list', '0', '884230589228650496', 'gen:userActivityCoupon:list', '2', '', null, '0');
INSERT INTO `t_sys_permission` VALUES ('884230589228650498', '用户优惠券添加', '用户优惠券添加', '/UserActivityCouponController/add', '0', '884230589228650496', 'gen:userActivityCoupon:add', '2', 'layui-icon layui-icon-add-1', null, '0');
INSERT INTO `t_sys_permission` VALUES ('884230589228650499', '用户优惠券删除', '用户优惠券删除', '/UserActivityCouponController/remove', '0', '884230589228650496', 'gen:userActivityCoupon:remove', '2', 'layui-icon layui-icon-delete', null, '0');
INSERT INTO `t_sys_permission` VALUES ('884230589228650500', '用户优惠券修改', '用户优惠券修改', '/UserActivityCouponController/edit', '0', '884230589228650496', 'gen:userActivityCoupon:edit', '2', 'layui-icon layui-icon-edit', null, '0');
INSERT INTO `t_sys_permission` VALUES ('884230928099053568', '用户商品管理', '用户商品展示', '/UserActivityGoodsController/view', '0', '882492338633576448', 'gen:userActivityGoods:view', '1', 'layui-icon layui-icon-face-smile', null, '0');
INSERT INTO `t_sys_permission` VALUES ('884230928099053569', '用户商品集合', '用户商品集合', '/UserActivityGoodsController/list', '0', '884230928099053568', 'gen:userActivityGoods:list', '2', '', null, '0');
INSERT INTO `t_sys_permission` VALUES ('884230928099053570', '用户商品添加', '用户商品添加', '/UserActivityGoodsController/add', '0', '884230928099053568', 'gen:userActivityGoods:add', '2', 'layui-icon layui-icon-add-1', null, '0');
INSERT INTO `t_sys_permission` VALUES ('884230928099053571', '用户商品删除', '用户商品删除', '/UserActivityGoodsController/remove', '0', '884230928099053568', 'gen:userActivityGoods:remove', '2', 'layui-icon layui-icon-delete', null, '0');
INSERT INTO `t_sys_permission` VALUES ('884230928099053572', '用户商品修改', '用户商品修改', '/UserActivityGoodsController/edit', '0', '884230928099053568', 'gen:userActivityGoods:edit', '2', 'layui-icon layui-icon-edit', null, '0');
INSERT INTO `t_sys_permission` VALUES ('884231175311331328', '邀请注册管理', '邀请注册展示', '/InvitedRegisterRecordController/view', '0', '882492338633576448', 'gen:invitedRegisterRecord:view', '1', 'layui-icon layui-icon-face-smile', null, '0');
INSERT INTO `t_sys_permission` VALUES ('884231175311331329', '邀请注册集合', '邀请注册集合', '/InvitedRegisterRecordController/list', '0', '884231175311331328', 'gen:invitedRegisterRecord:list', '2', '', null, '0');
INSERT INTO `t_sys_permission` VALUES ('884231175311331330', '邀请注册添加', '邀请注册添加', '/InvitedRegisterRecordController/add', '0', '884231175311331328', 'gen:invitedRegisterRecord:add', '2', 'layui-icon layui-icon-add-1', null, '0');
INSERT INTO `t_sys_permission` VALUES ('884231175311331331', '邀请注册删除', '邀请注册删除', '/InvitedRegisterRecordController/remove', '0', '884231175311331328', 'gen:invitedRegisterRecord:remove', '2', 'layui-icon layui-icon-delete', null, '0');
INSERT INTO `t_sys_permission` VALUES ('884231175311331332', '邀请注册修改', '邀请注册修改', '/InvitedRegisterRecordController/edit', '0', '884231175311331328', 'gen:invitedRegisterRecord:edit', '2', 'layui-icon layui-icon-edit', null, '0');
INSERT INTO `t_sys_permission` VALUES ('884231398729322496', '电价管理', '电价展示', '/ElectricityPriceController/view', '0', '882780788863668224', 'gen:electricityPrice:view', '1', 'layui-icon layui-icon-face-smile', null, '0');
INSERT INTO `t_sys_permission` VALUES ('884231398729322497', '电价集合', '电价集合', '/ElectricityPriceController/list', '0', '884231398729322496', 'gen:electricityPrice:list', '2', '', null, '0');
INSERT INTO `t_sys_permission` VALUES ('884231398729322498', '电价添加', '电价添加', '/ElectricityPriceController/add', '0', '884231398729322496', 'gen:electricityPrice:add', '2', 'layui-icon layui-icon-add-1', null, '0');
INSERT INTO `t_sys_permission` VALUES ('884231398729322499', '电价删除', '电价删除', '/ElectricityPriceController/remove', '0', '884231398729322496', 'gen:electricityPrice:remove', '2', 'layui-icon layui-icon-delete', null, '0');
INSERT INTO `t_sys_permission` VALUES ('884231398729322500', '电价修改', '电价修改', '/ElectricityPriceController/edit', '0', '884231398729322496', 'gen:electricityPrice:edit', '2', 'layui-icon layui-icon-edit', null, '0');
INSERT INTO `t_sys_permission` VALUES ('884231570616094720', '投资人详情管理', '投资人详情展示', '/InvestorDetailController/view', '0', '882607722879651840', 'gen:investorDetail:view', '1', 'layui-icon layui-icon-face-smile', null, '0');
INSERT INTO `t_sys_permission` VALUES ('884231570616094721', '投资人详情集合', '投资人详情集合', '/InvestorDetailController/list', '0', '884231570616094720', 'gen:investorDetail:list', '2', '', null, '0');
INSERT INTO `t_sys_permission` VALUES ('884231570616094722', '投资人详情添加', '投资人详情添加', '/InvestorDetailController/add', '0', '884231570616094720', 'gen:investorDetail:add', '2', 'layui-icon layui-icon-add-1', null, '0');
INSERT INTO `t_sys_permission` VALUES ('884231570616094723', '投资人详情删除', '投资人详情删除', '/InvestorDetailController/remove', '0', '884231570616094720', 'gen:investorDetail:remove', '2', 'layui-icon layui-icon-delete', null, '0');
INSERT INTO `t_sys_permission` VALUES ('884231570616094724', '投资人详情修改', '投资人详情修改', '/InvestorDetailController/edit', '0', '884231570616094720', 'gen:investorDetail:edit', '2', 'layui-icon layui-icon-edit', null, '0');
INSERT INTO `t_sys_permission` VALUES ('884231719748767744', '电价变更管理', '电价变更展示', '/ElectricityPriceHistoryController/view', '0', '882780788863668224', 'gen:electricityPriceHistory:view', '1', 'layui-icon layui-icon-face-smile', null, '0');
INSERT INTO `t_sys_permission` VALUES ('884231719748767745', '电价变更集合', '电价变更集合', '/ElectricityPriceHistoryController/list', '0', '884231719748767744', 'gen:electricityPriceHistory:list', '2', '', null, '0');
INSERT INTO `t_sys_permission` VALUES ('884231719748767746', '电价变更添加', '电价变更添加', '/ElectricityPriceHistoryController/add', '0', '884231719748767744', 'gen:electricityPriceHistory:add', '2', 'layui-icon layui-icon-add-1', null, '0');
INSERT INTO `t_sys_permission` VALUES ('884231719748767747', '电价变更删除', '电价变更删除', '/ElectricityPriceHistoryController/remove', '0', '884231719748767744', 'gen:electricityPriceHistory:remove', '2', 'layui-icon layui-icon-delete', null, '0');
INSERT INTO `t_sys_permission` VALUES ('884231719748767748', '电价变更修改', '电价变更修改', '/ElectricityPriceHistoryController/edit', '0', '884231719748767744', 'gen:electricityPriceHistory:edit', '2', 'layui-icon layui-icon-edit', null, '0');
INSERT INTO `t_sys_permission` VALUES ('884231964637401088', '全局配置管理', '全局配置展示', '/GlobalPropertyController/view', '0', '592059865673760768', 'gen:globalProperty:view', '1', 'layui-icon layui-icon-face-smile', null, '0');
INSERT INTO `t_sys_permission` VALUES ('884231964637401089', '全局配置集合', '全局配置集合', '/GlobalPropertyController/list', '0', '884231964637401088', 'gen:globalProperty:list', '2', '', null, '0');
INSERT INTO `t_sys_permission` VALUES ('884231964637401090', '全局配置添加', '全局配置添加', '/GlobalPropertyController/add', '0', '884231964637401088', 'gen:globalProperty:add', '2', 'layui-icon layui-icon-add-1', null, '0');
INSERT INTO `t_sys_permission` VALUES ('884231964637401091', '全局配置删除', '全局配置删除', '/GlobalPropertyController/remove', '0', '884231964637401088', 'gen:globalProperty:remove', '2', 'layui-icon layui-icon-delete', null, '0');
INSERT INTO `t_sys_permission` VALUES ('884231964637401092', '全局配置修改', '全局配置修改', '/GlobalPropertyController/edit', '0', '884231964637401088', 'gen:globalProperty:edit', '2', 'layui-icon layui-icon-edit', null, '0');
INSERT INTO `t_sys_permission` VALUES ('9', '角色管理', '角色展示', '/RoleController/view', '0', '411522822607867904', 'system:role:view', '1', 'fa fa-group', '2', '0');

-- ----------------------------
-- Table structure for `t_sys_permission_role`
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_permission_role`;
CREATE TABLE `t_sys_permission_role` (
  `id` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `role_id` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '角色id',
  `permission_id` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '权限id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 ROW_FORMAT=DYNAMIC COMMENT='角色权限中间表';

-- ----------------------------
-- Records of t_sys_permission_role
-- ----------------------------
INSERT INTO `t_sys_permission_role` VALUES ('0029ce4b-50df-4213-955d-8b596e03f9e1', '488243256161730560', '5946910264304599043');
INSERT INTO `t_sys_permission_role` VALUES ('0162052d-34a9-4eb8-a02e-eaf6952d4527', '488243256161730560', '884220364220141571');
INSERT INTO `t_sys_permission_role` VALUES ('032355c0-5222-431d-99c1-19f58bb47b40', '488243256161730560', '884231175311331328');
INSERT INTO `t_sys_permission_role` VALUES ('038aefd6-b378-4e08-b3e3-e641ac58bb44', '488243256161730560', '496127794879660032');
INSERT INTO `t_sys_permission_role` VALUES ('03a6b898-c7be-4c77-aff9-77b8059b3cdf', '882910089013497856', '884226785477267457');
INSERT INTO `t_sys_permission_role` VALUES ('04243a32-7710-498a-bb76-99fb3ae33071', '488243256161730560', '884025055770710016');
INSERT INTO `t_sys_permission_role` VALUES ('0481d5c4-bdf5-48da-ad69-969773919e7d', '488243256161730560', '6189186317948026883');
INSERT INTO `t_sys_permission_role` VALUES ('059bc8cb-279a-4c7f-9958-d7ff3eeaf744', '488243256161730560', '884230928099053572');
INSERT INTO `t_sys_permission_role` VALUES ('06ea6bcf-1dc7-4403-ba0f-f55ae91e1fae', '488243256161730560', '884230589228650499');
INSERT INTO `t_sys_permission_role` VALUES ('06eacb8b-e463-451e-b869-6771d7febe9c', '882910089013497856', '882884784056045568');
INSERT INTO `t_sys_permission_role` VALUES ('08538e4a-316d-4d2d-ae1e-720251b37b90', '882910089013497856', '884013484151541760');
INSERT INTO `t_sys_permission_role` VALUES ('085acf7b-b107-48c0-acf3-924cda84669f', '882910089013497856', '882879744083890177');
INSERT INTO `t_sys_permission_role` VALUES ('097ceb91-ff63-47b2-aa87-97f886a874fd', '882910089013497856', '884231398729322497');
INSERT INTO `t_sys_permission_role` VALUES ('0cf3fec1-da19-4c41-b158-2cff58b63ff2', '882910089013497856', '883225370877366276');
INSERT INTO `t_sys_permission_role` VALUES ('0d44e164-23a3-4b62-8d27-94935ca6a134', '488243256161730560', '884231398729322500');
INSERT INTO `t_sys_permission_role` VALUES ('0f767b86-04a8-4b67-a65e-c87514dc60b0', '488243256161730560', '884230589228650500');
INSERT INTO `t_sys_permission_role` VALUES ('0f9cba81-bb57-4f9d-bb55-2aa7579391f8', '488243256161730560', '332857281479839744');
INSERT INTO `t_sys_permission_role` VALUES ('0fba4316-f6a3-4645-ac8f-9b13693ceb00', '488243256161730560', '884228539791052801');
INSERT INTO `t_sys_permission_role` VALUES ('105580af-a93b-4589-9719-2666440f4662', '488243256161730560', '884223679888756740');
INSERT INTO `t_sys_permission_role` VALUES ('11349306-698e-4f00-8e7d-bbcc60ec34ee', '488243256161730560', '882927147621158915');
INSERT INTO `t_sys_permission_role` VALUES ('116aafe0-adb2-4474-8eee-60b36fd80a61', '488243256161730560', '882916565211811840');
INSERT INTO `t_sys_permission_role` VALUES ('12cb0833-6262-4e85-a80a-e323e34d963b', '488243256161730560', '882927147621158914');
INSERT INTO `t_sys_permission_role` VALUES ('13c1ee6e-2eea-4090-b3a0-b6b04f2c8eae', '488243256161730560', '410791701859405824');
INSERT INTO `t_sys_permission_role` VALUES ('147bbec9-e643-4e9d-8386-b01d409ee7a3', '488243256161730560', '882608812341727232');
INSERT INTO `t_sys_permission_role` VALUES ('1483acd2-55a2-4275-b856-c821c139e380', '488243256161730560', '883225370877366272');
INSERT INTO `t_sys_permission_role` VALUES ('169c9bc8-c410-418f-a4b6-4597077984a1', '488243256161730560', '884231398729322497');
INSERT INTO `t_sys_permission_role` VALUES ('176cb6b0-ea38-4a78-b398-6760ac5f7cfd', '882910089013497856', '884229692402569217');
INSERT INTO `t_sys_permission_role` VALUES ('18271a81-c06d-4485-a774-d77819601b06', '488243256161730560', '884231719748767745');
INSERT INTO `t_sys_permission_role` VALUES ('18d2f650-039e-47a4-af48-bf10c84b8540', '488243256161730560', '7');
INSERT INTO `t_sys_permission_role` VALUES ('19b24a41-9cab-4920-ae7c-76ccbbcf3237', '488243256161730560', '592167738407911424');
INSERT INTO `t_sys_permission_role` VALUES ('1ad93875-839f-46b8-bc9e-4b111e137716', '488243256161730560', '882529912307716096');
INSERT INTO `t_sys_permission_role` VALUES ('1b7ac42c-2979-4746-a70a-296901dcdd00', '488243256161730560', '884231719748767747');
INSERT INTO `t_sys_permission_role` VALUES ('1ba3293b-d729-4a40-9d56-388f80ea8870', '488243256161730560', '882609412651487232');
INSERT INTO `t_sys_permission_role` VALUES ('1cbbf911-b12e-4ebf-8769-d903bf3bb602', '488243256161730560', '592059865673760768');
INSERT INTO `t_sys_permission_role` VALUES ('1ced9b0a-4fa2-42da-8d69-562a8c1230ac', '488243256161730560', '610635950447394816');
INSERT INTO `t_sys_permission_role` VALUES ('1d64893f-5f87-490e-a099-b91cc0f27980', '882910089013497856', '340381240911859712');
INSERT INTO `t_sys_permission_role` VALUES ('1e0adf7e-d57d-47ea-82d9-7d2641302e75', '488243256161730560', '331778807298134016');
INSERT INTO `t_sys_permission_role` VALUES ('1e5ddc55-e44c-4d91-b16a-c829d0c7eb73', '488243256161730560', '3328572814798397473');
INSERT INTO `t_sys_permission_role` VALUES ('1e7dd27e-f74f-476b-98fe-48e4d09a6683', '882910089013497856', '884230928099053568');
INSERT INTO `t_sys_permission_role` VALUES ('1fb562d2-6c36-4eed-af40-653e87ccd7a0', '488243256161730560', '3303650266428252171');
INSERT INTO `t_sys_permission_role` VALUES ('2052014f-cdf4-467e-9751-1a84e9ad5793', '488243256161730560', '496127240363311104');
INSERT INTO `t_sys_permission_role` VALUES ('2115b045-6285-4852-bfad-6ad9ffb8551f', '488243256161730560', '5');
INSERT INTO `t_sys_permission_role` VALUES ('21ebe0e6-33ad-49e7-ba88-09bde4a177dc', '488243256161730560', '884223679888756737');
INSERT INTO `t_sys_permission_role` VALUES ('227ca326-b49c-4d54-a01f-8c03ea498637', '488243256161730560', '618918631769636864');
INSERT INTO `t_sys_permission_role` VALUES ('2330fe65-75ce-44e6-b97f-16a70daa03ad', '882910089013497856', '884229692402569216');
INSERT INTO `t_sys_permission_role` VALUES ('238fa45b-40ea-4c90-a2a6-2ecb47c2b420', '488243256161730560', '587453033487532032');
INSERT INTO `t_sys_permission_role` VALUES ('246ae7ab-2ab3-4ff5-b414-7ed44c9ffe86', '488243256161730560', '882877537343115266');
INSERT INTO `t_sys_permission_role` VALUES ('247ade8e-9d65-44f2-9f12-74535f7eb6f8', '882910089013497856', '330365026642825216');
INSERT INTO `t_sys_permission_role` VALUES ('2617af7e-de90-4b3d-b966-e6d169984b50', '488243256161730560', '23');
INSERT INTO `t_sys_permission_role` VALUES ('268acd17-d24b-4846-a152-92c67607a144', '882910089013497856', '583063272123531264');
INSERT INTO `t_sys_permission_role` VALUES ('29745153-b068-496c-86c8-6f1907b2237d', '488243256161730560', '884220364220141570');
INSERT INTO `t_sys_permission_role` VALUES ('29d9b21c-f5da-4659-98b5-0e78b64cb8ef', '488243256161730560', '496126970468237312');
INSERT INTO `t_sys_permission_role` VALUES ('2a7cde4d-14c6-4acd-b3ca-3ebce0942d85', '488243256161730560', '882879744083890179');
INSERT INTO `t_sys_permission_role` VALUES ('2b0d1bd3-0bf0-4a11-9064-ff8e7cf49bea', '882910089013497856', '884229692402569218');
INSERT INTO `t_sys_permission_role` VALUES ('2b33a8dc-7cfb-48cf-b494-e977007e067b', '488243256161730560', '5946910264304599042');
INSERT INTO `t_sys_permission_role` VALUES ('2c758dc7-714d-4f5a-a51c-c837b639b23f', '488243256161730560', '882877537343115267');
INSERT INTO `t_sys_permission_role` VALUES ('2cf235d5-1805-406b-92ce-f32b1bafc2d9', '882910089013497856', '882884784056045572');
INSERT INTO `t_sys_permission_role` VALUES ('2d12d7b9-badf-442c-90bf-787de374c5f1', '488243256161730560', '884014715003604992');
INSERT INTO `t_sys_permission_role` VALUES ('2f8ea7d7-beaf-45c8-8f7d-d5de92cda509', '488243256161730560', '882916565211811842');
INSERT INTO `t_sys_permission_role` VALUES ('2fb628ec-ddba-4c66-9183-e2e0630c85e2', '882910089013497856', '884231175311331329');
INSERT INTO `t_sys_permission_role` VALUES ('32533de1-bf79-41e5-ad63-f838df3bc519', '488243256161730560', '373489907429150720');
INSERT INTO `t_sys_permission_role` VALUES ('33ac7dfc-18fb-4911-95a6-012210929c74', '488243256161730560', '882916565211811841');
INSERT INTO `t_sys_permission_role` VALUES ('34b47846-a1e0-4b59-b764-d59e3bc7b25a', '488243256161730560', '882879744083890180');
INSERT INTO `t_sys_permission_role` VALUES ('34ffb533-4673-4da5-b9d7-ae8b378218ea', '882910089013497856', '884230589228650496');
INSERT INTO `t_sys_permission_role` VALUES ('36b61478-bbc4-4001-9883-9678bfa8158e', '488243256161730560', '884230928099053568');
INSERT INTO `t_sys_permission_role` VALUES ('3922018d-19f0-475c-8ec8-20db71115f74', '488243256161730560', '17');
INSERT INTO `t_sys_permission_role` VALUES ('39e84f75-6aa7-44f3-bf5d-7c4a111af866', '488243256161730560', '884231719748767744');
INSERT INTO `t_sys_permission_role` VALUES ('3a941355-38f9-45dc-9814-cbb68a29d45a', '488243256161730560', '882492338633576448');
INSERT INTO `t_sys_permission_role` VALUES ('3b0bf7ca-2573-4f17-91a1-bfa9593605d3', '488243256161730560', '884231570616094722');
INSERT INTO `t_sys_permission_role` VALUES ('3c877ef2-8d68-429e-8515-76ad130f253a', '882910089013497856', '592167738407911424');
INSERT INTO `t_sys_permission_role` VALUES ('3f41855c-9c83-4118-af6a-db84c231b7f8', '488243256161730560', '884020401519333376');
INSERT INTO `t_sys_permission_role` VALUES ('4083b219-2a4c-4aac-b477-a53541354e7f', '882910089013497856', '882529814760787968');
INSERT INTO `t_sys_permission_role` VALUES ('413f3254-cbdc-4ea8-bb1c-0f01d058da7a', '882910089013497856', '617766548966211584');
INSERT INTO `t_sys_permission_role` VALUES ('416b66a3-91e4-4572-8218-9fe8a15b1924', '488243256161730560', '884226785477267457');
INSERT INTO `t_sys_permission_role` VALUES ('422ed734-15ee-4653-bfb3-2600280b064b', '488243256161730560', '884220364220141569');
INSERT INTO `t_sys_permission_role` VALUES ('45d16033-3bba-4cde-a1a9-82e21a6b3943', '488243256161730560', '884014715003604995');
INSERT INTO `t_sys_permission_role` VALUES ('47457f0e-a5b7-44c1-aa2a-7f6603400362', '882910089013497856', '882492763348799488');
INSERT INTO `t_sys_permission_role` VALUES ('47e3d10d-09b0-4e69-9ced-9c51ef43bdf8', '488243256161730560', '330365026642825216');
INSERT INTO `t_sys_permission_role` VALUES ('4921e94b-c784-4031-9976-7eb4b3b44a77', '882910089013497856', '883225370877366275');
INSERT INTO `t_sys_permission_role` VALUES ('4a4b6acc-6adb-400e-9761-51d747529852', '488243256161730560', '3321578609202995233');
INSERT INTO `t_sys_permission_role` VALUES ('4ab7df5a-3cec-4da1-b892-bfe05cf13d18', '488243256161730560', '884231570616094721');
INSERT INTO `t_sys_permission_role` VALUES ('4ebae164-227f-4f67-8737-64f8c9fc6122', '882910089013497856', '884014715003604993');
INSERT INTO `t_sys_permission_role` VALUES ('4ff593df-19b7-4755-a8d6-409be70ddcae', '488243256161730560', '884220364220141568');
INSERT INTO `t_sys_permission_role` VALUES ('5078a444-f20a-4292-83c6-095d8e2a1765', '488243256161730560', '884231570616094724');
INSERT INTO `t_sys_permission_role` VALUES ('50aaaa0a-3d8c-47d5-8e0d-201be153688b', '488243256161730560', '884230928099053569');
INSERT INTO `t_sys_permission_role` VALUES ('5320012d-92d1-4391-bd3f-ddb8fc0c5e5c', '488243256161730560', '5946910264304599044');
INSERT INTO `t_sys_permission_role` VALUES ('538d490b-65ff-4d89-ada3-abce2fa15a38', '882910089013497856', '882608044100423680');
INSERT INTO `t_sys_permission_role` VALUES ('5404bf24-550c-4ee5-9944-839553542797', '882910089013497856', '882607288454615042');
INSERT INTO `t_sys_permission_role` VALUES ('547bc550-0b6c-4006-91a0-2dbb3817f9d3', '488243256161730560', '883225370877366275');
INSERT INTO `t_sys_permission_role` VALUES ('551e8f47-6598-4712-9e01-e14a4afe3180', '488243256161730560', '594691026430459904');
INSERT INTO `t_sys_permission_role` VALUES ('556983bd-cb3e-4ad2-9240-149bf1af050e', '488243256161730560', '884229692402569219');
INSERT INTO `t_sys_permission_role` VALUES ('572cd751-ef54-4cd7-b5e8-6ecd20ba2336', '488243256161730560', '486690002869157888');
INSERT INTO `t_sys_permission_role` VALUES ('577a44be-ad5e-404a-8d3e-b1e7cd8157a4', '882910089013497856', '882927147621158913');
INSERT INTO `t_sys_permission_role` VALUES ('5b107906-fd39-426a-ac01-c4b6a839aed8', '882910089013497856', '882608570519130112');
INSERT INTO `t_sys_permission_role` VALUES ('5b73f459-2977-498b-a790-e023e5864888', '488243256161730560', '884231964637401090');
INSERT INTO `t_sys_permission_role` VALUES ('5c116f20-38aa-4343-9ebd-c08590dea306', '488243256161730560', '884228539791052800');
INSERT INTO `t_sys_permission_role` VALUES ('5d945ca3-09b6-4a75-9872-5c1bbf647853', '882910089013497856', '882877537343115264');
INSERT INTO `t_sys_permission_role` VALUES ('5e340f43-f478-498d-9741-60698499b0ba', '488243256161730560', '884226785477267456');
INSERT INTO `t_sys_permission_role` VALUES ('5eefd3bf-7d8d-4f79-9bca-0ee5bb5d9480', '882910089013497856', '882877537343115265');
INSERT INTO `t_sys_permission_role` VALUES ('5f6ab35e-5968-4859-a98c-5803ca6dc2d7', '488243256161730560', '4109898056992071691');
INSERT INTO `t_sys_permission_role` VALUES ('610de4f6-fd68-4825-a6c3-9cfda6b37485', '882910089013497856', '335330315113467904');
INSERT INTO `t_sys_permission_role` VALUES ('6174224a-5356-40d8-b47d-74cdb3327c53', '488243256161730560', '884231175311331332');
INSERT INTO `t_sys_permission_role` VALUES ('64437a78-4292-44c3-9075-d30f49617280', '882910089013497856', '884231570616094721');
INSERT INTO `t_sys_permission_role` VALUES ('66bbfa81-838f-4677-98a1-a65b7751c203', '488243256161730560', '884229692402569218');
INSERT INTO `t_sys_permission_role` VALUES ('68aa9543-f146-42ed-80b2-50d0fc2caa3f', '488243256161730560', '884223003284606976');
INSERT INTO `t_sys_permission_role` VALUES ('7042c963-010e-41b4-9684-94bf16fb8534', '488243256161730560', '882492763348799488');
INSERT INTO `t_sys_permission_role` VALUES ('705108ab-ae0f-4399-a478-8362880daff7', '488243256161730560', '581541547099553792');
INSERT INTO `t_sys_permission_role` VALUES ('7141a254-f99f-4d4a-91bb-56d1033abd88', '882910089013497856', '884223679888756737');
INSERT INTO `t_sys_permission_role` VALUES ('72efc687-2ce5-4c85-9779-e7a0ad548a97', '488243256161730560', '882877537343115264');
INSERT INTO `t_sys_permission_role` VALUES ('73ab2afd-ae8e-4352-9efa-8e11c7717502', '882910089013497856', '884223003284606976');
INSERT INTO `t_sys_permission_role` VALUES ('73c8327b-77a3-4ba7-8e1e-4f98c2ef78f8', '488243256161730560', '882879744083890178');
INSERT INTO `t_sys_permission_role` VALUES ('74060985-d821-4668-96e9-4674d66e7380', '488243256161730560', '4107917018594058262');
INSERT INTO `t_sys_permission_role` VALUES ('75182076-10c8-4b7c-90f1-1bccba1983d1', '882910089013497856', '882884784056045570');
INSERT INTO `t_sys_permission_role` VALUES ('757566b8-d891-4521-a204-7a1aa0e76546', '882910089013497856', '883225370877366274');
INSERT INTO `t_sys_permission_role` VALUES ('758e360f-c215-435a-b8d4-c533539984a0', '488243256161730560', '6192095214866268161');
INSERT INTO `t_sys_permission_role` VALUES ('75d6534e-71ec-4f0b-b223-3d7d43f43bd0', '488243256161730560', '884230589228650498');
INSERT INTO `t_sys_permission_role` VALUES ('766a9019-8bc5-4b09-b040-68680f3f0a6f', '882910089013497856', '882879744083890176');
INSERT INTO `t_sys_permission_role` VALUES ('773a176b-c2e9-4319-92a0-a25051eb3c11', '488243256161730560', '884231398729322498');
INSERT INTO `t_sys_permission_role` VALUES ('7854fe32-89a7-407d-8b46-694f3f123b5f', '882910089013497856', '882879744083890179');
INSERT INTO `t_sys_permission_role` VALUES ('7933b942-f61f-4b8e-9a3e-279a4b58f0ed', '488243256161730560', '11');
INSERT INTO `t_sys_permission_role` VALUES ('7a8a1851-40b0-4eb7-a137-ab0ba4700c8f', '488243256161730560', '884025055774904320');
INSERT INTO `t_sys_permission_role` VALUES ('7ba7878d-59b4-4fc7-bfc3-a3f3f08a73b2', '882910089013497856', '884226785477267456');
INSERT INTO `t_sys_permission_role` VALUES ('7c190e7f-b124-44d4-9888-57f817e3de17', '488243256161730560', '411522822607867904');
INSERT INTO `t_sys_permission_role` VALUES ('7cf745d0-6e89-496e-8bf0-635831f42383', '488243256161730560', '10');
INSERT INTO `t_sys_permission_role` VALUES ('7d96d056-4a9e-469d-884d-5d81d5096537', '882910089013497856', '883225370877366273');
INSERT INTO `t_sys_permission_role` VALUES ('7fecd105-8627-4d94-a269-d037461120d9', '488243256161730560', '883225370877366276');
INSERT INTO `t_sys_permission_role` VALUES ('800762bd-5816-41c2-bd1d-96ac38a9be52', '488243256161730560', '884220364220141572');
INSERT INTO `t_sys_permission_role` VALUES ('8243132c-6549-4a60-8f9a-302973b62633', '882910089013497856', '884231719748767744');
INSERT INTO `t_sys_permission_role` VALUES ('8266f5c7-f291-4819-b409-89ff3c792917', '488243256161730560', '3734899074291507233');
INSERT INTO `t_sys_permission_role` VALUES ('844a13b2-519a-4f84-914f-705ce6150482', '488243256161730560', '4107917018594058284');
INSERT INTO `t_sys_permission_role` VALUES ('85229ef3-38ad-448f-bd4c-833e7bc8db89', '488243256161730560', '884223003284606979');
INSERT INTO `t_sys_permission_role` VALUES ('85e166dc-c608-4238-b433-890ea6b9aff4', '488243256161730560', '884223679888756736');
INSERT INTO `t_sys_permission_role` VALUES ('8615703e-e8be-4fe7-93db-aa2e29f10787', '882910089013497856', '882528193872007168');
INSERT INTO `t_sys_permission_role` VALUES ('86da11d9-fc38-4fa2-9172-f035b4551ad2', '488243256161730560', '13');
INSERT INTO `t_sys_permission_role` VALUES ('87723813-80b3-4779-877a-f4ac1587e0a0', '882910089013497856', '331778807298134016');
INSERT INTO `t_sys_permission_role` VALUES ('89c71862-d61c-4552-9c82-a5ac99513e08', '882910089013497856', '882492338633576448');
INSERT INTO `t_sys_permission_role` VALUES ('8a1b52fd-c230-40c7-8b63-30a23c905bb7', '488243256161730560', '16');
INSERT INTO `t_sys_permission_role` VALUES ('8bbbc6a2-75bd-4648-8f71-6cbfab9489c3', '882910089013497856', '884231964637401088');
INSERT INTO `t_sys_permission_role` VALUES ('8ce26320-69ed-41c0-9269-93478f0a4cba', '488243256161730560', '884231964637401091');
INSERT INTO `t_sys_permission_role` VALUES ('8d254268-0520-49d1-bfe9-ab26f8cee455', '488243256161730560', '882879744083890176');
INSERT INTO `t_sys_permission_role` VALUES ('8d9b7287-2eac-4636-9c33-f76e8dd6aafc', '882910089013497856', '884228539791052801');
INSERT INTO `t_sys_permission_role` VALUES ('8f8affb7-8d06-4c4d-88e6-e3ad8be411e4', '488243256161730560', '882529697622265856');
INSERT INTO `t_sys_permission_role` VALUES ('9067280c-ddcb-452c-90c4-885afcc896ef', '882910089013497856', '884223679888756738');
INSERT INTO `t_sys_permission_role` VALUES ('90e8697e-5dc6-40bc-8d0f-2cc3d3abb154', '882910089013497856', '496126970468237312');
INSERT INTO `t_sys_permission_role` VALUES ('91548c69-aea4-48fb-bfea-a0b8d7e58ab5', '488243256161730560', '3734899074291507211');
INSERT INTO `t_sys_permission_role` VALUES ('92a9f829-df1d-46e8-8c32-830c7bd8ed70', '488243256161730560', '3303650266428252182');
INSERT INTO `t_sys_permission_role` VALUES ('937ab033-c03d-4567-a897-ce934df7bf4a', '488243256161730560', '884230928099053570');
INSERT INTO `t_sys_permission_role` VALUES ('94388de4-7b18-4d4a-b124-232a05204a25', '488243256161730560', '619836559427895296');
INSERT INTO `t_sys_permission_role` VALUES ('98de3bdc-b94d-4b56-977a-820bb4052ad5', '488243256161730560', '882528193872007168');
INSERT INTO `t_sys_permission_role` VALUES ('98e4660a-426d-47e3-aeac-3b45ccb730cb', '488243256161730560', '882609124276310016');
INSERT INTO `t_sys_permission_role` VALUES ('995425e8-c1a9-4eeb-85e6-0b6c795f19fd', '488243256161730560', '884231570616094720');
INSERT INTO `t_sys_permission_role` VALUES ('9956995b-c130-454e-ad23-48f7d0115bad', '882910089013497856', '884231175311331328');
INSERT INTO `t_sys_permission_role` VALUES ('99f23ba5-094a-47f7-bfa9-5e1bbac4ac83', '488243256161730560', '884223003284606978');
INSERT INTO `t_sys_permission_role` VALUES ('99f3d279-ffb9-4f7a-af10-12966581e5a3', '488243256161730560', '884229692402569216');
INSERT INTO `t_sys_permission_role` VALUES ('99faafe1-bde4-418c-a5c5-5f465065e8cc', '488243256161730560', '6192095215075983363');
INSERT INTO `t_sys_permission_role` VALUES ('9a38057a-157e-44ae-9e11-9a0971539cb3', '488243256161730560', '617766548966211584');
INSERT INTO `t_sys_permission_role` VALUES ('9a3e0a18-ea68-485f-9615-ffbec72c8f11', '488243256161730560', '583063272123531264');
INSERT INTO `t_sys_permission_role` VALUES ('9c3e17c3-8cb3-49c6-b1b9-6ca0b4f33167', '882910089013497856', '592167738407911424');
INSERT INTO `t_sys_permission_role` VALUES ('9c90248b-71ab-4498-9fb1-c891b5b5e08d', '488243256161730560', '882884784056045570');
INSERT INTO `t_sys_permission_role` VALUES ('9cab19c4-4df5-4fe5-84a8-46aad4a19375', '488243256161730560', '884230589228650497');
INSERT INTO `t_sys_permission_role` VALUES ('9cfeac8f-237c-43e3-aada-5130b9541c02', '488243256161730560', '884223679888756739');
INSERT INTO `t_sys_permission_role` VALUES ('9dc0a3fb-f59d-4aa7-b31d-0835bb381f46', '488243256161730560', '884223679888756738');
INSERT INTO `t_sys_permission_role` VALUES ('9ebe3d25-c40b-415e-b689-0beabcc9e5b1', '488243256161730560', '5946910264304599041');
INSERT INTO `t_sys_permission_role` VALUES ('9fff55f6-d070-43af-a1df-16fbe782393f', '488243256161730560', '882877537343115268');
INSERT INTO `t_sys_permission_role` VALUES ('a082eba7-2596-437b-b39e-1e50bf6cf312', '882910089013497856', '882607288454615043');
INSERT INTO `t_sys_permission_role` VALUES ('a380b20b-dad5-4e74-8dab-d32947fa8411', '488243256161730560', '884025055770710017');
INSERT INTO `t_sys_permission_role` VALUES ('a48274b1-f631-43a8-8b82-a47ce164a12d', '488243256161730560', '22');
INSERT INTO `t_sys_permission_role` VALUES ('a4fc4a00-2bf8-48a3-a512-3d6a7943ae29', '488243256161730560', '882780788863668224');
INSERT INTO `t_sys_permission_role` VALUES ('a62b4511-6d7d-4375-94ba-912e736f505b', '488243256161730560', '884231570616094723');
INSERT INTO `t_sys_permission_role` VALUES ('a6edae81-c58a-4a4a-a8ae-3ce3a7c6e3c0', '488243256161730560', '882884784056045569');
INSERT INTO `t_sys_permission_role` VALUES ('a7dfd7e4-508f-4e15-b213-3a7186d887d1', '882910089013497856', '587453033487532032');
INSERT INTO `t_sys_permission_role` VALUES ('a8853da5-c653-4098-a711-c41ed8fd9b75', '882910089013497856', '884231570616094720');
INSERT INTO `t_sys_permission_role` VALUES ('a8b939c5-fb67-423d-8c6f-57228d8d1fb7', '882910089013497856', '883225370877366272');
INSERT INTO `t_sys_permission_role` VALUES ('a8e6e47a-8771-4b90-be73-7c20f41a1b90', '882910089013497856', '586003694080753664');
INSERT INTO `t_sys_permission_role` VALUES ('a9ce57e7-8512-4215-921d-0ba72cee8bd1', '882910089013497856', '882884784056045569');
INSERT INTO `t_sys_permission_role` VALUES ('ad575779-51f6-4b59-9766-e60848c08126', '488243256161730560', '20');
INSERT INTO `t_sys_permission_role` VALUES ('aed94d6d-135d-494e-91f5-4f2074afc488', '488243256161730560', '884230589228650496');
INSERT INTO `t_sys_permission_role` VALUES ('afccaced-7fdd-4205-9420-c19899c13e39', '488243256161730560', '884231175311331330');
INSERT INTO `t_sys_permission_role` VALUES ('afeae885-19e6-4ac9-a891-90d7dfd71451', '488243256161730560', '882879744083890177');
INSERT INTO `t_sys_permission_role` VALUES ('b0747472-4b35-48ea-91c5-55adf9133f1e', '488243256161730560', '21');
INSERT INTO `t_sys_permission_role` VALUES ('b12a5b1d-dea3-482e-a162-21f167a179fc', '488243256161730560', '410989805699207168');
INSERT INTO `t_sys_permission_role` VALUES ('b1378a6d-eadd-4259-8f2b-19ff4d8ab410', '488243256161730560', '610635485890478080');
INSERT INTO `t_sys_permission_role` VALUES ('b325e78b-3317-4307-af75-462046baa614', '488243256161730560', '18');
INSERT INTO `t_sys_permission_role` VALUES ('b52b7094-11a8-4304-8f8a-81ea5932e515', '488243256161730560', '4107917018594058273');
INSERT INTO `t_sys_permission_role` VALUES ('b600e7ed-7087-4a3a-ae60-f6fcb1c1a56f', '488243256161730560', '4109898056992071702');
INSERT INTO `t_sys_permission_role` VALUES ('b688b342-6af1-4120-b570-6f580bc5faed', '488243256161730560', '6189186317948026882');
INSERT INTO `t_sys_permission_role` VALUES ('b6bb919f-8026-4e2d-b628-175ac8d64e6d', '882910089013497856', '884223679888756740');
INSERT INTO `t_sys_permission_role` VALUES ('b79f07c0-8bda-4900-8661-5f21e49c9f39', '488243256161730560', '884231175311331331');
INSERT INTO `t_sys_permission_role` VALUES ('b7af4f18-b684-4f54-831e-909520271e21', '882910089013497856', '882927147621158912');
INSERT INTO `t_sys_permission_role` VALUES ('b7f19275-9738-467e-a8ff-7d02610e8373', '882910089013497856', '884223679888756739');
INSERT INTO `t_sys_permission_role` VALUES ('b855aeff-c316-4f7d-b8b6-45c28de2cbf4', '488243256161730560', '884231398729322499');
INSERT INTO `t_sys_permission_role` VALUES ('b8745a18-503c-4767-9cb4-f67183707e5d', '882910089013497856', '882607288454615044');
INSERT INTO `t_sys_permission_role` VALUES ('b87842b5-a2de-45b0-b98a-bef5e0dbfe9b', '488243256161730560', '884231964637401088');
INSERT INTO `t_sys_permission_role` VALUES ('b88aa235-2f15-4e4c-9aea-64f370aa83d0', '488243256161730560', '6192095214866268162');
INSERT INTO `t_sys_permission_role` VALUES ('b9d77e3e-1e64-439e-af95-6211ae620745', '882910089013497856', '882607288454615041');
INSERT INTO `t_sys_permission_role` VALUES ('bb388261-7827-4cd4-b82a-d4492955f0f0', '882910089013497856', '882884784056045571');
INSERT INTO `t_sys_permission_role` VALUES ('bcaca4c4-2d21-4547-a53c-2bd6be616422', '882910089013497856', '884229692402569220');
INSERT INTO `t_sys_permission_role` VALUES ('bd8189e8-1805-41e5-a816-2ff70502e5a8', '488243256161730560', '882529814760787968');
INSERT INTO `t_sys_permission_role` VALUES ('be13228a-62e3-4460-a520-fee7960c16be', '488243256161730560', '883225370877366274');
INSERT INTO `t_sys_permission_role` VALUES ('bfa3ea89-8b4d-4c38-991f-ccb74f66d0e5', '488243256161730560', '9');
INSERT INTO `t_sys_permission_role` VALUES ('c11ad8ee-ba27-408b-b56a-37f744565ddc', '488243256161730560', '884228539795247105');
INSERT INTO `t_sys_permission_role` VALUES ('c2c977e1-b815-49b8-a436-ae7d0b09cf94', '488243256161730560', '884228539795247106');
INSERT INTO `t_sys_permission_role` VALUES ('c318383c-bc8d-45e1-b571-db4204748a7c', '488243256161730560', '586003694080753664');
INSERT INTO `t_sys_permission_role` VALUES ('c3d39331-c49e-4c08-82aa-1124209ada8c', '882910089013497856', '882879744083890180');
INSERT INTO `t_sys_permission_role` VALUES ('c55a57e2-cee3-4a32-bc0a-3b87a1f32ecf', '882910089013497856', '882609412651487232');
INSERT INTO `t_sys_permission_role` VALUES ('c6534a3e-1267-4904-a5fc-a6bb82e6f27c', '488243256161730560', '882927147621158912');
INSERT INTO `t_sys_permission_role` VALUES ('c6863a89-91cd-4f1d-a1c3-b22d27a05255', '488243256161730560', '884229692402569217');
INSERT INTO `t_sys_permission_role` VALUES ('c70962d0-5774-4683-beaa-1e396f0b6655', '488243256161730560', '12');
INSERT INTO `t_sys_permission_role` VALUES ('c758d880-e5e7-4099-a38d-4f6b4c9f35c5', '488243256161730560', '335330315113467904');
INSERT INTO `t_sys_permission_role` VALUES ('c85b69d7-f4d8-421b-9357-e52e0d76e80f', '882910089013497856', '882780788863668224');
INSERT INTO `t_sys_permission_role` VALUES ('c9ec4578-8188-438d-8285-043af7fb4610', '488243256161730560', '4109898056992071724');
INSERT INTO `t_sys_permission_role` VALUES ('cacd05b0-fa9a-4721-a10d-db60533bfa7d', '488243256161730560', '3303650266428252204');
INSERT INTO `t_sys_permission_role` VALUES ('cb061f92-4d82-4543-bac5-2ff896b55af6', '488243256161730560', '884230928099053571');
INSERT INTO `t_sys_permission_role` VALUES ('ccef516f-e4f5-48d6-a285-a5f0b0649bc6', '488243256161730560', '884231964637401089');
INSERT INTO `t_sys_permission_role` VALUES ('ccf7a3f5-26b8-4466-b3df-68a3b068acb0', '488243256161730560', '4109898056992071713');
INSERT INTO `t_sys_permission_role` VALUES ('cd4c6cfc-2535-4977-917c-5724e0e8e969', '488243256161730560', '884014715003604994');
INSERT INTO `t_sys_permission_role` VALUES ('ce159bed-9d30-4e07-82b5-c33249d27c6c', '488243256161730560', '6');
INSERT INTO `t_sys_permission_role` VALUES ('cece17de-fd8c-42be-ada8-4c16a2877d3b', '488243256161730560', '14');
INSERT INTO `t_sys_permission_role` VALUES ('cf7219cc-9d40-471b-92c7-cf6d17c1adce', '488243256161730560', '3328572814798397451');
INSERT INTO `t_sys_permission_role` VALUES ('cfe7359d-a401-4f65-8b40-af9fd9110d9e', '488243256161730560', '4107917018594058251');
INSERT INTO `t_sys_permission_role` VALUES ('d1038a50-666a-4862-86a1-e3c4b18338d8', '882910089013497856', '884220364220141568');
INSERT INTO `t_sys_permission_role` VALUES ('d176c32b-acde-42e3-a8f2-2dfc68b4d92a', '488243256161730560', '882608044100423680');
INSERT INTO `t_sys_permission_role` VALUES ('d64876c9-ffbc-4346-b4c7-2e0937b9ccd0', '882910089013497856', '882916565211811841');
INSERT INTO `t_sys_permission_role` VALUES ('d69af848-c00e-4e66-87f0-d079b0f6c080', '882910089013497856', '882607722879651840');
INSERT INTO `t_sys_permission_role` VALUES ('d7ae7d38-eb49-4529-969f-c2ddaf18b47f', '488243256161730560', '882884784056045568');
INSERT INTO `t_sys_permission_role` VALUES ('d825b8ef-1574-4a9e-8660-c5a7915afe77', '488243256161730560', '884231964637401092');
INSERT INTO `t_sys_permission_role` VALUES ('d88de386-de21-4f1f-9d5b-bd6c0f449051', '882910089013497856', '882529697622265856');
INSERT INTO `t_sys_permission_role` VALUES ('d8becda2-1ade-4053-a269-a66730f14b58', '488243256161730560', '882608570519130112');
INSERT INTO `t_sys_permission_role` VALUES ('db169a05-05ad-4d60-8e10-6f6fa6b3f5bd', '488243256161730560', '882927147621158916');
INSERT INTO `t_sys_permission_role` VALUES ('dd011940-3411-44bb-a92e-4cc06c4eb1c0', '882910089013497856', '884231398729322496');
INSERT INTO `t_sys_permission_role` VALUES ('df2c7a51-8a1b-462c-964e-7ccc95695aed', '882910089013497856', '884230589228650497');
INSERT INTO `t_sys_permission_role` VALUES ('dffcb7d5-d6db-4aee-b724-8c63341f03f7', '488243256161730560', '884228539795247104');
INSERT INTO `t_sys_permission_role` VALUES ('e023cc04-6dfa-4e7a-a7f7-79104c9b818b', '488243256161730560', '6189186317989969924');
INSERT INTO `t_sys_permission_role` VALUES ('e210e240-50be-44a1-8a4b-7e484127d3aa', '488243256161730560', '884025055770710019');
INSERT INTO `t_sys_permission_role` VALUES ('e32fac04-4a69-469d-b3f5-64f855aa024c', '882910089013497856', '882879744083890178');
INSERT INTO `t_sys_permission_role` VALUES ('e47d804b-9c21-4e85-a89e-036f6c8193e0', '488243256161730560', '884229692402569220');
INSERT INTO `t_sys_permission_role` VALUES ('e532ac11-3c99-4bb1-bb5b-68fb30b51516', '488243256161730560', '882927147621158913');
INSERT INTO `t_sys_permission_role` VALUES ('e5dfef60-3866-4231-8749-1e71b5ec96d9', '488243256161730560', '3303650266428252193');
INSERT INTO `t_sys_permission_role` VALUES ('e5f2bf27-53c3-4351-8930-c41d4a3335ce', '488243256161730560', '884223003284606980');
INSERT INTO `t_sys_permission_role` VALUES ('e61c66c4-5d26-4388-ad10-2098598aa944', '488243256161730560', '19');
INSERT INTO `t_sys_permission_role` VALUES ('e695e666-5b71-44f3-a445-e5a7739986ce', '488243256161730560', '6192095215075983364');
INSERT INTO `t_sys_permission_role` VALUES ('e6a4ddc6-4095-4804-9b05-d72f0dd704fc', '488243256161730560', '332157860920299520');
INSERT INTO `t_sys_permission_role` VALUES ('e79fc727-3124-4289-b7a3-fe96b941db06', '488243256161730560', '3734899074291507244');
INSERT INTO `t_sys_permission_role` VALUES ('e7e05d53-01d9-4d84-956b-0ff89aca6600', '488243256161730560', '884231175311331329');
INSERT INTO `t_sys_permission_role` VALUES ('e861ec8f-8439-49ab-b401-55d2cc571155', '488243256161730560', '8');
INSERT INTO `t_sys_permission_role` VALUES ('e8c79f75-c96b-440c-9797-364d1608697c', '488243256161730560', '884014715003604996');
INSERT INTO `t_sys_permission_role` VALUES ('e9137735-7417-451c-8141-70eb3298851f', '488243256161730560', '3321578609202995211');
INSERT INTO `t_sys_permission_role` VALUES ('eb0b602a-10a7-41af-b627-7835418b8c98', '882910089013497856', '884228539791052800');
INSERT INTO `t_sys_permission_role` VALUES ('eb514574-3dd6-4c72-bd3b-27c732a7d31a', '882910089013497856', '882529912307716096');
INSERT INTO `t_sys_permission_role` VALUES ('ebab27f6-625d-4557-8f53-75927e18b5fe', '882910089013497856', '884223679888756736');
INSERT INTO `t_sys_permission_role` VALUES ('ec68fa5c-cce4-4a11-9ac6-4c36ebf33ecb', '488243256161730560', '884231719748767746');
INSERT INTO `t_sys_permission_role` VALUES ('eca1b4ff-f908-462e-b022-3ed5e30e03c3', '882910089013497856', '581541547099553792');
INSERT INTO `t_sys_permission_role` VALUES ('ee3233cb-276f-4c00-8432-0b8a23ab31ac', '488243256161730560', '882877537343115265');
INSERT INTO `t_sys_permission_role` VALUES ('ee642393-dcff-4db2-bcc6-eabb7aa38d8a', '488243256161730560', '15');
INSERT INTO `t_sys_permission_role` VALUES ('f01b6c31-f7fa-4384-8ce2-a2f094797c62', '488243256161730560', '883225370877366273');
INSERT INTO `t_sys_permission_role` VALUES ('f06da099-2564-4981-989e-8aa93a300ef9', '488243256161730560', '6189186317738311681');
INSERT INTO `t_sys_permission_role` VALUES ('f0c99ca7-faa8-4f52-8f1b-3731339af1e6', '488243256161730560', '882916565211811844');
INSERT INTO `t_sys_permission_role` VALUES ('f1e6e5a0-85df-4b8b-909d-e568799e03c9', '488243256161730560', '340381240911859712');
INSERT INTO `t_sys_permission_role` VALUES ('f2eaa5bc-b6a8-40fd-998d-a0ba550bf54b', '488243256161730560', '4');
INSERT INTO `t_sys_permission_role` VALUES ('f31205d0-e576-47fb-aa8c-3aa242510d18', '488243256161730560', '882884784056045572');
INSERT INTO `t_sys_permission_role` VALUES ('f372016b-50c8-479f-a051-b2f7394722fe', '882910089013497856', '884229692402569219');
INSERT INTO `t_sys_permission_role` VALUES ('f3e2ec61-3edd-4635-9114-2ee4ce40216e', '488243256161730560', '882607722879651840');
INSERT INTO `t_sys_permission_role` VALUES ('f4a42679-26e5-4b3f-8c57-68cd422b3462', '882910089013497856', '592059865673760768');
INSERT INTO `t_sys_permission_role` VALUES ('f6305e17-2fb1-400f-a156-0c90680c99f2', '488243256161730560', '3321578609202995244');
INSERT INTO `t_sys_permission_role` VALUES ('f6363611-fe2d-4ff0-9282-5ec61cc9da06', '882910089013497856', '884014715003604992');
INSERT INTO `t_sys_permission_role` VALUES ('f76fa0eb-94d9-4587-b42f-8c8ead77983b', '488243256161730560', '884223003284606977');
INSERT INTO `t_sys_permission_role` VALUES ('f8d7355d-8b08-4db6-a6ee-cb8f8b079021', '882910089013497856', '373489907429150720');
INSERT INTO `t_sys_permission_role` VALUES ('f9071bad-2758-46db-9d72-0210b5c1a208', '488243256161730560', '884013484151541760');
INSERT INTO `t_sys_permission_role` VALUES ('fab553be-1b6d-4f12-afbb-1d641d133319', '488243256161730560', '884231719748767748');
INSERT INTO `t_sys_permission_role` VALUES ('fbeca9cd-239a-4393-af2f-08dcfad2b06f', '488243256161730560', '3734899074291507222');
INSERT INTO `t_sys_permission_role` VALUES ('fc8b6683-1f34-40d2-bf0b-f16b6400e319', '882910089013497856', '884230928099053569');
INSERT INTO `t_sys_permission_role` VALUES ('fc9b6118-b182-473e-a8e5-0a9a965c12c5', '488243256161730560', '882884784056045571');
INSERT INTO `t_sys_permission_role` VALUES ('fdc52863-6939-4ec0-b034-6afe93c75213', '488243256161730560', '3321578609202995222');
INSERT INTO `t_sys_permission_role` VALUES ('fe3d9ba6-688a-4605-8399-670cb8760bcf', '488243256161730560', '884025055770710018');
INSERT INTO `t_sys_permission_role` VALUES ('fe538970-d312-4cee-ac43-1eb071c71c66', '488243256161730560', '884231398729322496');
INSERT INTO `t_sys_permission_role` VALUES ('fe6051cb-631a-4de5-9662-a63866763646', '488243256161730560', '882916565211811843');
INSERT INTO `t_sys_permission_role` VALUES ('feb8d83a-7954-4ad6-96ae-fd8196897e29', '488243256161730560', '884014715003604993');
INSERT INTO `t_sys_permission_role` VALUES ('ff0cbd60-cdd6-4ccd-8737-664c3b64a7d2', '882910089013497856', '882916565211811840');
INSERT INTO `t_sys_permission_role` VALUES ('ff303d64-b808-455f-b87a-6b76b9321138', '882910089013497856', '884223003284606977');

-- ----------------------------
-- Table structure for `t_sys_position`
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_position`;
CREATE TABLE `t_sys_position` (
  `id` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '主键',
  `post_name` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '岗位名称',
  `order_num` int DEFAULT NULL COMMENT '排序',
  `status` int DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 ROW_FORMAT=DYNAMIC COMMENT='岗位表';

-- ----------------------------
-- Records of t_sys_position
-- ----------------------------
INSERT INTO `t_sys_position` VALUES ('410792368778907648', '总经理', '1', '1');
INSERT INTO `t_sys_position` VALUES ('410792443127140352', '技术经理', '2', '1');
INSERT INTO `t_sys_position` VALUES ('410792478929719296', '人事经理', '3', '1');
INSERT INTO `t_sys_position` VALUES ('411477874382606336', '员工', '4', '1');
INSERT INTO `t_sys_position` VALUES ('883659896975921152', '运营经理', '5', '1');

-- ----------------------------
-- Table structure for `t_sys_province`
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_province`;
CREATE TABLE `t_sys_province` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '自增列',
  `province_code` varchar(40) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '省份代码',
  `province_name` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '省份名称',
  `short_name` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '简称',
  `lng` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '经度',
  `lat` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '纬度',
  `sort` int DEFAULT NULL COMMENT '排序',
  `gmt_create` datetime DEFAULT NULL COMMENT '创建时间',
  `gmt_modified` datetime DEFAULT NULL COMMENT '修改时间',
  `memo` varchar(250) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '备注',
  `data_state` int DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `Index_1` (`province_code`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb3 ROW_FORMAT=DYNAMIC COMMENT='省份表';

-- ----------------------------
-- Records of t_sys_province
-- ----------------------------
INSERT INTO `t_sys_province` VALUES ('22', '500000', '重庆', '重庆', '106.504959', '29.533155', '22', '2019-02-28 17:16:58', '2019-02-28 17:17:05', '', '0');

-- ----------------------------
-- Table structure for `t_sys_quartz_job`
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_quartz_job`;
CREATE TABLE `t_sys_quartz_job` (
  `id` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '日志id',
  `job_name` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '任务名称',
  `job_group` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '任务组名',
  `invoke_target` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '调用目标字符串',
  `cron_expression` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT 'cron执行表达式',
  `misfire_policy` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT 'cron计划策略',
  `concurrent` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '是否并发执行（0允许 1禁止）',
  `status` int DEFAULT NULL COMMENT '任务状态（0正常 1暂停）',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 ROW_FORMAT=DYNAMIC COMMENT='定时任务调度表';

-- ----------------------------
-- Records of t_sys_quartz_job
-- ----------------------------
INSERT INTO `t_sys_quartz_job` VALUES ('332182389491109888', 'v2Task2', 'SYSTEM', 'v2Task.runTask2(1,2l,\'asa\',true,2D)', '*/5 * * * * ?', '2', '0', '1');

-- ----------------------------
-- Table structure for `t_sys_quartz_job_log`
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_quartz_job_log`;
CREATE TABLE `t_sys_quartz_job_log` (
  `id` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '主键',
  `job_name` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '任务名称',
  `job_group` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '任务组名',
  `invoke_target` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '调用目标字符串',
  `job_message` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '日志信息',
  `status` int DEFAULT NULL COMMENT '执行状态（0正常 1失败）',
  `exception_info` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '异常信息',
  `start_time` datetime DEFAULT NULL COMMENT '开始时间',
  `end_time` datetime DEFAULT NULL COMMENT '结束时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 ROW_FORMAT=DYNAMIC COMMENT='定时任务调度日志表';

-- ----------------------------
-- Records of t_sys_quartz_job_log
-- ----------------------------

-- ----------------------------
-- Table structure for `t_sys_role`
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_role`;
CREATE TABLE `t_sys_role` (
  `id` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT 'id',
  `name` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '角色名称',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 ROW_FORMAT=DYNAMIC COMMENT='角色表';

-- ----------------------------
-- Records of t_sys_role
-- ----------------------------
INSERT INTO `t_sys_role` VALUES ('488243256161730560', '管理员');
INSERT INTO `t_sys_role` VALUES ('488289006124007424', '用户');
INSERT INTO `t_sys_role` VALUES ('488305788310257664', '能修改用户密码角色');
INSERT INTO `t_sys_role` VALUES ('882910089013497856', '运营人员');

-- ----------------------------
-- Table structure for `t_sys_role_user`
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_role_user`;
CREATE TABLE `t_sys_role_user` (
  `id` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `sys_user_id` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '用户id',
  `sys_role_id` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '角色id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 ROW_FORMAT=DYNAMIC COMMENT='用户角色中间表';

-- ----------------------------
-- Records of t_sys_role_user
-- ----------------------------
INSERT INTO `t_sys_role_user` VALUES ('353711021275353089', '353711021275353088', '488289006124007424');
INSERT INTO `t_sys_role_user` VALUES ('353714370687143936', '488294747442511872', '488289006124007424');
INSERT INTO `t_sys_role_user` VALUES ('354984037766533120', '354984005751410688', '488243256161730560');
INSERT INTO `t_sys_role_user` VALUES ('354988722443390977', '354988722443390976', '488243256161730560');
INSERT INTO `t_sys_role_user` VALUES ('354989789679849472', '354989789675655168', '488305788310257664');
INSERT INTO `t_sys_role_user` VALUES ('495571139645542400', '1', '488243256161730560');
INSERT INTO `t_sys_role_user` VALUES ('612107905532952576', '612107905532952576', '488289006124007424');
INSERT INTO `t_sys_role_user` VALUES ('612107905537146880', '612107905532952576', '488305788310257664');
INSERT INTO `t_sys_role_user` VALUES ('882517802739699712', '882517373159084032', '488243256161730560');
INSERT INTO `t_sys_role_user` VALUES ('882517802743894016', '882517373159084032', '488289006124007424');
INSERT INTO `t_sys_role_user` VALUES ('882517802752282624', '882517373159084032', '488305788310257664');
INSERT INTO `t_sys_role_user` VALUES ('884234618520342528', '884233123427127296', '488305788310257664');
INSERT INTO `t_sys_role_user` VALUES ('884234618528731136', '884233123427127296', '882910089013497856');
INSERT INTO `t_sys_role_user` VALUES ('884292169618296833', '884292169618296832', '488305788310257664');
INSERT INTO `t_sys_role_user` VALUES ('884292169677017088', '884292169618296832', '882910089013497856');

-- ----------------------------
-- Table structure for `t_sys_street`
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_street`;
CREATE TABLE `t_sys_street` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '自增列',
  `street_code` varchar(40) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '街道代码',
  `area_code` varchar(40) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '父级区代码',
  `street_name` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '街道名称',
  `short_name` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '简称',
  `lng` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '经度',
  `lat` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '纬度',
  `sort` int DEFAULT NULL COMMENT '排序',
  `gmt_create` datetime DEFAULT NULL COMMENT '创建时间',
  `gmt_modified` datetime DEFAULT NULL COMMENT '修改时间',
  `memo` varchar(250) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '备注',
  `data_state` int DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `Index_1` (`street_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 ROW_FORMAT=DYNAMIC COMMENT='街道设置';

-- ----------------------------
-- Records of t_sys_street
-- ----------------------------

-- ----------------------------
-- Table structure for `t_sys_user`
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_user`;
CREATE TABLE `t_sys_user` (
  `id` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NOT NULL COMMENT '主键',
  `username` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '用户账号',
  `password` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '用户密码',
  `nickname` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL COMMENT '昵称',
  `dep_id` int DEFAULT NULL COMMENT '部门id',
  `pos_id` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL COMMENT '岗位id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin ROW_FORMAT=DYNAMIC COMMENT='用户表';

-- ----------------------------
-- Records of t_sys_user
-- ----------------------------
INSERT INTO `t_sys_user` VALUES ('1', 'admin', 'admin', '管理员', '2', '410792368778907648');
INSERT INTO `t_sys_user` VALUES ('433236479427350528', 'yongge', '123456', '勇哥', '2', '410792443127140352');
INSERT INTO `t_sys_user` VALUES ('882517373159084032', 'tian', '123456', 'tian', '3', '410792368778907648');
INSERT INTO `t_sys_user` VALUES ('884233123427127296', 'qqq', '123456', '运营人1', '1', '411477874382606336');
INSERT INTO `t_sys_user` VALUES ('884292169618296832', 'zhangmei', '123456', '张美', '5', '410792368778907648');

-- ----------------------------
-- Table structure for `t_test`
-- ----------------------------
DROP TABLE IF EXISTS `t_test`;
CREATE TABLE `t_test` (
  `id` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '主键',
  `name` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '姓名',
  `age` int DEFAULT NULL COMMENT '年龄',
  `sex` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '性别',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `b_c_update` datetime DEFAULT NULL COMMENT '修改时间',
  `t_cb_name` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '写个字符串',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 ROW_FORMAT=DYNAMIC COMMENT='测试表';

-- ----------------------------
-- Records of t_test
-- ----------------------------
INSERT INTO `t_test` VALUES ('457067146405613568', '管理员', '2', '1', '2021-01-14 16:00:00', '2021-01-15 16:00:00', '7');
INSERT INTO `t_test` VALUES ('457067174939463680', '法院用户', '2', '1', '2021-01-12 16:00:00', '2021-01-18 16:00:00', '');
INSERT INTO `t_test` VALUES ('457067196175224832', 'guest', '1', '3', '2021-01-02 16:00:00', '2021-01-16 16:00:00', '');
INSERT INTO `t_test` VALUES ('457067220279889920', 'helloword', '1', '3', '2021-01-03 16:00:00', '2021-01-25 16:00:00', '');

-- ----------------------------
-- Table structure for `users`
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nick_name` varchar(255) DEFAULT NULL,
  `real_name` varchar(255) DEFAULT NULL,
  `user_password` varchar(255) DEFAULT NULL,
  `user_type` int DEFAULT NULL,
  `gender` int DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `avatar_url` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `id_no` varchar(255) DEFAULT NULL,
  `province` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `area` varchar(255) DEFAULT NULL,
  `detail_address` varchar(255) DEFAULT NULL,
  `status` int DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `money` decimal(10,2) DEFAULT NULL,
  `points` int DEFAULT NULL,
  `invitation_code` varchar(255) DEFAULT NULL,
  `invited_code` varchar(255) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', 'A驾2371', '张三峰', '123456', '1', '0', '1999-01-01', 'http://adasdsasdadasdsaa', '18257461202', '521423199901011452', '贵州省', '贵阳市', null, '百花大道111号', '0', '11111@qq.com', '120.00', '0', null, '22222', '2023-11-02 17:34:53');
INSERT INTO `users` VALUES ('3', 'A驾2373', 'aaa', '123456', '0', '1', '2023-11-08', null, '18257461206', '521423199901011454', null, null, null, 'XX路1号', '0', '11431@qq.com', '10.12', '0', null, '44444', '2023-11-02 11:01:28');
INSERT INTO `users` VALUES ('4', 'A驾2372', 'sss', '123456', '1', '1', '2023-11-09', null, '18257461201', '521423199901011453', null, null, null, 'XX路2号', '0', '12222@qq.com', '11.00', '0', null, '33333', '2023-11-02 17:30:39');
INSERT INTO `users` VALUES ('6', 'A驾2370', 'tiange', '123456', '0', '0', '1999-01-01', 'http://adasdsasdadasdsaa', '18257461203', '521423199901011451', '贵州省', '贵阳市', null, '百花大道111号', '0', '11111@qq.com', '99.99', '0', null, '11111', '2023-11-03 10:19:59');

-- ----------------------------
-- Table structure for `user_activity_coupon`
-- ----------------------------
DROP TABLE IF EXISTS `user_activity_coupon`;
CREATE TABLE `user_activity_coupon` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int DEFAULT NULL,
  `coupon_id` int DEFAULT NULL,
  `status` int DEFAULT NULL,
  `used_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of user_activity_coupon
-- ----------------------------

-- ----------------------------
-- Table structure for `user_activity_goods`
-- ----------------------------
DROP TABLE IF EXISTS `user_activity_goods`;
CREATE TABLE `user_activity_goods` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int DEFAULT NULL,
  `goods_info_id` int DEFAULT NULL,
  `status` int DEFAULT NULL,
  `used_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of user_activity_goods
-- ----------------------------
